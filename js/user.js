(function() {
	'use strict';
	$('#addUser').validator().on('submit', function (e) {
		var em = $("#add_email").val();
		var name = $("input[name='name']").val();
		var c_code1 = $("select[name='c_code1']").val();
		var c_code2 = $("select[name='c_code2']").val();
		var c_code3 = $("select[name='c_code3']").val();
		var contact1 = $("input[name='contact1']").val();
		var contact2 = $("input[name='contact2']").val();
		var contact3 = $("input[name='contact3']").val();
		var country = $("input[name='country']").val();
		var city = $("input[name='city']").val();
		var zip = $("input[name='zip']").val();
		var role = $("#ad_role").val();
		var email = $("#add_email").val();
		var pass = $("#add_pass").val();
		if (e.isDefaultPrevented()) {
			//console.log(0);
			$("#countryCodeError").show();
		} else {
				$("#countryCodeError").hide();	
				e.preventDefault();
				$.ajax({
					type: "POST",
					url: "user/chckEmail",
					dataType: "json",
					data:{em:em},
					success:
						function(data) {
							if(data == "true"){
								$("#addEmailDuplicate").show();
							}else{
								e.preventDefault();
								$("#addEmailDuplicate").hide();
								$.ajax({
									type: "POST",
									url: "user/addUser",
									dataType: "json",
									async:true,
									crossDomain:true,
									headers: {
										"accept": "application/json",
										"Access-Control-Allow-Origin":"*"
									},
									xhrFields: {
										withCredentials: true
									},
									data:{role:role,name:name,c_code1:c_code1,c_code2:c_code2,c_code3:c_code3,contact1:contact1,contact2:contact2,contact3:contact3,country:country,city:city,zip:zip,email:email,pass:pass},
									success:
										function(data) {
											console.log(data);
											if(data.status == "true"){
												window.location.href = "http://petpost.info/user/paynow/"+data.id+"/"+data.usr_pay;
												//$('#addUserSuccess').show(); 
												//$('#addUserError').hide(); 
												//setTimeout(function(){ window.location.reload(); }, 3000);
												//console.log("true");
											}
											else{
												$('#addUserSuccess').hide(); 
												$('#addUserError').show(); 
												console.log("false");
											}
										},
									error:
									function(data){
										//console.log(data);		
										//console.log("false");
										$('#addUserSuccess').hide(); 
										$('#addUserError').show(); 									
									}
								});
							}
						},
					error:
					function(data){
								
					}
				});
		}
	});
})();
function checkEmail(em){
			
				
				console.log(checkingMailDuplicate);
}
function userType(d){
	if(d == 1){
		return "Regular";
	}else if(d == 2){
		return "Donator";
	}else if(d == 3){
		return "VIP";
	}else if(d == 4){
		return "Updater";
	}else if(d == 5){
		return "Operator";
	}else if(d == 6){
		return "Admin";
	}else{
		return "Regular";
	}
	
}
function getMonthDesc(m){
	var mDesc = "";
	switch (m){
		case 1:
		mDesc = "January";
		break;
		case 2:
		mDesc = "February";
		break;
		case 3:
		mDesc = "March";
		break;
		case 4:
		mDesc = "April";
		break;
		case 5:
		mDesc = "May";
		break;
		case 6:
		mDesc = "June";
		break;
		case 7:
		mDesc = "July";
		break;
		case 8:
		mDesc = "August";
		break;
		case 9:
		mDesc = "September";
		break;
		case 10:
		mDesc = "October";
		break;
		case 11:
		mDesc = "November";
		break;
		case 12:
		mDesc = "December";
		break;
	}
	return mDesc;
}
function reformatDate(d){
	var dbDate = new Date(d);
	var yyyy = dbDate.getFullYear();
	var mm = dbDate.getMonth() + 1;
	var dd = dbDate.getDate();
	return getMonthDesc(mm)+" "+dd+", "+yyyy;
}

function setDate(n){
	var d = new Date(n);
	
	var month = d.getMonth();
	var mWord;
	switch(month){
		case 0:
        mWord = "January";
        break;
    case 1:
        mWord = "February";
        break;
	case 2:
        mWord = "March";
        break;
	case 3:
        mWord = "April";
        break;
	case 4:
        mWord = "May";
        break;
	case 5:
        mWord = "June";
        break;
	case 6:
        mWord = "July";
        break;
	case 7:
        mWord = "August";
        break;
	case 8:
        mWord = "September";
        break;
	case 9:
        mWord = "October";
        break;
	case 10:
        mWord = "November";
        break;
	case 11:
        mWord = "December";
        break;
	}
	var nDate = d.getDate();
	var nYear = d.getFullYear();
	
	return mWord+" "+nDate+", "+nYear;
}

function sortByKey(array, key) {
    return array.sort(function(a, b) {
        var x = a[key]; var y = b[key];
        return ((x > y) ? -1 : ((x < y) ? 1 : 0));
    });
}

function editUserData(id){
	//console.log(id);
	userID = id;
	$.ajax({
		type: "POST",
		url: "user/getThisUserData",
		dataType: "json",
		data:  {id:userID},
		success:
			function(data) {
			//	console.log(data);
				$('#fname').val(data[0].u_fullname);
				$('#email').val(data[0].u_email);
				document.getElementById('bday').value = data[0].u_bday;
				document.getElementById('user_role').value = data[0].u_role;
				$('#editUserModal').modal("show");

			},
		error:
		function(data){
			console.log("false");		
		}
	});
}