var cartArray = [];
var totalPayment = 0;
var shippingCost = 0;
(function() {
	'use strict';
	getAllProducts();
	getCart();
	getFinalCost();
	$("#showCart").click(function(){
		getCart();
		getFinalCost();
		$("#closeCart").show();
		$("#showCart").hide();
		$("#cartTable").slideDown();
		$("#totArea").slideDown();
		
	});
	$("#closeCart").click(function(){
		getCart();
		getFinalCost();
		$("#showCart").show();
		$("#closeCart").hide();
		$("#cartTable").slideUp();
		$("#totArea").slideUp();
		
	});

	$("#checkOutNow").click(function(){
		var empString = Cookies.get('allProd');
		var empArr = $.parseJSON(empString);
		var subTotPrice = 0;
		var totPrice = 0;
		var totWeight = 0;
		var shipCost = 0;
		//console.log(empArr);
		for(var x=0;x < empArr.length;x++){
			subTotPrice += empArr[x].price;
			totWeight += empArr[x].weight;
		}
		if(totWeight > 0 && totWeight <= 100){
			//console.log("5 usd");
			shipCost = 5;
		}
		else if(totWeight > 100 && totWeight <= 250){
			//console.log("10 usd");
			shipCost = 10;
		}
		else if(totWeight > 250){
			shipCost = 14;
			//console.log("14 usd");
		}
		totPrice = subTotPrice + shipCost;
		$("#totPrice").html(totPrice);
		$("#shipFee").html(shipCost);
		$("#subTotPrice").html(subTotPrice);
		cartArray = empArr;
		totalPayment = totPrice;
		shippingCost = shipCost;
		$("#shipPayment").html(shipCost);
		$("#totPayment").html(totPrice);
		$("#TotalFee").val(totPrice);
		$('#arrData').val(JSON.stringify(empArr));
		$('#shippingFee').val(shipCost);
		$("#invoiceModal").modal("show");
		
		//console.log(totWeight);
		//console.log(subTotPrice);

		//console.log(empArr);
		//console.log(totPrice);
	});
	if(Cookies.get('allProd') != undefined){
		var empString = Cookies.get('allProd');
		var empArr = $.parseJSON(empString);
		//console.log(empArr);
		$("#numOrder").html(empArr.length);
	}else{
		$("#numOrder").html(0);
	}
	//Cookies.remove('allProd', { path: '' });
	
	//console.log(Cookies.get('allProd'));

})();

function capitalizeFirstLetter(string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
}
function getAllProducts(){
	var base_url = $("#baseurl").val();
   $.ajax({
		type: "GET",
		url: "shop/getAllProducts",
		dataType: "json",
		success:
			function(data) {
				if(data.length > 0){
					$("#allProduct").empty();
					for(var x=0;x < data.length;x++){
						//data[x].product_name
						$("#allProduct").
							append($('<div>')
								.attr('class', "col-sm-4 col-xs-6 w3gallery-grids")
								.append($('<a>')
									.attr('style', "width: 300px;height: 250px;background: #fff;overflow: hidden;")
									.attr('href', "#")
									.attr('class', "imghvr-hinge-right figure")
									.append($('<img>')
										.attr('src', base_url+"images/products/"+data[x].user_id+"/"+data[x].product_pic)
										.attr('style', "width: 300px;height: auto;")
									)
									.append($('<div>')
										.attr('class', "agile-figcaption")
										.append($('<h5>')
											.attr('class', "txt-left")
											.append(data[x].product_desc)
										)
										.append($('<p>')
											.attr('class', "txt-left")
											.append("Weight: "+data[x].product_weight+" g")
										)
									)
								)
								.append($('<p>')
									.attr('class', "prod_name")
									.append(data[x].product_name)
								)
								.append($('<p>')
									.attr('class', "prod_price")
									.append("$"+data[x].product_price)
								)
								.append($('<div>')
									.append($('<button>')
										.attr('class', "byn")
										.attr('onclick', "addToCart("+data[x].product_id+","+JSON.stringify(data[x].product_name)+","+JSON.stringify(data[x].product_desc)+","+data[x].product_price+","+data[x].product_weight+","+JSON.stringify(data[x].product_pic)+","+data[x].user_id+")")
											.append("ADD TO CART")
									)
								)
							);
						
					}
				}
			},
		error:
		function(data){
			console.log("false");		
		}
	});		
}

function addToCart(product_id,name,desc,price,weight,pic,user_id){
	
	if(Cookies.get('allProd') == undefined){
		//console.log(1);
		var cartProducts = [];
		cartProducts.push({"product_id" : product_id, "name" : name, "desc" : desc, "price" : price, "weight" : weight, "pic" : pic, "user_id" : user_id});
		var allProd = JSON.stringify(cartProducts);
		Cookies.set('allProd', allProd, { expires: 7, path: ''});
		$("#numOrder").html($.parseJSON(Cookies.get('allProd')).length);
	}else{
		var empString = Cookies.get('allProd');
		var empArr = $.parseJSON(empString);
		empArr.push({"product_id" : product_id, "name" : name, "desc" : desc, "price" : price, "weight" : weight, "pic" : pic, "user_id" : user_id});
		var allProd = JSON.stringify(empArr);
		Cookies.set('allProd', allProd, { expires: 7, path: ''});
		$("#numOrder").html(empArr.length);
	}
	getCart();
	getFinalCost();
	
}
function getCart(){
	var base_url = $("#baseurl").val();
				
				if(Cookies.get('allProd') != undefined && Cookies.get('allProd') != [""]){
					//console.log(Cookies.get('allProd'));
					//console.log(123);
					var empString = Cookies.get('allProd');
					var empArr = $.parseJSON(empString);
					//console.log(empArr);
					$("#tbodyCartTable").empty();
					for(var x=0;x < empArr.length;x++){
						$("#cartTable").find('tbody')
							.append($('<tr>')
								.append($('<td>')
									.append($('<img>')
										.attr('src', base_url+"images/products/"+empArr[x].user_id+"/"+empArr[x].pic)
										.attr('style', "max-width: 80px;height: auto;")
									)
								)
								.append($('<td>')
									.append(empArr[x].name)
								)
								.append($('<td>')
									.append(empArr[x].desc)
								)
								.append($('<td>')
									.append(empArr[x].price)
								)
								.append($('<td>')
									.append(empArr[x].weight)
								)
								.append($('<td>')
									.append($('<button>')
										.attr('type', "btn")
										.attr('onclick', "deleteProdInCart("+x+")")
										.append("X")
									)
								)
							);
					}
					$("#nocart").hide();
				}
				
	
}
function getFinalCost(){
		var empString = Cookies.get('allProd');
		var empArr = $.parseJSON(empString);
		var subTotPrice = 0;
		var totPrice = 0;
		var totWeight = 0;
		var shipCost = 0;
		//console.log(empArr);
		for(var x=0;x < empArr.length;x++){
			subTotPrice += empArr[x].price;
			totWeight += empArr[x].weight;
		}
		if(totWeight > 0 && totWeight <= 100){
			//console.log("5 usd");
			shipCost = 5;
		}
		else if(totWeight > 100 && totWeight <= 250){
			//console.log("10 usd");
			shipCost = 10;
		}
		else if(totWeight > 250){
			shipCost = 14;
			//console.log("14 usd");
		}
		totPrice = subTotPrice + shipCost;
		$("#totPrice").html(totPrice);
		//$("#arrData").val(arrData);
		console.log(empArr);
		$('#arrData').val(JSON.stringify(empArr));
		$("#shipFee").html(shipCost);
		$("#subTotPrice").html(subTotPrice);
		//console.log(totWeight);
		//console.log(subTotPrice);
		//console.log(empArr);
		//console.log(totPrice);
}
function deleteProdInCart(x){

	var empString = Cookies.get('allProd');
	var empArr = $.parseJSON(empString);
	//console.log(empArr);
	var id = x;
	empArr.splice(x, 1);
	//console.log(empArr);
	Cookies.set('allProd', empArr, { expires: 7, path: ''});
	$("#numOrder").html(empArr.length);
	getCart();
	getFinalCost();
}
