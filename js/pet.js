var cartArray = [];
var totalPayment = 0;
var shippingCost = 0;
(function() {
	'use strict';
	getCart();
	getFinalCost();
	getPetComments();
	$("#showCart").click(function(){
		getCart();
		getFinalCost();
		$("#closeCart").show();
		$("#showCart").hide();
		$("#cartTable").slideDown();
		$("#totArea").slideDown();
		
	});
	$("#closeCart").click(function(){
		getCart();
		getFinalCost();
		$("#showCart").show();
		$("#closeCart").hide();
		$("#cartTable").slideUp();
		$("#totArea").slideUp();
		
	});

	$("#checkOutNow").click(function(){
		var empString = Cookies.get('allProd');
		var empArr = $.parseJSON(empString);
		var subTotPrice = 0;
		var totPrice = 0;
		var totWeight = 0;
		var shipCost = 0;
		//console.log(empArr);
		for(var x=0;x < empArr.length;x++){
			subTotPrice += empArr[x].price;
			totWeight += empArr[x].weight;
		}
		if(totWeight > 0 && totWeight <= 100){
			//console.log("5 usd");
			shipCost = 5;
		}
		else if(totWeight > 100 && totWeight <= 250){
			//console.log("10 usd");
			shipCost = 10;
		}
		else if(totWeight > 250){
			shipCost = 14;
			//console.log("14 usd");
		}
		totPrice = subTotPrice + shipCost;
		$("#totPrice").html(totPrice);
		$("#shipFee").html(shipCost);
		$("#subTotPrice").html(subTotPrice);
		cartArray = empArr;
		totalPayment = totPrice;
		shippingCost = shipCost;
		$("#shipPayment").html(shipCost);
		$("#totPayment").html(totPrice);
		$("#TotalFee").val(totPrice);
		$('#arrData').val(JSON.stringify(empArr));
		$('#shippingFee').val(shipCost);
		$("#invoiceModal").modal("show");
		
		//console.log(totWeight);
		//console.log(subTotPrice);

		//console.log(empArr);
		//console.log(totPrice);
	});
	if(Cookies.get('allProd') != undefined){
		var empString = Cookies.get('allProd');
		var empArr = $.parseJSON(empString);
		//console.log(empArr);
		$("#numOrder").html(empArr.length);
	}else{
		$("#numOrder").html(0);
	}
	//Cookies.remove('allProd', { path: '' });
	
	//console.log(Cookies.get('allProd'));
	$('#addCommentForm').validator().on('submit', function (e) {
		if (e.isDefaultPrevented()) {
			//console.log(0);
		} else {
			e.preventDefault();

			var passdata = new FormData(this);
				passdata.append('pet_id', $("#pet_id").val());
				passdata.append('pet_owner_id', $("#pet_owner_id").val());
				passdata.append('c_fname', $("#c_fname").val());
				passdata.append('c_address', $("#c_address").val());
				passdata.append('c_email', $("#c_email").val());
				passdata.append('date_found', $("#date_found").val());
				passdata.append('time_found', $("#time_found").val());
				passdata.append('c_contact', $("#c_contact").val());
				passdata.append('other_info', $("#other_info").val());
			$.ajax({
				type: "POST",
				url: "../../pet/addComment",
				data: passdata,
				processData:false,
				contentType:false,
				cache:false,
				async:true,
				success:
					function(data) {
						console.log(data);
						if(data == "true"){
							getPetComments();
							$('#addCommentSuccess').show(); 
							$('#addCommentError').hide(); 
							$("#c_fname").val("");
							$("#c_address").val("");
							$("#c_email").val("");
							$("#date_found").val("");
							$("#c_contact").val("");
							$("#other_info").val("");
							$("#c_imgfile").val("");
							
							setTimeout(function(){ $('#addCommentSuccess').hide(); $("#addCommentModal").modal("hide");}, 2200);	
						}else{
							
							$('#addCommentSuccess').hide(); 
							$('#addCommentError').show(); 	
						}
					},
				error:
				function(data){
					//console.log(data);
					$('#addCommentSuccess').hide(); 
					$('#addCommentError').show(); 	
				}
			});
		}
	});
})();

function capitalizeFirstLetter(string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
}

function getCart(){
	var base_url = $("#baseurl").val();
				
				if(Cookies.get('allProd') != undefined && Cookies.get('allProd') != [""]){
					//console.log(Cookies.get('allProd'));
					//console.log(123);
					var empString = Cookies.get('allProd');
					var empArr = $.parseJSON(empString);
					//console.log(empArr);
					$("#tbodyCartTable").empty();
					for(var x=0;x < empArr.length;x++){
						$("#cartTable").find('tbody')
							.append($('<tr>')
								.append($('<td>')
									.append($('<img>')
										.attr('src', base_url+"images/products/"+empArr[x].user_id+"/"+empArr[x].pic)
										.attr('style', "max-width: 80px;height: auto;")
									)
								)
								.append($('<td>')
									.append(empArr[x].name)
								)
								.append($('<td>')
									.append(empArr[x].desc)
								)
								.append($('<td>')
									.append(empArr[x].price)
								)
								.append($('<td>')
									.append(empArr[x].weight)
								)
								.append($('<td>')
									.append($('<button>')
										.attr('type', "btn")
										.attr('onclick', "deleteProdInCart("+x+")")
										.append("X")
									)
								)
							);
					}
					$("#nocart").hide();
				}
	
}
function getFinalCost(){
		var empString = Cookies.get('allProd');
		//console.log(empString);
		if(empString == undefined || empString == "" || empString == 'undefined'){
			var empArr = [];
		}else{
			var empArr = $.parseJSON(empString);
		}
		var subTotPrice = 0;
		var totPrice = 0;
		var totWeight = 0;
		var shipCost = 0;
		//console.log(empArr);
		for(var x=0;x < empArr.length;x++){
			subTotPrice += empArr[x].price;
			totWeight += empArr[x].weight;
		}
		if(totWeight > 0 && totWeight <= 100){
			//console.log("5 usd");
			shipCost = 5;
		}
		else if(totWeight > 100 && totWeight <= 250){
			//console.log("10 usd");
			shipCost = 10;
		}
		else if(totWeight > 250){
			shipCost = 14;
			//console.log("14 usd");
		}
		totPrice = subTotPrice + shipCost;
		$("#totPrice").html(totPrice);
		//$("#arrData").val(arrData);
		//console.log(empArr);
		$('#arrData').val(JSON.stringify(empArr));
		$("#shipFee").html(shipCost);
		$("#subTotPrice").html(subTotPrice);
		//console.log(totWeight);
		//console.log(subTotPrice);
		//console.log(empArr);
		//console.log(totPrice);
}

function deleteProdInCart(x){

	var empString = Cookies.get('allProd');
	var empArr = $.parseJSON(empString);
	//console.log(empArr);
	var id = x;
	empArr.splice(x, 1);
	//console.log(empArr);
	Cookies.set('allProd', empArr, { expires: 7, path: ''});
	$("#numOrder").html(empArr.length);
	getCart();
	getFinalCost();
}

function getPetComments(){
	var commentProfileTable = $('#commentProfileTable').DataTable();
	commentProfileTable.destroy();
	var id = $("#pet_id").val();
	var baseurl = $("#baseurl").val();
   $.ajax({
		type: "POST",
		url: "../../pet/getPetComments",
		data:{id:id},
		dataType: "json",
		success:
			function(data) {
				
				if(data.length > 0){
					$("#tbodycommentprofile").empty();
					for(var x=0;x < data.length;x++){
						$("#commentProfileTable").find('tbody')
							.append($('<tr>')
								.attr('class', "comment-s")
								.append($('<td>')
									.attr('style', "border-bottom:1px solid #ccc")
									.append($('<div>')
										.attr('class', "row")
										.append($('<div>')
											.attr('class', "col-md-3")
											.append($('<img>')
												.attr('style', "    margin: auto;display: block;max-height: 150px;width: auto;")
												.attr('src', baseurl+"images/comments/"+data[x].picture)
											)
										)
										.append($('<div>')
											.attr('class', "col-md-9")
											.append($('<div>')
												.attr('class', "row")
												.append($('<div>')
													.attr('class', "col-md-6")
													.append($('<p>')
														.append("Name: <span style='color:#222;'>"+data[x].fullname+"</span>")
													)
													.append($('<p>')
														.append("Seen Address: <span style='color:#222;'>"+data[x].address+"</span>")
													)
													.append($('<p>')
														.append("Email: <span style='color:#222;'>"+data[x].email+"</span>")
													)
												)
												.append($('<div>')
													.attr('class', "col-md-6")
													.append($('<p>')
														.append("Contact Number: <span style='color:#222;'>"+data[x].contact+"</span>")
													)
													.append($('<p>')
														.append("Date Found: <span style='color:#222;'>"+setDate(data[x].date_found)+" @ "+data[x].time_found+"</span>")
													)
													.append($('<p>')
														.append("Other Information: <span style='color:#222;'>"+data[x].other_info+"</span>")
													)
												)
											)
										)
									)
								)
							);
					}
					$("#commentProfileTable").show();
					$("#nocommentprofile").hide();
					commentProfileTable = $('#commentProfileTable').DataTable( {
						  "searching": false,
						  "lengthChange": false,
						  "ordering": false,
						  "pageLength": 10
						});
				}else{
					$("#commentProfileTable").hide();
					$("#nocommentprofile").show();
				}
			},
		error:
		function(data){
			console.log("false");		
		}
	});		
}

function pet_specie(n){
	var desc = "";
	switch (parseInt(n)){
		case 1:
		desc = "Dog";
		break;
		case 2:
		desc = "Cat";
		break;
		case 3:
		desc = "Bird";
		break;
		case 4:
		desc = "Snake";
		break;
		case 5:
		desc = "Horse";
		break;
		case 6:
		desc = "Others";
		break;
	}
	return desc;
}
function pet_gender(n){
	var desc = "";
	switch (parseInt(n)){
		case 1:
		desc = "Female";
		break;
		case 2:
		desc = "Male";
		break;
	}
	return desc;
}
function pet_neutered(n){
	var desc = "";
	switch (parseInt(n)){
		case 1:
		desc = "Yes";
		break;
		case 2:
		desc = "No";
		break;
	}
	return desc;
}
function pet_fur(n){
	var desc = "";
	switch (parseInt(n)){
		case 1:
		desc = "Longhaired";
		break;
		case 2:
		desc = "Shorthaired";
		break;
		case 3:
		desc = "Hairless";
		break;
	}
	return desc;
}
function pet_fur_color(n){
	var desc = "";
	switch (parseInt(n)){
		case 1:
		desc = "White";
		break;
		case 2:
		desc = "Beige";
		break;
		case 3:
		desc = "Yellow";
		break;
		case 4:
		desc = "Brown";
		break;
		case 5:
		desc = "Black";
		break;
		case 6:
		desc = "Grey";
		break;
		case 7:
		desc = "Mixed White-Red-Brown";
		break;
		case 8:
		desc = "Mixed Black-White";
		break;
		case 9:
		desc = "Mixed Black-Brown";
		break;
		case 10:
		desc = "Others";
		break;
	}
	return desc;
}
function pet_eye_color(n){
	var desc = "";
	switch (parseInt(n)){
		case 1:
		desc = "Blue";
		break;
		case 2:
		desc = "Brown";
		break;
		case 3:
		desc = "Black";
		break;
		case 4:
		desc = "Green";
		break;
		case 5:
		desc = "Grey";
		break;
		case 6:
		desc = "Others";
		break;
	}
	return desc;
}
function pet_size(n){
	var desc = "";
	switch (parseInt(n)){
		case 1:
		desc = "Micro";
		break;
		case 2:
		desc = "Mini";
		break;
		case 3:
		desc = "Small";
		break;
		case 4:
		desc = "Medium";
		break;
		case 5:
		desc = "Maxi";
		break;
		case 6:
		desc = "Big";
		break;
		case 7:
		desc = "Giant";
		break;
	}
	return desc;
}
function setDate(n){
	var d = new Date(n);
	
	var month = d.getMonth();
	var mWord;
	switch(month){
		case 0:
        mWord = "January";
        break;
    case 1:
        mWord = "February";
        break;
	case 2:
        mWord = "March";
        break;
	case 3:
        mWord = "April";
        break;
	case 4:
        mWord = "May";
        break;
	case 5:
        mWord = "June";
        break;
	case 6:
        mWord = "July";
        break;
	case 7:
        mWord = "August";
        break;
	case 8:
        mWord = "September";
        break;
	case 9:
        mWord = "October";
        break;
	case 10:
        mWord = "November";
        break;
	case 11:
        mWord = "December";
        break;
	}
	var nDate = d.getDate();
	var nYear = d.getFullYear();
	
	return mWord+" "+nDate+", "+nYear;
}
