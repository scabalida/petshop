var cartArray = [];
var totalPayment = 0;
var shippingCost = 0;
(function() {
	'use strict';
	getCart();
	getFinalCost();
	getAllFoundPet();
	getMyFoundPet();
	$("#showAdvancedSearch").click(function(){
		$("#closeAdvancedSearch").show();
		$("#showAdvancedSearch").hide();
		$("#searchArea").slideDown();
	});
	$("#closeAdvancedSearch").click(function(){
		$("#closeAdvancedSearch").hide();
		$("#showAdvancedSearch").show();
		$("#searchArea").slideUp();
	});
	$("#showCart").click(function(){
		getCart();
		getFinalCost();
		$("#closeCart").show();
		$("#showCart").hide();
		$("#cartTable").slideDown();
		$("#totArea").slideDown();
		
	});
	$("#closeCart").click(function(){
		getCart();
		getFinalCost();
		$("#showCart").show();
		$("#closeCart").hide();
		$("#cartTable").slideUp();
		$("#totArea").slideUp();
		
	});

	$("#checkOutNow").click(function(){
		var empString = Cookies.get('allProd');
		var empArr = $.parseJSON(empString);
		var subTotPrice = 0;
		var totPrice = 0;
		var totWeight = 0;
		var shipCost = 0;
		//console.log(empArr);
		for(var x=0;x < empArr.length;x++){
			subTotPrice += empArr[x].price;
			totWeight += empArr[x].weight;
		}
		if(totWeight > 0 && totWeight <= 100){
			//console.log("5 usd");
			shipCost = 5;
		}
		else if(totWeight > 100 && totWeight <= 250){
			//console.log("10 usd");
			shipCost = 10;
		}
		else if(totWeight > 250){
			shipCost = 14;
			//console.log("14 usd");
		}
		totPrice = subTotPrice + shipCost;
		$("#totPrice").html(totPrice);
		$("#shipFee").html(shipCost);
		$("#subTotPrice").html(subTotPrice);
		cartArray = empArr;
		totalPayment = totPrice;
		shippingCost = shipCost;
		$("#shipPayment").html(shipCost);
		$("#totPayment").html(totPrice);
		$("#TotalFee").val(totPrice);
		$('#arrData').val(JSON.stringify(empArr));
		$('#shippingFee').val(shipCost);
		$("#invoiceModal").modal("show");
		
		//console.log(totWeight);
		//console.log(subTotPrice);

		//console.log(empArr);
		//console.log(totPrice);
	});
	if(Cookies.get('allProd') != undefined){
		var empString = Cookies.get('allProd');
		var empArr = $.parseJSON(empString);
		//console.log(empArr);
		$("#numOrder").html(empArr.length);
	}else{
		$("#numOrder").html(0);
	}
	//Cookies.remove('allProd', { path: '' });
	
	//console.log(Cookies.get('allProd'));
	$("#searchBy").submit(function(e){
		e.preventDefault();
		var petID = $("#petID").val();
		var memberID = $("#memberID").val();
		var petName = $("#petName").val();
		var petSpecie = $("#petSpecie").val();
		var petFur = $("#petFur").val();
		var petFurColor = $("#petFurColor").val();
		var petEyeColor = $("#petEyeColor").val();
		var petSize = $("#petSize").val();
		var petGender = $("#petGender").val();
		var petMicrochip = $("#petMicrochip").val();
		var petTattoo = $("#petTattoo").val();
		var petNfc = $("#petNfc").val();
		var petKennel = $("#petKennel").val();

		if(petID != "" || memberID != "" || petName != "" || petSpecie != "" || petFur != "" || petFurColor != "" || petEyeColor != "" || petSize != "" || petGender != "" || petMicrochip != "" || petTattoo != "" || petNfc != "" || petKennel != ""){
			$.ajax({
				type: "POST",
				url: "found/searchFoundPet",
				dataType: "json",
				data:  $(this).serialize() ,
					success:
						function(data) {
							//console.log(data);
							var foundPetTable = $('#foundPetTable').DataTable();
							var base_url = $("#baseurl").val();
							foundPetTable.destroy();
							if(data.length > 0){
								$("#tbodyfoundpet").empty();
								for(var x=0;x < data.length;x++){
									$("#foundPetTable").find('tbody')
										.append($('<tr>')
											.append($('<td>')
												.attr('style', "text-align:center;")
												.append(padLeft(data[x].pet_id,10))
											)
											.append($('<td>')
												.attr('style', "text-align:center;")
												.append(padLeft(data[x].user_id,10))
											)
											.append($('<td>')
												.append(data[x].pet_name)
											)
											.append($('<td>')
												.append($('<img>')
													.attr('src', base_url+"images/uploads/"+data[x].user_id+"/"+data[x].picture)
													.attr('style', "max-height: 120px;width: auto;margin: auto;display: block;")
												)
											)
											.append($('<td>')
												.append(pet_specie(data[x].specie))
											)
											.append($('<td>')
												.append(pet_gender(data[x].gender))
											)
											.append($('<td>')
												.append(pet_fur(data[x].fur))
											)
											.append($('<td>')
												.append(pet_fur_color(data[x].fur_color))
											)
											.append($('<td>')
												.append(pet_eye_color(data[x].eyecolor))
											)
											.append($('<td>')
												.append(pet_size(data[x].size))
											)
											.append($('<td>')
												.attr('style', "width: 50px !important;")
												.append($('<a>')
													.attr('href', base_url+"pet/profile/"+data[x].pet_id)
													.attr('type', "button")
													.attr('class', "btn btn-info text-center")
													.attr('style', "display: block;margin: 0 auto;")
													.append($('<i>')
														.attr('class', "fa fa-search-plus")
													)
												)
											)
											.append($('<td>')
												.append($('<button>')
													.attr('onclick', "addPetToList("+data[x].pet_id+")")
													.attr('type', "button")
													.attr('class', "btn btn-primary text-center")
													.attr('style', "display: block;margin: 0 auto;")
													.append($('<i>')
														.attr('class', "fa fa-plus")
													)
												)
											)
										);
								}
								$("#foundPetTable").show();
								$("#nofoundpet").hide();
								foundPetTable = $('#foundPetTable').DataTable( {
									  "searching": false,
									  "lengthChange": false,
									  "ordering": false,
									  "pageLength": 10
									} );
							}else{
								$("#tbodyfoundpet").empty();
							}
						},
					error:
						function(data){
							//console.log(data);		
							//console.log("false");								
						}
			});
		}
	});

})();

function capitalizeFirstLetter(string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
}
function getAllFoundPet(){
	var foundPetTable = $('#foundPetTable').DataTable();
	var base_url = $("#baseurl").val();
	foundPetTable.destroy();
   $.ajax({
		type: "GET",
		url: "found/getAllFoundPet",
		dataType: "json",
		success:
			function(data) {
				if(data.length > 0){
					$("#tbodyfoundpet").empty();
					for(var x=0;x < data.length;x++){
						$("#foundPetTable").find('tbody')
							.append($('<tr>')
								.append($('<td>')
									.attr('style', "text-align:center;")
									.append(padLeft(data[x].pet_id,10))
								)
								.append($('<td>')
									.attr('style', "text-align:center;")
									.append(padLeft(data[x].user_id,10))
								)
								.append($('<td>')
									.append(data[x].pet_name)
								)
								.append($('<td>')
									.append($('<img>')
										.attr('src', base_url+"images/uploads/"+data[x].user_id+"/"+data[x].picture)
										.attr('style', "max-height: 120px;width: auto;margin: auto;display: block;")
									)
								)
								.append($('<td>')
									.append(pet_specie(data[x].specie))
								)
								.append($('<td>')
									.append(pet_gender(data[x].gender))
								)
								.append($('<td>')
									.append(pet_fur(data[x].fur))
								)
								.append($('<td>')
									.append(pet_fur_color(data[x].fur_color))
								)
								.append($('<td>')
									.append(pet_eye_color(data[x].eyecolor))
								)
								.append($('<td>')
									.append(pet_size(data[x].size))
								)
								.append($('<td>')
									.attr('style', "width: 50px !important;")
									.append($('<a>')
										.attr('href', base_url+"pet/profile/"+data[x].pet_id)
										.attr('type', "button")
										.attr('class', "btn btn-info text-center")
										.attr('style', "display: block;margin: 0 auto;")
										.append($('<i>')
											.attr('class', "fa fa-search-plus")
										)
									)
								)
								.append($('<td>')
									.append($('<button>')
										.attr('onclick', "addPetToList("+data[x].pet_id+")")
										.attr('type', "button")
										.attr('class', "btn btn-primary text-center")
										.attr('style', "display: block;margin: 0 auto;")
										.append($('<i>')
											.attr('class', "fa fa-plus")
										)
									)
								)
							);
					}
					$("#foundPetTable").show();
					$("#nofoundpet").hide();
					foundPetTable = $('#foundPetTable').DataTable( {
						  "searching": false,
						  "lengthChange": false,
						  "ordering": false,
						  "pageLength": 10
						  
						} );
				}else{
					$("#foundPetTable").hide();
					$("#nofoundpet").show();
				}
			},
		error:
		function(data){
			console.log("false");		
		}
	});		
}
function getMyFoundPet(){
	var myFoundPetTable = $('#myFoundPetTable').DataTable();
	var base_url = $("#baseurl").val();
	myFoundPetTable.destroy();
   $.ajax({
		type: "GET",
		url: "getMyFoundList",
		dataType: "json",
		success:
			function(data) {
				
				if(data.length > 0){
					$("#tbodymyfoundpet").empty();
					for(var x=0;x < data.length;x++){
						$("#myFoundPetTable").find('tbody')
							.append($('<tr>')
								.append($('<td>')
									.attr('style', "text-align:center;")
									.append(padLeft(data[x][0].pet_id,10))
								)
								.append($('<td>')
									.attr('style', "text-align:center;")
									.append(padLeft(data[x][0].user_id,10))
								)
								.append($('<td>')
									.append(data[x][0].pet_name)
								)
								.append($('<td>')
									.append($('<img>')
										.attr('src', base_url+"images/uploads/"+data[x][0].user_id+"/"+data[x][0].picture)
										.attr('style', "max-height: 120px;width: auto;margin: auto;display: block;")
									)
								)
								.append($('<td>')
									.append(pet_specie(data[x][0].specie))
								)
								.append($('<td>')
									.append(pet_gender(data[x][0].gender))
								)
								.append($('<td>')
									.append(pet_fur(data[x][0].fur))
								)
								.append($('<td>')
									.append(pet_fur_color(data[x][0].fur_color))
								)
								.append($('<td>')
									.append(pet_eye_color(data[x][0].eyecolor))
								)
								.append($('<td>')
									.append(pet_size(data[x][0].size))
								)
								.append($('<td>')
									.attr('style', "width: 50px !important;")
									.append($('<a>')
										.attr('href', base_url+"pet/profile/"+data[x][0].pet_id)
										.attr('type', "button")
										.attr('class', "btn btn-info text-center")
										.attr('style', "display: block;margin: 0 auto;")
										.append($('<i>')
											.attr('class', "fa fa-search-plus")
										)
									)
								)
							);
					}
					$("#myFoundPetTable").show();
					$("#nomyfoundpet").hide();
					myFoundPetTable = $('#myFoundPetTable').DataTable( {
						  "lengthChange": false,
						  "ordering": false,
						  "pageLength": 10
						  
						} );
				}else{
					$("#myFoundPetTable").hide();
					$("#nomyfoundpet").show();
				}
				
			},
		error:
		function(data){
			console.log("false");		
		}
	});		
}
function addPetToList(id){
	//console.log(id);
		$.ajax({
			type: "POST",
			url: "found/addToList",
			data:{id:id},
			dataType: "json",
			success:
				function(data) {
					if(data == "1"){
						$('#addPetListSuccess').show(); 
						$('#addPetListWarning').hide(); 
						$('#addPetListError').hide(); 
						setTimeout(function(){ $('#addPetListSuccess').hide();  }, 2000);
					}else if(data == "2"){
						$('#addPetListSuccess').hide(); 
						$('#addPetListError').show(); 
						$('#addPetListWarning').hide(); 
						setTimeout(function(){ $('#addPetListError').hide();  }, 2000);
					}
					else if(data == "3"){
						$('#addPetListSuccess').hide(); 
						$('#addPetListError').hide(); 
						$('#addPetListWarning').show(); 
						setTimeout(function(){ $('#addPetListWarning').hide();  }, 2000);
					}
					
				},
			error:
			function(data){
				//console.log(data);
			}
		});
}
function padLeft(nr, n, str){
    return Array(n-String(nr).length+1).join(str||'0')+nr;
}
function getCart(){
	var base_url = $("#baseurl").val();
				
				if(Cookies.get('allProd') != undefined && Cookies.get('allProd') != [""]){
					//console.log(Cookies.get('allProd'));
					//console.log(123);
					var empString = Cookies.get('allProd');
					var empArr = $.parseJSON(empString);
					//console.log(empArr);
					$("#tbodyCartTable").empty();
					for(var x=0;x < empArr.length;x++){
						$("#cartTable").find('tbody')
							.append($('<tr>')
								.append($('<td>')
									.append($('<img>')
										.attr('src', base_url+"images/products/"+empArr[x].user_id+"/"+empArr[x].pic)
										.attr('style', "max-width: 80px;height: auto;")
									)
								)
								.append($('<td>')
									.append(empArr[x].name)
								)
								.append($('<td>')
									.append(empArr[x].desc)
								)
								.append($('<td>')
									.append(empArr[x].price)
								)
								.append($('<td>')
									.append(empArr[x].weight)
								)
								.append($('<td>')
									.append($('<button>')
										.attr('type', "btn")
										.attr('onclick', "deleteProdInCart("+x+")")
										.append("X")
									)
								)
							);
					}
					$("#nocart").hide();
				}
	
}
function getFinalCost(){
		var empString = Cookies.get('allProd');
		if(empString == undefined || empString == "" || empString == 'undefined'){
			var empArr = [];
		}else{
			var empArr = $.parseJSON(empString);
		}
		var subTotPrice = 0;
		var totPrice = 0;
		var totWeight = 0;
		var shipCost = 0;
		//console.log(empArr);
		for(var x=0;x < empArr.length;x++){
			subTotPrice += empArr[x].price;
			totWeight += empArr[x].weight;
		}
		if(totWeight > 0 && totWeight <= 100){
			//console.log("5 usd");
			shipCost = 5;
		}
		else if(totWeight > 100 && totWeight <= 250){
			//console.log("10 usd");
			shipCost = 10;
		}
		else if(totWeight > 250){
			shipCost = 14;
			//console.log("14 usd");
		}
		totPrice = subTotPrice + shipCost;
		$("#totPrice").html(totPrice);
		//$("#arrData").val(arrData);
		console.log(empArr);
		$('#arrData').val(JSON.stringify(empArr));
		$("#shipFee").html(shipCost);
		$("#subTotPrice").html(subTotPrice);
		//console.log(totWeight);
		//console.log(subTotPrice);
		//console.log(empArr);
		//console.log(totPrice);
}
function deleteProdInCart(x){

	var empString = Cookies.get('allProd');
	var empArr = $.parseJSON(empString);
	//console.log(empArr);
	var id = x;
	empArr.splice(x, 1);
	//console.log(empArr);
	Cookies.set('allProd', empArr, { expires: 7, path: ''});
	$("#numOrder").html(empArr.length);
	getCart();
	getFinalCost();
}
function pet_specie(n){
	var desc = "";
	switch (parseInt(n)){
		case 1:
		desc = "Dog";
		break;
		case 2:
		desc = "Cat";
		break;
		case 3:
		desc = "Bird";
		break;
		case 4:
		desc = "Snake";
		break;
		case 5:
		desc = "Horse";
		break;
		case 6:
		desc = "Others";
		break;
	}
	return desc;
}
function pet_gender(n){
	var desc = "";
	switch (parseInt(n)){
		case 1:
		desc = "Female";
		break;
		case 2:
		desc = "Male";
		break;
	}
	return desc;
}
function pet_neutered(n){
	var desc = "";
	switch (parseInt(n)){
		case 1:
		desc = "Yes";
		break;
		case 2:
		desc = "No";
		break;
	}
	return desc;
}
function pet_fur(n){
	var desc = "";
	switch (parseInt(n)){
		case 1:
		desc = "Longhaired";
		break;
		case 2:
		desc = "Shorthaired";
		break;
		case 3:
		desc = "Hairless";
		break;
	}
	return desc;
}
function pet_fur_color(n){
	var desc = "";
	switch (parseInt(n)){
		case 1:
		desc = "White";
		break;
		case 2:
		desc = "Beige";
		break;
		case 3:
		desc = "Yellow";
		break;
		case 4:
		desc = "Brown";
		break;
		case 5:
		desc = "Black";
		break;
		case 6:
		desc = "Grey";
		break;
		case 7:
		desc = "Mixed White-Red-Brown";
		break;
		case 8:
		desc = "Mixed Black-White";
		break;
		case 9:
		desc = "Mixed Black-Brown";
		break;
		case 10:
		desc = "Others";
		break;
	}
	return desc;
}
function pet_eye_color(n){
	var desc = "";
	switch (parseInt(n)){
		case 1:
		desc = "Blue";
		break;
		case 2:
		desc = "Brown";
		break;
		case 3:
		desc = "Black";
		break;
		case 4:
		desc = "Green";
		break;
		case 5:
		desc = "Grey";
		break;
		case 6:
		desc = "Others";
		break;
	}
	return desc;
}
function pet_size(n){
	var desc = "";
	switch (parseInt(n)){
		case 1:
		desc = "Micro";
		break;
		case 2:
		desc = "Mini";
		break;
		case 3:
		desc = "Small";
		break;
		case 4:
		desc = "Medium";
		break;
		case 5:
		desc = "Maxi";
		break;
		case 6:
		desc = "Big";
		break;
		case 7:
		desc = "Giant";
		break;
	}
	return desc;
}
