var userID = 0;
var petID = 0;
var personID = 0;
var cartArray = [];
var totalPayment = 0;
var shippingCost = 0;
(function() {
	'use strict';
	getUserData();
	getAllUsers();
	getAllMyPets();
	getAllMyPerson();
		getCart();
	getFinalCost();
	$("#tag_petname").keyup(function(){
		$("#show_petname").html($("#tag_petname").val());
	});
	$("#tag_street").keyup(function(){
		$("#show_street").html($("#tag_street").val());
	});
	$("#tag_city").keyup(function(){
		$("#show_city").html($("#tag_city").val());
	});
	$("#tag_country").keyup(function(){
		$("#show_country").html($("#tag_country").val());
	});
	$("#tag_zip").keyup(function(){
		$("#show_zip").html($("#tag_zip").val());
	});
	$("#tag_contact").keyup(function(){
		$("#show_contact1").html($("#tag_contact").val());
	});
	$("#tag_contact2").keyup(function(){
		$("#show_contact2").html($("#tag_contact2").val());
	});
	
	$("#c_tag_petname").keyup(function(){
		$("#c_show_petname").html($("#c_tag_petname").val());
	});
	$("#c_tag_street").keyup(function(){
		$("#c_show_street").html($("#c_tag_street").val());
	});
	$("#c_tag_city").keyup(function(){
		$("#c_show_city").html($("#c_tag_city").val());
	});
	$("#c_tag_country").keyup(function(){
		$("#c_show_country").html($("#c_tag_country").val());
	});
	$("#c_tag_zip").keyup(function(){
		$("#c_show_zip").html($("#c_tag_zip").val());
	});
	$("#c_tag_contact").keyup(function(){
		$("#c_show_contact1").html($("#c_tag_contact").val());
	});
	$("#c_tag_contact2").keyup(function(){
		$("#c_show_contact2").html($("#c_tag_contact2").val());
	});
	$("#tag_picture").change(function(event) {
	
		var input = event.target;

		var reader = new FileReader();
		reader.onload = function(){
		  var dataURL = reader.result;
		  var output = document.getElementById('show_img');
		  output.src = dataURL;
		};
		reader.readAsDataURL(input.files[0]);

	});
	$("#c_tag_picture").change(function(event) {
	
		var input = event.target;

		var reader = new FileReader();
		reader.onload = function(){
		  var dataURL = reader.result;
		  var output = document.getElementById('c_show_img');
		  output.src = dataURL;
		};
		reader.readAsDataURL(input.files[0]);

	});
	$("#showCart").click(function(){
		getCart();
		getFinalCost();
		$("#closeCart").show();
		$("#showCart").hide();
		$("#cartTable").slideDown();
		$("#totArea").slideDown();
		
	});
	$("#closeCart").click(function(){
		getCart();
		getFinalCost();
		$("#showCart").show();
		$("#closeCart").hide();
		$("#cartTable").slideUp();
		$("#totArea").slideUp();
		
	});

	$("#checkOutNow").click(function(){
		var empString = Cookies.get('allProd');
		var empArr = $.parseJSON(empString);
		var subTotPrice = 0;
		var totPrice = 0;
		var totWeight = 0;
		var shipCost = 0;
		//console.log(empArr);
		for(var x=0;x < empArr.length;x++){
			subTotPrice += empArr[x].price;
			totWeight += empArr[x].weight;
		}
		if(totWeight > 0 && totWeight <= 100){
			//console.log("5 usd");
			shipCost = 5;
		}
		else if(totWeight > 100 && totWeight <= 250){
			//console.log("10 usd");
			shipCost = 10;
		}
		else if(totWeight > 250){
			shipCost = 14;
			//console.log("14 usd");
		}
		totPrice = subTotPrice + shipCost;
		$("#totPrice").html(totPrice);
		$("#shipFee").html(shipCost);
		$("#subTotPrice").html(subTotPrice);
		cartArray = empArr;
		totalPayment = totPrice;
		shippingCost = shipCost;
		$("#shipPayment").html(shipCost);
		$("#totPayment").html(totPrice);
		$("#TotalFee").val(totPrice);
		$('#arrData').val(JSON.stringify(empArr));
		$('#shippingFee').val(shipCost);
		$("#invoiceModal").modal("show");
		
		//console.log(totWeight);
		//console.log(subTotPrice);

		//console.log(empArr);
		//console.log(totPrice);
	});
	if(Cookies.get('allProd') != undefined){
		var empString = Cookies.get('allProd');
		var empArr = $.parseJSON(empString);
		//console.log(empArr);
		$("#numOrder").html(empArr.length);
	}else{
		$("#numOrder").html(0);
	}
	$('#updatePassword').validator().on('submit', function (e) {
		var password = $("input[name='upd_password2']").val();
		if (e.isDefaultPrevented()) {
			//console.log(0);
			e.preventDefault();
		} else {	
			if($("input[name='upd_password2']").val() == $("input[name='upd_password3']").val())
			{
				$("#updatePassNotEqualError").hide();	
				e.preventDefault();
				$.ajax({
					type: "POST",
					url: "settings/chckPassword",
					dataType: "json",
					data:  $(this).serialize(),
					success:
						function(data) {
							console.log(data);
							if(data == "true"){
								console.log("true");
								$("#updateWrongPassError").hide();
								$.ajax({
									type: "POST",
									url: "settings/updatePassword",
									dataType: "json",
									data: {password:password},
									success:
										function(data) {
											console.log(data);
											if(data == "true"){
												console.log("true");
												$("#updateUserPassError").hide();
												 $("#updateUserPassSuccess").show();
												setTimeout(function(){ $("#updateUserPassSuccess").hide();  }, 2000);
											}
											else{
												console.log("false");
												$("#updateUserPassError").show();
												$("#updateUserPassSuccess").hide();
											
											}
											
										},
									error:
										function(data){
											//console.log(data);		
											//console.log("false");
											$("#updateUserPassError").show();
											$("#updateUserPassSuccess").hide();
										}
								});
							}
							else{
								console.log("false");
								$("#updateWrongPassError").show();
							}
							
						},
					error:
						function(data){
							//console.log(data);		
							//console.log("false");
						}
				});
			}else{
				e.preventDefault();
				$("#updatePassNotEqualError").show();
			}
		}
	});
	$('#UpdateUserData').validator().on('submit', function (e) {
		if (e.isDefaultPrevented()) {
			//console.log(0);
			$("#updateCountryCodeError").show();
		} else {
				$("#updateCountryCodeError").hide();	
				e.preventDefault();

				$.ajax({
					type: "POST",
					url: "settings/updateUserData",
					dataType: "json",
					data:  $(this).serialize() ,
					success:
						function(data) {
							console.log(data);
							if(data == "true"){
								$('#updateUserSuccess').show(); 
								$('#updateUserError').hide(); 
								setTimeout(function(){ $('#updateUserSuccess').hide();  }, 2000);
								console.log("true");
								$("#btn_updateUser").hide();
								$("#btn_updateUserEDIT").show();
								$("input[name='upd_name']").prop("disabled", true); 
								$("input[name='upd_country']").prop("disabled", true); 
								$("input[name='upd_city']").prop("disabled", true); 
								$("input[name='upd_zip']").prop("disabled", true); 
								$("input[name='upd_c_code1']").prop("disabled", true); 
								$("input[name='upd_c_code2']").prop("disabled", true); 
								$("input[name='upd_c_code3']").prop("disabled", true); 
								$("input[name='upd_contact1']").prop("disabled", true); 
								$("input[name='upd_contact2']").prop("disabled", true); 
								$("input[name='upd_contact3']").prop("disabled", true); 
							}
							else{
								$('#updateUserSuccess').hide(); 
								$('#updateUserError').show(); 
								setTimeout(function(){ $('#updateUserError').hide();   }, 250);
								console.log("false");
							}
							
						},
					error:
						function(data){
							//console.log(data);		
							//console.log("false");
							$('#updateUserSuccess').hide(); 
							$('#updateUserError').show(); 		
							setTimeout(function(){ $('#updateUserError').hide();   }, 250);	
						}
				});

		}
	});
	$("#btn_updateUserEDIT").click(function(e){
		e.preventDefault();
		$("#btn_updateUser").show();
		$("#btn_updateUserEDIT").hide();
		$("input[name='upd_name']").prop("disabled", false); 
		$("input[name='upd_country']").prop("disabled", false); 
		$("input[name='upd_city']").prop("disabled", false); 
		$("input[name='upd_zip']").prop("disabled", false); 
		$("input[name='upd_c_code1']").prop("disabled", false); 
		$("input[name='upd_c_code2']").prop("disabled", false); 
		$("input[name='upd_c_code3']").prop("disabled", false); 
		$("input[name='upd_contact1']").prop("disabled", false); 
		$("input[name='upd_contact2']").prop("disabled", false); 
		$("input[name='upd_contact3']").prop("disabled", false); 
	});
	$('#UpdateUserAdminData').validator().on('submit', function (e) {
		if (e.isDefaultPrevented()) {
			//console.log(0);
			$("#updateCountryCodeAdminError").show();
		} else {
				$("#updateCountryCodeAdminError").hide();	
				e.preventDefault();

				$.ajax({
					type: "POST",
					url: "settings/updateUserDataByAdmin",
					dataType: "json",
					data:  $(this).serialize() ,
					success:
						function(data) {
							console.log(data);
							if(data == "true"){
								$('#updateUserAdminSuccess').show(); 
								$('#updateUserAdminError').hide(); 
								setTimeout(function(){ 
									$('#updateUserAdminSuccess').hide(); 
									$("#editUserModal").modal("hide");
								}, 3000);
								getAllUsers()
								console.log("true"); 
								
							}
							else{
								$('#updateUserAdminSuccess').hide(); 
								$('#updateUserAdminError').show(); 
								setTimeout(function(){ $('#updateUserAdminError').hide();   }, 250);
								console.log("false");
							}
							
						},
					error:
						function(data){
							//console.log(data);		
							//console.log("false");
							$('#updateUserAdminSuccess').hide(); 
							$('#updateUserAdminError').show(); 		
							setTimeout(function(){ $('#updateUserAdminError').hide();   }, 250);	
						}
				});

		}
	});
	$("#dltUser").click(function(e){
		e.preventDefault();
		var id = userID; 
		$.ajax({
			type: "POST",
			url: "settings/deleteuser",
			data:{id:id},
			dataType: "json",
			success:
				function(data) {
					//console.log(data);
					
						getAllUsers();
						$("#deleteUserModal").modal("hide");
					
				},
			error:
			function(data){
				//console.log(data);
			}
		});
	});

	$("#addPet").click(function(e){
		e.preventDefault();
		$("#addPetModal").modal("show");
		$("#addPetBtn").prop("disabled", false);
	});
	$("#addPerson").click(function(e){
		e.preventDefault();
		$("#addPersonModal").modal("show");
	});
	$('#addPetForm').validator().on('submit', function (e) {
		if (e.isDefaultPrevented()) {
			//console.log(0);
		} else {
			e.preventDefault();
			$("#addPetBtn").prop("disabled", true);
			
			var passdata = new FormData(this);
				passdata.append('pet_specie', $("#pet_specie").val());
				passdata.append('pet_name', $("#pet_name").val());
				passdata.append('pet_bday', $("#pet_bday").val());
				passdata.append('pet_gender', $("#pet_gender").val());
				passdata.append('pet_neutered', $("#pet_neutered").val());
				passdata.append('pet_fur', $("#pet_fur").val());
				passdata.append('pet_fur_color', $("#pet_fur_color").val());
				passdata.append('pet_eye_color', $("#pet_eye_color").val());
				passdata.append('pet_size', $("#pet_size").val());
				passdata.append('pet_microchip', $("#pet_microchip").val());
				passdata.append('pet_tattoo', $("#pet_tattoo").val());
				passdata.append('pet_status', $("#pet_status").val());
				passdata.append('pet_nfc', $("#pet_nfc").val());
				passdata.append('pet_kennel', $("#pet_kennel").val());
				passdata.append('pet_comment', $("#pet_comment").val());
			$.ajax({
				type: "POST",
				url: "settings/addPet",
				data: passdata,
				processData:false,
				contentType:false,
				cache:false,
				async:true,
				success:
					function(data) {
						console.log(data);
						if(data == "true"){
							
							getAllMyPets();
							$('#addPetSuccess').show(); 
							$('#addPetError').hide(); 
							$("#pet_name").val("");
							$("#pet_microchip").val("");
							$("#pet_tattoo").val("");
							$("#pet_kennel").val("");
							$("#pet_comment").val("");
							
							setTimeout(function(){ $('#addPetSuccess').hide(); $("#addPetModal").modal("hide");}, 2000);	
						}else{
							
							$('#addPetSuccess').hide(); 
							$('#addPetError').show(); 	
						}
					},
				error:
				function(data){
					//console.log(data);
					$('#addPetSuccess').hide(); 
					$('#addPetError').show(); 	
				}
			});
		}
	});	
	$('#updatePersonForm').validator().on('submit', function (e) {
		if (e.isDefaultPrevented()) {
			//console.log(0);
		} else {
			e.preventDefault();
			var passdata = new FormData(this);
				passdata.append('person_id', $("#upd_person_id").val());
				passdata.append('person_name', $("#upd_person_name").val());
				passdata.append('person_country', $("#upd_person_country").val());
				passdata.append('person_city', $("#upd_person_city").val());
				passdata.append('person_gender', $("#upd_person_gender").val());
				passdata.append('person_eyecolor', $("#upd_person_eyecolor").val());
				passdata.append('person_haircolor', $("#upd_person_haircolor").val());
				passdata.append('person_last_seen_date', $("#upd_person_last_seen_date").val());
				passdata.append('person_last_seen_place', $("#upd_person_last_seen_place").val());
				passdata.append('person_specific_features', $("#upd_person_specific_features").val());
				passdata.append('person_bday', $("#upd_person_bday").val());
				passdata.append('person_height', $("#upd_person_height").val());
				passdata.append('person_weight', $("#upd_person_weight").val());
				passdata.append('person_handed', $("#upd_person_handed").val());
				passdata.append('person_contact1', $("#upd_person_contact1").val());
				passdata.append('person_contact2', $("#upd_person_contact2").val());
				passdata.append('person_contact3', $("#upd_person_contact3").val());
				passdata.append('person_comment', $("#upd_person_comment").val());
				passdata.append('person_status', $("#upd_person_status").val());
			$.ajax({
				type: "POST",
				url: "settings/updatePerson",
				data: passdata,
				processData:false,
				contentType:false,
				cache:false,
				async:true,
				success:
					function(data) {
						console.log(data);
						if(data == "true"){
							
							getAllMyPerson();
							$('#updPersonSuccess').show(); 
							$('#updPersonError').hide(); 
							setTimeout(function(){ $('#updPersonSuccess').hide(); $("#updatePersonModal").modal("hide");}, 2000);	
						}else{
							
							$('#updPersonSuccess').hide(); 
							$('#updPersonError').show(); 	
						}
					},
				error:
				function(data){
					//console.log(data);
					$('#updPersonSuccess').hide(); 
					$('#updPersonError').show();  	
				}
			});
		}
	});
	$('#updatePetForm').validator().on('submit', function (e) {
		if (e.isDefaultPrevented()) {
			//console.log(0);
		} else {
			e.preventDefault();
			$("#updatePetBtn").attr("disabled", true);
			var passdata = new FormData(this);
				passdata.append('pet_id', $("#upd_pet_id").val());
				passdata.append('pet_specie', $("#upd_pet_specie").val());
				passdata.append('pet_name', $("#upd_pet_name").val());
				passdata.append('pet_bday', $("#upd_pet_bday").val());
				passdata.append('pet_gender', $("#upd_pet_gender").val());
				passdata.append('pet_neutered', $("#upd_pet_neutered").val());
				passdata.append('pet_fur', $("#upd_pet_fur").val());
				passdata.append('pet_fur_color', $("#upd_pet_fur_color").val());
				passdata.append('pet_eye_color', $("#upd_pet_eye_color").val());
				passdata.append('pet_size', $("#upd_pet_size").val());
				passdata.append('pet_microchip', $("#upd_pet_microchip").val());
				passdata.append('pet_tattoo', $("#upd_pet_tattoo").val());
				passdata.append('pet_status', $("#upd_pet_status").val());
				passdata.append('pet_nfc', $("#upd_pet_nfc").val());
				passdata.append('pet_kennel', $("#upd_pet_kennel").val());
				passdata.append('pet_comment', $("#upd_pet_comment").val());
			$.ajax({
				type: "POST",
				url: "settings/updatePet",
				data: passdata,
				processData:false,
				contentType:false,
				cache:false,
				async:true,
				success:
					function(data) {
						console.log(data);
						if(data == "true"){
							
							getAllMyPets();
							$('#updatePetSuccess').show(); 
							$('#updatePetError').hide(); 
							setTimeout(function(){ $('#updatePetSuccess').hide(); $("#updatePetModal").modal("hide");}, 2000);	
						}else{
							
							$('#updatePetSuccess').hide(); 
							$('#updatePetError').show(); 	
						}
					},
				error:
				function(data){
					//console.log(data);
					$('#updatePetSuccess').hide(); 
					$('#updatePetError').show();  	
				}
			});
		}
	});	
	$("#dltPet").click(function(e){
		e.preventDefault();
		var id = petID; 
		$.ajax({
			type: "POST",
			url: "settings/deletePet",
			data:{id:id},
			dataType: "json",
			success:
				function(data) {
					//console.log(data);
					
						getAllMyPets();
						$("#deletePetModal").modal("hide");
					
				},
			error:
			function(data){
				//console.log(data);
			}
		});
	});
	$("#dltPerson").click(function(e){
		e.preventDefault();
		var id = personID; 
		$.ajax({
			type: "POST",
			url: "settings/deletePerson",
			data:{id:id},
			dataType: "json",
			success:
				function(data) {
					//console.log(data);
					
						getAllMyPerson();
						$("#deletePersonModal").modal("hide");
					
				},
			error:
			function(data){
				//console.log(data);
			}
		});
	});
	$('#tagOrderForm').validator().on('submit', function (e) {
		if (e.isDefaultPrevented()) {
			//console.log(0);
		} else {
			e.preventDefault();
			var passdata = new FormData(this);
				passdata.append('tag_petname', $("#tag_petname").val());
				passdata.append('tag_street', $("#tag_street").val());
				passdata.append('tag_city', $("#tag_city").val());
				passdata.append('tag_contact', $("#tag_contact").val());
				passdata.append('tag_country', $("#tag_country").val());
				passdata.append('tag_zip', $("#tag_zip").val());
				passdata.append('tag_contact2', $("#tag_contact2").val());
			$.ajax({
				type: "POST",
				url: "pet/orderTag",
				data: passdata,
				processData:false,
				contentType:false,
				cache:false,
				async:true,
				success:
					function(data) {
						console.log(data);
						if(data > 0){
							var base_url = $("#baseurl").val();
							$("#tag_petname").val();
							$("#tag_street").val();
							$("#tag_city").val();
							$("#tag_contact").val();
							$("#tag_country").val();
							$("#tag_zip").val();
							$("#tag_contact2").val();
							window.location.href = base_url+"pet/payTagNow/"+data;
							//$('#orderTagSuccess').show(); 
							//$('#orderTagError').hide(); 
							//setTimeout(function(){ $('#orderTagSuccess').hide(); $("#tagOrderModal").modal("hide");}, 2000);	
						}else{
							
							$('#orderTagSuccess').hide(); 
							$('#orderTagError').show(); 	
						}
					},
				error:
				function(data){
					//console.log(data);
					$('#orderTagSuccess').hide(); 
					$('#orderTagError').show();  	
				}
			});
		}
	});	
	$('#contactIDOrderForm').validator().on('submit', function (e) {
		if (e.isDefaultPrevented()) {
			//console.log(0);
		} else {
			e.preventDefault();
			var passdata = new FormData(this);
				passdata.append('c_tag_street', $("#c_tag_street").val());
				passdata.append('c_tag_city', $("#c_tag_city").val());
				passdata.append('c_tag_contact', $("#c_tag_contact").val());
				passdata.append('c_tag_country', $("#c_tag_country").val());
				passdata.append('c_tag_zip', $("#c_tag_zip").val());
				passdata.append('c_tag_contact2', $("#c_tag_contact2").val());
			$.ajax({
				type: "POST",
				url: "user/orderContactID",
				data: passdata,
				processData:false,
				contentType:false,
				cache:false,
				async:true,
				success:
					function(data) {
						console.log(data);
						if(data > 0){
							var base_url = $("#baseurl").val();
							$("#c_tag_street").val();
							$("#c_tag_city").val();
							$("#c_tag_contact").val();
							$("#c_tag_country").val();
							$("#c_tag_zip").val();
							$("#c_tag_contact2").val();
							window.location.href = base_url+"user/payContactIDNow/"+data;	
						}else{
							
							$('#orderContactSuccess').hide(); 
							$('#orderContactError').show(); 	
						}
					},
				error:
				function(data){
					//console.log(data);
					$('#orderTagSuccess').hide(); 
					$('#orderTagError').show();  	
				}
			});
		}
	});
	$('#addPersonForm').validator().on('submit', function (e) {
		if (e.isDefaultPrevented()) {
			//console.log(0);
		} else {
			e.preventDefault();
			var passdata = new FormData(this);
				passdata.append('person_name', $("#person_name").val());
				passdata.append('person_country', $("#person_country").val());
				passdata.append('person_city', $("#person_city").val());
				passdata.append('person_gender', $("#person_gender").val());
				passdata.append('person_eyecolor', $("#person_eyecolor").val());
				passdata.append('person_haircolor', $("#person_haircolor").val());
				passdata.append('person_last_seen_date', $("#person_last_seen_date").val());
				passdata.append('person_last_seen_place', $("#person_last_seen_place").val());
				passdata.append('person_specific_features', $("#person_specific_features").val());
				passdata.append('person_bday', $("#person_bday").val());
				passdata.append('person_height', $("#person_height").val());
				passdata.append('person_weight', $("#person_weight").val());
				passdata.append('person_handed', $("#person_handed").val());
				passdata.append('person_contact1', $("#person_contact1").val());
				passdata.append('person_contact2', $("#person_contact2").val());
				passdata.append('person_contact3', $("#person_contact3").val());
				passdata.append('person_comment', $("#person_comment").val());
				passdata.append('person_status', $("#person_status").val());
			$.ajax({
				type: "POST",
				url: "settings/addPerson",
				data: passdata,
				processData:false,
				contentType:false,
				cache:false,
				async:true,
				success:
					function(data) {
						console.log(data);
						if(data == "true"){
							getAllMyPerson();
							$('#addPersonSuccess').show(); 
							$('#addPersonError').hide(); 
							$("#addPersonForm input").val("");
							$("#addPersonForm textarea").val("");
							
							setTimeout(function(){ $('#addPersonSuccess').hide(); $("#addPersonModal").modal("hide");}, 2000);	
						}else{
							
							$('#addPersonSuccess').hide(); 
							$('#addPersonError').show(); 	
						}
					},
				error:
				function(data){
					//console.log(data);
					$('#addPersonSuccess').hide(); 
					$('#addPersonError').show(); 	
				}
			});
		}
	});	
})();
function capitalizeFirstLetter(string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
}
function getUserData(){
	$.ajax({
		type: "POST",
		url: "settings/getUserData",
		dataType: "json",
		success:
			function(data) {
				//console.log(data);
				if(data.length > 0){
					
					var contact1 = data[0].u_contact1;
					var contact2 = data[0].u_contact2;
					var contact3 = data[0].u_contact3;
					var arr_contact1 = contact1.split("-");
					var arr_contact2 = contact2.split("-");
					var arr_contact3 = contact3.split("-");
					if(data[0].u_role == 1){
						$("#memberPackage").html("Basic");
					}else if(data[0].u_role == 2){
						$("#memberPackage").html("Standard");
					}else if(data[0].u_role == 3){
						$("#memberPackage").html("Admin");
					}
					$("#memberExp").html(setDate(data[0].date_expired));
					$("input[name='upd_email']").val(data[0].u_email);
					$("input[name='upd_name']").val(capitalizeFirstLetter(data[0].u_fullname));
					$("input[name='upd_country']").val(data[0].u_country);
					$("input[name='upd_city']").val(data[0].u_city);
					$("input[name='upd_zip']").val(data[0].u_zip);
					$("input[name='upd_c_code1']").val(arr_contact1[0]);
					$("input[name='upd_contact1']").val(arr_contact1[1]);
					$("input[name='upd_c_code2']").val(arr_contact2[0]);
					$("input[name='upd_contact2']").val(arr_contact2[1]);
					$("input[name='upd_c_code3']").val(arr_contact3[0]);
					$("input[name='upd_contact3']").val(arr_contact3[1]);
				}
			},
		error:
		function(data){
			//console.log(data);
		}
	});
}
function getAllUsers(){
   $.ajax({
		type: "GET",
		url: "settings/getAllUsersExceptAdmin",
		dataType: "json",
		success:
			function(data) {
				if(data.length > 0){
					$("#tbodyallusers").empty();
					for(var x=0;x < data.length;x++){
						$("#usersTable").find('tbody')
							.append($('<tr>')
								.append($('<td>')
									.append(data[x].u_fullname)
								)
								.append($('<td>')
									.append(data[x].u_email)
								)
								.append($('<td>')
									.append(data[x].u_contact1)
								)
								.append($('<td>')
									.append(data[x].u_contact2)
								)
								.append($('<td>')
									.append(data[x].u_contact3)
								)
								.append($('<td>')
									.append(data[x].u_city)
								)
								.append($('<td>')
									.append($('<button>')
										.attr('onclick', "editUserData("+data[x].u_id+")")
										.attr('type', "button")
										.attr('class', "pull-right btn btn-success text-center")
										.append($('<i>')
											.attr('class', "fa fa-edit")
										)
									)
								)
								.append($('<td>')
									.append($('<button>')
										.attr('onclick', "deleteUser("+data[x].u_id+")")
										.attr('type', "button")
										.attr('class', "pull-right btn btn-danger text-center")
										.append($('<i>')
											.attr('class', "fa fa-trash")
										)
									)
								)
							);
					}
					$("#usersTable").show();
					$("#nousers").hide();
					$('#usersTable').DataTable();
				}else{
					$("#usersTable").hide();
					$("#nousers").show();
				}
			},
		error:
		function(data){
			console.log("false");		
		}
	});		
}
function editUserData(id){
	//console.log(id);
	//userID = id;
	$.ajax({
		type: "POST",
		url: "settings/getThisUserData",
		dataType: "json",
		data:  {id:id},
		success:
			function(data) {
				//console.log(data);
				if(data.length > 0){
					var contact1 = data[0].u_contact1;
					var contact2 = data[0].u_contact2;
					var contact3 = data[0].u_contact3;
					var arr_contact1 = contact1.split("-");
					var arr_contact2 = contact2.split("-");
					var arr_contact3 = contact3.split("-");
					
					$("#ad_u_id").val(data[0].u_id);
					$("#upd_ad_name").val(data[0].u_fullname);
					$("#upd_ad_country").val(data[0].u_country);
					$("#upd_ad_city").val(data[0].u_city);
					document.getElementById("upd_ad_role").value = data[0].u_role;
					$("#upd_ad_zip").val(data[0].u_zip);
					
					$("input[name='upd_ad_c_code1']").val(arr_contact1[0]);
					$("input[name='upd_ad_contact1']").val(arr_contact1[1]);
					$("input[name='upd_ad_c_code2']").val(arr_contact2[0]);
					$("input[name='upd_ad_contact2']").val(arr_contact2[1]);
					$("input[name='upd_ad_c_code3']").val(arr_contact3[0]);
					$("input[name='upd_ad_contact3']").val(arr_contact3[1]);
					
					$("#editUserModal").modal("show");
				}
			},
		error:
		function(data){
			console.log("false");		
		}
	});
}
function deleteUser(id){
	userID = id;
	$("#deleteUserModal").modal("show");
	
}
function deletePet(id){
	petID = id;
	$("#deletePetModal").modal("show");
	
}
function deletePerson(id){
	personID = id;
	$("#deletePersonModal").modal("show");
	
}
function getAllMyPets(){
	var petDataTable = $('#petsTable').DataTable();
	petDataTable.destroy();
   $.ajax({
		type: "GET",
		url: "settings/getAllPets",
		dataType: "json",
		success:
			function(data) {
				
				if(data.length > 0){
					$("#tbodyallpets").empty();
					for(var x=0;x < data.length;x++){
						$("#petsTable").find('tbody')
							.append($('<tr>')
								.append($('<td>')
									.append(data[x].pet_name)
								)
								.append($('<td>')
									.append(pet_specie(data[x].specie))
								)
								.append($('<td>')
									.append(pet_gender(data[x].gender))
								)
								.append($('<td>')
									.append(pet_fur(data[x].fur))
								)
								.append($('<td>')
									.append(pet_fur_color(data[x].fur_color))
								)
								.append($('<td>')
									.append(pet_size(data[x].size))
								)
								.append($('<td>')
									.append(pet_status(data[x].status))
								)
								.append($('<td>')
									.append($('<button>')
										.attr('onclick', "clickUpdPet("+data[x].pet_id+")")
										.attr('type', "button")
										.attr('class', "pull-right btn btn-success text-center")
										.append($('<i>')
											.attr('class', "fa fa-edit")
										)
									)
								)
								.append($('<td>')
									.append($('<button>')
										.attr('onclick', "deletePet("+data[x].pet_id+")")
										.attr('type', "button")
										.attr('class', "pull-right btn btn-danger text-center")
										.append($('<i>')
											.attr('class', "fa fa-trash")
										)
									)
								)
								.append($('<td>')
									.append($('<button>')
										.attr('style', "padding: 4px 8px 2px 9px;")
										.attr('onclick', "shareNow("+data[x].user_id+","+data[x].pet_id+","+JSON.stringify(data[x].picture)+","+JSON.stringify(data[x].pet_name)+")")
										.attr('type', "button")
										.attr('class', "pull-right btn btn-primary text-center")
										.append($('<i>')
											.attr('style', "font-size: 20px;")
											.attr('class', "fa fa-facebook-square")
										)
									)
								)
							);
					}
					$("#petsTable").show();
					$("#nopets").hide();
					petDataTable = $('#petsTable').DataTable( {
						  "sorting": false
						} );
				}else{
					$("#petsTable").hide();
					$("#nopets").show();
				}
			},
		error:
		function(data){
			console.log("false");		
		}
	});		
}

function clickUpdPerson(id){
	var base_url = $("#baseurl").val();
	var usr_id = $("#usr_id").val();
	$.ajax({
		type: "POST",
		url: "settings/getThisPersonData",
		dataType: "json",
		data:  {id:id},
		success:
			function(data) {
				//console.log(data);
				if(data.length > 0){
					$("#upd_person_id").val(data[0].person_id);
					$("#upd_person_name").val(data[0].person_name);
					document.getElementById("upd_person_country").value = data[0].country;
					document.getElementById("upd_person_city").value = data[0].city;
					document.getElementById("upd_person_gender").value = data[0].gender;
					document.getElementById("upd_person_eyecolor").value = data[0].eyecolor;
					document.getElementById("upd_person_haircolor").value = data[0].haircolor;
					document.getElementById("upd_person_last_seen_date").value = data[0].last_seen_date;
					document.getElementById("upd_person_last_seen_place").value = data[0].last_seen_place;
					document.getElementById("upd_person_specific_features").value = data[0].specific_features;
					document.getElementById("upd_person_bday").value = data[0].bday;
					document.getElementById("upd_person_height").value = data[0].height;
					document.getElementById("upd_person_weight").value = data[0].weight;
					document.getElementById("upd_person_handed").value = data[0].handed;
					document.getElementById("upd_person_contact1").value = data[0].contact1;
					document.getElementById("upd_person_contact2").value = data[0].contact2;
					document.getElementById("upd_person_contact3").value = data[0].contact3;
					document.getElementById("upd_person_comment").value = data[0].comment;
					document.getElementById("upd_person_status").value = data[0].status;
					$("#updPersonBtn").removeAttr("class");
					document.getElementById("updPersonImgOutput").src = base_url+"images/persons/"+usr_id+"/"+data[0].pic;
					$("#updatePersonModal").modal("show");
					
				}
			},
		error:
		function(data){
			console.log("false");		
		}
	});
	
	
}
function clickUpdPet(id){
	var base_url = $("#baseurl").val();
	var usr_id = $("#usr_id").val();
	$.ajax({
		type: "POST",
		url: "settings/getThisPetData",
		dataType: "json",
		data:  {id:id},
		success:
			function(data) {
				//console.log(data);
				if(data.length > 0){
					$("#upd_pet_id").val(data[0].pet_id);
					$("#upd_pet_name").val(data[0].pet_name);
					document.getElementById("upd_pet_specie").value = data[0].specie;
					document.getElementById("upd_pet_bday").value = data[0].birthdate;
					document.getElementById("upd_pet_gender").value = data[0].gender;
					document.getElementById("upd_pet_neutered").value = data[0].neutered;
					document.getElementById("upd_pet_fur").value = data[0].fur;
					document.getElementById("upd_pet_fur_color").value = data[0].fur_color;
					document.getElementById("upd_pet_eye_color").value = data[0].eyecolor;
					document.getElementById("upd_pet_size").value = data[0].size;
					document.getElementById("upd_pet_microchip").value = data[0].microchip;
					document.getElementById("upd_pet_tattoo").value = data[0].tatto;
					document.getElementById("upd_pet_status").value = data[0].status;
					document.getElementById("upd_pet_nfc").value = data[0].nfc_chip;
					document.getElementById("upd_pet_kennel").value = data[0].kennel_id;
					document.getElementById("upd_pet_comment").value = data[0].comment;
					document.getElementById("updImgOutput").src = base_url+"images/uploads/"+usr_id+"/"+data[0].picture;
					$("#updatePetBtn").removeAttr("disabled");
					$("#updatePetModal").modal("show");
					
				}
			},
		error:
		function(data){
			console.log("false");		
		}
	});
	
	
}
function getCart(){
	var base_url = $("#baseurl").val();
				
				if(Cookies.get('allProd') != undefined && Cookies.get('allProd') != [""]){
					//console.log(Cookies.get('allProd'));
					//console.log(123);
					var empString = Cookies.get('allProd');
					var empArr = $.parseJSON(empString);
					//console.log(empArr);
					$("#tbodyCartTable").empty();
					for(var x=0;x < empArr.length;x++){
						$("#cartTable").find('tbody')
							.append($('<tr>')
								.append($('<td>')
									.append($('<img>')
										.attr('src', base_url+"images/products/"+empArr[x].user_id+"/"+empArr[x].pic)
										.attr('style', "max-width: 80px;height: auto;")
									)
								)
								.append($('<td>')
									.append(empArr[x].name)
								)
								.append($('<td>')
									.append(empArr[x].desc)
								)
								.append($('<td>')
									.append(empArr[x].price)
								)
								.append($('<td>')
									.append(empArr[x].weight)
								)
								.append($('<td>')
									.append($('<button>')
										.attr('type', "btn")
										.attr('onclick', "deleteProdInCart("+x+")")
										.append("X")
									)
								)
							);
					}
					$("#nocart").hide();
				}
	
}
function getFinalCost(){
		var empString = Cookies.get('allProd');
		if(empString == undefined || empString == "" || empString == 'undefined'){
			var empArr = [];
		}else{
			var empArr = $.parseJSON(empString);
		}
		var subTotPrice = 0;
		var totPrice = 0;
		var totWeight = 0;
		var shipCost = 0;
		//console.log(empArr);
		for(var x=0;x < empArr.length;x++){
			subTotPrice += empArr[x].price;
			totWeight += empArr[x].weight;
		}
		if(totWeight > 0 && totWeight <= 100){
			//console.log("5 usd");
			shipCost = 5;
		}
		else if(totWeight > 100 && totWeight <= 250){
			//console.log("10 usd");
			shipCost = 10;
		}
		else if(totWeight > 250){
			shipCost = 14;
			//console.log("14 usd");
		}
		totPrice = subTotPrice + shipCost;
		$("#totPrice").html(totPrice);
		//$("#arrData").val(arrData);
		console.log(empArr);
		$('#arrData').val(JSON.stringify(empArr));
		$("#shipFee").html(shipCost);
		$("#subTotPrice").html(subTotPrice);
		//console.log(totWeight);
		//console.log(subTotPrice);
		//console.log(empArr);
		//console.log(totPrice);
}
function deleteProdInCart(x){

	var empString = Cookies.get('allProd');
	var empArr = $.parseJSON(empString);
	//console.log(empArr);
	var id = x;
	empArr.splice(x, 1);
	//console.log(empArr);
	Cookies.set('allProd', empArr, { expires: 7, path: ''});
	$("#numOrder").html(empArr.length);
	getCart();
	getFinalCost();
}
function getAllMyPerson(){
	var personTable = $('#personTable').DataTable();
	personTable.destroy();
   $.ajax({
		type: "GET",
		url: "settings/getAllPerson",
		dataType: "json",
		success:
			function(data) {
				
				if(data.length > 0){
					$("#tbodyallperson").empty();
					for(var x=0;x < data.length;x++){
						$("#personTable").find('tbody')
							.append($('<tr>')
								.append($('<td>')
									.append(data[x].person_name)
								)
								.append($('<td>')
									.append(capitalizeFirstLetter(data[x].country)+ ", " + capitalizeFirstLetter(data[x].city))
								)
								.append($('<td>')
									.append(pet_gender(data[x].gender))
								)
								.append($('<td>')
									.append(data[x].contact1)
								)
								.append($('<td>')
									.append(person_eyecolor(data[x].eyecolor))
								)
								.append($('<td>')
									.append(person_haircolor(data[x].haircolor))
								)
								.append($('<td>')
									.append(person_status(data[x].status))
								)
								.append($('<td>')
									.append($('<button>')
										.attr('onclick', "clickUpdPerson("+data[x].person_id+")")
										.attr('type', "button")
										.attr('class', "pull-right btn btn-success text-center")
										.append($('<i>')
											.attr('class', "fa fa-edit")
										)
									)
								)
								.append($('<td>')
									.append($('<button>')
										.attr('onclick', "deletePerson("+data[x].person_id+")")
										.attr('type', "button")
										.attr('class', "pull-right btn btn-danger text-center")
										.append($('<i>')
											.attr('class', "fa fa-trash")
										)
									)
								)
							);
					}
					$("#personTable").show();
					$("#noperson").hide();
					personTable = $('#personTable').DataTable( {
						  "sorting": false
						} );
				}else{
					$("#personTable").hide();
					$("#noperson").show();
				}
			},
		error:
		function(data){
			console.log("false");		
		}
	});		
}
function person_eyecolor(n){
	var desc = "";
	switch (parseInt(n)){
		case 1:
		desc = "Blue";
		break;
		case 2:
		desc = "Brown";
		break;
		case 3:
		desc = "Green";
		break;
		case 4:
		desc = "Gray";
		break;
		case 5:
		desc = "Mixed";
		break;
	}
	return desc;
}
function person_haircolor(n){
	var desc = "";
	switch (parseInt(n)){
		case 1:
		desc = "White";
		break;
		case 2:
		desc = "Gray";
		break;
		case 3:
		desc = "Blond";
		break;
		case 4:
		desc = "Brown";
		break;
		case 5:
		desc = "Red";
		break;
		case 6:
		desc = "Black";
		break;
		case 7:
		desc = "Other";
		break;
	}
	return desc;
}
function person_status(n){
	var desc = "";
	switch (parseInt(n)){
		case 1:
		desc = "<span class='text-danger' style='font-size:16px;font-weight:bold;'>Missing</span>";
		break;
		case 2:
		desc = "<span class='text-primary' style='font-size:16px;font-weight:bold;'>Cancelled</span>";
		break;
	}
	return desc;
}
function pet_specie(n){
	var desc = "";
	switch (parseInt(n)){
		case 1:
		desc = "Dog";
		break;
		case 2:
		desc = "Cat";
		break;
		case 3:
		desc = "Bird";
		break;
		case 4:
		desc = "Snake";
		break;
		case 5:
		desc = "Horse";
		break;
		case 6:
		desc = "Others";
		break;
	}
	return desc;
}
function pet_gender(n){
	var desc = "";
	switch (parseInt(n)){
		case 1:
		desc = "Female";
		break;
		case 2:
		desc = "Male";
		break;
	}
	return desc;
}
function pet_neutered(n){
	var desc = "";
	switch (parseInt(n)){
		case 1:
		desc = "Yes";
		break;
		case 2:
		desc = "No";
		break;
	}
	return desc;
}
function pet_fur(n){
	var desc = "";
	switch (parseInt(n)){
		case 1:
		desc = "Longhaired";
		break;
		case 2:
		desc = "Shorthaired";
		break;
		case 3:
		desc = "Hairless";
		break;
	}
	return desc;
}
function pet_fur_color(n){
	var desc = "";
	switch (parseInt(n)){
		case 1:
		desc = "White";
		break;
		case 2:
		desc = "Beige";
		break;
		case 3:
		desc = "Yellow";
		break;
		case 4:
		desc = "Brown";
		break;
		case 5:
		desc = "Black";
		break;
		case 6:
		desc = "Grey";
		break;
		case 7:
		desc = "Mixed White-Red-Brown";
		break;
		case 8:
		desc = "Mixed Black-White";
		break;
		case 9:
		desc = "Mixed Black-Brown";
		break;
		case 10:
		desc = "Others";
		break;
	}
	return desc;
}
function pet_eye_color(n){
	var desc = "";
	switch (parseInt(n)){
		case 1:
		desc = "Blue";
		break;
		case 2:
		desc = "Brown";
		break;
		case 3:
		desc = "Black";
		break;
		case 4:
		desc = "Green";
		break;
		case 5:
		desc = "Grey";
		break;
		case 6:
		desc = "Others";
		break;
	}
	return desc;
}
function pet_size(n){
	var desc = "";
	switch (parseInt(n)){
		case 1:
		desc = "Micro";
		break;
		case 2:
		desc = "Mini";
		break;
		case 3:
		desc = "Small";
		break;
		case 4:
		desc = "Medium";
		break;
		case 5:
		desc = "Maxi";
		break;
		case 6:
		desc = "Big";
		break;
		case 7:
		desc = "Giant";
		break;
	}
	return desc;
}
function pet_status(n){
	var desc = "";
	switch (parseInt(n)){
		case 1:
		desc = "<span class='text-primary' style='font-size:16px;font-weight:bold;'>Home</span>";
		break;
		case 2:
		desc = "<span style='color:#000;font-size:16px;font-weight:bold;'>Dead</span>";
		break;
		case 3:
		desc = "<span class='text-warning' style='font-size:16px;font-weight:bold;'>Sold</span>";
		break;
		case 4:
		desc = "<span class='text-danger' style='font-size:16px;font-weight:bold;'>Lost</span>";
		break;
		case 5:
		desc = "<span class='text-success' style='font-size:16px;font-weight:bold;'>Found</span>";
		break;

	}
	return desc;
}
function setDate(n){
	var d = new Date(n);
	
	var month = d.getMonth();
	var mWord;
	switch(month){
		case 0:
        mWord = "January";
        break;
    case 1:
        mWord = "February";
        break;
	case 2:
        mWord = "March";
        break;
	case 3:
        mWord = "April";
        break;
	case 4:
        mWord = "May";
        break;
	case 5:
        mWord = "June";
        break;
	case 6:
        mWord = "July";
        break;
	case 7:
        mWord = "August";
        break;
	case 8:
        mWord = "September";
        break;
	case 9:
        mWord = "October";
        break;
	case 10:
        mWord = "November";
        break;
	case 11:
        mWord = "December";
        break;
	}
	var nDate = d.getDate();
	var nYear = d.getFullYear();
	
	return mWord+" "+nDate+", "+nYear;
}