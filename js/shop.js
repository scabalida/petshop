var userID = 0;
var petID = 0;
var productID = 0;
var orderID = 0;
var tagOrderID = 0;
var contactOrderID = 0;

(function() {
	'use strict';
	getAllProducts();
	getForDelivery();
	getShipped();
	getTagOrdersForDelivery();
	getContactIDOrdersForDelivery();
	$("#addProduct").click(function(e){
		e.preventDefault();
		$("#addProductModal").modal("show");
	});
	$('#addProductForm').validator().on('submit', function (e) {
		if (e.isDefaultPrevented()) {
			//console.log(0);
		} else {
			e.preventDefault();
			var passdata = new FormData(this);
				passdata.append('product_name', $("#product_name").val());
				passdata.append('product_desc', $("#product_desc").val());
				passdata.append('product_price', $("#product_price").val());
				passdata.append('product_weight', $("#product_weight").val());
			$.ajax({
				type: "POST",
				url: "shop/addProduct",
				data: passdata,
				processData:false,
				contentType:false,
				cache:false,
				async:true,
				success:
					function(data) {
						console.log(data);
						if(data == "true"){
							getAllProducts();
							$('#addProductSuccess').show(); 
							$('#addProductError').hide(); 
							$("#product_name").val("");
							$("#product_desc").val("");
							$("#product_price").val("");
							$("#productPic").val("");
							$("#product_weight").val("");
							setTimeout(function(){ $('#addProductSuccess').hide(); $("#addProductModal").modal("hide");}, 3000);	
						}else{
							
							$('#addProductSuccess').hide(); 
							$('#addProductError').show(); 	
						}
					},
				error:
				function(data){
					//console.log(data);
					$('#addProductSuccess').hide(); 
					$('#addProductError').show(); 	
				}
			});
		}
	});	

	$('#updateProductForm').validator().on('submit', function (e) {
		if (e.isDefaultPrevented()) {
			//console.log(0);
		} else {
			e.preventDefault();
			var passdata = new FormData(this);
				passdata.append('product_id', $("#upd_product_id").val());
				passdata.append('product_name', $("#upd_product_name").val());
				passdata.append('product_desc', $("#upd_product_desc").val());
				passdata.append('product_price', $("#upd_product_price").val());
				passdata.append('product_weight', $("#upd_product_weight").val());
				passdata.append('pictureone', $("#upd_productPic").val());
				passdata.append('picturetwo', $("#upd_productPic2").val());
			$.ajax({
				type: "POST",
				url: "shop/updateProduct",
				data: passdata,
				processData:false,
				contentType:false,
				cache:false,
				async:true,
				success:
					function(data) {
						console.log(data);
						if(data == "true"){
							
							getAllProducts();
							$('#updateProductSuccess').show(); 
							$('#updateProductError').hide(); 
							setTimeout(function(){ $('#updateProductSuccess').hide(); $("#updateProductModal").modal("hide");}, 3000);	
						}else{
							
							$('#updateProductSuccess').hide(); 
							$('#updateProductError').show(); 	
						}
					},
				error:
				function(data){
					//console.log(data);
					$('#updateProductSuccess').hide(); 
					$('#updateProductError').show();  	
				}
			});
		}
	});	
	
	$("#dltProduct").click(function(e){
		e.preventDefault();
		var id = productID; 
		$.ajax({
			type: "POST",
			url: "shop/deleteProduct",
			data:{id:id},
			dataType: "json",
			success:
				function(data) {
					//console.log(data);
					
						getAllProducts();
						$("#deleteProductModal").modal("hide");
					
				},
			error:
			function(data){
				//console.log(data);
			}
		});
	});
	$("#updToShipped").click(function(e){
		e.preventDefault();
		var id = orderID; 
		
		$.ajax({
			type: "POST",
			url: "shop/updateToShipped",
			data:{id:id},
			dataType: "json",
			success:
				function(data) {
					//console.log(data);
						getForDelivery();
						getShipped();
						$("#changeToShippedModal").modal("hide");
				},
			error:
			function(data){
				//console.log(data);
			}
		});
	});
	$("#updToShippedTagOrder").click(function(e){
		e.preventDefault();
		var id = tagOrderID; 
		
		$.ajax({
			type: "POST",
			url: "pet/updateToShipped",
			data:{id:id},
			dataType: "json",
			success:
				function(data) {
					//console.log(data);
						getTagOrdersForDelivery();
						$("#changeToShippedTagOrderModal").modal("hide");
				},
			error:
			function(data){
				//console.log(data);
			}
		});
	});
	$("#updToShippedContactIDOrder").click(function(e){
		e.preventDefault();
		var id = contactOrderID; 
		
		$.ajax({
			type: "POST",
			url: "user/updateToContactIDOrderShipped",
			data:{id:id},
			dataType: "json",
			success:
				function(data) {
					//console.log(data);
						getContactIDOrdersForDelivery();
						$("#changeToShippedContactIDOrderModal").modal("hide");
				},
			error:
			function(data){
				//console.log(data);
			}
		});
	});
})();
function capitalizeFirstLetter(string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
}
function getAllProducts(){
	var productDataTable = $('#productTable').DataTable();
	var base_url = $("#baseurl").val();
	var usr_id = $("#usr_id").val();
	productDataTable.destroy();
   $.ajax({
		type: "GET",
		url: "shop/getAllProducts",
		dataType: "json",
		success:
			function(data) {
				if(data.length > 0){
					$("#tbodyallproduct").empty();
					for(var x=0;x < data.length;x++){
						$("#productTable").find('tbody')
							.append($('<tr>')
								.append($('<td>')
									.append(data[x].product_name)
									.attr('style', "min-width: 100px;")
								)
								.append($('<td>')
									.append($('<img>')
										.attr('src', base_url+"images/products/"+data[x].user_id+"/"+data[x].product_pic)
										.attr('style', "max-height: 120px;width: auto;margin: auto;display: block;")
									)
								)
								.append($('<td>')
									.append($('<img>')
										.attr('src', base_url+"images/products/"+data[x].user_id+"/"+data[x].product_pic2)
										.attr('style', "max-height: 120px;width: auto;margin: auto;display: block;")
									)
								)
								.append($('<td>')
									.append(data[x].product_desc)
									.attr('class', "text-center")
								)
								.append($('<td>')
									.append(data[x].product_price)
									.attr('class', "text-center")
								)
								.append($('<td>')
									.append(data[x].product_weight)
									.attr('class', "text-center")
								)
								.append($('<td>')
									.append($('<button>')
										.attr('onclick', "clickUpdProduct("+data[x].product_id+")")
										.attr('type', "button")
										.attr('style', "display: block;margin: auto;")
										.attr('class', "btn btn-success text-center")
										.append($('<i>')
											.attr('class', "fa fa-edit")
										)
									)
								)
								.append($('<td>')
									.append($('<button>')
										.attr('onclick', "deleteProduct("+data[x].product_id+")")
										.attr('type', "button")
										.attr('style', "display: block;margin: auto;")
										.attr('class', "btn btn-danger text-center")
										.append($('<i>')
											.attr('class', "fa fa-trash")
										)
									)
								)
							);
					}
					$("#productTable").show();
					$("#noproduct").hide();
					productDataTable = $('#productTable').DataTable( {
						  "sorting": false
						} );
				}else{
					$("#productTable").hide();
					$("#noproduct").show();
				}
			},
		error:
		function(data){
			console.log("false");		
		}
	});		
}

function deleteProduct(id){
	productID = id;
	$("#deleteProductModal").modal("show");
	
}
function clickUpdProduct(id){
	var base_url = $("#baseurl").val();
	var usr_id = $("#usr_id").val();
	$.ajax({
		type: "POST",
		url: "shop/getThisProductData",
		dataType: "json",
		data:  {id:id},
		success:
			function(data) {
				//console.log(data);
				if(data.length > 0){
					$("#upd_product_id").val(data[0].product_id);
					$("#upd_product_name").val(data[0].product_name);
					$("#upd_product_weight").val(data[0].product_weight);
					$("#upd_product_desc").val(data[0].product_desc);
					$("#upd_product_price").val(data[0].product_price);
					document.getElementById("updProductImg").src = base_url+"images/products/"+usr_id+"/"+data[0].product_pic;
					document.getElementById("updProductImg2").src = base_url+"images/products/"+usr_id+"/"+data[0].product_pic2;
					$("#upd_productPic").val("");
					$("#upd_productPic2").val("");
					$("#updateProductModal").modal("show");
				}
			},
		error:
		function(data){
			console.log("false");		
		}
	});
	
	
}
function getForDelivery(){
	var deliveryTable = $('#deliveryTable').DataTable();
	var base_url = $("#baseurl").val();
	var usr_id = $("#usr_id").val();
	deliveryTable.destroy();
   $.ajax({
		type: "GET",
		url: "shop/getForDelivery",
		dataType: "json",
		success:
			function(data) {
				console.log(data);
				if(data.length > 0){
					$("#tbodyalldelivery").empty();
					for(var x=0;x < data.length;x++){
						$("#deliveryTable").find('tbody')
							.append($('<tr>')
								.append($('<td>')
									.append(data[x].id)
								)
								.append($('<td>')
									.append(data[x].fullname)
								)
								.append($('<td>')
									.append(data[x].home_address+", "+data[x].country)
								)
								.append($('<td>')
									.append(data[x].email)
								)
								.append($('<td>')
									.append(data[x].contact1)
								)
								.append($('<td>')
									.append(data[x].contact2)
								)
								.append($('<td>')
									.append(data[x].datetime)
								)
								.append($('<td>')
									.append($('<div>')
										.attr('class', "dropdown")
										.append($('<button>')
											.attr('onclick', "")
											.attr('type', "button")
											.attr('data-toggle', "dropdown")
											.attr('style', "display: block;margin: auto;")
											.attr('class', "btn btn-primary dropdown-toggle")
											.append($('<i>')
												.attr('class', "fa fa-cog")
											)
										)
										.append($('<ul>')
											.attr('class', "dropdown-menu")
											.append($('<li>')
												.append("<a style='cursor:pointer' onclick='viewProducts("+data[x].prod_id+")'>View</a>")
												.append("<a style='cursor:pointer' onclick='changeToShipped("+data[x].prod_id+")'>Change To Shipped</a>")
											)
										)
									)
								)
							);
					}
					$("#deliveryTable").show();
					$("#nodelivery").hide();
					productDataTable = $('#deliveryTable').DataTable( {
						  "sorting": false
						} );
				}else{
					$("#deliveryTable").hide();
					$("#nodelivery").show();
				}
			},
		error:
		function(data){
			console.log("false");		
		}
	});		
}
function changeToShipped(id){
	orderID = id;
	console.log(id);
	$("#changeToShippedModal").modal("show");
}
function viewProducts(id){
	var base_url = $("#baseurl").val();
	$.ajax({
		type: "POST",
		url: "shop/showProducts",
		data:{id:id},
		dataType: "json",
		success:
			function(data) {
				//console.log(data[0].contact1);
				//console.log(data[0].products);
				
				if(data.length > 0){
					var products = data[0].products;
					$("#tbodyshowproduct").empty();
					for(var x=0;x < products.length ;x++){
						console.log(products[x][0]);
						$("#showProductTable").find('tbody')
							.append($('<tr>')
								.append($('<td>')
									.append("1")
								)
								.append($('<td>')
									.append(products[x][1])
								)
								.append($('<td>')
									.append($('<img>')
										.attr('style', "max-height:90px;width:auto;border-radius:0;")
										.attr('src', base_url+"images/products/"+products[x][3]+"/"+products[x][2])
									)
								)
							);
					}
					$("#showProductTable").show();
					$("#showOrderName").html("<b>Name:</b> "+data[0].fullname);
					$("#showOrderaddress").html("<b>Home Address:</b> "+data[0].home_address);
					$("#showOrderCountry").html("<b>Country:</b> "+data[0].country);
					$("#showOrderEmail").html("<b>Email Address:</b> "+data[0].email);
					$("#showOrderContact1").html("<b>Contact Number 1:</b> "+data[0].contact1);
					$("#showOrderContact2").html("<b>Contact Number 2:</b> "+data[0].contact2);
					$("#showOrderDateOrdered").html("<b>Date Ordered:</b> "+data[0].datetime);
					$("#showOrderTotal").html("<b>Total Payment:</b> "+data[0].total_payment);
				}else{
					$("#showProductTable").hide();
				}
			},
		error:
			function(data){
				//console.log(data);
			}
	});
	$("#showProduct").modal("show");
}
function getShipped(){
	var shipTable = $('#shipTable').DataTable();
	var base_url = $("#baseurl").val();
	var usr_id = $("#usr_id").val();
	shipTable.destroy();
   $.ajax({
		type: "GET",
		url: "shop/getShipped",
		dataType: "json",
		success:
			function(data) {
				if(data.length > 0){
					$("#tbodyallship").empty();
					for(var x=0;x < data.length;x++){
						$("#shipTable").find('tbody')
							.append($('<tr>')
								.append($('<td>')
									.append(data[x].id)
								)
								.append($('<td>')
									.append(data[x].fullname)
								)
								.append($('<td>')
									.append(data[x].home_address+", "+data[x].country)
								)
								.append($('<td>')
									.append(data[x].email)
								)
								.append($('<td>')
									.append(data[x].contact1)
								)
								.append($('<td>')
									.append(data[x].contact2)
								)
								.append($('<td>')
									.append(data[x].date_shipped)
								)
								.append($('<td>')
									.append($('<button>')
										.attr('onclick', "viewShippedProducts("+data[x].prod_id+")")
										.attr('type', "button")
										.attr('style', "display: block;margin: auto;")
										.attr('class', "btn btn-primary text-center")
										.append($('<i>')
											.attr('class', "fa fa-search-plus")
										)
									)
								)
							);
					}
					$("#shipTable").show();
					$("#noship").hide();
					productDataTable = $('#shipTable').DataTable( {
						  "sorting": false
						} );
				}else{
					$("#shipTable").hide();
					$("#noship").show();
				}
			},
		error:
		function(data){
			console.log("false");		
		}
	});		
}
function viewShippedProducts(id){
	var base_url = $("#baseurl").val();
	$.ajax({
		type: "POST",
		url: "shop/showProducts",
		data:{id:id},
		dataType: "json",
		success:
			function(data) {
				//console.log(data[0].contact1);
				//console.log(data[0].products);
				
				if(data.length > 0){
					var products = data[0].products;
					$("#tbodyshowproduct_s").empty();
					for(var x=0;x < products.length ;x++){
						console.log(products[x][0]);
						$("#showProductTable_s").find('tbody')
							.append($('<tr>')
								.append($('<td>')
									.append("1")
								)
								.append($('<td>')
									.append(products[x][1])
								)
								.append($('<td>')
									.append($('<img>')
										.attr('style', "max-height:90px;width:auto;border-radius:0;")
										.attr('src', base_url+"images/products/"+products[x][3]+"/"+products[x][2])
									)
								)
							);
					}
					$("#showProductTable_s").show();
					$("#showOrderName_s").html("<b>Name:</b> "+data[0].fullname);
					$("#showOrderaddress_s").html("<b>Home Address:</b> "+data[0].home_address);
					$("#showOrderCountry_s").html("<b>Country:</b> "+data[0].country);
					$("#showOrderEmail_s").html("<b>Email Address:</b> "+data[0].email);
					$("#showOrderContact1_s").html("<b>Contact Number 1:</b> "+data[0].contact1);
					$("#showOrderContact2_s").html("<b>Contact Number 2:</b> "+data[0].contact2);
					$("#showOrderDateOrdered_s").html("<b>Date Ordered:</b> "+data[0].datetime);
					$("#showOrderTotal_s").html("<b>Total Payment:</b> "+data[0].total_payment);
				}else{
					$("#showProductTable_s").hide();
				}
			},
		error:
			function(data){
				//console.log(data);
			}
	});
	$("#showProduct_s").modal("show");
}

function getTagOrdersForDelivery(){
	var tagOrderTable = $('#tagOrderTable').DataTable();
	var base_url = $("#baseurl").val();
	tagOrderTable.destroy();
   $.ajax({
		type: "GET",
		url: "pet/getAllOrders",
		dataType: "json",
		success:
			function(data) {
				console.log(data);
				if(data.length > 0){
					$("#tbodytagOrder").empty();
					for(var x=0;x < data.length;x++){
						$("#tagOrderTable").find('tbody')
							.append($('<tr>')
								.append($('<td>')
									.append(data[x].prod_id)
								)
								.append($('<td>')
									.append(data[x].pet_name)
								)
								.append($('<td>')
									.append(data[x].address)
								)
								.append($('<td>')
									.append(data[x].email)
								)
								.append($('<td>')
									.append(data[x].contact1)
								)
								.append($('<td>')
									.append(data[x].contact2)
								)
								.append($('<td>')
									.append(tagStatus(data[x].order_status))
								)
								.append($('<td>')
									.append(data[x].date_purchased)
								)
								.append($('<td>')
									.append($('<div>')
										.attr('class', "dropdown")
										.append($('<button>')
											.attr('onclick', "")
											.attr('type', "button")
											.attr('data-toggle', "dropdown")
											.attr('style', "display: block;margin: auto;")
											.attr('class', "btn btn-primary dropdown-toggle")
											.append($('<i>')
												.attr('class', "fa fa-cog")
											)
										)
										.append($('<ul>')
											.attr('class', "dropdown-menu")
											.append($('<li>')
												.append("<a style='cursor:pointer' onclick='viewThisTagOrder("+data[x].id+")'>View</a>")
												.append("<a style='cursor:pointer' onclick='changeTagOrderToShipped("+data[x].id+")'>Change To Shipped</a>")
											)
										)
									)
								)
							);
					}
					$("#tagOrderTable").show();
					$("#notagorder").hide();
					tagOrderTable = $('#tagOrderTable').DataTable( {
						  "sorting": false
						} );
				}else{
					$("#tagOrderTable").hide();
					$("#notagorder").show();
				}
			},
		error:
		function(data){
			console.log("false");		
		}
	});		
}
function tagStatus(stat){
	if(stat == 1){
		return '<h5 class="text-center"><span class="label label-primary">For Delivery</span></h5>';
	}else if(stat == 2){
		return '<h5 class="text-center"><span class="label label-success ">Shipped</span></h5>';
	}
}
function viewThisTagOrder(id){
	$.ajax({
		type: "POST",
		url: "pet/getTagOrder",
		data:{id:id},
		dataType: "json",
		success:
			function(data) {
				if(data.length > 0){
					$("#v_show_country").html(data[0].country);
					$("#v_show_contact1").html(data[0].contact1);
					$("#v_show_contact2").html(data[0].contact2);
					$("#v_show_petname").html(data[0].pet_name);
					$("#v_show_street").html(data[0].street);
					$("#v_show_city").html(data[0].city);
					$("#v_show_zip").html(data[0].zip);
					$("#v_show_img").attr("src", data[0].pet_image);
				}
			},
		error:
			function(data){
				//console.log(data);
			}
	});
	$("#viewTagOrderModal").modal("show");
}
function changeTagOrderToShipped(id){
	tagOrderID = id;
	//console.log(id);
	$("#changeToShippedTagOrderModal").modal("show");
}

function getContactIDOrdersForDelivery(){
	var contactIDOrderTable = $('#contactIDOrderTable').DataTable();
	var base_url = $("#baseurl").val();
	contactIDOrderTable.destroy();
   $.ajax({
		type: "GET",
		url: "user/getAllContactIDOrders",
		dataType: "json",
		success:
			function(data) {
				console.log(data);
				if(data.length > 0){
					$("#tbodyContactIDOrder").empty();
					for(var x=0;x < data.length;x++){
						$("#contactIDOrderTable").find('tbody')
							.append($('<tr>')
								.append($('<td>')
									.append(data[x].prod_id)
								)
								.append($('<td>')
									.append(data[x].fullname)
								)
								.append($('<td>')
									.append(data[x].address)
								)
								.append($('<td>')
									.append(data[x].email)
								)
								.append($('<td>')
									.append(data[x].contact1)
								)
								.append($('<td>')
									.append(data[x].contact2)
								)
								.append($('<td>')
									.append(tagStatus(data[x].order_status))
								)
								.append($('<td>')
									.append(data[x].date_purchased)
								)
								.append($('<td>')
									.append($('<div>')
										.attr('class', "dropdown")
										.append($('<button>')
											.attr('onclick', "")
											.attr('type', "button")
											.attr('data-toggle', "dropdown")
											.attr('style', "display: block;margin: auto;")
											.attr('class', "btn btn-primary dropdown-toggle")
											.append($('<i>')
												.attr('class', "fa fa-cog")
											)
										)
										.append($('<ul>')
											.attr('class', "dropdown-menu")
											.append($('<li>')
												.append("<a style='cursor:pointer' onclick='viewThisContactIDOrder("+data[x].id+")'>View</a>")
												.append("<a style='cursor:pointer' onclick='changeContactIDOrderToShipped("+data[x].id+")'>Change To Shipped</a>")
											)
										)
									)
								)
							);
					}
					$("#contactIDOrderTable").show();
					$("#nocontactidorder").hide();
					contactIDOrderTable = $('#contactIDOrderTable').DataTable( {
						  "sorting": false
						} );
				}else{
					$("#contactIDOrderTable").hide();
					$("#nocontactidorder").show();
				}
			},
		error:
		function(data){
			console.log("false");		
		}
	});		
}
function viewThisContactIDOrder(id){
	$.ajax({
		type: "POST",
		url: "user/getContactIDOrder",
		data:{id:id},
		dataType: "json",
		success:
			function(data) {
				if(data.length > 0){
					$("#v_c_show_country").html(data[0].country);
					$("#v_c_show_contact1").html(data[0].contact1);
					$("#v_c_show_contact2").html(data[0].contact2);
					$("#v_c_show_petname").html(data[0].fullname);
					$("#v_c_show_street").html(data[0].street);
					$("#v_c_show_city").html(data[0].city);
					$("#v_c_show_zip").html(data[0].zip);
					$("#v_c_show_img").attr("src", data[0].owner_image);
				}
			},
		error:
			function(data){
				//console.log(data);
			}
	});
	$("#viewContactIDOrderModal").modal("show");
}
function changeContactIDOrderToShipped(id){
	contactOrderID = id;
	//console.log(id);
	$("#changeToShippedContactIDOrderModal").modal("show");
}