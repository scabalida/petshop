(function($){
	saveVisitor();
	$('#loginUser').validator().on('submit', function (e) {
		if (e.isDefaultPrevented()) {
			//console.log(0);
		} else {
			e.preventDefault();
							 
			$.ajax({
				type: "POST",
				url: "login/loginNow",
				dataType: "json",
				data:  $(this).serialize() ,
				success:
					function(data) {
						console.log(data);
						if(data == "0"){
							window.location.reload();
							console.log("true");
							$("#loginInvalid").hide();
							$("#accountExpired").hide();
						}
						else if(data == "1"){
							console.log("false");
							$("#loginInvalid").show();
							$("#accountExpired").hide();
						}else if(data == "2"){
							console.log("false");
							$("#loginInvalid").hide();
							$("#accountExpired").show();
						}
					},
				error:
					function(data){
						console.log("error");	
						//$("#loginInvalid").show();						
					}
			});
		}
	});
	$('#renewForm').validator().on('submit', function (e) {
		if (e.isDefaultPrevented()) {
			//console.log(0);
		} else {
			e.preventDefault();
							 
			$.ajax({
				type: "POST",
				url: "renew/getID",
				dataType: "json",
				data:  $(this).serialize() ,
				success:
					function(data) {
						//console.log(data.id);
						var baseurl = $("#baseurl").val();
						var role = $("#usr_role").val();
						window.location.href = baseurl+"renew/pay/"+data.id+"/"+role;
						$("#r_email").val("");
					},
				error:
					function(data){
						console.log("error");	
						//$("#loginInvalid").show();						
					}
			});
		}
	});
})(jQuery);

function saveVisitor(){
			$.ajax({
				type: "GET",
				url: "https://api.ipify.org?format=json",
				dataType: "json",
				success:
					function(data) {
						//console.log(data.ip);
						var ipAddress = data.ip; 
						$.ajax({
							type: "POST",
							url: "login/save_visitor",
							dataType: "json",
							data: {ip:ipAddress},
							success:
								function(data) {
									//console.log(data.ip);
								},
							error:
								function(data){
									//console.log("error");	
									//$("#loginInvalid").show();						
								}
						});
					},
				error:
					function(data){
						//console.log("error");	
						//$("#loginInvalid").show();						
					}
			});
}