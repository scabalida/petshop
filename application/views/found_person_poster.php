<!DOCTYPE html>
<html lang="en">
<head> 
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<style>
@font-face {
  font-family: 'KristenITC-Regular';

  src: url('../fonts/KristenITC-Regular.woff2') format('woff2'), url('../fonts/KristenITC-Regular.woff') format('woff');
}

</style>
</head>
  <body>
    <div>
		<p style=" font-family: 'KristenITC';font-size:42px;font-weight:bold;text-align:center">PetPost Found Person Alert</p>
		
		<img src="<?php echo base_url();?>images/heart.png" style="max-width:100px;margin-left:290px;margin-top:-50px">
		
		<p style="color:#ae21e2;font-family: 'KristenITC';font-size:23px;font-weight:bold;text-align:center;line-height:37px;"><u style="color:#000"><?php echo $name;?></u> was found <u style="color:#000"><?php echo $date;?></u><br/>in zip <u style="color:#000"><?php echo $zip;?></u> street <u style="color:#000;"><?php echo $street;?></u></p>
		<p style="text-align: center"><img style="max-height:330px;" src="<?php echo $pic;?>"></p>
		<p style="color:#f70329;font-family: 'KristenITC';font-size:22px;font-weight:bold;text-align:center;margin-top:20px">Hair Color <u style="color:#000"><?php echo $breed;?></u> Eye Color <u style="color:#000"><?php echo $color;?></u> Sex <u style="color:#000"><?php echo $gender;?></u> Age <u style="color:#000"><?php echo $age;?></u></p>
		<p style="font-family: 'KristenITC';font-size:23px;font-weight:bold;text-align:left;margin-left:40px;padding-top:-15px;">Details:</p>
		<p style="font-family: 'KristenITC';font-size:20px;font-weight:bold;text-align:left;margin-left:40px;padding-top:-20px;color:#f70329;"><?php echo $addDet;?></p>
		<p style="color:#ff00ff;font-family: 'KristenITC';font-size:30px;font-weight:bold;text-align:center;padding-top:15px;">Do you know my owner? Please contact</p>
		<p style="color:#ff00ff;font-family: 'KristenITC';font-size:30px;font-weight:bold;text-align:center;padding-top:-45px;"><u style="color:#000"><?php echo $contact;?></u> or comment me on</p>
		<p style="font-family: 'KristenITC';font-size:28px;font-weight:bold;text-align:center;padding-top:-15px;">www.petpost.info</p>
	</div>
  </body>
</html>