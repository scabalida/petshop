<!DOCTYPE html>
<html lang="en">

<head>
	<title>Pet Post</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<script type="application/x-javascript">
		addEventListener("load", function () {
			setTimeout(hideURLbar, 0);
		}, false);

		function hideURLbar() {
			window.scrollTo(0, 1);
		}
	</script>
	<link href="<?php echo base_url();?>css/bootstrap.css" type="text/css" rel="stylesheet" media="all">
	<link rel="shortcut icon" href="<?php echo base_url();?>images/logo.png" />
	<link href="<?php echo base_url();?>css/style.css" type="text/css" rel="stylesheet" media="all">
	<link href="<?php echo base_url();?>css/font-awesome.css" rel="stylesheet">
	<link href="<?php echo base_url();?>css/dataTables.bootstrap4.min.css" rel="stylesheet">
	<script src="<?php echo base_url();?>js/jquery-2.2.3.min.js"></script>

	<link href="//fonts.googleapis.com/css?family=Limelight" rel="stylesheet">
	<link href='//fonts.googleapis.com/css?family=Roboto:400,100,100italic,300,300italic,400italic,500,500italic,700,700italic,900,900italic'
	    rel='stylesheet' type='text/css'>
	<style>
		.payDet{
			color: #000;
			font-size: 1.5em;
			font-weight: bold;
			text-align: left;
		}
		#cartTable input[type="text"]{
			text-align:left;
		}
		#cartTable label{float:left}
		#cartTable th{
			color:#d93025;
		}
		#cartTable td{
			color:#333;
		}
		.checkout{
			border: none;
			outline: 2px solid #d93025;
			color: #fff;
			padding: .6em 3em;
			font-size: 1em;
			position: relative;
			margin: 10px auto;
			display: block;
			-webkit-appearance: none;
			background: #d93025;
		}
		#searchBy .form-group{    margin-bottom: 8px;}
	</style>
</head>

<body>
	<!-- banner -->
	<div class="agileits-banner about-w3banner">
		<div class="bnr-agileinfo">
			<div class="banner-top w3layouts">
				<div class="container">
					<ul class="agile_top_section">
						<li>
							<p><a href="<?php echo base_url();?>home"><img style="    max-width: 130px;" src="<?php echo base_url();?>images/petpostlogo.png"></a></p>
						</li>
						
						<?php if($this->session->userdata('loggedin') != TRUE){ ?>
						<li>
							<p>&ensp;</p>
						</li>
						<li><a class="sign" href="#" data-toggle="modal" data-target="#myModal2"><i class="fa fa-sign-in" aria-hidden="true"></i> Sign In</a>&nbsp;<a class="sign" href="#" data-toggle="modal" data-target="#cartModal" style="color: #fff; background-color: #f0ad4e;border-color: #eea236;"><i class="fa fa-shopping-cart" aria-hidden="true"></i> My Cart</a></li>
						<?php }else{ ?>
						<li>
							<p style="color:#fff;font-weight:bold">My ID : <?php echo str_pad($this->session->userdata('u_id'), 10, '0', STR_PAD_LEFT); ?></p>
						</li>
						<li><a class="sign" href="<?php echo base_url();?>settings"><i class="fa fa-cog" aria-hidden="true"></i> DASHBOARD</a>&nbsp;<a class="sign" href="#" data-toggle="modal" data-target="#cartModal" style="color: #fff; background-color: #f0ad4e;border-color: #eea236;"><i class="fa fa-shopping-cart" aria-hidden="true"></i> My Cart</a>&nbsp;<a class="sign" href="<?php echo base_url();?>login/logout"><i class="fa fa-power-off" aria-hidden="true"></i></a>
						</li>
						<?php } ?>							
					</ul>
				</div>
			</div>
			<div class="banner-w3text w3layouts">


				<h2>Pet Post</h2>
			</div>
			<!-- navigation -->
			<div class="top-nav w3-agiletop">
				<div class="agile_inner_nav_w3ls">
					<div class="navbar-header w3llogo">
						<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
							<span class="sr-only">Toggle navigation</span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>

						</button>
						<p><a href="<?php echo base_url();?>home"><img style="    max-width: 100px;" src="<?php echo base_url();?>images/logo.png"></a></p>
					</div>
					<!-- Collect the nav links, forms, and other content for toggling -->
					<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
						<div class="w3menu navbar-left">
							<ul class="nav navbar">
								<li><a href="<?php echo base_url();?>home" >Home</a></li>
								<li><a href="<?php echo base_url();?>about">About Us</a></li>
								<?php if($this->session->userdata('loggedin') == TRUE){ ?>
								<li><a href="<?php echo base_url();?>lost" class="active">Lost</a></li>
								<li><a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><span data-letters="Pages">Found</span><span class="caret"></span></a>
									<ul class="dropdown-menu">
										<li><a href="<?php echo base_url();?>found">All Found Pets</a></li>
										<li><a href="<?php echo base_url();?>found/mylist">My Found Pets</a></li>
									</ul>
								</li>
								<li><a href="<?php echo base_url();?>missing">Missing Person</a></li>
								<?php } ?>
								<li><a href="<?php echo base_url();?>shop">Shop</a></li>
								<li><a href="<?php echo base_url();?>contact">Contact</a></li>
							</ul>
						</div>
						<div class="w3ls-bnr-icons social-icon navbar-right">
							<a href="https://www.facebook.com/petpost.info/" target="_blank" class="social-button facebook"><i class="fa fa-facebook"></i></a>
							<a href="https://www.instagram.com/petpost.info/" target="_blank" class="social-button instagram"><i class="fa fa-instagram"></i></a>
						</div>
						<div class="clearfix"> </div>
					</div>
				</div>
			</div>
			<!-- //navigation -->
		</div>
	</div>
	<!-- //banner -->
	<div class="modal fade" id="cartModal" tabindex="-1" role="dialog">
		<div class="modal-dialog modal-lg">
			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<div class="signin-form profile">
						<h3 class="agileinfo_sign">My Cart</h3>
						<div class="row">
							<div class="col-md-8">
								<table id="cartTable" class="table table-striped" style="width:100%;">
									<thead>
										<tr>
											<th scope="col text-left">Picture</th>
											<th scope="col text-left">Product Name</th>
											<th scope="col text-left">Description</th>
											<th scope="col text-left">Price (USD)</th>
											<th scope="col text-left">Weight (g)</th>
											<th scope="col text-left"></th>
										</tr>
									</thead>
									<tbody id="tbodyCartTable">
																
									</tbody>
								</table>
								<div style="display:none" id="nocart" class="alert alert-info" role="alert">
									<h4>No products in the cart.</h4>
								</div>
							</div>
							<div class="col-md-4">
								<div id="totArea" style="text-align:right;padding-right:25px;    padding-top: 15px;">
									<p style="font-size:16px;color:#000;font-weight:bold">Shipping Fee ($): <span id="shipFee">0</span></p>
									<p style="font-size:16px;color:#000;font-weight:bold">Subtotal Price ($): <span id="subTotPrice">0</span></p>
									<p style="font-size:16px;color:#000;font-weight:bold">Total Price ($): <span id="totPrice">0</span></p>
										<table border="0" cellpadding="10" cellspacing="0" align="right"><tr><td align="center"></td></tr><tr><td align="center"><p style="cursor:pointer" id="checkOutNow"><img src="https://www.paypalobjects.com/webstatic/en_US/i/buttons/checkout-logo-large.png" alt="Check out with PayPal" style="border-radius:0;" /></p></td></tr></table>
								</div>
							</div>
						</div>
						<input type="hidden" id="baseurl" value="<?php echo base_url();?>">
						<input type="hidden" id="usr_id" value="<?php echo $this->session->userdata('u_id');?>">
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- Modal1 -->
	<div class="modal fade" id="myModal2" tabindex="-1" role="dialog">
		<div class="modal-dialog">
			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>

					<div class="signin-form profile">
						<h3 class="agileinfo_sign">Sign In</h3>
						<div class="login-form">
							<form id="loginUser"  method="post" data-toggle="validator" role="form">
								<div class="alert alert-danger display-none" id="loginInvalid">
									<p><strong>Invalid!</strong> Email or Password is incorrect.</p>
								</div>
								<div class="form-group">
									<input type="email" name="email" placeholder="E-mail" required="">
									<div class="help-block with-errors"></div>
								</div>
								<div class="form-group">
									<input type="password" name="password" placeholder="Password" required="">
									<div class="help-block with-errors"></div>
								</div>
								<div class="tp">
									<input type="submit" value="Sign In">
								</div>
							</form>
						</div>
						<div class="login-social-grids">
							<ul>
								<li><a href="#"><i class="fa fa-facebook"></i></a></li>
								<li><a href="#"><i class="fa fa-twitter"></i></a></li>
								<li><a href="#"><i class="fa fa-rss"></i></a></li>
							</ul>
						</div>
						<p><a href="#" data-toggle="modal" data-target="#myModal3">Create your account now today, not tomorrow.</a></p>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- //Modal1 -->
	<!-- Modal2 -->
	<div class="modal fade" id="myModal3" tabindex="-1" role="dialog">
		<div class="modal-dialog">
			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>

					<div class="signin-form profile">
						<h3 class="agileinfo_sign">Sign Up</h3>
						<div class="login-form">
							<form id="addUser" method="post" data-toggle="validator" role="form">
								<div class="alert alert-success display-none" id="addUserSuccess">
									<p><strong>Success!</strong> Your account has been created. You can now login.</p>
								</div>
								<div class="alert alert-danger display-none" id="addEmailDuplicate">
									<p><strong>Error!</strong> Email is already exist.</p>
								</div>
								<div class="alert alert-danger display-none" id="addUserError">
									<p><strong>Error!</strong> Please try again.</p>
								</div>
								<div class="alert alert-danger display-none" id="countryCodeError">
									<p><strong>Error!</strong> Please include country code for each contact number.</p>
								</div>
								<div class="form-group">
									<input type="text" name="name" placeholder="Full name" required>
									<div class="help-block with-errors"></div>
								</div>
								<div class="form-group">
									<select name="c_code1" style="width:43%;display:inline;font-size: 11px;">
											<option data-countryCode="DZ" value="213">Algeria (+213)</option>
											<option data-countryCode="AD" value="376">Andorra (+376)</option>
											<option data-countryCode="AO" value="244">Angola (+244)</option>
											<option data-countryCode="AI" value="1264">Anguilla (+1264)</option>
											<option data-countryCode="AG" value="1268">Antigua &amp; Barbuda (+1268)</option>
											<option data-countryCode="AR" value="54">Argentina (+54)</option>
											<option data-countryCode="AM" value="374">Armenia (+374)</option>
											<option data-countryCode="AW" value="297">Aruba (+297)</option>
											<option data-countryCode="AU" value="61">Australia (+61)</option>
											<option data-countryCode="AT" value="43">Austria (+43)</option>
											<option data-countryCode="AZ" value="994">Azerbaijan (+994)</option>
											<option data-countryCode="BS" value="1242">Bahamas (+1242)</option>
											<option data-countryCode="BH" value="973">Bahrain (+973)</option>
											<option data-countryCode="BD" value="880">Bangladesh (+880)</option>
											<option data-countryCode="BB" value="1246">Barbados (+1246)</option>
											<option data-countryCode="BY" value="375">Belarus (+375)</option>
											<option data-countryCode="BE" value="32">Belgium (+32)</option>
											<option data-countryCode="BZ" value="501">Belize (+501)</option>
											<option data-countryCode="BJ" value="229">Benin (+229)</option>
											<option data-countryCode="BM" value="1441">Bermuda (+1441)</option>
											<option data-countryCode="BT" value="975">Bhutan (+975)</option>
											<option data-countryCode="BO" value="591">Bolivia (+591)</option>
											<option data-countryCode="BA" value="387">Bosnia Herzegovina (+387)</option>
											<option data-countryCode="BW" value="267">Botswana (+267)</option>
											<option data-countryCode="BR" value="55">Brazil (+55)</option>
											<option data-countryCode="BN" value="673">Brunei (+673)</option>
											<option data-countryCode="BG" value="359">Bulgaria (+359)</option>
											<option data-countryCode="BF" value="226">Burkina Faso (+226)</option>
											<option data-countryCode="BI" value="257">Burundi (+257)</option>
											<option data-countryCode="KH" value="855">Cambodia (+855)</option>
											<option data-countryCode="CM" value="237">Cameroon (+237)</option>
											<option data-countryCode="CA" value="1">Canada (+1)</option>
											<option data-countryCode="CV" value="238">Cape Verde Islands (+238)</option>
											<option data-countryCode="KY" value="1345">Cayman Islands (+1345)</option>
											<option data-countryCode="CF" value="236">Central African Republic (+236)</option>
											<option data-countryCode="CL" value="56">Chile (+56)</option>
											<option data-countryCode="CN" value="86">China (+86)</option>
											<option data-countryCode="CO" value="57">Colombia (+57)</option>
											<option data-countryCode="KM" value="269">Comoros (+269)</option>
											<option data-countryCode="CG" value="242">Congo (+242)</option>
											<option data-countryCode="CK" value="682">Cook Islands (+682)</option>
											<option data-countryCode="CR" value="506">Costa Rica (+506)</option>
											<option data-countryCode="HR" value="385">Croatia (+385)</option>
											<option data-countryCode="CU" value="53">Cuba (+53)</option>
											<option data-countryCode="CY" value="90392">Cyprus North (+90392)</option>
											<option data-countryCode="CY" value="357">Cyprus South (+357)</option>
											<option data-countryCode="CZ" value="42">Czech Republic (+42)</option>
											<option data-countryCode="DK" value="45">Denmark (+45)</option>
											<option data-countryCode="DJ" value="253">Djibouti (+253)</option>
											<option data-countryCode="DM" value="1809">Dominica (+1809)</option>
											<option data-countryCode="DO" value="1809">Dominican Republic (+1809)</option>
											<option data-countryCode="EC" value="593">Ecuador (+593)</option>
											<option data-countryCode="EG" value="20">Egypt (+20)</option>
											<option data-countryCode="SV" value="503">El Salvador (+503)</option>
											<option data-countryCode="GQ" value="240">Equatorial Guinea (+240)</option>
											<option data-countryCode="ER" value="291">Eritrea (+291)</option>
											<option data-countryCode="EE" value="372">Estonia (+372)</option>
											<option data-countryCode="ET" value="251">Ethiopia (+251)</option>
											<option data-countryCode="FK" value="500">Falkland Islands (+500)</option>
											<option data-countryCode="FO" value="298">Faroe Islands (+298)</option>
											<option data-countryCode="FJ" value="679">Fiji (+679)</option>
											<option data-countryCode="FI" value="358">Finland (+358)</option>
											<option data-countryCode="FR" value="33">France (+33)</option>
											<option data-countryCode="GF" value="594">French Guiana (+594)</option>
											<option data-countryCode="PF" value="689">French Polynesia (+689)</option>
											<option data-countryCode="GA" value="241">Gabon (+241)</option>
											<option data-countryCode="GM" value="220">Gambia (+220)</option>
											<option data-countryCode="GE" value="7880">Georgia (+7880)</option>
											<option data-countryCode="DE" value="49">Germany (+49)</option>
											<option data-countryCode="GH" value="233">Ghana (+233)</option>
											<option data-countryCode="GI" value="350">Gibraltar (+350)</option>
											<option data-countryCode="GR" value="30">Greece (+30)</option>
											<option data-countryCode="GL" value="299">Greenland (+299)</option>
											<option data-countryCode="GD" value="1473">Grenada (+1473)</option>
											<option data-countryCode="GP" value="590">Guadeloupe (+590)</option>
											<option data-countryCode="GU" value="671">Guam (+671)</option>
											<option data-countryCode="GT" value="502">Guatemala (+502)</option>
											<option data-countryCode="GN" value="224">Guinea (+224)</option>
											<option data-countryCode="GW" value="245">Guinea - Bissau (+245)</option>
											<option data-countryCode="GY" value="592">Guyana (+592)</option>
											<option data-countryCode="HT" value="509">Haiti (+509)</option>
											<option data-countryCode="HN" value="504">Honduras (+504)</option>
											<option data-countryCode="HK" value="852">Hong Kong (+852)</option>
											<option data-countryCode="HU" value="36">Hungary (+36)</option>
											<option data-countryCode="IS" value="354">Iceland (+354)</option>
											<option data-countryCode="IN" value="91">India (+91)</option>
											<option data-countryCode="ID" value="62">Indonesia (+62)</option>
											<option data-countryCode="IR" value="98">Iran (+98)</option>
											<option data-countryCode="IQ" value="964">Iraq (+964)</option>
											<option data-countryCode="IE" value="353">Ireland (+353)</option>
											<option data-countryCode="IL" value="972">Israel (+972)</option>
											<option data-countryCode="IT" value="39">Italy (+39)</option>
											<option data-countryCode="JM" value="1876">Jamaica (+1876)</option>
											<option data-countryCode="JP" value="81">Japan (+81)</option>
											<option data-countryCode="JO" value="962">Jordan (+962)</option>
											<option data-countryCode="KZ" value="7">Kazakhstan (+7)</option>
											<option data-countryCode="KE" value="254">Kenya (+254)</option>
											<option data-countryCode="KI" value="686">Kiribati (+686)</option>
											<option data-countryCode="KP" value="850">Korea North (+850)</option>
											<option data-countryCode="KR" value="82">Korea South (+82)</option>
											<option data-countryCode="KW" value="965">Kuwait (+965)</option>
											<option data-countryCode="KG" value="996">Kyrgyzstan (+996)</option>
											<option data-countryCode="LA" value="856">Laos (+856)</option>
											<option data-countryCode="LV" value="371">Latvia (+371)</option>
											<option data-countryCode="LB" value="961">Lebanon (+961)</option>
											<option data-countryCode="LS" value="266">Lesotho (+266)</option>
											<option data-countryCode="LR" value="231">Liberia (+231)</option>
											<option data-countryCode="LY" value="218">Libya (+218)</option>
											<option data-countryCode="LI" value="417">Liechtenstein (+417)</option>
											<option data-countryCode="LT" value="370">Lithuania (+370)</option>
											<option data-countryCode="LU" value="352">Luxembourg (+352)</option>
											<option data-countryCode="MO" value="853">Macao (+853)</option>
											<option data-countryCode="MK" value="389">Macedonia (+389)</option>
											<option data-countryCode="MG" value="261">Madagascar (+261)</option>
											<option data-countryCode="MW" value="265">Malawi (+265)</option>
											<option data-countryCode="MY" value="60">Malaysia (+60)</option>
											<option data-countryCode="MV" value="960">Maldives (+960)</option>
											<option data-countryCode="ML" value="223">Mali (+223)</option>
											<option data-countryCode="MT" value="356">Malta (+356)</option>
											<option data-countryCode="MH" value="692">Marshall Islands (+692)</option>
											<option data-countryCode="MQ" value="596">Martinique (+596)</option>
											<option data-countryCode="MR" value="222">Mauritania (+222)</option>
											<option data-countryCode="YT" value="269">Mayotte (+269)</option>
											<option data-countryCode="MX" value="52">Mexico (+52)</option>
											<option data-countryCode="FM" value="691">Micronesia (+691)</option>
											<option data-countryCode="MD" value="373">Moldova (+373)</option>
											<option data-countryCode="MC" value="377">Monaco (+377)</option>
											<option data-countryCode="MN" value="976">Mongolia (+976)</option>
											<option data-countryCode="MS" value="1664">Montserrat (+1664)</option>
											<option data-countryCode="MA" value="212">Morocco (+212)</option>
											<option data-countryCode="MZ" value="258">Mozambique (+258)</option>
											<option data-countryCode="MN" value="95">Myanmar (+95)</option>
											<option data-countryCode="NA" value="264">Namibia (+264)</option>
											<option data-countryCode="NR" value="674">Nauru (+674)</option>
											<option data-countryCode="NP" value="977">Nepal (+977)</option>
											<option data-countryCode="NL" value="31">Netherlands (+31)</option>
											<option data-countryCode="NC" value="687">New Caledonia (+687)</option>
											<option data-countryCode="NZ" value="64">New Zealand (+64)</option>
											<option data-countryCode="NI" value="505">Nicaragua (+505)</option>
											<option data-countryCode="NE" value="227">Niger (+227)</option>
											<option data-countryCode="NG" value="234">Nigeria (+234)</option>
											<option data-countryCode="NU" value="683">Niue (+683)</option>
											<option data-countryCode="NF" value="672">Norfolk Islands (+672)</option>
											<option data-countryCode="NP" value="670">Northern Marianas (+670)</option>
											<option data-countryCode="NO" value="47">Norway (+47)</option>
											<option data-countryCode="OM" value="968">Oman (+968)</option>
											<option data-countryCode="PW" value="680">Palau (+680)</option>
											<option data-countryCode="PA" value="507">Panama (+507)</option>
											<option data-countryCode="PG" value="675">Papua New Guinea (+675)</option>
											<option data-countryCode="PY" value="595">Paraguay (+595)</option>
											<option data-countryCode="PE" value="51">Peru (+51)</option>
											<option data-countryCode="PH" value="63">Philippines (+63)</option>
											<option data-countryCode="PL" value="48">Poland (+48)</option>
											<option data-countryCode="PT" value="351">Portugal (+351)</option>
											<option data-countryCode="PR" value="1787">Puerto Rico (+1787)</option>
											<option data-countryCode="QA" value="974">Qatar (+974)</option>
											<option data-countryCode="RE" value="262">Reunion (+262)</option>
											<option data-countryCode="RO" value="40">Romania (+40)</option>
											<option data-countryCode="RU" value="7">Russia (+7)</option>
											<option data-countryCode="RW" value="250">Rwanda (+250)</option>
											<option data-countryCode="SM" value="378">San Marino (+378)</option>
											<option data-countryCode="ST" value="239">Sao Tome &amp; Principe (+239)</option>
											<option data-countryCode="SA" value="966">Saudi Arabia (+966)</option>
											<option data-countryCode="SN" value="221">Senegal (+221)</option>
											<option data-countryCode="CS" value="381">Serbia (+381)</option>
											<option data-countryCode="SC" value="248">Seychelles (+248)</option>
											<option data-countryCode="SL" value="232">Sierra Leone (+232)</option>
											<option data-countryCode="SG" value="65">Singapore (+65)</option>
											<option data-countryCode="SK" value="421">Slovak Republic (+421)</option>
											<option data-countryCode="SI" value="386">Slovenia (+386)</option>
											<option data-countryCode="SB" value="677">Solomon Islands (+677)</option>
											<option data-countryCode="SO" value="252">Somalia (+252)</option>
											<option data-countryCode="ZA" value="27">South Africa (+27)</option>
											<option data-countryCode="ES" value="34">Spain (+34)</option>
											<option data-countryCode="LK" value="94">Sri Lanka (+94)</option>
											<option data-countryCode="SH" value="290">St. Helena (+290)</option>
											<option data-countryCode="KN" value="1869">St. Kitts (+1869)</option>
											<option data-countryCode="SC" value="1758">St. Lucia (+1758)</option>
											<option data-countryCode="SD" value="249">Sudan (+249)</option>
											<option data-countryCode="SR" value="597">Suriname (+597)</option>
											<option data-countryCode="SZ" value="268">Swaziland (+268)</option>
											<option data-countryCode="SE" value="46">Sweden (+46)</option>
											<option data-countryCode="CH" value="41">Switzerland (+41)</option>
											<option data-countryCode="SI" value="963">Syria (+963)</option>
											<option data-countryCode="TW" value="886">Taiwan (+886)</option>
											<option data-countryCode="TJ" value="7">Tajikstan (+7)</option>
											<option data-countryCode="TH" value="66">Thailand (+66)</option>
											<option data-countryCode="TG" value="228">Togo (+228)</option>
											<option data-countryCode="TO" value="676">Tonga (+676)</option>
											<option data-countryCode="TT" value="1868">Trinidad &amp; Tobago (+1868)</option>
											<option data-countryCode="TN" value="216">Tunisia (+216)</option>
											<option data-countryCode="TR" value="90">Turkey (+90)</option>
											<option data-countryCode="TM" value="7">Turkmenistan (+7)</option>
											<option data-countryCode="TM" value="993">Turkmenistan (+993)</option>
											<option data-countryCode="TC" value="1649">Turks &amp; Caicos Islands (+1649)</option>
											<option data-countryCode="TV" value="688">Tuvalu (+688)</option>
											<option data-countryCode="UG" value="256">Uganda (+256)</option>
											<option data-countryCode="GB" value="44">UK (+44)</option>
											<option data-countryCode="UA" value="380">Ukraine (+380)</option>
											<option data-countryCode="AE" value="971">United Arab Emirates (+971)</option>
											<option data-countryCode="UY" value="598">Uruguay (+598)</option>
											<option data-countryCode="US" value="1">USA (+1)</option>
											<option data-countryCode="UZ" value="7">Uzbekistan (+7)</option>
											<option data-countryCode="VU" value="678">Vanuatu (+678)</option>
											<option data-countryCode="VA" value="379">Vatican City (+379)</option>
											<option data-countryCode="VE" value="58">Venezuela (+58)</option>
											<option data-countryCode="VN" value="84">Vietnam (+84)</option>
											<option data-countryCode="VG" value="84">Virgin Islands - British (+1284)</option>
											<option data-countryCode="VI" value="84">Virgin Islands - US (+1340)</option>
											<option data-countryCode="WF" value="681">Wallis &amp; Futuna (+681)</option>
											<option data-countryCode="YE" value="969">Yemen (North)(+969)</option>
											<option data-countryCode="YE" value="967">Yemen (South)(+967)</option>
											<option data-countryCode="ZM" value="260">Zambia (+260)</option>
											<option data-countryCode="ZW" value="263">Zimbabwe (+263)</option>
									</select>
									<input type="text" name="contact1" style="width:56%;display:inline" placeholder="Contact Number 1" required>
									<div class="help-block with-errors"></div>
								</div>
								<div class="form-group">
									<select name="c_code2" style="width:43%;display:inline;font-size: 11px;">
											<option data-countryCode="DZ" value="213">Algeria (+213)</option>
											<option data-countryCode="AD" value="376">Andorra (+376)</option>
											<option data-countryCode="AO" value="244">Angola (+244)</option>
											<option data-countryCode="AI" value="1264">Anguilla (+1264)</option>
											<option data-countryCode="AG" value="1268">Antigua &amp; Barbuda (+1268)</option>
											<option data-countryCode="AR" value="54">Argentina (+54)</option>
											<option data-countryCode="AM" value="374">Armenia (+374)</option>
											<option data-countryCode="AW" value="297">Aruba (+297)</option>
											<option data-countryCode="AU" value="61">Australia (+61)</option>
											<option data-countryCode="AT" value="43">Austria (+43)</option>
											<option data-countryCode="AZ" value="994">Azerbaijan (+994)</option>
											<option data-countryCode="BS" value="1242">Bahamas (+1242)</option>
											<option data-countryCode="BH" value="973">Bahrain (+973)</option>
											<option data-countryCode="BD" value="880">Bangladesh (+880)</option>
											<option data-countryCode="BB" value="1246">Barbados (+1246)</option>
											<option data-countryCode="BY" value="375">Belarus (+375)</option>
											<option data-countryCode="BE" value="32">Belgium (+32)</option>
											<option data-countryCode="BZ" value="501">Belize (+501)</option>
											<option data-countryCode="BJ" value="229">Benin (+229)</option>
											<option data-countryCode="BM" value="1441">Bermuda (+1441)</option>
											<option data-countryCode="BT" value="975">Bhutan (+975)</option>
											<option data-countryCode="BO" value="591">Bolivia (+591)</option>
											<option data-countryCode="BA" value="387">Bosnia Herzegovina (+387)</option>
											<option data-countryCode="BW" value="267">Botswana (+267)</option>
											<option data-countryCode="BR" value="55">Brazil (+55)</option>
											<option data-countryCode="BN" value="673">Brunei (+673)</option>
											<option data-countryCode="BG" value="359">Bulgaria (+359)</option>
											<option data-countryCode="BF" value="226">Burkina Faso (+226)</option>
											<option data-countryCode="BI" value="257">Burundi (+257)</option>
											<option data-countryCode="KH" value="855">Cambodia (+855)</option>
											<option data-countryCode="CM" value="237">Cameroon (+237)</option>
											<option data-countryCode="CA" value="1">Canada (+1)</option>
											<option data-countryCode="CV" value="238">Cape Verde Islands (+238)</option>
											<option data-countryCode="KY" value="1345">Cayman Islands (+1345)</option>
											<option data-countryCode="CF" value="236">Central African Republic (+236)</option>
											<option data-countryCode="CL" value="56">Chile (+56)</option>
											<option data-countryCode="CN" value="86">China (+86)</option>
											<option data-countryCode="CO" value="57">Colombia (+57)</option>
											<option data-countryCode="KM" value="269">Comoros (+269)</option>
											<option data-countryCode="CG" value="242">Congo (+242)</option>
											<option data-countryCode="CK" value="682">Cook Islands (+682)</option>
											<option data-countryCode="CR" value="506">Costa Rica (+506)</option>
											<option data-countryCode="HR" value="385">Croatia (+385)</option>
											<option data-countryCode="CU" value="53">Cuba (+53)</option>
											<option data-countryCode="CY" value="90392">Cyprus North (+90392)</option>
											<option data-countryCode="CY" value="357">Cyprus South (+357)</option>
											<option data-countryCode="CZ" value="42">Czech Republic (+42)</option>
											<option data-countryCode="DK" value="45">Denmark (+45)</option>
											<option data-countryCode="DJ" value="253">Djibouti (+253)</option>
											<option data-countryCode="DM" value="1809">Dominica (+1809)</option>
											<option data-countryCode="DO" value="1809">Dominican Republic (+1809)</option>
											<option data-countryCode="EC" value="593">Ecuador (+593)</option>
											<option data-countryCode="EG" value="20">Egypt (+20)</option>
											<option data-countryCode="SV" value="503">El Salvador (+503)</option>
											<option data-countryCode="GQ" value="240">Equatorial Guinea (+240)</option>
											<option data-countryCode="ER" value="291">Eritrea (+291)</option>
											<option data-countryCode="EE" value="372">Estonia (+372)</option>
											<option data-countryCode="ET" value="251">Ethiopia (+251)</option>
											<option data-countryCode="FK" value="500">Falkland Islands (+500)</option>
											<option data-countryCode="FO" value="298">Faroe Islands (+298)</option>
											<option data-countryCode="FJ" value="679">Fiji (+679)</option>
											<option data-countryCode="FI" value="358">Finland (+358)</option>
											<option data-countryCode="FR" value="33">France (+33)</option>
											<option data-countryCode="GF" value="594">French Guiana (+594)</option>
											<option data-countryCode="PF" value="689">French Polynesia (+689)</option>
											<option data-countryCode="GA" value="241">Gabon (+241)</option>
											<option data-countryCode="GM" value="220">Gambia (+220)</option>
											<option data-countryCode="GE" value="7880">Georgia (+7880)</option>
											<option data-countryCode="DE" value="49">Germany (+49)</option>
											<option data-countryCode="GH" value="233">Ghana (+233)</option>
											<option data-countryCode="GI" value="350">Gibraltar (+350)</option>
											<option data-countryCode="GR" value="30">Greece (+30)</option>
											<option data-countryCode="GL" value="299">Greenland (+299)</option>
											<option data-countryCode="GD" value="1473">Grenada (+1473)</option>
											<option data-countryCode="GP" value="590">Guadeloupe (+590)</option>
											<option data-countryCode="GU" value="671">Guam (+671)</option>
											<option data-countryCode="GT" value="502">Guatemala (+502)</option>
											<option data-countryCode="GN" value="224">Guinea (+224)</option>
											<option data-countryCode="GW" value="245">Guinea - Bissau (+245)</option>
											<option data-countryCode="GY" value="592">Guyana (+592)</option>
											<option data-countryCode="HT" value="509">Haiti (+509)</option>
											<option data-countryCode="HN" value="504">Honduras (+504)</option>
											<option data-countryCode="HK" value="852">Hong Kong (+852)</option>
											<option data-countryCode="HU" value="36">Hungary (+36)</option>
											<option data-countryCode="IS" value="354">Iceland (+354)</option>
											<option data-countryCode="IN" value="91">India (+91)</option>
											<option data-countryCode="ID" value="62">Indonesia (+62)</option>
											<option data-countryCode="IR" value="98">Iran (+98)</option>
											<option data-countryCode="IQ" value="964">Iraq (+964)</option>
											<option data-countryCode="IE" value="353">Ireland (+353)</option>
											<option data-countryCode="IL" value="972">Israel (+972)</option>
											<option data-countryCode="IT" value="39">Italy (+39)</option>
											<option data-countryCode="JM" value="1876">Jamaica (+1876)</option>
											<option data-countryCode="JP" value="81">Japan (+81)</option>
											<option data-countryCode="JO" value="962">Jordan (+962)</option>
											<option data-countryCode="KZ" value="7">Kazakhstan (+7)</option>
											<option data-countryCode="KE" value="254">Kenya (+254)</option>
											<option data-countryCode="KI" value="686">Kiribati (+686)</option>
											<option data-countryCode="KP" value="850">Korea North (+850)</option>
											<option data-countryCode="KR" value="82">Korea South (+82)</option>
											<option data-countryCode="KW" value="965">Kuwait (+965)</option>
											<option data-countryCode="KG" value="996">Kyrgyzstan (+996)</option>
											<option data-countryCode="LA" value="856">Laos (+856)</option>
											<option data-countryCode="LV" value="371">Latvia (+371)</option>
											<option data-countryCode="LB" value="961">Lebanon (+961)</option>
											<option data-countryCode="LS" value="266">Lesotho (+266)</option>
											<option data-countryCode="LR" value="231">Liberia (+231)</option>
											<option data-countryCode="LY" value="218">Libya (+218)</option>
											<option data-countryCode="LI" value="417">Liechtenstein (+417)</option>
											<option data-countryCode="LT" value="370">Lithuania (+370)</option>
											<option data-countryCode="LU" value="352">Luxembourg (+352)</option>
											<option data-countryCode="MO" value="853">Macao (+853)</option>
											<option data-countryCode="MK" value="389">Macedonia (+389)</option>
											<option data-countryCode="MG" value="261">Madagascar (+261)</option>
											<option data-countryCode="MW" value="265">Malawi (+265)</option>
											<option data-countryCode="MY" value="60">Malaysia (+60)</option>
											<option data-countryCode="MV" value="960">Maldives (+960)</option>
											<option data-countryCode="ML" value="223">Mali (+223)</option>
											<option data-countryCode="MT" value="356">Malta (+356)</option>
											<option data-countryCode="MH" value="692">Marshall Islands (+692)</option>
											<option data-countryCode="MQ" value="596">Martinique (+596)</option>
											<option data-countryCode="MR" value="222">Mauritania (+222)</option>
											<option data-countryCode="YT" value="269">Mayotte (+269)</option>
											<option data-countryCode="MX" value="52">Mexico (+52)</option>
											<option data-countryCode="FM" value="691">Micronesia (+691)</option>
											<option data-countryCode="MD" value="373">Moldova (+373)</option>
											<option data-countryCode="MC" value="377">Monaco (+377)</option>
											<option data-countryCode="MN" value="976">Mongolia (+976)</option>
											<option data-countryCode="MS" value="1664">Montserrat (+1664)</option>
											<option data-countryCode="MA" value="212">Morocco (+212)</option>
											<option data-countryCode="MZ" value="258">Mozambique (+258)</option>
											<option data-countryCode="MN" value="95">Myanmar (+95)</option>
											<option data-countryCode="NA" value="264">Namibia (+264)</option>
											<option data-countryCode="NR" value="674">Nauru (+674)</option>
											<option data-countryCode="NP" value="977">Nepal (+977)</option>
											<option data-countryCode="NL" value="31">Netherlands (+31)</option>
											<option data-countryCode="NC" value="687">New Caledonia (+687)</option>
											<option data-countryCode="NZ" value="64">New Zealand (+64)</option>
											<option data-countryCode="NI" value="505">Nicaragua (+505)</option>
											<option data-countryCode="NE" value="227">Niger (+227)</option>
											<option data-countryCode="NG" value="234">Nigeria (+234)</option>
											<option data-countryCode="NU" value="683">Niue (+683)</option>
											<option data-countryCode="NF" value="672">Norfolk Islands (+672)</option>
											<option data-countryCode="NP" value="670">Northern Marianas (+670)</option>
											<option data-countryCode="NO" value="47">Norway (+47)</option>
											<option data-countryCode="OM" value="968">Oman (+968)</option>
											<option data-countryCode="PW" value="680">Palau (+680)</option>
											<option data-countryCode="PA" value="507">Panama (+507)</option>
											<option data-countryCode="PG" value="675">Papua New Guinea (+675)</option>
											<option data-countryCode="PY" value="595">Paraguay (+595)</option>
											<option data-countryCode="PE" value="51">Peru (+51)</option>
											<option data-countryCode="PH" value="63">Philippines (+63)</option>
											<option data-countryCode="PL" value="48">Poland (+48)</option>
											<option data-countryCode="PT" value="351">Portugal (+351)</option>
											<option data-countryCode="PR" value="1787">Puerto Rico (+1787)</option>
											<option data-countryCode="QA" value="974">Qatar (+974)</option>
											<option data-countryCode="RE" value="262">Reunion (+262)</option>
											<option data-countryCode="RO" value="40">Romania (+40)</option>
											<option data-countryCode="RU" value="7">Russia (+7)</option>
											<option data-countryCode="RW" value="250">Rwanda (+250)</option>
											<option data-countryCode="SM" value="378">San Marino (+378)</option>
											<option data-countryCode="ST" value="239">Sao Tome &amp; Principe (+239)</option>
											<option data-countryCode="SA" value="966">Saudi Arabia (+966)</option>
											<option data-countryCode="SN" value="221">Senegal (+221)</option>
											<option data-countryCode="CS" value="381">Serbia (+381)</option>
											<option data-countryCode="SC" value="248">Seychelles (+248)</option>
											<option data-countryCode="SL" value="232">Sierra Leone (+232)</option>
											<option data-countryCode="SG" value="65">Singapore (+65)</option>
											<option data-countryCode="SK" value="421">Slovak Republic (+421)</option>
											<option data-countryCode="SI" value="386">Slovenia (+386)</option>
											<option data-countryCode="SB" value="677">Solomon Islands (+677)</option>
											<option data-countryCode="SO" value="252">Somalia (+252)</option>
											<option data-countryCode="ZA" value="27">South Africa (+27)</option>
											<option data-countryCode="ES" value="34">Spain (+34)</option>
											<option data-countryCode="LK" value="94">Sri Lanka (+94)</option>
											<option data-countryCode="SH" value="290">St. Helena (+290)</option>
											<option data-countryCode="KN" value="1869">St. Kitts (+1869)</option>
											<option data-countryCode="SC" value="1758">St. Lucia (+1758)</option>
											<option data-countryCode="SD" value="249">Sudan (+249)</option>
											<option data-countryCode="SR" value="597">Suriname (+597)</option>
											<option data-countryCode="SZ" value="268">Swaziland (+268)</option>
											<option data-countryCode="SE" value="46">Sweden (+46)</option>
											<option data-countryCode="CH" value="41">Switzerland (+41)</option>
											<option data-countryCode="SI" value="963">Syria (+963)</option>
											<option data-countryCode="TW" value="886">Taiwan (+886)</option>
											<option data-countryCode="TJ" value="7">Tajikstan (+7)</option>
											<option data-countryCode="TH" value="66">Thailand (+66)</option>
											<option data-countryCode="TG" value="228">Togo (+228)</option>
											<option data-countryCode="TO" value="676">Tonga (+676)</option>
											<option data-countryCode="TT" value="1868">Trinidad &amp; Tobago (+1868)</option>
											<option data-countryCode="TN" value="216">Tunisia (+216)</option>
											<option data-countryCode="TR" value="90">Turkey (+90)</option>
											<option data-countryCode="TM" value="7">Turkmenistan (+7)</option>
											<option data-countryCode="TM" value="993">Turkmenistan (+993)</option>
											<option data-countryCode="TC" value="1649">Turks &amp; Caicos Islands (+1649)</option>
											<option data-countryCode="TV" value="688">Tuvalu (+688)</option>
											<option data-countryCode="UG" value="256">Uganda (+256)</option>
											<option data-countryCode="GB" value="44">UK (+44)</option>
											<option data-countryCode="UA" value="380">Ukraine (+380)</option>
											<option data-countryCode="AE" value="971">United Arab Emirates (+971)</option>
											<option data-countryCode="UY" value="598">Uruguay (+598)</option>
											<option data-countryCode="US" value="1">USA (+1)</option>
											<option data-countryCode="UZ" value="7">Uzbekistan (+7)</option>
											<option data-countryCode="VU" value="678">Vanuatu (+678)</option>
											<option data-countryCode="VA" value="379">Vatican City (+379)</option>
											<option data-countryCode="VE" value="58">Venezuela (+58)</option>
											<option data-countryCode="VN" value="84">Vietnam (+84)</option>
											<option data-countryCode="VG" value="84">Virgin Islands - British (+1284)</option>
											<option data-countryCode="VI" value="84">Virgin Islands - US (+1340)</option>
											<option data-countryCode="WF" value="681">Wallis &amp; Futuna (+681)</option>
											<option data-countryCode="YE" value="969">Yemen (North)(+969)</option>
											<option data-countryCode="YE" value="967">Yemen (South)(+967)</option>
											<option data-countryCode="ZM" value="260">Zambia (+260)</option>
											<option data-countryCode="ZW" value="263">Zimbabwe (+263)</option>
									</select>
									<input type="text" name="contact2" style="width:56%;display:inline" placeholder="Contact Number 2" required>
									<div class="help-block with-errors"></div>
								</div>
								<div class="form-group">
									<select name="c_code3" style="width:43%;display:inline;font-size: 11px;">
											<option data-countryCode="DZ" value="213">Algeria (+213)</option>
											<option data-countryCode="AD" value="376">Andorra (+376)</option>
											<option data-countryCode="AO" value="244">Angola (+244)</option>
											<option data-countryCode="AI" value="1264">Anguilla (+1264)</option>
											<option data-countryCode="AG" value="1268">Antigua &amp; Barbuda (+1268)</option>
											<option data-countryCode="AR" value="54">Argentina (+54)</option>
											<option data-countryCode="AM" value="374">Armenia (+374)</option>
											<option data-countryCode="AW" value="297">Aruba (+297)</option>
											<option data-countryCode="AU" value="61">Australia (+61)</option>
											<option data-countryCode="AT" value="43">Austria (+43)</option>
											<option data-countryCode="AZ" value="994">Azerbaijan (+994)</option>
											<option data-countryCode="BS" value="1242">Bahamas (+1242)</option>
											<option data-countryCode="BH" value="973">Bahrain (+973)</option>
											<option data-countryCode="BD" value="880">Bangladesh (+880)</option>
											<option data-countryCode="BB" value="1246">Barbados (+1246)</option>
											<option data-countryCode="BY" value="375">Belarus (+375)</option>
											<option data-countryCode="BE" value="32">Belgium (+32)</option>
											<option data-countryCode="BZ" value="501">Belize (+501)</option>
											<option data-countryCode="BJ" value="229">Benin (+229)</option>
											<option data-countryCode="BM" value="1441">Bermuda (+1441)</option>
											<option data-countryCode="BT" value="975">Bhutan (+975)</option>
											<option data-countryCode="BO" value="591">Bolivia (+591)</option>
											<option data-countryCode="BA" value="387">Bosnia Herzegovina (+387)</option>
											<option data-countryCode="BW" value="267">Botswana (+267)</option>
											<option data-countryCode="BR" value="55">Brazil (+55)</option>
											<option data-countryCode="BN" value="673">Brunei (+673)</option>
											<option data-countryCode="BG" value="359">Bulgaria (+359)</option>
											<option data-countryCode="BF" value="226">Burkina Faso (+226)</option>
											<option data-countryCode="BI" value="257">Burundi (+257)</option>
											<option data-countryCode="KH" value="855">Cambodia (+855)</option>
											<option data-countryCode="CM" value="237">Cameroon (+237)</option>
											<option data-countryCode="CA" value="1">Canada (+1)</option>
											<option data-countryCode="CV" value="238">Cape Verde Islands (+238)</option>
											<option data-countryCode="KY" value="1345">Cayman Islands (+1345)</option>
											<option data-countryCode="CF" value="236">Central African Republic (+236)</option>
											<option data-countryCode="CL" value="56">Chile (+56)</option>
											<option data-countryCode="CN" value="86">China (+86)</option>
											<option data-countryCode="CO" value="57">Colombia (+57)</option>
											<option data-countryCode="KM" value="269">Comoros (+269)</option>
											<option data-countryCode="CG" value="242">Congo (+242)</option>
											<option data-countryCode="CK" value="682">Cook Islands (+682)</option>
											<option data-countryCode="CR" value="506">Costa Rica (+506)</option>
											<option data-countryCode="HR" value="385">Croatia (+385)</option>
											<option data-countryCode="CU" value="53">Cuba (+53)</option>
											<option data-countryCode="CY" value="90392">Cyprus North (+90392)</option>
											<option data-countryCode="CY" value="357">Cyprus South (+357)</option>
											<option data-countryCode="CZ" value="42">Czech Republic (+42)</option>
											<option data-countryCode="DK" value="45">Denmark (+45)</option>
											<option data-countryCode="DJ" value="253">Djibouti (+253)</option>
											<option data-countryCode="DM" value="1809">Dominica (+1809)</option>
											<option data-countryCode="DO" value="1809">Dominican Republic (+1809)</option>
											<option data-countryCode="EC" value="593">Ecuador (+593)</option>
											<option data-countryCode="EG" value="20">Egypt (+20)</option>
											<option data-countryCode="SV" value="503">El Salvador (+503)</option>
											<option data-countryCode="GQ" value="240">Equatorial Guinea (+240)</option>
											<option data-countryCode="ER" value="291">Eritrea (+291)</option>
											<option data-countryCode="EE" value="372">Estonia (+372)</option>
											<option data-countryCode="ET" value="251">Ethiopia (+251)</option>
											<option data-countryCode="FK" value="500">Falkland Islands (+500)</option>
											<option data-countryCode="FO" value="298">Faroe Islands (+298)</option>
											<option data-countryCode="FJ" value="679">Fiji (+679)</option>
											<option data-countryCode="FI" value="358">Finland (+358)</option>
											<option data-countryCode="FR" value="33">France (+33)</option>
											<option data-countryCode="GF" value="594">French Guiana (+594)</option>
											<option data-countryCode="PF" value="689">French Polynesia (+689)</option>
											<option data-countryCode="GA" value="241">Gabon (+241)</option>
											<option data-countryCode="GM" value="220">Gambia (+220)</option>
											<option data-countryCode="GE" value="7880">Georgia (+7880)</option>
											<option data-countryCode="DE" value="49">Germany (+49)</option>
											<option data-countryCode="GH" value="233">Ghana (+233)</option>
											<option data-countryCode="GI" value="350">Gibraltar (+350)</option>
											<option data-countryCode="GR" value="30">Greece (+30)</option>
											<option data-countryCode="GL" value="299">Greenland (+299)</option>
											<option data-countryCode="GD" value="1473">Grenada (+1473)</option>
											<option data-countryCode="GP" value="590">Guadeloupe (+590)</option>
											<option data-countryCode="GU" value="671">Guam (+671)</option>
											<option data-countryCode="GT" value="502">Guatemala (+502)</option>
											<option data-countryCode="GN" value="224">Guinea (+224)</option>
											<option data-countryCode="GW" value="245">Guinea - Bissau (+245)</option>
											<option data-countryCode="GY" value="592">Guyana (+592)</option>
											<option data-countryCode="HT" value="509">Haiti (+509)</option>
											<option data-countryCode="HN" value="504">Honduras (+504)</option>
											<option data-countryCode="HK" value="852">Hong Kong (+852)</option>
											<option data-countryCode="HU" value="36">Hungary (+36)</option>
											<option data-countryCode="IS" value="354">Iceland (+354)</option>
											<option data-countryCode="IN" value="91">India (+91)</option>
											<option data-countryCode="ID" value="62">Indonesia (+62)</option>
											<option data-countryCode="IR" value="98">Iran (+98)</option>
											<option data-countryCode="IQ" value="964">Iraq (+964)</option>
											<option data-countryCode="IE" value="353">Ireland (+353)</option>
											<option data-countryCode="IL" value="972">Israel (+972)</option>
											<option data-countryCode="IT" value="39">Italy (+39)</option>
											<option data-countryCode="JM" value="1876">Jamaica (+1876)</option>
											<option data-countryCode="JP" value="81">Japan (+81)</option>
											<option data-countryCode="JO" value="962">Jordan (+962)</option>
											<option data-countryCode="KZ" value="7">Kazakhstan (+7)</option>
											<option data-countryCode="KE" value="254">Kenya (+254)</option>
											<option data-countryCode="KI" value="686">Kiribati (+686)</option>
											<option data-countryCode="KP" value="850">Korea North (+850)</option>
											<option data-countryCode="KR" value="82">Korea South (+82)</option>
											<option data-countryCode="KW" value="965">Kuwait (+965)</option>
											<option data-countryCode="KG" value="996">Kyrgyzstan (+996)</option>
											<option data-countryCode="LA" value="856">Laos (+856)</option>
											<option data-countryCode="LV" value="371">Latvia (+371)</option>
											<option data-countryCode="LB" value="961">Lebanon (+961)</option>
											<option data-countryCode="LS" value="266">Lesotho (+266)</option>
											<option data-countryCode="LR" value="231">Liberia (+231)</option>
											<option data-countryCode="LY" value="218">Libya (+218)</option>
											<option data-countryCode="LI" value="417">Liechtenstein (+417)</option>
											<option data-countryCode="LT" value="370">Lithuania (+370)</option>
											<option data-countryCode="LU" value="352">Luxembourg (+352)</option>
											<option data-countryCode="MO" value="853">Macao (+853)</option>
											<option data-countryCode="MK" value="389">Macedonia (+389)</option>
											<option data-countryCode="MG" value="261">Madagascar (+261)</option>
											<option data-countryCode="MW" value="265">Malawi (+265)</option>
											<option data-countryCode="MY" value="60">Malaysia (+60)</option>
											<option data-countryCode="MV" value="960">Maldives (+960)</option>
											<option data-countryCode="ML" value="223">Mali (+223)</option>
											<option data-countryCode="MT" value="356">Malta (+356)</option>
											<option data-countryCode="MH" value="692">Marshall Islands (+692)</option>
											<option data-countryCode="MQ" value="596">Martinique (+596)</option>
											<option data-countryCode="MR" value="222">Mauritania (+222)</option>
											<option data-countryCode="YT" value="269">Mayotte (+269)</option>
											<option data-countryCode="MX" value="52">Mexico (+52)</option>
											<option data-countryCode="FM" value="691">Micronesia (+691)</option>
											<option data-countryCode="MD" value="373">Moldova (+373)</option>
											<option data-countryCode="MC" value="377">Monaco (+377)</option>
											<option data-countryCode="MN" value="976">Mongolia (+976)</option>
											<option data-countryCode="MS" value="1664">Montserrat (+1664)</option>
											<option data-countryCode="MA" value="212">Morocco (+212)</option>
											<option data-countryCode="MZ" value="258">Mozambique (+258)</option>
											<option data-countryCode="MN" value="95">Myanmar (+95)</option>
											<option data-countryCode="NA" value="264">Namibia (+264)</option>
											<option data-countryCode="NR" value="674">Nauru (+674)</option>
											<option data-countryCode="NP" value="977">Nepal (+977)</option>
											<option data-countryCode="NL" value="31">Netherlands (+31)</option>
											<option data-countryCode="NC" value="687">New Caledonia (+687)</option>
											<option data-countryCode="NZ" value="64">New Zealand (+64)</option>
											<option data-countryCode="NI" value="505">Nicaragua (+505)</option>
											<option data-countryCode="NE" value="227">Niger (+227)</option>
											<option data-countryCode="NG" value="234">Nigeria (+234)</option>
											<option data-countryCode="NU" value="683">Niue (+683)</option>
											<option data-countryCode="NF" value="672">Norfolk Islands (+672)</option>
											<option data-countryCode="NP" value="670">Northern Marianas (+670)</option>
											<option data-countryCode="NO" value="47">Norway (+47)</option>
											<option data-countryCode="OM" value="968">Oman (+968)</option>
											<option data-countryCode="PW" value="680">Palau (+680)</option>
											<option data-countryCode="PA" value="507">Panama (+507)</option>
											<option data-countryCode="PG" value="675">Papua New Guinea (+675)</option>
											<option data-countryCode="PY" value="595">Paraguay (+595)</option>
											<option data-countryCode="PE" value="51">Peru (+51)</option>
											<option data-countryCode="PH" value="63">Philippines (+63)</option>
											<option data-countryCode="PL" value="48">Poland (+48)</option>
											<option data-countryCode="PT" value="351">Portugal (+351)</option>
											<option data-countryCode="PR" value="1787">Puerto Rico (+1787)</option>
											<option data-countryCode="QA" value="974">Qatar (+974)</option>
											<option data-countryCode="RE" value="262">Reunion (+262)</option>
											<option data-countryCode="RO" value="40">Romania (+40)</option>
											<option data-countryCode="RU" value="7">Russia (+7)</option>
											<option data-countryCode="RW" value="250">Rwanda (+250)</option>
											<option data-countryCode="SM" value="378">San Marino (+378)</option>
											<option data-countryCode="ST" value="239">Sao Tome &amp; Principe (+239)</option>
											<option data-countryCode="SA" value="966">Saudi Arabia (+966)</option>
											<option data-countryCode="SN" value="221">Senegal (+221)</option>
											<option data-countryCode="CS" value="381">Serbia (+381)</option>
											<option data-countryCode="SC" value="248">Seychelles (+248)</option>
											<option data-countryCode="SL" value="232">Sierra Leone (+232)</option>
											<option data-countryCode="SG" value="65">Singapore (+65)</option>
											<option data-countryCode="SK" value="421">Slovak Republic (+421)</option>
											<option data-countryCode="SI" value="386">Slovenia (+386)</option>
											<option data-countryCode="SB" value="677">Solomon Islands (+677)</option>
											<option data-countryCode="SO" value="252">Somalia (+252)</option>
											<option data-countryCode="ZA" value="27">South Africa (+27)</option>
											<option data-countryCode="ES" value="34">Spain (+34)</option>
											<option data-countryCode="LK" value="94">Sri Lanka (+94)</option>
											<option data-countryCode="SH" value="290">St. Helena (+290)</option>
											<option data-countryCode="KN" value="1869">St. Kitts (+1869)</option>
											<option data-countryCode="SC" value="1758">St. Lucia (+1758)</option>
											<option data-countryCode="SD" value="249">Sudan (+249)</option>
											<option data-countryCode="SR" value="597">Suriname (+597)</option>
											<option data-countryCode="SZ" value="268">Swaziland (+268)</option>
											<option data-countryCode="SE" value="46">Sweden (+46)</option>
											<option data-countryCode="CH" value="41">Switzerland (+41)</option>
											<option data-countryCode="SI" value="963">Syria (+963)</option>
											<option data-countryCode="TW" value="886">Taiwan (+886)</option>
											<option data-countryCode="TJ" value="7">Tajikstan (+7)</option>
											<option data-countryCode="TH" value="66">Thailand (+66)</option>
											<option data-countryCode="TG" value="228">Togo (+228)</option>
											<option data-countryCode="TO" value="676">Tonga (+676)</option>
											<option data-countryCode="TT" value="1868">Trinidad &amp; Tobago (+1868)</option>
											<option data-countryCode="TN" value="216">Tunisia (+216)</option>
											<option data-countryCode="TR" value="90">Turkey (+90)</option>
											<option data-countryCode="TM" value="7">Turkmenistan (+7)</option>
											<option data-countryCode="TM" value="993">Turkmenistan (+993)</option>
											<option data-countryCode="TC" value="1649">Turks &amp; Caicos Islands (+1649)</option>
											<option data-countryCode="TV" value="688">Tuvalu (+688)</option>
											<option data-countryCode="UG" value="256">Uganda (+256)</option>
											<option data-countryCode="GB" value="44">UK (+44)</option>
											<option data-countryCode="UA" value="380">Ukraine (+380)</option>
											<option data-countryCode="AE" value="971">United Arab Emirates (+971)</option>
											<option data-countryCode="UY" value="598">Uruguay (+598)</option>
											<option data-countryCode="US" value="1">USA (+1)</option>
											<option data-countryCode="UZ" value="7">Uzbekistan (+7)</option>
											<option data-countryCode="VU" value="678">Vanuatu (+678)</option>
											<option data-countryCode="VA" value="379">Vatican City (+379)</option>
											<option data-countryCode="VE" value="58">Venezuela (+58)</option>
											<option data-countryCode="VN" value="84">Vietnam (+84)</option>
											<option data-countryCode="VG" value="84">Virgin Islands - British (+1284)</option>
											<option data-countryCode="VI" value="84">Virgin Islands - US (+1340)</option>
											<option data-countryCode="WF" value="681">Wallis &amp; Futuna (+681)</option>
											<option data-countryCode="YE" value="969">Yemen (North)(+969)</option>
											<option data-countryCode="YE" value="967">Yemen (South)(+967)</option>
											<option data-countryCode="ZM" value="260">Zambia (+260)</option>
											<option data-countryCode="ZW" value="263">Zimbabwe (+263)</option>
									</select>
									<input type="text" name="contact3" style="width:56%;display:inline" placeholder="Contact Number 3" required>
									<div class="help-block with-errors"></div>
								</div>
								<div class="form-group">
									<input type="text" name="country" placeholder="Country" required>
									<div class="help-block with-errors"></div>
								</div>
								<div class="form-group">
									<input type="text" name="city" placeholder="City/State" required>
									<div class="help-block with-errors"></div>
								</div>
								<div class="form-group">
									<input type="text" name="zip" placeholder="Post/Zip Code" required>
									<div class="help-block with-errors"></div>
								</div>
								<div class="form-group">
									<input type="email" name="email" id="add_email" placeholder="Email Address" required>
									<div class="help-block with-errors"></div>
								</div>
								<div class="form-group">
									<input type="password" data-minlength="6" class="password" name="password" id="add_pass" placeholder="Password" required>
									<div class="help-block with-errors"></div>
								</div>
								<div class="form-group">
									<input type="password" data-minlength="6" class="password" name="password2" id="password2" placeholder="Confirm Password" required>
									<div class="help-block with-errors"></div>
								</div>
								<div class="form-group">
									<label for="ad_role">Membership ($40)</label>
									<select name="ad_role" id="ad_role">
										<option value="1">Basic</option>
										<option value="2">Standard</option>
									</select>
								</div>
								<input type="submit" value="Sign Up">
							</form>
						
						</div>
						<p>By clicking register, I agree to your terms</p>
					</div>
				</div>
			</div>
		</div>
	</div>
	
	<!-- //Modal2 -->
	<div class="modal fade" id="invoiceModal" tabindex="-1" role="dialog">
		<div class="modal-dialog modal-lg">
			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>

					<div class="signin-form profile">
						<h3 class="agileinfo_sign">Billing/Delivery Information</h3>
						<div class="login-form">
							<form action="<?php echo base_url();?>shop/checkout"  method="post" data-toggle="validator" role="form">
								<div class="row">
									<div class="col-md-6 contact-left">
										<div class="form-group">
											<label for="inv_name">Full Name</label>
											<input type="text" name="inv_name" id="inv_name" required="">
											<input type="hidden" name="arrData[]" id="arrData">
											<input type="hidden" name="shippingFee" id="shippingFee">
											<input type="hidden" name="TotalFee" id="TotalFee">
											<div class="help-block with-errors"></div>
										</div>
										<div class="form-group">
											<label for="inv_country">Country</label>
											<input type="text" name="inv_country" id="inv_country" required="">
											<div class="help-block with-errors"></div>
										</div>
										<div class="form-group">
											<label for="inv_address">Home Address</label>
											<input type="text" name="inv_address" id="inv_address" required="">
											<div class="help-block with-errors"></div>
										</div>
										<p class="payDet">Shipping Fee($): <span id="shipPayment" style="color: #d93025;"></span></p>
										<p class="payDet">Total Payment($): <span id="totPayment" style="color: #d93025;"></span></p>
									</div>
									<div class="col-md-6 contact-right">
										<div class="form-group">
											<label for="inv_email">Email Address</label>
											<input type="text" name="inv_email" id="inv_email" required="">
											<div class="help-block with-errors"></div>
										</div>
										<div class="form-group">
											<label for="inv_contact">Contact Number 1</label>
											<input type="text" name="inv_contact" id="inv_contact" required="">
											<div class="help-block with-errors"></div>
										</div>
										<div class="form-group">
											<label for="inv_contact2">Contact Number 2</label>
											<input type="text" name="inv_contact2" id="inv_contact2" required="">
											<div class="help-block with-errors"></div>
										</div>
									</div>
								</div>
								<div class="tp">
									<input type="submit" value="PAY NOW" style="width:50%">
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- contact -->
	<div class="contact">
		<div class="container">
			<h3 class="agileits-title">Missing Persons</h3>
			<input type="hidden" id="baseurl" value="<?php echo base_url();?>">
			<input type="hidden" id="usr_id" value="<?php echo $this->session->userdata('u_id');?>">
			
			<div class="contact-agileinfo" style="margin-top:0;">
				<table id="missingPersonTable" class="table table-striped table-bordered" style="width:100%;display:none">
					<thead>
						<tr>
							<th scope="col text-center" style="text-align:center">Missing Person ID</th>
							<th scope="col text-center" style="text-align:center">Members Number</th>
							<th scope="col text-center">Missing Name</th>
							<th scope="col text-center" style="text-align:center">Picture</th>
							<th scope="col text-center">Gender</th>
							<th scope="col text-center">Hair Color</th>
							<th scope="col text-center">Eye Color</th>
							<th scope="col text-center">Last Seen</th>
							<th scope="col text-center" style="text-align:center">View</th>
						</tr>
					</thead>
					<tbody id="tbodymissingperson">
														
					</tbody>
				</table>
				<div style="display:none" id="nomissingperson" class="alert alert-info" role="alert">
					<h4>No data to be displayed.</h4>
				</div>
				<div class="clearfix"></div>
			</div>
		</div>
	</div>
	<!-- //contact -->
	<!-- w3-agilesale -->
	<div class="w3-agilesale welcome">
		<div class="container">
			<h3 class="agileits-title two">Lost pets. Found pets.<br><span>Pet Shop</span></h3>
			<a href="<?php echo base_url();?>contact" class="button button-isi"><span>Contact Us </span><i class="icon glyphicon glyphicon-arrow-right"></i></a>
		</div>
	</div>
	<!-- //w3-agilesale -->
	<!-- copy rights start here -->
	<div class="copy-w3right">
		<div class="container">
			<div class="top-nav bottom-w3lnav">
				<ul>
					<li><a href="<?php echo base_url();?>home">Home</a></li>
					<li><a href="<?php echo base_url();?>about">About Us</a></li>
					<li><a href="<?php echo base_url();?>lost">Lost</a></li>
					<li><a href="<?php echo base_url();?>found">Found</a></li>
					<li><a href="<?php echo base_url();?>shop">Shop</a></li>
					<li><a href="<?php echo base_url();?>contact">Contact</a></li>
				</ul>
			</div>
			<p>© 2019 Pet Post. All Rights Reserved | Design by W3layouts | Developed by IPHENCreations </p>
		</div>
	</div>
	<!-- //copy right end here -->
	<!-- password-script -->
	<script type="text/javascript">
		window.onload = function () {
			document.getElementById("password").onchange = validatePassword;
			document.getElementById("password2").onchange = validatePassword;
		}

		function validatePassword() {
			var pass2 = document.getElementById("password2").value;
			var pass1 = document.getElementById("password").value;
			if (pass1 != pass2)
				document.getElementById("password2").setCustomValidity("Passwords Don't Match");
			else
				document.getElementById("password2").setCustomValidity('');
			//empty string means no validation error
		}
	</script>
	<script src="<?php echo base_url();?>js/SmoothScroll.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>js/move-top.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>js/easing.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>js/validator.min.js"></script>
	<script type="text/javascript">
		jQuery(document).ready(function ($) {
			$(".scroll").click(function (event) {
				event.preventDefault();

				$('html,body').animate({
					scrollTop: $(this.hash).offset().top
				}, 1000);
			});
		});
	</script>
	<script type="text/javascript">
		$(document).ready(function () {
			/*
			var defaults = {
				containerID: 'toTop', // fading element id
				containerHoverID: 'toTopHover', // fading element hover id
				scrollSpeed: 1200,
				easingType: 'linear' 
			};
			*/

			$().UItoTop({
				easingType: 'easeOutQuart'
			});

		});
	</script>
	<!-- Placed at the end of the document so the pages load faster -->
	<script src="<?php echo base_url();?>js/bootstrap.js"></script>
	<script src="<?php echo base_url();?>js/jquery.dataTables.min.js"></script>
	<script src="<?php echo base_url();?>js/dataTables.bootstrap4.min.js"></script>
	<script src="<?php echo base_url();?>js/user.js"></script>
	<script src="<?php echo base_url();?>js/cookie.js"></script>
	<script src="<?php echo base_url();?>js/login.js"></script>
	<script src="<?php echo base_url();?>js/missing.js"></script>
</body>

</html>