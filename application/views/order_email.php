<!DOCTYPE html>
<html lang="en">

<head>
    <title>Pet Post</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link href="<?php echo base_url();?>css/bootstrap.css" type="text/css" rel="stylesheet" media="all">
    <link rel="shortcut icon" href="<?php echo base_url();?>images/logo.png" />
    <link href="<?php echo base_url();?>css/font-awesome.css" rel="stylesheet">
    <link href="<?php echo base_url();?>css/style.css" rel="stylesheet">

    <link href="//fonts.googleapis.com/css?family=Limelight" rel="stylesheet">
    <link href='//fonts.googleapis.com/css?family=Roboto:400,100,100italic,300,300italic,400italic,500,500italic,700,700italic,900,900italic' rel='stylesheet' type='text/css'>

</head>

<body>
    <div style="margin:0px;padding:0px;color:rgb(32,32,32);font-size:16px;font-weight:normal;font-family:Helvetica,Arial,sans-serif!important;line-height:150%!important">
        <div style="margin: auto;max-width: 730px;    border: 1px solid #bbb;">
            <table width="100%" border="0" cellpadding="0" cellspacing="0">
                <tbody>
                    <tr>
                        <td colspan="2"></td>
                        <td colspan="3" bgcolor="#E8E8E8" height="1px"></td>
                        <td colspan="3"></td>
                    </tr>
                    <tr>
                        <td>
                            <div>
                                <div>
                                    <table lang="header" cellpadding="0" cellspacing="0" width="100%" border="0" style="width:100%">
                                        <tbody>
                                            <tr>
                                                <td width="100%" height="70" valign="top" bgcolor="#183545" style="background:#333;height:70px">
                                                    <table cellpadding="0" cellspacing="0" width="100%" height="70" border="0" style="width:100%;height:70px">
                                                        <tbody>
                                                            <tr>
                                                                <td style="width:20px" width="20">
                                                                    <div></div>
                                                                </td>
                                                                <td valign="middle" align="left">
                                                                    <div style="font-size:15px;line-height:15px;height:15px">&nbsp; </div>
                                                                    <a href="<?php echo base_url(); ?>" style="text-decoration:none" target="_blank">
                            <img border="0" alt="" src="<?php echo base_url(); ?>images/petpostlogo.png" width="100%" style="display:block;max-width:100px;border:none" class="CToWUd">
                        </a>
                                                                    <div style="font-size:15px;line-height:15px;height:15px">&nbsp; </div>
                                                                </td>
                                                                <td>
                                                                    <h1 style="color:#fff">Pet Post</h1>
                                                                </td>
                                                                <td>
                                                                    &nbsp;
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </td>
                                            </tr>

                                        </tbody>
                                    </table>
                                </div>
                                <div style="font-size: 20px;font-weight: bold;padding-top: 25px;padding-left: 25px;">Your Order is being processed</div>

                            </div>

                            <div>
                                <div>
                                    <p style="padding-top: 25px;padding-left: 25px;color:#d93025"><b>This is a system-generated email. Please do not reply.</b></p>
                                    <h2 style="padding-top: 5px;padding-left: 25px;font-family:Roboto;font-weight:bold">Dear <?php echo $fullname; ?>,</h2>

                                    <div>
                                    </div>
                                    <p style="padding-top: 0;padding-left: 25px;font-family:Roboto;font-weight:bold;color:#000">Your Order <span style="color:#d93025">#<?php echo $invoice_number; ?></span> has been placed on <?php echo $order_date; ?>.</p>

                                </div>
                            </div>
							<p style="padding-top: 15px;padding-left:25px;font-weight:bold;color:#337ab7;font-size: 22px;"> Product Details</p>
							<table class="table table-striped" style="width: 80%;    margin-left: 4%;">
								<thead>
									<th style="padding:5px !important;color:#111;text-align:left">Product ID</th>
									<th style="padding:5px !important;color:#111;text-align:left">Product Name</th>
									<th style="padding:5px !important;color:#111;text-align:left">Qty</th>
								</thead>
								<?php for($x = 0; $x<count($order_data); $x++){ ?>
									<tr style="color:#d93025">
										<td style="padding:5px !important;color:#555"><?php echo $order_data[$x][0]; ?></td>
										<td style="padding:5px !important;color:#555"><?php echo $order_data[$x][1]; ?></td>
										<td style="padding:5px !important;color:#555">1</td>
									</tr>
								<?php } ?>
							</table>
							<table>
								<tr>
									<td>
										<div>
											<div style="padding-top: 25px;padding-left: 25px;font-weight:bold;color:#337ab7;font-size: 22px;"><i></i>Your Package</div>
											<div style="padding-top: 5px;padding-left: 25px;">
												<b>Estimated Delivery Time Frame: <span style="color:#d93025">1 - 2 Days</span></b>
												<br>
											</div>
										</div>
										
										<div  style="padding-top: 5px;padding-left: 25px;">
										<div>
											<p style="padding-top: 5px;font-weight:bold;color:#337ab7;font-size: 22px;"> Order Status</p>
										</div>
										<div><b>Your Order will be Delivered to</b></div>
										<div>
											<div>
												<span><?php echo $fullname; ?></span>
												<br><?php echo $address; ?>, <?php echo $country; ?>
											</div>
											<div>
												Contact Number 1: <?php echo $contact1; ?>
												<br>
												Contact Number 2: <?php echo $contact2; ?>
												<br> Email Address: <?php echo $email; ?>
											</div>
										</div>
									</div>
									</td>
									<td align="right" style="    width: 48.5%;">
										<div>
											<div style="padding-top: 25px;padding-left: 25px;font-weight:bold;color:#337ab7;font-size: 22px;">Order Details</div>
											<div style="padding-top:10px">
												<div>
													<div>
														<table cellpadding="0" cellspacing="0">
															<tbody>
																<tr class="m_-4075023876992090674total" style="color: #d93025;">
																	<td valign="top"><strong>Total:&emsp;&emsp;</strong></td>
																	<td valign="top" align="right">$&nbsp;</td>
																	<td valign="top" align="right"><strong><?php echo $total_payment; ?></strong></td>
																</tr>
															</tbody>
														</table>
														<div>VAT included</div>
													</div>
												</div>
											</div>
										</div>
									<td>
								</tr>
							</table>
					   </td>
                    </tr>
                    <tr>
						<td>
							</br>
							<h4 style="padding-top: 35px;padding-left: 25px;font-family:Roboto;font-size:27px;margin-bottom: 0px;">Happy Shopping!</h4>
							<h2 style="padding-top: 5px;padding-left: 25px;font-family:Roboto;font-size:22px;color:#d93025;margin-bottom: 30px;">Petpost.info</h2>

						</td>
                    </tr>
                </tbody>
            </table>

        </div>
    </div>
    <script src="<?php echo base_url();?>js/jquery-2.2.3.min.js"></script>
    <script src="<?php echo base_url();?>js/bootstrap.js"></script>
</body>

</html>