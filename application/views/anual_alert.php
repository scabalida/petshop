<!DOCTYPE html>
<html lang="en">

<head>
    <title>Pet Post</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link href="<?php echo base_url();?>css/bootstrap.css" type="text/css" rel="stylesheet" media="all">
    <link rel="shortcut icon" href="<?php echo base_url();?>images/logo.png" />
    <link href="<?php echo base_url();?>css/font-awesome.css" rel="stylesheet">
    <link href="<?php echo base_url();?>css/style.css" rel="stylesheet">

    <link href="//fonts.googleapis.com/css?family=Limelight" rel="stylesheet">
    <link href='//fonts.googleapis.com/css?family=Roboto:400,100,100italic,300,300italic,400italic,500,500italic,700,700italic,900,900italic' rel='stylesheet' type='text/css'>

</head>

<body>
    <div style="margin:0px;padding:0px;color:rgb(32,32,32);font-size:16px;font-weight:normal;font-family:Helvetica,Arial,sans-serif!important;line-height:150%!important">
        <div style="margin: auto;max-width: 730px;    border: 1px solid #bbb;">
            <table width="100%" border="0" cellpadding="0" cellspacing="0">
                <tbody>
                    <tr>
                        <td colspan="2"></td>
                        <td colspan="3" bgcolor="#E8E8E8" height="1px"></td>
                        <td colspan="3"></td>
                    </tr>
                    <tr>
                        <td>
                            <div>
                                <div>
                                    <table lang="header" cellpadding="0" cellspacing="0" width="100%" border="0" style="width:100%">
                                        <tbody>
                                            <tr>
                                                <td width="100%" height="70" valign="top" bgcolor="#183545" style="background:#333;height:70px">
                                                    <table cellpadding="0" cellspacing="0" width="100%" height="70" border="0" style="width:100%;height:70px">
                                                        <tbody>
                                                            <tr>
                                                                <td style="width:20px" width="20">
                                                                    <div></div>
                                                                </td>
                                                                <td valign="middle" align="left">
                                                                    <div style="font-size:15px;line-height:15px;height:15px">&nbsp; </div>
                                                                    <a href="<?php echo base_url(); ?>" style="text-decoration:none" target="_blank">
                            <img border="0" alt="" src="<?php echo base_url(); ?>images/petpostlogo.png" width="100%" style="display:block;max-width:100px;border:none" class="CToWUd">
                        </a>
                                                                    <div style="font-size:15px;line-height:15px;height:15px">&nbsp; </div>
                                                                </td>
                                                                <td>
                                                                    <h1 style="color:#fff">Pet Post</h1>
                                                                </td>
                                                                <td>
                                                                    &nbsp;
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </td>
                                            </tr>

                                        </tbody>
                                    </table>
                                </div>
                            </div>

                            <div>
                                <div>
                                    <h2 style="padding-top: 65px;padding-left: 35px;font-family:Roboto;font-weight:bold;padding-bottom:10px;">Good Day!</h2>

                                </div>
                            </div>
							<table>
								<tr>
									<td>

										<div>
											<p style="padding-left:35px;line-height:25px;padding-top: 5px;font-weight:bold;;font-size: 21px;color:#000"> Please renew your account within this month because<br>your account is about to expired.<br>Click the link below to renew membership<br><a href="<?php echo base_url();?>renew" target="_blank"><?php echo base_url();?>renew</a></p>
										</div>

									</td>

								</tr>
							</table>
					   </td>
                    </tr>
                    <tr>
						<td>
							</br>
							<h2 style="padding-bottom: 70px;padding-top: 40px;padding-left: 35px;font-family:Roboto;font-size:22px;color:#d93025;margin-bottom: 30px;">Petpost.info</h2>

						</td>
                    </tr>
                </tbody>
            </table>

        </div>
    </div>
    <script src="<?php echo base_url();?>js/jquery-2.2.3.min.js"></script>
    <script src="<?php echo base_url();?>js/bootstrap.js"></script>
</body>

</html>