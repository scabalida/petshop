<!DOCTYPE html>
<html lang="en">

<head>
	<title>Pet Post</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

	<style>
		body{font-family:Roboto}
		.payDet{
			color: #000;
			font-size: 1.5em;
			font-weight: bold;
			text-align: left;
		}
		#cartTable input[type="text"]{
			text-align:left;
		}
		#cartTable label{float:left}
		#cartTable th{
			color:#d93025;
		}
		#cartTable td{
			color:#333;
		}
		.checkout{
			border: none;
			outline: 2px solid #d93025;
			color: #fff;
			padding: .6em 3em;
			font-size: 1em;
			position: relative;
			margin: 10px auto;
			display: block;
			-webkit-appearance: none;
			background: #d93025;
		}
		.petprofile h3{font-family: 'Open Sans', sans-serif;padding-bottom:15px;font-size:19px;}
		.petprofile span{font-weight:normal}
		.markets-grids{    border: 1px solid #ccc;}
		.about-w3right img {
			width: 90%;
			margin: 0 auto;
			display: block;
			max-height: 410px;
		}
		.comment-section{padding-top:30px;}
		.comment-section .cs{font-size:2em;font-weight:bold;color: #d93025;    border-bottom: 3px solid #bbb}
		#addCommentForm input[type="email"],#addCommentForm input[type="text"],#addCommentForm textarea {
			text-align: left;
		}
		#addCommentForm label {
			float: left;
		}
		#addCommentForm input[type="date"],
		#addCommentForm input[type="time"] {
			padding: 0;
		}
		#addCommentForm input[type="date"],
		#addCommentForm input[type="time"]{
			height: 40px;
		}
		#addCommentForm input[type="date"], #addCommentForm input[type="file"], #addCommentForm input[type="file"], #addCommentForm input[type="time"]{
			width: 100%;
			padding: 1em 1em 1em 1em;
			font-size: 0.9em;
			margin: 0.5em 0 0 0;
			outline: none;
			color: #212121;
			border: 1px solid #ccc;
			letter-spacing: 1px;
			text-align: center;
		}
		
		.comment-s p{
			font-size:1.4em;
			color:#333;
			font-family:Open Sans;
			padding-bottom:8px;
			color: #d93025;
			font-weight:bold
		}
		#commentProfileTable thead th{display:none}
		.text-primary {
    color: #337ab7;
}
	</style>
	</style>
</head>

<body>
		<!-- banner -->
	<h3 style="font-size: 3em;color: #333;text-align: center;letter-spacing: 4px;"><?php echo ($pet[0]->pet_name)." is Lost"; ?></h3>
	<div style="padding-left:30px;padding-right:30px;">
		<table cellpadding="0" cellspacing="0" style="width:100%;border:1px solid #909090;">		
			<tr>
				<td style="width:40%">
					<img style="margin: 0 auto;width: 90%; padding: 25px;max-height: 410px;display: block;" src="<?php echo base_url()."images/uploads/".($pet[0]->user_id)."/".$pet[0]->picture?>" alt="">
				</td>
				<td  style="width:60%">
					<table cellpadding="0" cellspacing="0" style="width:100%;border-left:1px solid #909090;">
						<tr><td colspan='2'><h4 style="font-size: 1.4em;padding-top: 15px;padding-left: 25px;padding-bottom: 10px;background:#d93025;color:#fff;margin: 0;">Pets Information</h4></td></tr>
						<tr>
							<td>
								<div style="padding-left: 30px;">
									<h3><strong class="text-primary">Name: </strong><span style="font-family:Roboto"><?php echo $pet[0]->pet_name; ?></span></h3>
									<h3><strong class="text-primary">Specie: </strong><span style="font-family:Roboto"><?php 
										switch ($pet[0]->specie){
											case 1:
											echo "Dog";
											break;
											case 2:
											echo "Cat";
											break;
											case 3:
											echo "Bird";
											break;
											case 4:
											echo "Snake";
											break;
											case 5:
											echo "Horse";
											break;
											case 6:
											echo "Others";
											break;
										}
									?></span></h3>
									<h3><strong class="text-primary">Birthdate: </strong><span style="font-family:Roboto"><?php echo $pet[0]->birthdate; ?></span></h3>
									<h3><strong class="text-primary">Gender: </strong><span style="font-family:Roboto"><?php if($pet[0]->gender == 1){echo "Female";}else{echo "Male";} ?></span></h3>
									<h3><strong class="text-primary">Neutered: </strong><span style="font-family:Roboto"><?php if($pet[0]->neutered == 1){echo "Yes";}else{echo "No";} ?></span></h3>
									<h3><strong class="text-primary">Tattoo: </strong><span style="font-family:Roboto"><?php echo $pet[0]->tatto; ?></span></h3>
								</div> 
							</td>
							<td>
								<div style="padding-left: 30px;">
									<h3><strong class="text-primary">Fur: </strong><span style="font-family:Roboto"><?php 
										switch ($pet[0]->fur){
												case 1:
												echo "Longhaired";
												break;
												case 2:
												echo "Shorthaired";
												break;
												case 3:
												echo "Hairless";
												break;
										}								
									?></span></h3>
									<h3><strong class="text-primary">Fur Color: </strong><span style="font-family:Roboto"><?php
										switch ($pet[0]->fur_color){
											case 1:
											echo "White";
											break;
											case 2:
											echo "Beige";
											break;
											case 3:
											echo "Yellow";
											break;
											case 4:
											echo "Brown";
											break;
											case 5:
											echo "Black";
											break;
											case 6:
											echo "Grey";
											break;
											case 7:
											echo "Mixed White-Red-Brown";
											break;
											case 8:
											echo "Mixed Black-White";
											break;
											case 9:
											echo "Mixed Black-Brown";
											break;
											case 10:
											echo "Others";
											break;
										}
									?></span></h3>
									<h3><strong class="text-primary">Eye Color: </strong><span style="font-family:Roboto"><?php 
										switch ($pet[0]->eyecolor){
											case 1:
											echo "Blue";
											break;
											case 2:
											echo "Brown";
											break;
											case 3:
											echo "Black";
											break;
											case 4:
											echo "Green";
											break;
											case 5:
											echo "Grey";
											break;
											case 6:
											echo "Others";
											break;
										}
									?></span></h3>
									<h3><strong class="text-primary">Size: </strong><span style="font-family:Roboto"><?php 
										switch ($pet[0]->size){
											case 1:
											echo "Micro";
											break;
											case 2:
											echo "Mini";
											break;
											case 3:
											echo "Small";
											break;
											case 4:
											echo "Medium";
											break;
											case 5:
											echo "Maxi";
											break;
											case 6:
											echo "Big";
											break;
											case 7:
											echo "Giant";
											break;
										}
									?></span></h3>
									<h3><strong class="text-primary">Other Info:</strong></h3>
									<h4 style="font-size:16px;margin-top: -5px;"><span style="font-family:Roboto"><?php echo $pet[0]->comment; ?></span></h4>
								</div>
							</td>
						</tr>
						<tr>
							<td>
								&nbsp;
							</td>
						</tr>	
					</table>
					<table cellpadding="0" cellspacing="0" style="border-left:1px solid #909090;    width: 100%;">
						<tr><td colspan='2'><h4 style="font-size: 1.4em;padding-top: 15px;padding-left: 25px;padding-bottom: 10px;background:#d93025;color:#fff;margin: 0;">Owner's Details</h4></td></tr>
						<tr>
							<td style="width:49%">
								
								<div style="padding-left:30px;padding-top: 15px;">
									<h3><strong class="text-primary">Name: </strong><span style="font-family:Roboto"><?php echo ucwords($this->session->userdata("u_fullname")); ?></span></h3>
									<h3><strong class="text-primary">Address: </strong><span style="font-family:Roboto"><?php echo ucwords($this->session->userdata("u_city")).", ". ucwords($this->session->userdata("u_country"))." ".$this->session->userdata("u_zip"); ?></span></h3>
									<h3><strong class="text-primary">Email Address: </strong><span style="font-family:Roboto"><?php echo $this->session->userdata("u_email"); ?></span></h3>
								</div>
								
							</td>
							<td style="width:51%">
								<div style="padding-top: 15px;">
									<h3><strong class="text-primary">Contact Number 1: </strong><span style="font-family:Roboto"><?php echo $this->session->userdata("u_contact1"); ?></span></h3>
									<h3><strong class="text-primary">Contact Number 2: </strong><span style="font-family:Roboto"><?php echo $this->session->userdata("u_contact2"); ?></span></h3>
									<h3><strong class="text-primary">Contact Number 3: </strong><span style="font-family:Roboto"><?php echo $this->session->userdata("u_contact3"); ?></span></h3>
								</div>
							</td>
						</tr>
						<tr>
							<td>
								&nbsp;
							</td>
						</tr>
					</table>
				</td>
			</tr>
		</table>	
	</div>
	
	</body>

</html>