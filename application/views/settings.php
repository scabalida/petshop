<!DOCTYPE html>
<html lang="en" xmlns="http://www.w3.org/1999/xhtml" xmlns:fb="http://www.facebook.com/2008/fbml">

<head>
	<title>Pet Post</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<script type="application/x-javascript">
		addEventListener("load", function () {
			setTimeout(hideURLbar, 0);
		}, false);

		function hideURLbar() {
			window.scrollTo(0, 1);
		}
	</script>
	<link href="<?php echo base_url();?>css/bootstrap.css" type="text/css" rel="stylesheet" media="all">
	<link rel="shortcut icon" href="<?php echo base_url();?>images/logo.png" />
	<link href="<?php echo base_url();?>css/easy-responsive-tabs.css" rel='stylesheet' type='text/css' />
	<link href="<?php echo base_url();?>css/style.css" type="text/css" rel="stylesheet" media="all">
	<link href="<?php echo base_url();?>css/font-awesome.css" rel="stylesheet">
	<link href="<?php echo base_url();?>css/dataTables.bootstrap4.min.css" rel="stylesheet">
	<script src="<?php echo base_url();?>js/jquery-2.2.3.min.js"></script>

	<link href="//fonts.googleapis.com/css?family=Limelight" rel="stylesheet">
	<link href='//fonts.googleapis.com/css?family=Roboto:400,100,100italic,300,300italic,400italic,500,500italic,700,700italic,900,900italic'
	    rel='stylesheet' type='text/css'>
	<style>
		#v_tag_container,
		#tag_container{
			display: block;
			height: 330px;
			position: relative;
			background-image: url(images/tag_card.png);
			background-position: center;
			background-repeat: no-repeat;
			background-size: 540px;
		}
		#contactCard_container{
			display: block;
			height: 285px;
			width: 450px;
			position: relative;
			background-image: url(images/contact_design.jpg);
			background-repeat: no-repeat;
			background-size: 450px;
			
			margin-left: 40px;
		}
		#v_contactCard_container{
			display: block;
			height: 285px;
			width: 450px;
			position: relative;
			background-image: url(images/contact_design.jpg);
			background-repeat: no-repeat;
			background-size: 450px;
			
			margin-left: 40px;
		}
		#v_show_country,
		#show_country{
			color: #58a6dc;
			font-size: 35px;
			line-height: 28px;
			font-family: Arial, sans-serif;
			font-weight: bold;
			position: absolute;
			top: 66px;
			text-align: center;
			left: 55px;
			width: 282px;
		}
		#c_show_country{
			font-size: 15px;
			line-height: 28px;
			font-family: Arial;
			font-weight: 800;
			position: absolute;
			top: 149px;
			text-align: left;
			left: 207px;
			width: 172px;
			color: #222;
		}
		#v_c_show_country{
			font-size: 15px;
			line-height: 28px;
			font-family: Arial;
			font-weight: 800;
			position: absolute;
			top: 150px;
			text-align: left;
			left: 207px;
			width: 172px;
			color: #222;
		}
		#v_show_contact1,
		#show_contact1{
			font-size: 17px;
			line-height: 28px;
			font-family: Arial;
			font-weight: 800;
			position: absolute;
			top: 120px;
			text-align: left;
			left: 20px;
			width: 172px;
			color: #222;
		}
		#c_show_contact1{
			font-size: 15px;
			line-height: 28px;
			font-family: Arial;
			font-weight: 800;
			position: absolute;
			top: 187px;
			text-align: left;
			left: 205px;
			width: 172px;
			color: #222;
		}
		#v_c_show_contact1{
			font-size: 15px;
			line-height: 28px;
			font-family: Arial;
			font-weight: 800;
			position: absolute;
			top: 187px;
			text-align: left;
			left: 205px;
			width: 172px;
			color: #222;
		}
		#v_show_contact2,
		#show_contact2{
			font-size: 17px;
			line-height: 28px;
			font-family: Arial;
			font-weight: 800;
			position: absolute;
			top: 140px;
			text-align: left;
			left: 20px;
			width: 172px;
			color: #222;
		}
		#c_show_contact2{
			font-size: 15px;
			line-height: 28px;
			font-family: Arial;
			font-weight: 800;
			position: absolute;
			top: 204px;
			text-align: left;
			left: 205px;
			width: 172px;
			color: #222;
		}
		#v_c_show_contact2{
			font-size: 15px;
			line-height: 28px;
			font-family: Arial;
			font-weight: 800;
			position: absolute;
			top: 204px;
			text-align: left;
			left: 205px;
			width: 172px;
			color: #222;
		}
		#v_show_petname,
		#show_petname{
			font-size: 18px;
			line-height: 28px;
			font-family: Arial;
			font-weight: 800;
			position: absolute;
			top: 175px;
			text-align: left;
			left: 22px;
			width: 190px;
			color: #222;
			letter-spacing: -1px;
		}
		#c_show_petname{
			font-size: 17px;
			line-height: 28px;
			font-family: Arial;
			font-weight: 800;
			position: absolute;
			top: 80px;
			text-align: left;
			left: 205px;
			width: 190px;
			color: #222;
			letter-spacing: -1px;
			text-transform: uppercase;
		}
		#v_c_show_petname{
			font-size: 17px;
			line-height: 28px;
			font-family: Arial;
			font-weight: 800;
			position: absolute;
			top: 80px;
			text-align: left;
			left: 205px;
			width: 190px;
			color: #222;
			letter-spacing: -1px;
			text-transform: uppercase;
		}
		#cn_show_petname{
			font-size: 13px;
			line-height: 28px;
			font-family: Arial;
			font-weight: 800;
			position: absolute;
			top: 65px;
			text-align: left;
			left: 205px;
			width: 190px;
			color: #d93025;
			letter-spacing: -1px;
			text-transform: uppercase;
			text-shadow: 1px 1px #000;
		}
		#v_cn_show_petname{
			font-size: 13px;
			line-height: 28px;
			font-family: Arial;
			font-weight: 800;
			position: absolute;
			top: 65px;
			text-align: left;
			left: 205px;
			width: 190px;
			color: #d93025;
			letter-spacing: -1px;
			text-transform: uppercase;
			text-shadow: 1px 1px #000;
		}
		#cn_show_det{
			font-size: 13px;
			line-height: 28px;
			font-family: Arial;
			font-weight: 800;
			position: absolute;
			top: 101px;
			text-align: left;
			left: 208px;
			width: 242px;
			color: #d93025;
			letter-spacing: -1px;
			text-transform: uppercase;
			text-shadow: 1px 1px #000;
		}
		#v_cn_show_det{
			font-size: 13px;
			line-height: 28px;
			font-family: Arial;
			font-weight: 800;
			position: absolute;
			top: 101px;
			text-align: left;
			left: 208px;
			width: 242px;
			color: #d93025;
			letter-spacing: -1px;
			text-transform: uppercase;
			text-shadow: 1px 1px #000;
		}
		#v_show_street,
		#show_street{
			font-size: 15px;
			line-height: 28px;
			font-family: Arial;
			font-weight: 800;
			position: absolute;
			top: 195px;
			text-align: left;
			left: 22px;
			width: 190px;
			color: #222;
			letter-spacing: -1px;
			
		}
		#c_show_street{
			font-size: 15px;
			line-height: 28px;
			font-family: Arial;
			font-weight: 800;
			position: absolute;
			top: 116px;
			text-align: left;
			left: 207px;
			width: 190px;
			color: #222;
			letter-spacing: -1px;
		}
		#v_c_show_street{
			font-size: 15px;
			line-height: 28px;
			font-family: Arial;
			font-weight: 800;
			position: absolute;
			top: 118px;
			text-align: left;
			left: 207px;
			width: 190px;
			color: #222;
			letter-spacing: -1px;
		}
		#v_show_city,
		#show_city{
			font-size: 15px;
			line-height: 28px;
			font-family: Arial;
			font-weight: 900;
			position: absolute;
			bottom: 90px;
			text-align: left;
			left: 22px;
			width: 190px;
			color: #222;
			letter-spacing: -1px;
		}
		#c_show_city{
			font-size: 15px;
			line-height: 28px;
			font-family: Arial;
			font-weight: 900;
			position: absolute;
			bottom: 123px;
			text-align: left;
			left: 207px;
			width: 190px;
			color: #222;
			letter-spacing: -1px;
		}
		#v_c_show_city{
			font-size: 15px;
			line-height: 28px;
			font-family: Arial;
			font-weight: 900;
			position: absolute;
			bottom: 123px;
			text-align: left;
			left: 207px;
			width: 190px;
			color: #222;
			letter-spacing: -1px;
		}
		#v_show_zip,
		#show_zip{
			font-size: 15px;
			line-height: 28px;
			font-family: Arial;
			font-weight: 900;
			position: absolute;
			bottom: 90px;
			text-align: left;
			left: 128px;
			width: 50px;
			color: #222;
			letter-spacing: -1px;
		}
		#c_show_zip{
			font-size: 15px;
			line-height: 28px;
			font-family: Arial;
			font-weight: 900;
			position: absolute;
			bottom: 90px;
			text-align: left;
			left: 208px;
			width: 50px;
			color: #222;
			letter-spacing: -1px;
		}
		#v_c_show_zip{
			font-size: 15px;
			line-height: 28px;
			font-family: Arial;
			font-weight: 900;
			position: absolute;
			bottom: 90px;
			text-align: left;
			left: 208px;
			width: 50px;
			color: #222;
			letter-spacing: -1px;
		}
		#v_show_user_id,
		#show_user_id{
			font-size: 18px;
			line-height: 28px;
			font-family: Arial;
			font-weight: 800;
			position: absolute;
			bottom: 68px;
			text-align: left;
			right: 199px;
			width: 150px;
			color: #0b4647;
			letter-spacing: -1px;
		}
		#c_show_user_id{
			font-size: 18px;
			line-height: 28px;
			font-family: Arial;
			font-weight: 800;
			position: absolute;
			bottom: 8px;
			text-align: left;
			right: 10px;
			width: 150px;
			color: #0b4647;
			letter-spacing: -1px;
		}
		#v_c_show_user_id{
			font-size: 18px;
			line-height: 28px;
			font-family: Arial;
			font-weight: 800;
			position: absolute;
			bottom: 8px;
			text-align: left;
			right: 10px;
			width: 150px;
			color: #0b4647;
			letter-spacing: -1px;
		}
		#v_show_img,
		#show_img{
			border-radius: 2px;
			width: 100%;
			position: absolute;
			left: 0;
			top: 0;
			
		}
		#c_show_img{
			border-radius: 2px;
			width: 100%;
			position: absolute;
			left: 0;
			right: 0;
			top: 0;
			bottom: 0;
			margin: auto;
		}
		#v_c_show_img{
			border-radius: 2px;
			width: 100%;
			position: absolute;
			left: 0;
			right: 0;
			top: 0;
			bottom: 0;
			margin: auto;
		}
		.payDet{
			color: #000;
			font-size: 1.5em;
			font-weight: bold;
			text-align: left;
		}
		#cartTable input[type="text"]{
			text-align:left;
		}
		#cartTable label{float:left}
		#cartTable th{
			color:#d93025;
		}
		#cartTable td{
			color:#333;
		}
		.checkout{
			border: none;
			outline: 2px solid #d93025;
			color: #fff;
			padding: .6em 3em;
			font-size: 1em;
			position: relative;
			margin: 10px auto;
			display: block;
			-webkit-appearance: none;
			background: #d93025;
		}
		#tagOrderForm label{display:block;    text-align: left;}
		#contactIDOrderForm label{display:block;    text-align: left;}
		#UpdateUserAdminData label{display:block;    text-align: left;}
		#UpdateUserAdminData select{
			width: 100%;
			padding: 1em 1em 1em 1em;
			font-size: 0.9em;
			margin: 0.5em 0 0 0;
			outline: none;
			color: #212121;
			border: 1px solid #ccc;
			letter-spacing: 1px;
			text-align: center;
		}
		#updateProductForm select,
		#addPetForm select,
		#updatePersonForm select,
		#addPersonForm select,
		#addProductForm select,
		#addPetForm input[type="date"],
		#updatePersonForm input[type="date"],
		#addPersonForm input[type="date"],
		#addPetForm input[type="file"],
		#updatePersonForm input[type="file"],
		#addPersonForm input[type="file"],
		#contactIDOrderForm input[type="file"],
		#tagOrderForm input[type="file"],
		#updateProductForm input[type="file"],
		#addProductForm input[type="file"],
		#updatePetForm select,
		#updatePetForm input[type="date"],
		#updatePetForm input[type="file"]{
			width: 100%;
			padding: 1em 1em 1em 1em;
			font-size: 0.9em;
			margin: 0.5em 0 0 0;
			outline: none;
			color: #212121;
			border: 1px solid #ccc;
			letter-spacing: 1px;
			text-align: center;
		}
		#addPetForm input[type="date"],
		#addPersonForm input[type="date"],
		#updatePersonForm input[type="date"],
		#updatePetForm input[type="date"]{
			padding:0;
		}

		#downloadLostPoster input[type="text"],
		#downloadFoundPoster input[type="text"],
		#tagOrderForm input[type="text"],
		#contactIDOrderForm input[type="text"],
		#updateProductForm input[type="text"],
		#addPetForm input[type="text"],
		#addPersonForm input[type="text"],
		#updatePersonForm input[type="text"],
		#addProductForm input[type="text"],
		#updatePetForm input[type="text"]{
			text-align:left;
		}
		#tagOrderForm input::placeholder,
		#contactIDOrderForm input::placeholder,
		#updateProductForm input::placeholder,
		#addPetForm input::placeholder,
		#addPersonForm input::placeholder,
		#updatePersonForm input::placeholder,
		#addProductForm input::placeholder,
		#updatePetForm input::placeholder{
			text-align:left;
		}
		#updateProductForm textarea::placeholder,
		#addProductForm textarea::placeholder,
		#addPetForm textarea::placeholder,
		#addPersonForm textarea::placeholder,
		#updatePersonForm textarea::placeholder,
		#updatePetForm textarea::placeholder{
			text-align:left;
		}
		#updateProductForm textarea,
		#addProductForm textarea,
		#addPetForm textarea,
		#addPersonForm textarea,
		#updatePersonForm textarea,
		#updatePersonForm textarea,
		#updatePetForm textarea{
			text-align:left;
			min-height: 80px;
		}
		#tagOrderForm label{float:left;}
		#updateProductForm label{float:left;}
		#addProductForm label{float:left;}
		#updatePetForm label{float:left;}
		#addPetForm label{float:left;}
		#updatePersonForm label{float:left;}
		#addPersonForm label{float:left;}
		.table > thead:first-child > tr:first-child > th{text-align:center}
		.resp-tabs-list li {    width: 275px;}
		.resp-tabs-container { width:75%}
		.contact-agileinfo input[type="email"] {
			width: 100%;
			color: #444;
			    background: #ddd;
			outline: none;
			font-size: 1em;
			padding: .7em .8em;
			border: solid 1px #ccc;
			-webkit-appearance: none;
			display: inline-block;
		}
		.contact-agileinfo input[type="password"] {
			width: 100%;
			color: #444;
			background: none;
			outline: none;
			font-size: 1em;
			padding: .7em .8em;
			border: solid 1px #ccc;
			-webkit-appearance: none;
			display: inline-block;
		}
		.contact-agileinfo input[type="text"] {color: #444;}
	</style>

</head>

<body>
<div id="fb-root"></div>
    <script type="text/javascript">
            window.fbAsyncInit = function() {
                FB.init({appId: '263302517915123', status: true, cookie: true, xfbml: true});
			
            };
            (function() {
				
                var e = document.createElement('script');
                e.type = 'text/javascript';
                e.src = document.location.protocol +
                    '//connect.facebook.net/en_US/all.js';
                e.async = true;
                document.getElementById('fb-root').appendChild(e);
            }());
			function shareNow(user_id,id,pic,name){
                var share = {
                    method: 'stream.share',
                    quote: 'PLEASE HELP ME FIND MY PET!! \n Name: '+name+'  \n Picture: https://www.petpost.info/images/uploads/'+user_id+'/'+pic,
                    u: 'https://www.petpost.info/pet/profile/'+id
                };
 
                FB.ui(share, function(response) { console.log(response); });
            }
	</script>
	<script async defer crossorigin="anonymous" src="https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v3.2&appId=263302517915123&autoLogAppEvents=1"></script>
	<!-- banner -->
	<div class="agileits-banner about-w3banner">
		<div class="bnr-agileinfo">
			<div class="banner-top w3layouts">
				<div class="container">
					<ul class="agile_top_section">
						<li>
							<p><a href="<?php echo base_url();?>home"><img style="    max-width: 130px;" src="<?php echo base_url();?>images/petpostlogo.png"></a></p>
						</li>
						
						<?php if($this->session->userdata('loggedin') != TRUE){ ?>
						<li>
							<p>&ensp;</p>
						</li>
						<li><a class="sign" href="#" data-toggle="modal" data-target="#myModal2"><i class="fa fa-sign-in" aria-hidden="true"></i> Sign In</a>&nbsp;<a class="sign" href="#" data-toggle="modal" data-target="#cartModal" style="color: #fff; background-color: #f0ad4e;border-color: #eea236;"><i class="fa fa-shopping-cart" aria-hidden="true"></i> My Cart</a></li>
						<?php }else{ ?>
						<li>
							<p style="color:#fff;font-weight:bold">My ID : <?php echo str_pad($this->session->userdata('u_id'), 10, '0', STR_PAD_LEFT); ?></p>
						</li>
						<li><a class="sign" href="<?php echo base_url();?>settings"><i class="fa fa-cog" aria-hidden="true"></i> DASHBOARD</a>&nbsp;<a class="sign" href="#" data-toggle="modal" data-target="#cartModal" style="color: #fff; background-color: #f0ad4e;border-color: #eea236;"><i class="fa fa-shopping-cart" aria-hidden="true"></i> My Cart</a>&nbsp;<a class="sign" href="<?php echo base_url();?>login/logout"><i class="fa fa-power-off" aria-hidden="true"></i></a>
						</li>
						<?php } ?>							
					</ul>
				</div>
			</div>
			<div class="banner-w3text w3layouts">


				<h2>Pet Post</h2>
			</div>
			<!-- navigation -->
			<div class="top-nav w3-agiletop">
				<div class="agile_inner_nav_w3ls">
					<div class="navbar-header w3llogo">
						<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
							<span class="sr-only">Toggle navigation</span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>

						</button>
						<p><a href="<?php echo base_url();?>home"><img style="    max-width: 100px;" src="<?php echo base_url();?>images/logo.png"></a></p>
					</div>
					<!-- Collect the nav links, forms, and other content for toggling -->
					<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
						<div class="w3menu navbar-left">
							<ul class="nav navbar">
								<li><a href="<?php echo base_url();?>home">Home</a></li>
								<li><a href="<?php echo base_url();?>about">About Us</a></li>
								<?php if($this->session->userdata('loggedin') == TRUE){ ?>
								<li><a href="<?php echo base_url();?>lost">Lost</a></li>
								<li><a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><span data-letters="Pages">Found</span><span class="caret"></span></a>
									<ul class="dropdown-menu">
										<li><a href="<?php echo base_url();?>found">All Found Pets</a></li>
										<li><a href="<?php echo base_url();?>found/mylist">My Found Pets</a></li>
									</ul>
								</li>
								<li><a href="<?php echo base_url();?>missing">Missing Person</a></li>
								<?php } ?>
								<li><a href="<?php echo base_url();?>shop">Shop</a></li>
								<li><a href="<?php echo base_url();?>contact">Contact</a></li>
							</ul>
						</div>
						<div class="w3ls-bnr-icons social-icon navbar-right">
							<a href="https://www.facebook.com/petpost.info/" target="_blank" class="social-button facebook"><i class="fa fa-facebook"></i></a>
							<a href="https://www.instagram.com/petpost.info/" target="_blank" class="social-button instagram"><i class="fa fa-instagram"></i></a>
						</div>
						<div class="clearfix"> </div>
					</div>
				</div>
			</div>
			<!-- //navigation -->
		</div>
	</div>
	<div class="modal fade" id="cartModal" tabindex="-1" role="dialog">
		<div class="modal-dialog modal-lg">
			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<div class="signin-form profile">
						<h3 class="agileinfo_sign">My Cart</h3>
						<div class="row">
							<div class="col-md-8">
								<table id="cartTable" class="table table-striped" style="width:100%;">
									<thead>
										<tr>
											<th scope="col text-left">Picture</th>
											<th scope="col text-left">Product Name</th>
											<th scope="col text-left">Description</th>
											<th scope="col text-left">Price (USD)</th>
											<th scope="col text-left">Weight (g)</th>
											<th scope="col text-left"></th>
										</tr>
									</thead>
									<tbody id="tbodyCartTable">
																
									</tbody>
								</table>
								<div style="display:none" id="nocart" class="alert alert-info" role="alert">
									<h4>No products in the cart.</h4>
								</div>
							</div>
							<div class="col-md-4">
								<div id="totArea" style="text-align:right;padding-right:25px;    padding-top: 15px;">
									<p style="font-size:16px;color:#000;font-weight:bold">Shipping Fee ($): <span id="shipFee">0</span></p>
									<p style="font-size:16px;color:#000;font-weight:bold">Subtotal Price ($): <span id="subTotPrice">0</span></p>
									<p style="font-size:16px;color:#000;font-weight:bold">Total Price ($): <span id="totPrice">0</span></p>
										<table border="0" cellpadding="10" cellspacing="0" align="right"><tr><td align="center"></td></tr><tr><td align="center"><p style="cursor:pointer" id="checkOutNow"><img src="https://www.paypalobjects.com/webstatic/en_US/i/buttons/checkout-logo-large.png" alt="Check out with PayPal" style="border-radius:0;" /></p></td></tr></table>
								</div>
							</div>
						</div>
						<input type="hidden" id="baseurl" value="<?php echo base_url();?>">
						<input type="hidden" id="usr_id" value="<?php echo $this->session->userdata('u_id');?>">
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- //banner -->
	<div class="modal fade" id="editUserModal" tabindex="-1" role="dialog">
		<div class="modal-dialog modal-lg">
			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>

							<div class="signin-form profile">
									<h3 class="agileinfo_sign">Edit User</h3>
									<div class="alert alert-success display-none" id="updateUserAdminSuccess">
										<p><strong>Success!</strong> The account has been updated.</p>
									</div>
									<div class="alert alert-danger display-none" id="updateUserAdminError">
										<p><strong>Error!</strong> Please try again.</p>
									</div>
									<div class="alert alert-danger display-none" id="updateCountryCodeAdminError">
										<p><strong>Error!</strong> Please include country code for each contact number.</p>
									</div>
									<div class="row">
										<form id="UpdateUserAdminData" method="post" data-toggle="validator" role="form">
										<div class="col-md-6 contact-right">
											<div class="form-group">
												<label for="upd_ad_name">Full Name</label>
												<input type="text" name="upd_ad_name" id="upd_ad_name" placeholder="Full name" required>
												<div class="help-block with-errors"></div>
											</div>
											<div class="form-group">
												<label for="upd_ad_country">Country</label>
												<input type="text" name="upd_ad_country" id="upd_ad_country" placeholder="Country" required>
												<div class="help-block with-errors"></div>
											</div>
											<div class="form-group">
												<label for="upd_ad_city">City/State</label>
												<input type="text" name="upd_ad_city" id="upd_ad_city" placeholder="City/State" required>
												<div class="help-block with-errors"></div>
											</div>
											<div class="form-group">
												<label for="upd_ad_role">User Role</label>
												<select name="upd_ad_role" id="upd_ad_role">
												  <option value="1">Basic</option>
												  <option value="2">Standard</option>
												</select>
												<input type="hidden" name="ad_u_id" id="ad_u_id" placeholder="User Role" required>
												<div class="help-block with-errors"></div>
											</div>
										</div>
										<div class="col-md-6 contact-left">
											<div class="form-group">
												<label for="upd_ad_zip">Post/Zip Code</label>
												<input type="text" name="upd_ad_zip" id="upd_ad_zip" placeholder="Post/Zip Code" required>
												<div class="help-block with-errors"></div>
											</div>
											<div class="form-group" style="margin-bottom: -1px;">
												<label for="upd_ad_contact1">Contact Number 1</label>
												<input type="text" name="upd_ad_c_code1" id="upd_ad_c_code1" placeholder="CCode" style="width:25%;display:inline" required><input type="text" name="upd_ad_contact1" style="width:75%;display:inline" placeholder="Contact Number 1" required>
												<div class="help-block with-errors"></div>
											</div>
											<div class="form-group" style="margin-bottom: 0px;">
											<label for="upd_ad_contact2">Contact Number 2</label>
												<input type="text" name="upd_ad_c_code2" id="upd_ad_c_code2" placeholder="CCode" required style="width:25%;display:inline"><input type="text" name="upd_ad_contact2" style="width:75%;display:inline" placeholder="Contact Number 2" required>
												<div class="help-block with-errors"></div>
											</div>
											<div class="form-group">
												<label for="upd_ad_contact3">Contact Number 3</label>
												<input type="text" name="upd_ad_c_code3" id="upd_ad_c_code3" placeholder="CCode" required style="width:25%;display:inline"><input type="text" name="upd_ad_contact3" style="width:75%;display:inline" placeholder="Contact Number 3" required>
												<div class="help-block with-errors"></div>
											</div>
											
										</div>
										<div class="form-group">
												<input type="submit" value="SAVE" style="    width: 50%;">
											</div>
										</form>
									</div>
								
						
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="modal fade" id="invoiceModal" tabindex="-1" role="dialog">
		<div class="modal-dialog modal-lg">
			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>

					<div class="signin-form profile">
						<h3 class="agileinfo_sign">Billing/Delivery Information</h3>
						<div class="login-form">
							<form action="<?php echo base_url();?>shop/checkout"  method="post" data-toggle="validator" role="form">
								<div class="row">
									<div class="col-md-6 contact-left">
										<div class="form-group">
											<label for="inv_name">Full Name</label>
											<input type="text" name="inv_name" id="inv_name" required="">
											<input type="hidden" name="arrData[]" id="arrData">
											<input type="hidden" name="shippingFee" id="shippingFee">
											<input type="hidden" name="TotalFee" id="TotalFee">
											<div class="help-block with-errors"></div>
										</div>
										<div class="form-group">
											<label for="inv_country">Country</label>
											<input type="text" name="inv_country" id="inv_country" required="">
											<div class="help-block with-errors"></div>
										</div>
										<div class="form-group">
											<label for="inv_address">Home Address</label>
											<input type="text" name="inv_address" id="inv_address" required="">
											<div class="help-block with-errors"></div>
										</div>
										<p class="payDet">Shipping Fee($): <span id="shipPayment" style="color: #d93025;"></span></p>
										<p class="payDet">Total Payment($): <span id="totPayment" style="color: #d93025;"></span></p>
									</div>
									<div class="col-md-6 contact-right">
										<div class="form-group">
											<label for="inv_email">Email Address</label>
											<input type="text" name="inv_email" id="inv_email" required="">
											<div class="help-block with-errors"></div>
										</div>
										<div class="form-group">
											<label for="inv_contact">Contact Number 1</label>
											<input type="text" name="inv_contact" id="inv_contact" required="">
											<div class="help-block with-errors"></div>
										</div>
										<div class="form-group">
											<label for="inv_contact2">Contact Number 2</label>
											<input type="text" name="inv_contact2" id="inv_contact2" required="">
											<div class="help-block with-errors"></div>
										</div>
									</div>
								</div>
								<div class="tp">
									<input type="submit" value="PAY NOW" style="width:50%">
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="modal fade" id="contactIDOrderModal" tabindex="-1" role="dialog">
		<div class="modal-dialog modal-lg" style="width:1000px">
			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>

					<div class="signin-form profile">
						<h3 class="agileinfo_sign">Identification Card Form</h3>
						<div class="login-form">
							<form id="contactIDOrderForm"  method="post" data-toggle="validator" role="form">
								<div class="alert alert-success display-none" id="orderContactSuccess">
									<p><strong>Success!</strong> Your contact ID card is now processed.</p>
								</div>
								<div class="alert alert-danger display-none" id="orderContactError">
									<p><strong>Error!</strong> Please try again.</p>
								</div>
								<div class="row">
									<div class="col-md-7 contact-left">
										<div id="contactCard_container">
											<p id="cn_show_petname">Name:</p>
											<p id="cn_show_det">Address and Contact Details</p>
											<p id="c_show_country"></p>
											<p id="c_show_contact1"></p>
											<p id="c_show_contact2"></p>
											<p id="c_show_petname"></p>
											<p id="c_show_street"></p>
											<p id="c_show_city"></p>
											<p id="c_show_zip"></p>
											<div style="width:160px;height:150px;position:absolute;top: 76px;left: 25px;overflow: hidden;border: 1px solid #909090;background: #fff;"><img src="" id="c_show_img"></div>
											<p id="c_show_user_id">ID : <?php echo str_pad($this->session->userdata('u_id'), 10, '0', STR_PAD_LEFT); ?></p>
										</div>
									</div>
									<div class="col-md-5 contact-right">
										<div class="form-group">
											<label for="c_tag_picture">Picture</label>
											<input type="file" name="c_tag_picture" id="c_tag_picture" required="">
											<div class="help-block with-errors"></div>
										</div>
										<div class="form-group">
											<label for="c_tag_petname">Full Name</label>
											<input type="text" name="c_tag_petname" id="c_tag_petname" required="">
											<div class="help-block with-errors"></div>
										</div>
										<div class="row">
											<div class="col-md-6 contact-left">
												<div class="form-group">
													<label for="c_tag_street">Street</label>
													<input type="text" name="c_tag_street" id="c_tag_street" required="">
													<div class="help-block with-errors"></div>
												</div>
												<div class="form-group">
													<label for="c_tag_city">City</label>
													<input type="text" name="c_tag_city" id="c_tag_city" required="">
													<div class="help-block with-errors"></div>
												</div>
												
												<div class="form-group">
													<label for="c_tag_contact">Contact Number 1</label>
													<input type="text" name="c_tag_contact" id="c_tag_contact" required="">
													<div class="help-block with-errors"></div>
												</div>
											</div>
											<div class="col-md-6 contact-right">
												<div class="form-group">
													<label for="c_tag_country">Country</label>
													<input type="text" name="c_tag_country" id="c_tag_country" required="">
													<div class="help-block with-errors"></div>
												</div>
												<div class="form-group">
													<label for="c_tag_zip">Zip</label>
													<input type="text" name="c_tag_zip" id="c_tag_zip" required="">
													<div class="help-block with-errors"></div>
												</div>
												<div class="form-group">
													<label for="c_tag_contact2">Contact Number 2</label>
													<input type="text" name="c_tag_contact2" id="c_tag_contact2" required="">
													<div class="help-block with-errors"></div>
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="tp">
									<input type="submit" value="ORDER NOW" style="width:50%">
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="modal fade" id="viewContactIDOrderModal" tabindex="-1" role="dialog">
		<div class="modal-dialog modal-md">
			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<div class="signin-form profile">
						<h3 class="agileinfo_sign">Identification Card</h3>
						<div class="login-form">
								<div class="row">
									<div class="col-md-12 contact-left">
										<div id="v_contactCard_container">
											<p id="v_cn_show_petname">Name:</p>
											<p id="v_cn_show_det">Address and Contact Details</p>
											<p id="v_c_show_country"></p>
											<p id="v_c_show_contact1"></p>
											<p id="v_c_show_contact2"></p>
											<p id="v_c_show_petname"></p>
											<p id="v_c_show_street"></p>
											<p id="v_c_show_city"></p>
											<p id="v_c_show_zip"></p>
											<div style="width:160px;height:150px;position:absolute;top: 76px;left: 25px;overflow: hidden;border: 1px solid #909090;background: #fff;"><img src="" id="v_c_show_img"></div>
											<p id="v_c_show_user_id">ID : <?php echo str_pad($this->session->userdata('u_id'), 10, '0', STR_PAD_LEFT); ?></p>
										</div>
									</div>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="modal fade" id="tagOrderModal" tabindex="-1" role="dialog">
		<div class="modal-dialog modal-lg" style="width:1000px">
			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>

					<div class="signin-form profile">
						<h3 class="agileinfo_sign">Pet Tag Form</h3>
						<div class="login-form">
							<form id="tagOrderForm"  method="post" data-toggle="validator" role="form">
								<div class="alert alert-success display-none" id="orderTagSuccess">
									<p><strong>Success!</strong> Your pet's tag is now processed.</p>
								</div>
								<div class="alert alert-danger display-none" id="orderTagError">
									<p><strong>Error!</strong> Please try again.</p>
								</div>
								<div class="row">
									<div class="col-md-7 contact-left">
										<div id="tag_container">
											<p id="show_country"></p>
											<p id="show_contact1"></p>
											<p id="show_contact2"></p>
											<p id="show_petname"></p>
											<p id="show_street"></p>
											<p id="show_city"></p>
											<p id="show_zip"></p>
											<div style="width:140px;height:125px;position:absolute;top: 110px;right: 200px;overflow: hidden;"><img src="" id="show_img"></div>
											<p id="show_user_id">ID : <?php echo str_pad($this->session->userdata('u_id'), 10, '0', STR_PAD_LEFT); ?></p>
										</div>
									</div>
									<div class="col-md-5 contact-right">
										<div class="form-group">
											<label for="tag_picture">Pet Picture</label>
											<input type="file" name="tag_picture" id="tag_picture" required="">
											<div class="help-block with-errors"></div>
										</div>
										<div class="form-group">
											<label for="tag_petname">Pet Name</label>
											<input type="text" name="tag_petname" id="tag_petname" required="">
											<div class="help-block with-errors"></div>
										</div>
										<div class="row">
											<div class="col-md-6 contact-left">
												<div class="form-group">
													<label for="tag_street">Street</label>
													<input type="text" name="tag_street" id="tag_street" required="">
													<div class="help-block with-errors"></div>
												</div>
												<div class="form-group">
													<label for="tag_city">City</label>
													<input type="text" name="tag_city" id="tag_city" required="">
													<div class="help-block with-errors"></div>
												</div>
												
												<div class="form-group">
													<label for="tag_contact">Contact Number 1</label>
													<input type="text" name="tag_contact" id="tag_contact" required="">
													<div class="help-block with-errors"></div>
												</div>
											</div>
											<div class="col-md-6 contact-right">
												<div class="form-group">
													<label for="tag_country">Country</label>
													<input type="text" name="tag_country" id="tag_country" required="">
													<div class="help-block with-errors"></div>
												</div>
												<div class="form-group">
													<label for="tag_zip">Zip</label>
													<input type="text" name="tag_zip" id="tag_zip" required="">
													<div class="help-block with-errors"></div>
												</div>
												<div class="form-group">
													<label for="tag_contact2">Contact Number 2</label>
													<input type="text" name="tag_contact2" id="tag_contact2" required="">
													<div class="help-block with-errors"></div>
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="tp">
									<input type="submit" value="ORDER NOW" style="width:50%">
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	
	<div class="modal fade" id="viewTagOrderModal" tabindex="-1" role="dialog">
		<div class="modal-dialog">
			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>

					<div class="signin-form profile">
						<h3 class="agileinfo_sign">Pet Tag</h3>
						<div class="login-form">
								<div class="row">
									<div class="col-md-12 contact-left">
										<div id="v_tag_container">
											<p id="v_show_country"></p>
											<p id="v_show_contact1"></p>
											<p id="v_show_contact2"></p>
											<p id="v_show_petname"></p>
											<p id="v_show_street"></p>
											<p id="v_show_city"></p>
											<p id="v_show_zip"></p>
											<div style="width:140px;height:125px;position:absolute;top: 110px;right: 200px;overflow: hidden;"><img src="" id="v_show_img"></div>
											<p id="v_show_user_id">ID : <?php echo str_pad($this->session->userdata('u_id'), 10, '0', STR_PAD_LEFT); ?></p>
										</div>
									</div>
								</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="modal fade" id="personfoundPosterModal" tabindex="-1" role="dialog">
		<div class="modal-dialog">
			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>

					<div class="signin-form profile">
						<h3 class="agileinfo_sign">Add Details to Poster</h3>
						<div class="login-form">
							<form method="post" id="downloadFoundPersonPoster" action="<?php echo base_url();?>settings/downloadFoundPersonPoster" enctype="multipart/form-data">
								<div class="row text-left">
									<div class="col-sm-6">
										<div class="form-group">
											<label for="addNameFoundPersonPoster">Full Name</label>
											<input type="text" name="addNameFoundPersonPoster" class="form-control" required>
										</div>	
										<div class="form-group">
											<label for="addDateFoundPersonPoster">Date Found</label>
											<input type="date" name="addDateFoundPersonPoster" class="form-control" required>
										</div>
										<div class="form-group">
											<label for="addZipFoundPersonPoster">Zip/Postal Code</label>
											<input type="text" name="addZipFoundPersonPoster" class="form-control" required>
										</div>
										<div class="form-group">
											<label for="addStreetFoundPersonPoster">Street</label>
											<input type="text" name="addStreetFoundPersonPoster" class="form-control" required>
										</div>
										<div class="form-group">
											<label for="addContactFoundPersonPoster">Contact Number</label>
											<input type="text" name="addContactFoundPersonPoster" class="form-control" required>
										</div>
										<div class="form-group">
											<label for="addAddDetFoundMissingPoster">Additional Details</label>
											<input type="text" name="addAddDetFoundMissingPoster" class="form-control" required>
										</div>
									</div>
									<div class="col-sm-6">
										<div class="form-group">
											<label for="addPicFoundPersonPoster">Personal Picture</label>
											<input type="file" name="addPicFoundPersonPoster" class="form-control" required>
										</div>	
										<div class="form-group">
											<label for="addBreedFoundPersonPoster">Hair Color</label>
											<input type="text" name="addBreedFoundPersonPoster" class="form-control" required>
										</div>
										<div class="form-group">
											<label for="addColorFoundPersonPoster">Eye Color</label>
											<input type="text" name="addColorFoundPersonPoster" class="form-control" required>
										</div>
										<div class="form-group">
											<label for="addGenderFoundPersonPoster">Gender</label>
											<select name="addGenderFoundPersonPoster" class="form-control">
												<option value="Female">Female</option>
												<option value="Male">Male</option>
											</select>
										</div>
										<div class="form-group">
											<label for="addAgeFoundPersonPoster">Age</label>
											<input type="text" name="addAgeFoundPersonPoster" class="form-control" required>
										</div>
									</div>
								</div>
									<button type="submit" class="btn btn-primary"><i class="fa fa-download"></i> DOWNLOAD NOW</button>
								
							</form>
										
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="modal fade" id="foundPosterModal" tabindex="-1" role="dialog">
		<div class="modal-dialog">
			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>

					<div class="signin-form profile">
						<h3 class="agileinfo_sign">Add Details to Poster</h3>
						<div class="login-form">
							<form method="post" id="downloadFoundPoster" action="<?php echo base_url();?>settings/downloadFoundPoster" enctype="multipart/form-data">
								<div class="row text-left">
									<div class="col-sm-6">
										<div class="form-group">
											<label for="addNameFoundPoster">Pet Name</label>
											<input type="text" name="addNameFoundPoster" class="form-control" required>
										</div>	
										<div class="form-group">
											<label for="addDateFoundPoster">Date Found</label>
											<input type="date" name="addDateFoundPoster" class="form-control" required>
										</div>
										<div class="form-group">
											<label for="addZipFoundPoster">Zip/Postal Code</label>
											<input type="text" name="addZipFoundPoster" class="form-control" required>
										</div>
										<div class="form-group">
											<label for="addStreetFoundPoster">Street</label>
											<input type="text" name="addStreetFoundPoster" class="form-control" required>
										</div>
										<div class="form-group">
											<label for="addContactFoundPoster">Contact Number</label>
											<input type="text" name="addContactFoundPoster" class="form-control" required>
										</div>
										<div class="form-group">
											<label for="addAddDetFoundPoster">Additional Details</label>
											<input type="text" name="addAddDetFoundPoster" class="form-control" required>
										</div>
									</div>
									<div class="col-sm-6">
										<div class="form-group">
											<label for="addPicFoundPoster">Pet Picture</label>
											<input type="file" name="addPicFoundPoster" class="form-control" required>
										</div>	
										<div class="form-group">
											<label for="addBreedFoundPoster">Breed</label>
											<input type="text" name="addBreedFoundPoster" class="form-control" required>
										</div>
										<div class="form-group">
											<label for="addColorFoundPoster">Color</label>
											<input type="text" name="addColorFoundPoster" class="form-control" required>
										</div>
										<div class="form-group">
											<label for="addGenderFoundPoster">Gender</label>
											<select name="addGenderFoundPoster" class="form-control">
												<option value="Female">Female</option>
												<option value="Male">Male</option>
											</select>
										</div>
										<div class="form-group">
											<label for="addAgeFoundPoster">Age</label>
											<input type="text" name="addAgeFoundPoster" class="form-control" required>
										</div>
									</div>
								</div>
									<button type="submit" class="btn btn-primary"><i class="fa fa-download"></i> DOWNLOAD NOW</button>
								
							</form>
										
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="modal fade" id="personposterModal" tabindex="-1" role="dialog">
		<div class="modal-dialog">
			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>

					<div class="signin-form profile">
						<h3 class="agileinfo_sign">Add Details to Poster</h3>
						<div class="login-form">
							<form method="post" id="downloadMissingPoster" action="<?php echo base_url();?>settings/downloadMissingPoster" enctype="multipart/form-data">
								<div class="row text-left">
									<div class="col-sm-6">
										<div class="form-group">
											<label for="addNameMissingPoster">Full Name</label>
											<input type="text" name="addNameMissingPoster" class="form-control" required>
										</div>	
										<div class="form-group">
											<label for="addDateMissingPoster">Date Lost</label>
											<input type="date" name="addDateMissingPoster" class="form-control" required>
										</div>
										<div class="form-group">
											<label for="addZipMissingPoster">Zip/Postal Code</label>
											<input type="text" name="addZipMissingPoster" class="form-control" required>
										</div>
										<div class="form-group">
											<label for="addStreetMissingPoster">Street</label>
											<input type="text" name="addStreetMissingPoster" class="form-control" required>
										</div>
										<div class="form-group">
											<label for="addContactMissingPoster">Contact Number</label>
											<input type="text" name="addContactMissingPoster" class="form-control" required>
										</div>
										<div class="form-group">
											<label for="addAddDetMissingPoster">Additional Details</label>
											<input type="text" name="addAddDetMissingPoster" class="form-control" required>
										</div>
									</div>
									<div class="col-sm-6">
										<div class="form-group">
											<label for="addPicMissingPoster">Personal Picture</label>
											<input type="file" name="addPicMissingPoster" class="form-control" required>
										</div>	
										<div class="form-group">
											<label for="addHairColorMissingPoster">Hair Color</label>
											<input type="text" name="addHairColorMissingPoster" class="form-control" required>
										</div>
										<div class="form-group">
											<label for="addEyeColorMissingPoster">Eye Color</label>
											<input type="text" name="addEyeColorMissingPoster" class="form-control" required>
										</div>
										<div class="form-group">
											<label for="addGenderMissingPoster">Gender</label>
											<select name="addGenderMissingPoster" class="form-control">
												<option value="Female">Female</option>
												<option value="Male">Male</option>
											</select>
										</div>
										<div class="form-group">
											<label for="addAgeMissingPoster">Age</label>
											<input type="text" name="addAgeMissingPoster" class="form-control" required>
										</div>
									</div>
								</div>
									<button type="submit" class="btn btn-primary"><i class="fa fa-download"></i> DOWNLOAD NOW</button>
						
							</form>
										
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="modal fade" id="posterModal" tabindex="-1" role="dialog">
		<div class="modal-dialog">
			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>

					<div class="signin-form profile">
						<h3 class="agileinfo_sign">Add Details to Poster</h3>
						<div class="login-form">
							<form method="post" id="downloadLostPoster" action="<?php echo base_url();?>settings/downloadPoster" enctype="multipart/form-data">
								<div class="row text-left">
									<div class="col-sm-6">
										<div class="form-group">
											<label for="addNameLostPoster">Pet Name</label>
											<input type="text" name="addNameLostPoster" class="form-control" required>
										</div>	
										<div class="form-group">
											<label for="addDateLostPoster">Date Lost</label>
											<input type="date" name="addDateLostPoster" class="form-control" required>
										</div>
										<div class="form-group">
											<label for="addZipLostPoster">Zip/Postal Code</label>
											<input type="text" name="addZipLostPoster" class="form-control" required>
										</div>
										<div class="form-group">
											<label for="addStreetLostPoster">Street</label>
											<input type="text" name="addStreetLostPoster" class="form-control" required>
										</div>
										<div class="form-group">
											<label for="addContactLostPoster">Contact Number</label>
											<input type="text" name="addContactLostPoster" class="form-control" required>
										</div>
										<div class="form-group">
											<label for="addAddDetLostPoster">Additional Details</label>
											<input type="text" name="addAddDetLostPoster" class="form-control" required>
										</div>
									</div>
									<div class="col-sm-6">
										<div class="form-group">
											<label for="addPicLostPoster">Pet Picture</label>
											<input type="file" name="addPicLostPoster" class="form-control" required>
										</div>	
										<div class="form-group">
											<label for="addBreedLostPoster">Breed</label>
											<input type="text" name="addBreedLostPoster" class="form-control" required>
										</div>
										<div class="form-group">
											<label for="addColorLostPoster">Color</label>
											<input type="text" name="addColorLostPoster" class="form-control" required>
										</div>
										<div class="form-group">
											<label for="addGenderLostPoster">Gender</label>
											<select name="addGenderLostPoster" class="form-control">
												<option value="Female">Female</option>
												<option value="Male">Male</option>
											</select>
										</div>
										<div class="form-group">
											<label for="addAgeLostPoster">Age</label>
											<input type="text" name="addAgeLostPoster" class="form-control" required>
										</div>
									</div>
								</div>
									<button type="submit" class="btn btn-primary"><i class="fa fa-download"></i> DOWNLOAD NOW</button>
						
							</form>
										
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="modal fade" id="deleteUserModal" tabindex="-1" role="dialog">
		<div class="modal-dialog">
			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>

					<div class="signin-form profile">
						<h3 class="agileinfo_sign">REMOVE THIS USER?</h3>
						<button type="button" class="btn btn-default" data-dismiss="modal">CLOSE</button>
						<button type="button" class="btn btn-danger" id="dltUser">DELETE</button>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="modal fade" id="deletePetModal" tabindex="-1" role="dialog">
		<div class="modal-dialog">
			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>

					<div class="signin-form profile">
						<h3 class="agileinfo_sign">REMOVE THIS PET?</h3>
						<button type="button" class="btn btn-default" data-dismiss="modal">CLOSE</button>
						<button type="button" class="btn btn-danger" id="dltPet">DELETE</button>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="modal fade" id="deletePersonModal" tabindex="-1" role="dialog">
		<div class="modal-dialog">
			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>

					<div class="signin-form profile">
						<h3 class="agileinfo_sign">REMOVE THIS PERSON?</h3>
						<button type="button" class="btn btn-default" data-dismiss="modal">CLOSE</button>
						<button type="button" class="btn btn-danger" id="dltPerson">REMOVE</button>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="modal fade" id="deleteProductModal" tabindex="-1" role="dialog">
		<div class="modal-dialog">
			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>

					<div class="signin-form profile">
						<h3 class="agileinfo_sign">REMOVE THIS PRODUCT?</h3>
						<button type="button" class="btn btn-default" data-dismiss="modal">CLOSE</button>
						<button type="button" class="btn btn-danger" id="dltProduct">DELETE</button>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="modal fade" id="changeToShippedModal" tabindex="-1" role="dialog">
		<div class="modal-dialog">
			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>

					<div class="signin-form profile">
						<h3 class="agileinfo_sign">Already delivered?</h3>
						<button type="button" class="btn btn-default" data-dismiss="modal">CLOSE</button>
						<button type="button" class="btn btn-danger" id="updToShipped">YES</button>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="modal fade" id="changeToShippedTagOrderModal" tabindex="-1" role="dialog">
		<div class="modal-dialog">
			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>

					<div class="signin-form profile">
						<h3 class="agileinfo_sign">Already delivered?</h3>
						<button type="button" class="btn btn-default" data-dismiss="modal">CLOSE</button>
						<button type="button" class="btn btn-danger" id="updToShippedTagOrder">YES</button>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="modal fade" id="changeToShippedContactIDOrderModal" tabindex="-1" role="dialog">
		<div class="modal-dialog">
			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>

					<div class="signin-form profile">
						<h3 class="agileinfo_sign">Already delivered?</h3>
						<button type="button" class="btn btn-default" data-dismiss="modal">CLOSE</button>
						<button type="button" class="btn btn-danger" id="updToShippedContactIDOrder">YES</button>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- Modal1 -->
	<div class="modal fade" id="myModal2" tabindex="-1" role="dialog">
		<div class="modal-dialog">
			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>

					<div class="signin-form profile">
						<h3 class="agileinfo_sign">Sign In</h3>
						<div class="login-form">
							<form id="loginUser"  method="post" data-toggle="validator" role="form">
								<div class="alert alert-danger display-none" id="loginInvalid">
									<p><strong>Invalid!</strong> Email or Password is incorrect.</p>
								</div>
								<div class="form-group">
									<input type="email" name="email" placeholder="E-mail" required="">
									<div class="help-block with-errors"></div>
								</div>
								<div class="form-group">
									<input type="password" name="password" placeholder="Password" required="">
									<div class="help-block with-errors"></div>
								</div>
								<div class="tp">
									<input type="submit" value="Sign In">
								</div>
							</form>
						</div>
						<div class="login-social-grids">
							<ul>
								<li><a href="#"><i class="fa fa-facebook"></i></a></li>
								<li><a href="#"><i class="fa fa-twitter"></i></a></li>
								<li><a href="#"><i class="fa fa-rss"></i></a></li>
							</ul>
						</div>
						<p><a href="#" data-toggle="modal" data-target="#myModal3">Create your account now today, not tomorrow.</a></p>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="modal fade" id="showProduct" tabindex="-1" role="dialog">
		<div class="modal-dialog">
			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<div class="signin-form profile">
						<h3 class="agileinfo_sign">Order Details</h3>
						<table id="showProductTable" class="table table-striped table-bordered" style="width:100%;display:none">
							<thead>
								<tr>
									<th scope="col text-center">Quantity</th>
									<th scope="col text-center">Product Name</th>
									<th scope="col text-center">Product Picture</th>
								</tr>
							</thead>
							<tbody id="tbodyshowproduct">
														
							</tbody>
						</table>
						
						<div class="row">
							<div class="col-md-6">
								<h4 class="text-left">Delivery Information</h4>
								<p id="showOrderName" class="text-left"></p>
								<p id="showOrderaddress" class="text-left"></p>
								<p id="showOrderCountry" class="text-left"></p>
								<p id="showOrderEmail" class="text-left"></p>
							</div>
							<div class="col-md-6">
								<br/>
								<p id="showOrderContact1" class="text-left"></p>
								<p id="showOrderContact2" class="text-left"></p>
								<p id="showOrderDateOrdered" class="text-left"></p>
								<p id="showOrderTotal" class="text-left"></p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="modal fade" id="showProduct_s" tabindex="-1" role="dialog">
		<div class="modal-dialog">
			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<div class="signin-form profile">
						<h3 class="agileinfo_sign">Order Details</h3>
						<table id="showProductTable_s" class="table table-striped table-bordered" style="width:100%;display:none">
							<thead>
								<tr>
									<th scope="col text-center">Quantity</th>
									<th scope="col text-center">Product Name</th>
									<th scope="col text-center">Product Picture</th>
								</tr>
							</thead>
							<tbody id="tbodyshowproduct_s">
														
							</tbody>
						</table>
						
						<div class="row">
							<div class="col-md-6">
								<h4 class="text-left">Delivery Information</h4>
								<p id="showOrderName_s" class="text-left"></p>
								<p id="showOrderaddress_s" class="text-left"></p>
								<p id="showOrderCountry_s" class="text-left"></p>
								<p id="showOrderEmail_s" class="text-left"></p>
							</div>
							<div class="col-md-6">
								<br/>
								<p id="showOrderContact1_s" class="text-left"></p>
								<p id="showOrderContact2_s" class="text-left"></p>
								<p id="showOrderDateOrdered_s" class="text-left"></p>
								<p id="showOrderTotal_s" class="text-left"></p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- //Modal1 -->
	<!-- Modal2 -->
	<div class="modal fade" id="myModal3" tabindex="-1" role="dialog">
		<div class="modal-dialog">
			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>

					<div class="signin-form profile">
						<h3 class="agileinfo_sign">Sign Up</h3>
						<div class="login-form">
							<form id="addUser" method="post" data-toggle="validator" role="form">
								<div class="alert alert-success display-none" id="addUserSuccess">
									<p><strong>Success!</strong> Your account has been created. You can now login.</p>
								</div>
								<div class="alert alert-danger display-none" id="addEmailDuplicate">
									<p><strong>Error!</strong> Email is already exist.</p>
								</div>
								<div class="alert alert-danger display-none" id="addUserError">
									<p><strong>Error!</strong> Please try again.</p>
								</div>
								<div class="alert alert-danger display-none" id="countryCodeError">
									<p><strong>Error!</strong> Please include country code for each contact number.</p>
								</div>
								<div class="form-group">
									<input type="text" name="name" placeholder="Full name" required>
									<div class="help-block with-errors"></div>
								</div>
								<div class="form-group">
									<select name="c_code1" style="width:43%;display:inline;font-size: 11px;">
											<option data-countryCode="DZ" value="213">Algeria (+213)</option>
											<option data-countryCode="AD" value="376">Andorra (+376)</option>
											<option data-countryCode="AO" value="244">Angola (+244)</option>
											<option data-countryCode="AI" value="1264">Anguilla (+1264)</option>
											<option data-countryCode="AG" value="1268">Antigua &amp; Barbuda (+1268)</option>
											<option data-countryCode="AR" value="54">Argentina (+54)</option>
											<option data-countryCode="AM" value="374">Armenia (+374)</option>
											<option data-countryCode="AW" value="297">Aruba (+297)</option>
											<option data-countryCode="AU" value="61">Australia (+61)</option>
											<option data-countryCode="AT" value="43">Austria (+43)</option>
											<option data-countryCode="AZ" value="994">Azerbaijan (+994)</option>
											<option data-countryCode="BS" value="1242">Bahamas (+1242)</option>
											<option data-countryCode="BH" value="973">Bahrain (+973)</option>
											<option data-countryCode="BD" value="880">Bangladesh (+880)</option>
											<option data-countryCode="BB" value="1246">Barbados (+1246)</option>
											<option data-countryCode="BY" value="375">Belarus (+375)</option>
											<option data-countryCode="BE" value="32">Belgium (+32)</option>
											<option data-countryCode="BZ" value="501">Belize (+501)</option>
											<option data-countryCode="BJ" value="229">Benin (+229)</option>
											<option data-countryCode="BM" value="1441">Bermuda (+1441)</option>
											<option data-countryCode="BT" value="975">Bhutan (+975)</option>
											<option data-countryCode="BO" value="591">Bolivia (+591)</option>
											<option data-countryCode="BA" value="387">Bosnia Herzegovina (+387)</option>
											<option data-countryCode="BW" value="267">Botswana (+267)</option>
											<option data-countryCode="BR" value="55">Brazil (+55)</option>
											<option data-countryCode="BN" value="673">Brunei (+673)</option>
											<option data-countryCode="BG" value="359">Bulgaria (+359)</option>
											<option data-countryCode="BF" value="226">Burkina Faso (+226)</option>
											<option data-countryCode="BI" value="257">Burundi (+257)</option>
											<option data-countryCode="KH" value="855">Cambodia (+855)</option>
											<option data-countryCode="CM" value="237">Cameroon (+237)</option>
											<option data-countryCode="CA" value="1">Canada (+1)</option>
											<option data-countryCode="CV" value="238">Cape Verde Islands (+238)</option>
											<option data-countryCode="KY" value="1345">Cayman Islands (+1345)</option>
											<option data-countryCode="CF" value="236">Central African Republic (+236)</option>
											<option data-countryCode="CL" value="56">Chile (+56)</option>
											<option data-countryCode="CN" value="86">China (+86)</option>
											<option data-countryCode="CO" value="57">Colombia (+57)</option>
											<option data-countryCode="KM" value="269">Comoros (+269)</option>
											<option data-countryCode="CG" value="242">Congo (+242)</option>
											<option data-countryCode="CK" value="682">Cook Islands (+682)</option>
											<option data-countryCode="CR" value="506">Costa Rica (+506)</option>
											<option data-countryCode="HR" value="385">Croatia (+385)</option>
											<option data-countryCode="CU" value="53">Cuba (+53)</option>
											<option data-countryCode="CY" value="90392">Cyprus North (+90392)</option>
											<option data-countryCode="CY" value="357">Cyprus South (+357)</option>
											<option data-countryCode="CZ" value="42">Czech Republic (+42)</option>
											<option data-countryCode="DK" value="45">Denmark (+45)</option>
											<option data-countryCode="DJ" value="253">Djibouti (+253)</option>
											<option data-countryCode="DM" value="1809">Dominica (+1809)</option>
											<option data-countryCode="DO" value="1809">Dominican Republic (+1809)</option>
											<option data-countryCode="EC" value="593">Ecuador (+593)</option>
											<option data-countryCode="EG" value="20">Egypt (+20)</option>
											<option data-countryCode="SV" value="503">El Salvador (+503)</option>
											<option data-countryCode="GQ" value="240">Equatorial Guinea (+240)</option>
											<option data-countryCode="ER" value="291">Eritrea (+291)</option>
											<option data-countryCode="EE" value="372">Estonia (+372)</option>
											<option data-countryCode="ET" value="251">Ethiopia (+251)</option>
											<option data-countryCode="FK" value="500">Falkland Islands (+500)</option>
											<option data-countryCode="FO" value="298">Faroe Islands (+298)</option>
											<option data-countryCode="FJ" value="679">Fiji (+679)</option>
											<option data-countryCode="FI" value="358">Finland (+358)</option>
											<option data-countryCode="FR" value="33">France (+33)</option>
											<option data-countryCode="GF" value="594">French Guiana (+594)</option>
											<option data-countryCode="PF" value="689">French Polynesia (+689)</option>
											<option data-countryCode="GA" value="241">Gabon (+241)</option>
											<option data-countryCode="GM" value="220">Gambia (+220)</option>
											<option data-countryCode="GE" value="7880">Georgia (+7880)</option>
											<option data-countryCode="DE" value="49">Germany (+49)</option>
											<option data-countryCode="GH" value="233">Ghana (+233)</option>
											<option data-countryCode="GI" value="350">Gibraltar (+350)</option>
											<option data-countryCode="GR" value="30">Greece (+30)</option>
											<option data-countryCode="GL" value="299">Greenland (+299)</option>
											<option data-countryCode="GD" value="1473">Grenada (+1473)</option>
											<option data-countryCode="GP" value="590">Guadeloupe (+590)</option>
											<option data-countryCode="GU" value="671">Guam (+671)</option>
											<option data-countryCode="GT" value="502">Guatemala (+502)</option>
											<option data-countryCode="GN" value="224">Guinea (+224)</option>
											<option data-countryCode="GW" value="245">Guinea - Bissau (+245)</option>
											<option data-countryCode="GY" value="592">Guyana (+592)</option>
											<option data-countryCode="HT" value="509">Haiti (+509)</option>
											<option data-countryCode="HN" value="504">Honduras (+504)</option>
											<option data-countryCode="HK" value="852">Hong Kong (+852)</option>
											<option data-countryCode="HU" value="36">Hungary (+36)</option>
											<option data-countryCode="IS" value="354">Iceland (+354)</option>
											<option data-countryCode="IN" value="91">India (+91)</option>
											<option data-countryCode="ID" value="62">Indonesia (+62)</option>
											<option data-countryCode="IR" value="98">Iran (+98)</option>
											<option data-countryCode="IQ" value="964">Iraq (+964)</option>
											<option data-countryCode="IE" value="353">Ireland (+353)</option>
											<option data-countryCode="IL" value="972">Israel (+972)</option>
											<option data-countryCode="IT" value="39">Italy (+39)</option>
											<option data-countryCode="JM" value="1876">Jamaica (+1876)</option>
											<option data-countryCode="JP" value="81">Japan (+81)</option>
											<option data-countryCode="JO" value="962">Jordan (+962)</option>
											<option data-countryCode="KZ" value="7">Kazakhstan (+7)</option>
											<option data-countryCode="KE" value="254">Kenya (+254)</option>
											<option data-countryCode="KI" value="686">Kiribati (+686)</option>
											<option data-countryCode="KP" value="850">Korea North (+850)</option>
											<option data-countryCode="KR" value="82">Korea South (+82)</option>
											<option data-countryCode="KW" value="965">Kuwait (+965)</option>
											<option data-countryCode="KG" value="996">Kyrgyzstan (+996)</option>
											<option data-countryCode="LA" value="856">Laos (+856)</option>
											<option data-countryCode="LV" value="371">Latvia (+371)</option>
											<option data-countryCode="LB" value="961">Lebanon (+961)</option>
											<option data-countryCode="LS" value="266">Lesotho (+266)</option>
											<option data-countryCode="LR" value="231">Liberia (+231)</option>
											<option data-countryCode="LY" value="218">Libya (+218)</option>
											<option data-countryCode="LI" value="417">Liechtenstein (+417)</option>
											<option data-countryCode="LT" value="370">Lithuania (+370)</option>
											<option data-countryCode="LU" value="352">Luxembourg (+352)</option>
											<option data-countryCode="MO" value="853">Macao (+853)</option>
											<option data-countryCode="MK" value="389">Macedonia (+389)</option>
											<option data-countryCode="MG" value="261">Madagascar (+261)</option>
											<option data-countryCode="MW" value="265">Malawi (+265)</option>
											<option data-countryCode="MY" value="60">Malaysia (+60)</option>
											<option data-countryCode="MV" value="960">Maldives (+960)</option>
											<option data-countryCode="ML" value="223">Mali (+223)</option>
											<option data-countryCode="MT" value="356">Malta (+356)</option>
											<option data-countryCode="MH" value="692">Marshall Islands (+692)</option>
											<option data-countryCode="MQ" value="596">Martinique (+596)</option>
											<option data-countryCode="MR" value="222">Mauritania (+222)</option>
											<option data-countryCode="YT" value="269">Mayotte (+269)</option>
											<option data-countryCode="MX" value="52">Mexico (+52)</option>
											<option data-countryCode="FM" value="691">Micronesia (+691)</option>
											<option data-countryCode="MD" value="373">Moldova (+373)</option>
											<option data-countryCode="MC" value="377">Monaco (+377)</option>
											<option data-countryCode="MN" value="976">Mongolia (+976)</option>
											<option data-countryCode="MS" value="1664">Montserrat (+1664)</option>
											<option data-countryCode="MA" value="212">Morocco (+212)</option>
											<option data-countryCode="MZ" value="258">Mozambique (+258)</option>
											<option data-countryCode="MN" value="95">Myanmar (+95)</option>
											<option data-countryCode="NA" value="264">Namibia (+264)</option>
											<option data-countryCode="NR" value="674">Nauru (+674)</option>
											<option data-countryCode="NP" value="977">Nepal (+977)</option>
											<option data-countryCode="NL" value="31">Netherlands (+31)</option>
											<option data-countryCode="NC" value="687">New Caledonia (+687)</option>
											<option data-countryCode="NZ" value="64">New Zealand (+64)</option>
											<option data-countryCode="NI" value="505">Nicaragua (+505)</option>
											<option data-countryCode="NE" value="227">Niger (+227)</option>
											<option data-countryCode="NG" value="234">Nigeria (+234)</option>
											<option data-countryCode="NU" value="683">Niue (+683)</option>
											<option data-countryCode="NF" value="672">Norfolk Islands (+672)</option>
											<option data-countryCode="NP" value="670">Northern Marianas (+670)</option>
											<option data-countryCode="NO" value="47">Norway (+47)</option>
											<option data-countryCode="OM" value="968">Oman (+968)</option>
											<option data-countryCode="PW" value="680">Palau (+680)</option>
											<option data-countryCode="PA" value="507">Panama (+507)</option>
											<option data-countryCode="PG" value="675">Papua New Guinea (+675)</option>
											<option data-countryCode="PY" value="595">Paraguay (+595)</option>
											<option data-countryCode="PE" value="51">Peru (+51)</option>
											<option data-countryCode="PH" value="63">Philippines (+63)</option>
											<option data-countryCode="PL" value="48">Poland (+48)</option>
											<option data-countryCode="PT" value="351">Portugal (+351)</option>
											<option data-countryCode="PR" value="1787">Puerto Rico (+1787)</option>
											<option data-countryCode="QA" value="974">Qatar (+974)</option>
											<option data-countryCode="RE" value="262">Reunion (+262)</option>
											<option data-countryCode="RO" value="40">Romania (+40)</option>
											<option data-countryCode="RU" value="7">Russia (+7)</option>
											<option data-countryCode="RW" value="250">Rwanda (+250)</option>
											<option data-countryCode="SM" value="378">San Marino (+378)</option>
											<option data-countryCode="ST" value="239">Sao Tome &amp; Principe (+239)</option>
											<option data-countryCode="SA" value="966">Saudi Arabia (+966)</option>
											<option data-countryCode="SN" value="221">Senegal (+221)</option>
											<option data-countryCode="CS" value="381">Serbia (+381)</option>
											<option data-countryCode="SC" value="248">Seychelles (+248)</option>
											<option data-countryCode="SL" value="232">Sierra Leone (+232)</option>
											<option data-countryCode="SG" value="65">Singapore (+65)</option>
											<option data-countryCode="SK" value="421">Slovak Republic (+421)</option>
											<option data-countryCode="SI" value="386">Slovenia (+386)</option>
											<option data-countryCode="SB" value="677">Solomon Islands (+677)</option>
											<option data-countryCode="SO" value="252">Somalia (+252)</option>
											<option data-countryCode="ZA" value="27">South Africa (+27)</option>
											<option data-countryCode="ES" value="34">Spain (+34)</option>
											<option data-countryCode="LK" value="94">Sri Lanka (+94)</option>
											<option data-countryCode="SH" value="290">St. Helena (+290)</option>
											<option data-countryCode="KN" value="1869">St. Kitts (+1869)</option>
											<option data-countryCode="SC" value="1758">St. Lucia (+1758)</option>
											<option data-countryCode="SD" value="249">Sudan (+249)</option>
											<option data-countryCode="SR" value="597">Suriname (+597)</option>
											<option data-countryCode="SZ" value="268">Swaziland (+268)</option>
											<option data-countryCode="SE" value="46">Sweden (+46)</option>
											<option data-countryCode="CH" value="41">Switzerland (+41)</option>
											<option data-countryCode="SI" value="963">Syria (+963)</option>
											<option data-countryCode="TW" value="886">Taiwan (+886)</option>
											<option data-countryCode="TJ" value="7">Tajikstan (+7)</option>
											<option data-countryCode="TH" value="66">Thailand (+66)</option>
											<option data-countryCode="TG" value="228">Togo (+228)</option>
											<option data-countryCode="TO" value="676">Tonga (+676)</option>
											<option data-countryCode="TT" value="1868">Trinidad &amp; Tobago (+1868)</option>
											<option data-countryCode="TN" value="216">Tunisia (+216)</option>
											<option data-countryCode="TR" value="90">Turkey (+90)</option>
											<option data-countryCode="TM" value="7">Turkmenistan (+7)</option>
											<option data-countryCode="TM" value="993">Turkmenistan (+993)</option>
											<option data-countryCode="TC" value="1649">Turks &amp; Caicos Islands (+1649)</option>
											<option data-countryCode="TV" value="688">Tuvalu (+688)</option>
											<option data-countryCode="UG" value="256">Uganda (+256)</option>
											<option data-countryCode="GB" value="44">UK (+44)</option>
											<option data-countryCode="UA" value="380">Ukraine (+380)</option>
											<option data-countryCode="AE" value="971">United Arab Emirates (+971)</option>
											<option data-countryCode="UY" value="598">Uruguay (+598)</option>
											<option data-countryCode="US" value="1">USA (+1)</option>
											<option data-countryCode="UZ" value="7">Uzbekistan (+7)</option>
											<option data-countryCode="VU" value="678">Vanuatu (+678)</option>
											<option data-countryCode="VA" value="379">Vatican City (+379)</option>
											<option data-countryCode="VE" value="58">Venezuela (+58)</option>
											<option data-countryCode="VN" value="84">Vietnam (+84)</option>
											<option data-countryCode="VG" value="84">Virgin Islands - British (+1284)</option>
											<option data-countryCode="VI" value="84">Virgin Islands - US (+1340)</option>
											<option data-countryCode="WF" value="681">Wallis &amp; Futuna (+681)</option>
											<option data-countryCode="YE" value="969">Yemen (North)(+969)</option>
											<option data-countryCode="YE" value="967">Yemen (South)(+967)</option>
											<option data-countryCode="ZM" value="260">Zambia (+260)</option>
											<option data-countryCode="ZW" value="263">Zimbabwe (+263)</option>
									</select>
									<input type="text" name="contact1" style="width:56%;display:inline" placeholder="Contact Number 1" required>
									<div class="help-block with-errors"></div>
								</div>
								<div class="form-group">
									<select name="c_code2" style="width:43%;display:inline;font-size: 11px;">
											<option data-countryCode="DZ" value="213">Algeria (+213)</option>
											<option data-countryCode="AD" value="376">Andorra (+376)</option>
											<option data-countryCode="AO" value="244">Angola (+244)</option>
											<option data-countryCode="AI" value="1264">Anguilla (+1264)</option>
											<option data-countryCode="AG" value="1268">Antigua &amp; Barbuda (+1268)</option>
											<option data-countryCode="AR" value="54">Argentina (+54)</option>
											<option data-countryCode="AM" value="374">Armenia (+374)</option>
											<option data-countryCode="AW" value="297">Aruba (+297)</option>
											<option data-countryCode="AU" value="61">Australia (+61)</option>
											<option data-countryCode="AT" value="43">Austria (+43)</option>
											<option data-countryCode="AZ" value="994">Azerbaijan (+994)</option>
											<option data-countryCode="BS" value="1242">Bahamas (+1242)</option>
											<option data-countryCode="BH" value="973">Bahrain (+973)</option>
											<option data-countryCode="BD" value="880">Bangladesh (+880)</option>
											<option data-countryCode="BB" value="1246">Barbados (+1246)</option>
											<option data-countryCode="BY" value="375">Belarus (+375)</option>
											<option data-countryCode="BE" value="32">Belgium (+32)</option>
											<option data-countryCode="BZ" value="501">Belize (+501)</option>
											<option data-countryCode="BJ" value="229">Benin (+229)</option>
											<option data-countryCode="BM" value="1441">Bermuda (+1441)</option>
											<option data-countryCode="BT" value="975">Bhutan (+975)</option>
											<option data-countryCode="BO" value="591">Bolivia (+591)</option>
											<option data-countryCode="BA" value="387">Bosnia Herzegovina (+387)</option>
											<option data-countryCode="BW" value="267">Botswana (+267)</option>
											<option data-countryCode="BR" value="55">Brazil (+55)</option>
											<option data-countryCode="BN" value="673">Brunei (+673)</option>
											<option data-countryCode="BG" value="359">Bulgaria (+359)</option>
											<option data-countryCode="BF" value="226">Burkina Faso (+226)</option>
											<option data-countryCode="BI" value="257">Burundi (+257)</option>
											<option data-countryCode="KH" value="855">Cambodia (+855)</option>
											<option data-countryCode="CM" value="237">Cameroon (+237)</option>
											<option data-countryCode="CA" value="1">Canada (+1)</option>
											<option data-countryCode="CV" value="238">Cape Verde Islands (+238)</option>
											<option data-countryCode="KY" value="1345">Cayman Islands (+1345)</option>
											<option data-countryCode="CF" value="236">Central African Republic (+236)</option>
											<option data-countryCode="CL" value="56">Chile (+56)</option>
											<option data-countryCode="CN" value="86">China (+86)</option>
											<option data-countryCode="CO" value="57">Colombia (+57)</option>
											<option data-countryCode="KM" value="269">Comoros (+269)</option>
											<option data-countryCode="CG" value="242">Congo (+242)</option>
											<option data-countryCode="CK" value="682">Cook Islands (+682)</option>
											<option data-countryCode="CR" value="506">Costa Rica (+506)</option>
											<option data-countryCode="HR" value="385">Croatia (+385)</option>
											<option data-countryCode="CU" value="53">Cuba (+53)</option>
											<option data-countryCode="CY" value="90392">Cyprus North (+90392)</option>
											<option data-countryCode="CY" value="357">Cyprus South (+357)</option>
											<option data-countryCode="CZ" value="42">Czech Republic (+42)</option>
											<option data-countryCode="DK" value="45">Denmark (+45)</option>
											<option data-countryCode="DJ" value="253">Djibouti (+253)</option>
											<option data-countryCode="DM" value="1809">Dominica (+1809)</option>
											<option data-countryCode="DO" value="1809">Dominican Republic (+1809)</option>
											<option data-countryCode="EC" value="593">Ecuador (+593)</option>
											<option data-countryCode="EG" value="20">Egypt (+20)</option>
											<option data-countryCode="SV" value="503">El Salvador (+503)</option>
											<option data-countryCode="GQ" value="240">Equatorial Guinea (+240)</option>
											<option data-countryCode="ER" value="291">Eritrea (+291)</option>
											<option data-countryCode="EE" value="372">Estonia (+372)</option>
											<option data-countryCode="ET" value="251">Ethiopia (+251)</option>
											<option data-countryCode="FK" value="500">Falkland Islands (+500)</option>
											<option data-countryCode="FO" value="298">Faroe Islands (+298)</option>
											<option data-countryCode="FJ" value="679">Fiji (+679)</option>
											<option data-countryCode="FI" value="358">Finland (+358)</option>
											<option data-countryCode="FR" value="33">France (+33)</option>
											<option data-countryCode="GF" value="594">French Guiana (+594)</option>
											<option data-countryCode="PF" value="689">French Polynesia (+689)</option>
											<option data-countryCode="GA" value="241">Gabon (+241)</option>
											<option data-countryCode="GM" value="220">Gambia (+220)</option>
											<option data-countryCode="GE" value="7880">Georgia (+7880)</option>
											<option data-countryCode="DE" value="49">Germany (+49)</option>
											<option data-countryCode="GH" value="233">Ghana (+233)</option>
											<option data-countryCode="GI" value="350">Gibraltar (+350)</option>
											<option data-countryCode="GR" value="30">Greece (+30)</option>
											<option data-countryCode="GL" value="299">Greenland (+299)</option>
											<option data-countryCode="GD" value="1473">Grenada (+1473)</option>
											<option data-countryCode="GP" value="590">Guadeloupe (+590)</option>
											<option data-countryCode="GU" value="671">Guam (+671)</option>
											<option data-countryCode="GT" value="502">Guatemala (+502)</option>
											<option data-countryCode="GN" value="224">Guinea (+224)</option>
											<option data-countryCode="GW" value="245">Guinea - Bissau (+245)</option>
											<option data-countryCode="GY" value="592">Guyana (+592)</option>
											<option data-countryCode="HT" value="509">Haiti (+509)</option>
											<option data-countryCode="HN" value="504">Honduras (+504)</option>
											<option data-countryCode="HK" value="852">Hong Kong (+852)</option>
											<option data-countryCode="HU" value="36">Hungary (+36)</option>
											<option data-countryCode="IS" value="354">Iceland (+354)</option>
											<option data-countryCode="IN" value="91">India (+91)</option>
											<option data-countryCode="ID" value="62">Indonesia (+62)</option>
											<option data-countryCode="IR" value="98">Iran (+98)</option>
											<option data-countryCode="IQ" value="964">Iraq (+964)</option>
											<option data-countryCode="IE" value="353">Ireland (+353)</option>
											<option data-countryCode="IL" value="972">Israel (+972)</option>
											<option data-countryCode="IT" value="39">Italy (+39)</option>
											<option data-countryCode="JM" value="1876">Jamaica (+1876)</option>
											<option data-countryCode="JP" value="81">Japan (+81)</option>
											<option data-countryCode="JO" value="962">Jordan (+962)</option>
											<option data-countryCode="KZ" value="7">Kazakhstan (+7)</option>
											<option data-countryCode="KE" value="254">Kenya (+254)</option>
											<option data-countryCode="KI" value="686">Kiribati (+686)</option>
											<option data-countryCode="KP" value="850">Korea North (+850)</option>
											<option data-countryCode="KR" value="82">Korea South (+82)</option>
											<option data-countryCode="KW" value="965">Kuwait (+965)</option>
											<option data-countryCode="KG" value="996">Kyrgyzstan (+996)</option>
											<option data-countryCode="LA" value="856">Laos (+856)</option>
											<option data-countryCode="LV" value="371">Latvia (+371)</option>
											<option data-countryCode="LB" value="961">Lebanon (+961)</option>
											<option data-countryCode="LS" value="266">Lesotho (+266)</option>
											<option data-countryCode="LR" value="231">Liberia (+231)</option>
											<option data-countryCode="LY" value="218">Libya (+218)</option>
											<option data-countryCode="LI" value="417">Liechtenstein (+417)</option>
											<option data-countryCode="LT" value="370">Lithuania (+370)</option>
											<option data-countryCode="LU" value="352">Luxembourg (+352)</option>
											<option data-countryCode="MO" value="853">Macao (+853)</option>
											<option data-countryCode="MK" value="389">Macedonia (+389)</option>
											<option data-countryCode="MG" value="261">Madagascar (+261)</option>
											<option data-countryCode="MW" value="265">Malawi (+265)</option>
											<option data-countryCode="MY" value="60">Malaysia (+60)</option>
											<option data-countryCode="MV" value="960">Maldives (+960)</option>
											<option data-countryCode="ML" value="223">Mali (+223)</option>
											<option data-countryCode="MT" value="356">Malta (+356)</option>
											<option data-countryCode="MH" value="692">Marshall Islands (+692)</option>
											<option data-countryCode="MQ" value="596">Martinique (+596)</option>
											<option data-countryCode="MR" value="222">Mauritania (+222)</option>
											<option data-countryCode="YT" value="269">Mayotte (+269)</option>
											<option data-countryCode="MX" value="52">Mexico (+52)</option>
											<option data-countryCode="FM" value="691">Micronesia (+691)</option>
											<option data-countryCode="MD" value="373">Moldova (+373)</option>
											<option data-countryCode="MC" value="377">Monaco (+377)</option>
											<option data-countryCode="MN" value="976">Mongolia (+976)</option>
											<option data-countryCode="MS" value="1664">Montserrat (+1664)</option>
											<option data-countryCode="MA" value="212">Morocco (+212)</option>
											<option data-countryCode="MZ" value="258">Mozambique (+258)</option>
											<option data-countryCode="MN" value="95">Myanmar (+95)</option>
											<option data-countryCode="NA" value="264">Namibia (+264)</option>
											<option data-countryCode="NR" value="674">Nauru (+674)</option>
											<option data-countryCode="NP" value="977">Nepal (+977)</option>
											<option data-countryCode="NL" value="31">Netherlands (+31)</option>
											<option data-countryCode="NC" value="687">New Caledonia (+687)</option>
											<option data-countryCode="NZ" value="64">New Zealand (+64)</option>
											<option data-countryCode="NI" value="505">Nicaragua (+505)</option>
											<option data-countryCode="NE" value="227">Niger (+227)</option>
											<option data-countryCode="NG" value="234">Nigeria (+234)</option>
											<option data-countryCode="NU" value="683">Niue (+683)</option>
											<option data-countryCode="NF" value="672">Norfolk Islands (+672)</option>
											<option data-countryCode="NP" value="670">Northern Marianas (+670)</option>
											<option data-countryCode="NO" value="47">Norway (+47)</option>
											<option data-countryCode="OM" value="968">Oman (+968)</option>
											<option data-countryCode="PW" value="680">Palau (+680)</option>
											<option data-countryCode="PA" value="507">Panama (+507)</option>
											<option data-countryCode="PG" value="675">Papua New Guinea (+675)</option>
											<option data-countryCode="PY" value="595">Paraguay (+595)</option>
											<option data-countryCode="PE" value="51">Peru (+51)</option>
											<option data-countryCode="PH" value="63">Philippines (+63)</option>
											<option data-countryCode="PL" value="48">Poland (+48)</option>
											<option data-countryCode="PT" value="351">Portugal (+351)</option>
											<option data-countryCode="PR" value="1787">Puerto Rico (+1787)</option>
											<option data-countryCode="QA" value="974">Qatar (+974)</option>
											<option data-countryCode="RE" value="262">Reunion (+262)</option>
											<option data-countryCode="RO" value="40">Romania (+40)</option>
											<option data-countryCode="RU" value="7">Russia (+7)</option>
											<option data-countryCode="RW" value="250">Rwanda (+250)</option>
											<option data-countryCode="SM" value="378">San Marino (+378)</option>
											<option data-countryCode="ST" value="239">Sao Tome &amp; Principe (+239)</option>
											<option data-countryCode="SA" value="966">Saudi Arabia (+966)</option>
											<option data-countryCode="SN" value="221">Senegal (+221)</option>
											<option data-countryCode="CS" value="381">Serbia (+381)</option>
											<option data-countryCode="SC" value="248">Seychelles (+248)</option>
											<option data-countryCode="SL" value="232">Sierra Leone (+232)</option>
											<option data-countryCode="SG" value="65">Singapore (+65)</option>
											<option data-countryCode="SK" value="421">Slovak Republic (+421)</option>
											<option data-countryCode="SI" value="386">Slovenia (+386)</option>
											<option data-countryCode="SB" value="677">Solomon Islands (+677)</option>
											<option data-countryCode="SO" value="252">Somalia (+252)</option>
											<option data-countryCode="ZA" value="27">South Africa (+27)</option>
											<option data-countryCode="ES" value="34">Spain (+34)</option>
											<option data-countryCode="LK" value="94">Sri Lanka (+94)</option>
											<option data-countryCode="SH" value="290">St. Helena (+290)</option>
											<option data-countryCode="KN" value="1869">St. Kitts (+1869)</option>
											<option data-countryCode="SC" value="1758">St. Lucia (+1758)</option>
											<option data-countryCode="SD" value="249">Sudan (+249)</option>
											<option data-countryCode="SR" value="597">Suriname (+597)</option>
											<option data-countryCode="SZ" value="268">Swaziland (+268)</option>
											<option data-countryCode="SE" value="46">Sweden (+46)</option>
											<option data-countryCode="CH" value="41">Switzerland (+41)</option>
											<option data-countryCode="SI" value="963">Syria (+963)</option>
											<option data-countryCode="TW" value="886">Taiwan (+886)</option>
											<option data-countryCode="TJ" value="7">Tajikstan (+7)</option>
											<option data-countryCode="TH" value="66">Thailand (+66)</option>
											<option data-countryCode="TG" value="228">Togo (+228)</option>
											<option data-countryCode="TO" value="676">Tonga (+676)</option>
											<option data-countryCode="TT" value="1868">Trinidad &amp; Tobago (+1868)</option>
											<option data-countryCode="TN" value="216">Tunisia (+216)</option>
											<option data-countryCode="TR" value="90">Turkey (+90)</option>
											<option data-countryCode="TM" value="7">Turkmenistan (+7)</option>
											<option data-countryCode="TM" value="993">Turkmenistan (+993)</option>
											<option data-countryCode="TC" value="1649">Turks &amp; Caicos Islands (+1649)</option>
											<option data-countryCode="TV" value="688">Tuvalu (+688)</option>
											<option data-countryCode="UG" value="256">Uganda (+256)</option>
											<option data-countryCode="GB" value="44">UK (+44)</option>
											<option data-countryCode="UA" value="380">Ukraine (+380)</option>
											<option data-countryCode="AE" value="971">United Arab Emirates (+971)</option>
											<option data-countryCode="UY" value="598">Uruguay (+598)</option>
											<option data-countryCode="US" value="1">USA (+1)</option>
											<option data-countryCode="UZ" value="7">Uzbekistan (+7)</option>
											<option data-countryCode="VU" value="678">Vanuatu (+678)</option>
											<option data-countryCode="VA" value="379">Vatican City (+379)</option>
											<option data-countryCode="VE" value="58">Venezuela (+58)</option>
											<option data-countryCode="VN" value="84">Vietnam (+84)</option>
											<option data-countryCode="VG" value="84">Virgin Islands - British (+1284)</option>
											<option data-countryCode="VI" value="84">Virgin Islands - US (+1340)</option>
											<option data-countryCode="WF" value="681">Wallis &amp; Futuna (+681)</option>
											<option data-countryCode="YE" value="969">Yemen (North)(+969)</option>
											<option data-countryCode="YE" value="967">Yemen (South)(+967)</option>
											<option data-countryCode="ZM" value="260">Zambia (+260)</option>
											<option data-countryCode="ZW" value="263">Zimbabwe (+263)</option>
									</select>
									<input type="text" name="contact2" style="width:56%;display:inline" placeholder="Contact Number 2" required>
									<div class="help-block with-errors"></div>
								</div>
								<div class="form-group">
									<select name="c_code3" style="width:43%;display:inline;font-size: 11px;">
											<option data-countryCode="DZ" value="213">Algeria (+213)</option>
											<option data-countryCode="AD" value="376">Andorra (+376)</option>
											<option data-countryCode="AO" value="244">Angola (+244)</option>
											<option data-countryCode="AI" value="1264">Anguilla (+1264)</option>
											<option data-countryCode="AG" value="1268">Antigua &amp; Barbuda (+1268)</option>
											<option data-countryCode="AR" value="54">Argentina (+54)</option>
											<option data-countryCode="AM" value="374">Armenia (+374)</option>
											<option data-countryCode="AW" value="297">Aruba (+297)</option>
											<option data-countryCode="AU" value="61">Australia (+61)</option>
											<option data-countryCode="AT" value="43">Austria (+43)</option>
											<option data-countryCode="AZ" value="994">Azerbaijan (+994)</option>
											<option data-countryCode="BS" value="1242">Bahamas (+1242)</option>
											<option data-countryCode="BH" value="973">Bahrain (+973)</option>
											<option data-countryCode="BD" value="880">Bangladesh (+880)</option>
											<option data-countryCode="BB" value="1246">Barbados (+1246)</option>
											<option data-countryCode="BY" value="375">Belarus (+375)</option>
											<option data-countryCode="BE" value="32">Belgium (+32)</option>
											<option data-countryCode="BZ" value="501">Belize (+501)</option>
											<option data-countryCode="BJ" value="229">Benin (+229)</option>
											<option data-countryCode="BM" value="1441">Bermuda (+1441)</option>
											<option data-countryCode="BT" value="975">Bhutan (+975)</option>
											<option data-countryCode="BO" value="591">Bolivia (+591)</option>
											<option data-countryCode="BA" value="387">Bosnia Herzegovina (+387)</option>
											<option data-countryCode="BW" value="267">Botswana (+267)</option>
											<option data-countryCode="BR" value="55">Brazil (+55)</option>
											<option data-countryCode="BN" value="673">Brunei (+673)</option>
											<option data-countryCode="BG" value="359">Bulgaria (+359)</option>
											<option data-countryCode="BF" value="226">Burkina Faso (+226)</option>
											<option data-countryCode="BI" value="257">Burundi (+257)</option>
											<option data-countryCode="KH" value="855">Cambodia (+855)</option>
											<option data-countryCode="CM" value="237">Cameroon (+237)</option>
											<option data-countryCode="CA" value="1">Canada (+1)</option>
											<option data-countryCode="CV" value="238">Cape Verde Islands (+238)</option>
											<option data-countryCode="KY" value="1345">Cayman Islands (+1345)</option>
											<option data-countryCode="CF" value="236">Central African Republic (+236)</option>
											<option data-countryCode="CL" value="56">Chile (+56)</option>
											<option data-countryCode="CN" value="86">China (+86)</option>
											<option data-countryCode="CO" value="57">Colombia (+57)</option>
											<option data-countryCode="KM" value="269">Comoros (+269)</option>
											<option data-countryCode="CG" value="242">Congo (+242)</option>
											<option data-countryCode="CK" value="682">Cook Islands (+682)</option>
											<option data-countryCode="CR" value="506">Costa Rica (+506)</option>
											<option data-countryCode="HR" value="385">Croatia (+385)</option>
											<option data-countryCode="CU" value="53">Cuba (+53)</option>
											<option data-countryCode="CY" value="90392">Cyprus North (+90392)</option>
											<option data-countryCode="CY" value="357">Cyprus South (+357)</option>
											<option data-countryCode="CZ" value="42">Czech Republic (+42)</option>
											<option data-countryCode="DK" value="45">Denmark (+45)</option>
											<option data-countryCode="DJ" value="253">Djibouti (+253)</option>
											<option data-countryCode="DM" value="1809">Dominica (+1809)</option>
											<option data-countryCode="DO" value="1809">Dominican Republic (+1809)</option>
											<option data-countryCode="EC" value="593">Ecuador (+593)</option>
											<option data-countryCode="EG" value="20">Egypt (+20)</option>
											<option data-countryCode="SV" value="503">El Salvador (+503)</option>
											<option data-countryCode="GQ" value="240">Equatorial Guinea (+240)</option>
											<option data-countryCode="ER" value="291">Eritrea (+291)</option>
											<option data-countryCode="EE" value="372">Estonia (+372)</option>
											<option data-countryCode="ET" value="251">Ethiopia (+251)</option>
											<option data-countryCode="FK" value="500">Falkland Islands (+500)</option>
											<option data-countryCode="FO" value="298">Faroe Islands (+298)</option>
											<option data-countryCode="FJ" value="679">Fiji (+679)</option>
											<option data-countryCode="FI" value="358">Finland (+358)</option>
											<option data-countryCode="FR" value="33">France (+33)</option>
											<option data-countryCode="GF" value="594">French Guiana (+594)</option>
											<option data-countryCode="PF" value="689">French Polynesia (+689)</option>
											<option data-countryCode="GA" value="241">Gabon (+241)</option>
											<option data-countryCode="GM" value="220">Gambia (+220)</option>
											<option data-countryCode="GE" value="7880">Georgia (+7880)</option>
											<option data-countryCode="DE" value="49">Germany (+49)</option>
											<option data-countryCode="GH" value="233">Ghana (+233)</option>
											<option data-countryCode="GI" value="350">Gibraltar (+350)</option>
											<option data-countryCode="GR" value="30">Greece (+30)</option>
											<option data-countryCode="GL" value="299">Greenland (+299)</option>
											<option data-countryCode="GD" value="1473">Grenada (+1473)</option>
											<option data-countryCode="GP" value="590">Guadeloupe (+590)</option>
											<option data-countryCode="GU" value="671">Guam (+671)</option>
											<option data-countryCode="GT" value="502">Guatemala (+502)</option>
											<option data-countryCode="GN" value="224">Guinea (+224)</option>
											<option data-countryCode="GW" value="245">Guinea - Bissau (+245)</option>
											<option data-countryCode="GY" value="592">Guyana (+592)</option>
											<option data-countryCode="HT" value="509">Haiti (+509)</option>
											<option data-countryCode="HN" value="504">Honduras (+504)</option>
											<option data-countryCode="HK" value="852">Hong Kong (+852)</option>
											<option data-countryCode="HU" value="36">Hungary (+36)</option>
											<option data-countryCode="IS" value="354">Iceland (+354)</option>
											<option data-countryCode="IN" value="91">India (+91)</option>
											<option data-countryCode="ID" value="62">Indonesia (+62)</option>
											<option data-countryCode="IR" value="98">Iran (+98)</option>
											<option data-countryCode="IQ" value="964">Iraq (+964)</option>
											<option data-countryCode="IE" value="353">Ireland (+353)</option>
											<option data-countryCode="IL" value="972">Israel (+972)</option>
											<option data-countryCode="IT" value="39">Italy (+39)</option>
											<option data-countryCode="JM" value="1876">Jamaica (+1876)</option>
											<option data-countryCode="JP" value="81">Japan (+81)</option>
											<option data-countryCode="JO" value="962">Jordan (+962)</option>
											<option data-countryCode="KZ" value="7">Kazakhstan (+7)</option>
											<option data-countryCode="KE" value="254">Kenya (+254)</option>
											<option data-countryCode="KI" value="686">Kiribati (+686)</option>
											<option data-countryCode="KP" value="850">Korea North (+850)</option>
											<option data-countryCode="KR" value="82">Korea South (+82)</option>
											<option data-countryCode="KW" value="965">Kuwait (+965)</option>
											<option data-countryCode="KG" value="996">Kyrgyzstan (+996)</option>
											<option data-countryCode="LA" value="856">Laos (+856)</option>
											<option data-countryCode="LV" value="371">Latvia (+371)</option>
											<option data-countryCode="LB" value="961">Lebanon (+961)</option>
											<option data-countryCode="LS" value="266">Lesotho (+266)</option>
											<option data-countryCode="LR" value="231">Liberia (+231)</option>
											<option data-countryCode="LY" value="218">Libya (+218)</option>
											<option data-countryCode="LI" value="417">Liechtenstein (+417)</option>
											<option data-countryCode="LT" value="370">Lithuania (+370)</option>
											<option data-countryCode="LU" value="352">Luxembourg (+352)</option>
											<option data-countryCode="MO" value="853">Macao (+853)</option>
											<option data-countryCode="MK" value="389">Macedonia (+389)</option>
											<option data-countryCode="MG" value="261">Madagascar (+261)</option>
											<option data-countryCode="MW" value="265">Malawi (+265)</option>
											<option data-countryCode="MY" value="60">Malaysia (+60)</option>
											<option data-countryCode="MV" value="960">Maldives (+960)</option>
											<option data-countryCode="ML" value="223">Mali (+223)</option>
											<option data-countryCode="MT" value="356">Malta (+356)</option>
											<option data-countryCode="MH" value="692">Marshall Islands (+692)</option>
											<option data-countryCode="MQ" value="596">Martinique (+596)</option>
											<option data-countryCode="MR" value="222">Mauritania (+222)</option>
											<option data-countryCode="YT" value="269">Mayotte (+269)</option>
											<option data-countryCode="MX" value="52">Mexico (+52)</option>
											<option data-countryCode="FM" value="691">Micronesia (+691)</option>
											<option data-countryCode="MD" value="373">Moldova (+373)</option>
											<option data-countryCode="MC" value="377">Monaco (+377)</option>
											<option data-countryCode="MN" value="976">Mongolia (+976)</option>
											<option data-countryCode="MS" value="1664">Montserrat (+1664)</option>
											<option data-countryCode="MA" value="212">Morocco (+212)</option>
											<option data-countryCode="MZ" value="258">Mozambique (+258)</option>
											<option data-countryCode="MN" value="95">Myanmar (+95)</option>
											<option data-countryCode="NA" value="264">Namibia (+264)</option>
											<option data-countryCode="NR" value="674">Nauru (+674)</option>
											<option data-countryCode="NP" value="977">Nepal (+977)</option>
											<option data-countryCode="NL" value="31">Netherlands (+31)</option>
											<option data-countryCode="NC" value="687">New Caledonia (+687)</option>
											<option data-countryCode="NZ" value="64">New Zealand (+64)</option>
											<option data-countryCode="NI" value="505">Nicaragua (+505)</option>
											<option data-countryCode="NE" value="227">Niger (+227)</option>
											<option data-countryCode="NG" value="234">Nigeria (+234)</option>
											<option data-countryCode="NU" value="683">Niue (+683)</option>
											<option data-countryCode="NF" value="672">Norfolk Islands (+672)</option>
											<option data-countryCode="NP" value="670">Northern Marianas (+670)</option>
											<option data-countryCode="NO" value="47">Norway (+47)</option>
											<option data-countryCode="OM" value="968">Oman (+968)</option>
											<option data-countryCode="PW" value="680">Palau (+680)</option>
											<option data-countryCode="PA" value="507">Panama (+507)</option>
											<option data-countryCode="PG" value="675">Papua New Guinea (+675)</option>
											<option data-countryCode="PY" value="595">Paraguay (+595)</option>
											<option data-countryCode="PE" value="51">Peru (+51)</option>
											<option data-countryCode="PH" value="63">Philippines (+63)</option>
											<option data-countryCode="PL" value="48">Poland (+48)</option>
											<option data-countryCode="PT" value="351">Portugal (+351)</option>
											<option data-countryCode="PR" value="1787">Puerto Rico (+1787)</option>
											<option data-countryCode="QA" value="974">Qatar (+974)</option>
											<option data-countryCode="RE" value="262">Reunion (+262)</option>
											<option data-countryCode="RO" value="40">Romania (+40)</option>
											<option data-countryCode="RU" value="7">Russia (+7)</option>
											<option data-countryCode="RW" value="250">Rwanda (+250)</option>
											<option data-countryCode="SM" value="378">San Marino (+378)</option>
											<option data-countryCode="ST" value="239">Sao Tome &amp; Principe (+239)</option>
											<option data-countryCode="SA" value="966">Saudi Arabia (+966)</option>
											<option data-countryCode="SN" value="221">Senegal (+221)</option>
											<option data-countryCode="CS" value="381">Serbia (+381)</option>
											<option data-countryCode="SC" value="248">Seychelles (+248)</option>
											<option data-countryCode="SL" value="232">Sierra Leone (+232)</option>
											<option data-countryCode="SG" value="65">Singapore (+65)</option>
											<option data-countryCode="SK" value="421">Slovak Republic (+421)</option>
											<option data-countryCode="SI" value="386">Slovenia (+386)</option>
											<option data-countryCode="SB" value="677">Solomon Islands (+677)</option>
											<option data-countryCode="SO" value="252">Somalia (+252)</option>
											<option data-countryCode="ZA" value="27">South Africa (+27)</option>
											<option data-countryCode="ES" value="34">Spain (+34)</option>
											<option data-countryCode="LK" value="94">Sri Lanka (+94)</option>
											<option data-countryCode="SH" value="290">St. Helena (+290)</option>
											<option data-countryCode="KN" value="1869">St. Kitts (+1869)</option>
											<option data-countryCode="SC" value="1758">St. Lucia (+1758)</option>
											<option data-countryCode="SD" value="249">Sudan (+249)</option>
											<option data-countryCode="SR" value="597">Suriname (+597)</option>
											<option data-countryCode="SZ" value="268">Swaziland (+268)</option>
											<option data-countryCode="SE" value="46">Sweden (+46)</option>
											<option data-countryCode="CH" value="41">Switzerland (+41)</option>
											<option data-countryCode="SI" value="963">Syria (+963)</option>
											<option data-countryCode="TW" value="886">Taiwan (+886)</option>
											<option data-countryCode="TJ" value="7">Tajikstan (+7)</option>
											<option data-countryCode="TH" value="66">Thailand (+66)</option>
											<option data-countryCode="TG" value="228">Togo (+228)</option>
											<option data-countryCode="TO" value="676">Tonga (+676)</option>
											<option data-countryCode="TT" value="1868">Trinidad &amp; Tobago (+1868)</option>
											<option data-countryCode="TN" value="216">Tunisia (+216)</option>
											<option data-countryCode="TR" value="90">Turkey (+90)</option>
											<option data-countryCode="TM" value="7">Turkmenistan (+7)</option>
											<option data-countryCode="TM" value="993">Turkmenistan (+993)</option>
											<option data-countryCode="TC" value="1649">Turks &amp; Caicos Islands (+1649)</option>
											<option data-countryCode="TV" value="688">Tuvalu (+688)</option>
											<option data-countryCode="UG" value="256">Uganda (+256)</option>
											<option data-countryCode="GB" value="44">UK (+44)</option>
											<option data-countryCode="UA" value="380">Ukraine (+380)</option>
											<option data-countryCode="AE" value="971">United Arab Emirates (+971)</option>
											<option data-countryCode="UY" value="598">Uruguay (+598)</option>
											<option data-countryCode="US" value="1">USA (+1)</option>
											<option data-countryCode="UZ" value="7">Uzbekistan (+7)</option>
											<option data-countryCode="VU" value="678">Vanuatu (+678)</option>
											<option data-countryCode="VA" value="379">Vatican City (+379)</option>
											<option data-countryCode="VE" value="58">Venezuela (+58)</option>
											<option data-countryCode="VN" value="84">Vietnam (+84)</option>
											<option data-countryCode="VG" value="84">Virgin Islands - British (+1284)</option>
											<option data-countryCode="VI" value="84">Virgin Islands - US (+1340)</option>
											<option data-countryCode="WF" value="681">Wallis &amp; Futuna (+681)</option>
											<option data-countryCode="YE" value="969">Yemen (North)(+969)</option>
											<option data-countryCode="YE" value="967">Yemen (South)(+967)</option>
											<option data-countryCode="ZM" value="260">Zambia (+260)</option>
											<option data-countryCode="ZW" value="263">Zimbabwe (+263)</option>
									</select>
									<input type="text" name="contact3" style="width:56%;display:inline" placeholder="Contact Number 3" required>
									<div class="help-block with-errors"></div>
								</div>
								<div class="form-group">
									<input type="text" name="country" placeholder="Country" required>
									<div class="help-block with-errors"></div>
								</div>
								<div class="form-group">
									<input type="text" name="city" placeholder="City/State" required>
									<div class="help-block with-errors"></div>
								</div>
								<div class="form-group">
									<input type="text" name="zip" placeholder="Post/Zip Code" required>
									<div class="help-block with-errors"></div>
								</div>
								<div class="form-group">
									<input type="email" name="email" id="add_email" placeholder="Email Address" required>
									<div class="help-block with-errors"></div>
								</div>
								<div class="form-group">
									<input type="password" data-minlength="6" class="password" name="password" id="add_pass" placeholder="Password" required>
									<div class="help-block with-errors"></div>
								</div>
								<div class="form-group">
									<input type="password" data-minlength="6" class="password" name="password2" id="password2" placeholder="Confirm Password" required>
									<div class="help-block with-errors"></div>
								</div>
								<div class="form-group">
									<label for="ad_role">Membership ($40)</label>
									<select name="ad_role" id="ad_role">
										<option value="1">Basic</option>
										<option value="2">Standard</option>
									</select>
								</div>
								<input type="submit" value="Sign Up">
							</form>
						
						</div>
						<p>By clicking register, I agree to your terms</p>
					</div>
				</div>
			</div>
		</div>
	</div>
	
	<!-- //Modal2 -->
	
	<!-- contact -->
	<div class="contact">
		<div class="container">
			<h3 class="agileits-title">DASHBOARD</h3>
			<?php if($this->session->userdata('u_role') == 3){ ?>
				<div>
					<div>Number of Visitors: <strong class="text-danger" style="font-size:20px;"><?php echo $visitors; ?></strong></div>
				</div>
			<?php } ?>
				
			<input type="hidden" id="baseurl" value="<?php echo base_url();?>">
			<input type="hidden" id="usr_id" value="<?php echo $this->session->userdata('u_id');?>">
			<div class="contact-agileinfo">
				<div class="top_tabs_agile">
					<div id="horizontalTab" class="top_tabs_agile" style="display: block; width: 100%; margin: 0px;">
						<ul class="resp-tabs-list">
							<li class="resp-tab-item resp-tab-active" aria-controls="tab_item-0" role="tab">
								<i class="fa fa-user" aria-hidden="true"></i> Profile
							</li>
							<li class="resp-tab-item" aria-controls="tab_item-1" role="tab">
								<i class="fa fa-lock" aria-hidden="true"></i> Change Password
							</li>
							<?php if($this->session->userdata('u_role') == 3){ ?><li class="resp-tab-item" aria-controls="tab_item-2" role="tab">
								<i class="fa fa-cogs" aria-hidden="true"></i> Manage Users
							</li>
							<?php } ?>
							<?php if($this->session->userdata('u_role') == 2 || $this->session->userdata('u_role') == 3){ ?>
								<li class="resp-tab-item" aria-controls="tab_item-3" role="tab">
									<i class="fa fa-paw" aria-hidden="true"></i> My Pets
								</li>
							<?php } ?>		
							<?php if($this->session->userdata('u_role') == 3){ ?>
								<li class="resp-tab-item" aria-controls="tab_item-4" role="tab">
									<i class="fa fa-shopping-cart" aria-hidden="true"></i> Manage Shop
								</li>
							<?php } ?>
							<?php if($this->session->userdata('u_role') == 2 || $this->session->userdata('u_role') == 3){ ?>
								<li class="resp-tab-item" aria-controls="tab_item-5" role="tab">
									<i class="fa fa-users" aria-hidden="true"></i> List of Person
								</li>
							<?php } ?>	
						</ul>
						<div class="resp-tabs-container">
							<h2 class="resp-accordion resp-tab-active" role="tab" aria-controls="tab_item-0">
								<span class="resp-arrow"></span>
							</h2>
							<div class="tab1 resp-tab-content resp-tab-content-active" aria-labelledby="tab_item-0" style="display:block">
								<div class="services-right-agileits">
									<h4>My Profile<button type="button" class="btn btn-primary" style="margin-right: 10px;float:right;    font-family: 'Roboto', sans-serif;" data-toggle="modal" data-target="#contactIDOrderModal"><i class="fa fa-shopping-bag"></i> Order ID Card</button></h4>
									<div class="alert alert-success display-none" id="updateUserSuccess">
										<p><strong>Success!</strong> Your account has been updated.</p>
									</div>
									<div class="alert alert-danger display-none" id="updateUserError">
										<p><strong>Error!</strong> Please try again.</p>
									</div>
									<div class="alert alert-danger display-none" id="updateCountryCodeError">
										<p><strong>Error!</strong> Please include country code for each contact number.</p>
									</div>
									<div>
										
									</div>
									<div class="row">
										<form id="UpdateUserData" method="post" data-toggle="validator" role="form">
										<div class="col-md-6 contact-right">	
											<p style='font-weight:bold;font-size:16px;padding-bottom:10px'>Membership: <span id="memberPackage" class="text-primary"></span></p>
											<div class="form-group">
												<label for="upd_email">Email Address</label>
												<input type="email" name="upd_email" placeholder="Email Address" disabled>
											</div>
											<div class="form-group">
												<label for="upd_name">Full name</label>
												<input type="text" name="upd_name" placeholder="Full name" required disabled>
												<div class="help-block with-errors"></div>
											</div>
											<div class="form-group">
												<label for="upd_country">Country</label>
												<input type="text" name="upd_country" placeholder="Country" required disabled>
												<div class="help-block with-errors"></div>
											</div>
											<div class="form-group">
												<label for="upd_city">City/State</label>
												<input type="text" name="upd_city" placeholder="City/State" required disabled>
												<div class="help-block with-errors"></div>
											</div>
											
										</div>
										<div class="col-md-6 contact-left">
											<p style='font-weight:bold;font-size:16px;padding-bottom:10px'>Member Expiration: <span id="memberExp" class="text-primary"></span></p>
											<div class="form-group">
												<label for="upd_zip">Post/Zip Code</label>
												<input type="text" name="upd_zip" placeholder="Post/Zip Code" required disabled>
												<div class="help-block with-errors"></div>
											</div>
											<div class="form-group">
												<label for="upd_contact1" style="display:block">Contact Number 1</label>
												<input type="text" name="upd_c_code1" placeholder="CCode" style="width:25%;display:inline" required disabled><input type="text" name="upd_contact1" style="width:75%;display:inline" placeholder="Contact Number 1" required disabled>
												<div class="help-block with-errors"></div>
											</div>
											<div class="form-group">
												<label for="upd_contact2" style="display:block">Contact Number 2</label>
												<input type="text" name="upd_c_code2" placeholder="CCode" required style="width:25%;display:inline" disabled><input type="text" name="upd_contact2" style="width:75%;display:inline" placeholder="Contact Number 2" required disabled>
												<div class="help-block with-errors"></div>
											</div>
											<div class="form-group">
												<label for="upd_contact3" style="display:block">Contact Number 3</label>
												<input type="text" name="upd_c_code3" placeholder="CCode" required style="width:25%;display:inline" disabled><input type="text" name="upd_contact3" style="width:75%;display:inline" placeholder="Contact Number 3" required disabled>
												<div class="help-block with-errors"></div>
											</div>
											<div class="form-group">
												<input type="submit" id="btn_updateUserEDIT" value="EDIT" style="float:right">
												<input type="submit" id="btn_updateUser" value="SAVE" style="display:none;float:right">
											</div>
										</div>
										</form>
									</div>
								</div>
							</div>

							<h2 class="resp-accordion" role="tab" aria-controls="tab_item-1">
								<span class="resp-arrow"></span>
							</h2>
							<div class="tab2 resp-tab-content" aria-labelledby="tab_item-1">
								<div class="services-right-agileits bar-grids bargrids-left">
									<h4>Change Password</h4>
									<div class="alert alert-success display-none" id="updateUserPassSuccess">
										<p><strong>Success!</strong> Your password has been updated.</p>
									</div>
									<div class="alert alert-danger display-none" id="updateUserPassError">
										<p><strong>Error!</strong> Please try again.</p>
									</div>
									<div class="alert alert-danger display-none" id="updatePassNotEqualError">
										<p><strong>Error!</strong> New password does not match confirm new password.</p>
									</div>
									<div class="alert alert-danger display-none" id="updateWrongPassError">
										<p><strong>Error!</strong> Old password is incorrect.</p>
									</div>
									<div class="row">
										<form id="updatePassword" method="post" data-toggle="validator" role="form">
											<div class="col-md-8 contact-left">
												<div class="form-group">
													<input type="password" data-minlength="6" class="password" name="upd_password1" placeholder="Old Password" required>
													<div class="help-block with-errors"></div>
												</div>
												<div class="form-group">
													<input type="password" data-minlength="6" class="password" name="upd_password2" placeholder="New Password" required>
													<div class="help-block with-errors"></div>
												</div>
												<div class="form-group">
													<input type="password" data-minlength="6" class="password" name="upd_password3" placeholder="Confirm New Password" required>
													<div class="help-block with-errors"></div>
												</div>
												<div class="form-group">
													<input type="submit" value="SAVE">
												</div>
											</div>
										</form>
									</div>
								</div>
							</div>
							<?php if($this->session->userdata('u_role') == 3){ ?>
							<h2 class="resp-accordion" role="tab" aria-controls="tab_item-2">
								<span class="resp-arrow"></span>
							</h2>
							<div class="tab3 resp-tab-content" aria-labelledby="tab_item-2">
								<div class="services-right-agileits img-top">
									<h4>Manage Users</h4>
									<table id="usersTable" class="table table-striped table-bordered" style="width:100%;display:none">
										<thead>
											<tr>
												<th scope="col text-center">Full Name</th>
												<th scope="col text-center">Email</th>
												<th scope="col text-center">Contact 1</th>
												<th scope="col text-center">Contact 2</th>
												<th scope="col text-center">Contact 3</th>
												<th scope="col text-center">City</th>
												<th scope="col text-center" style="width:25px">Edit</th>
												<th scope="col text-center" style="width:25px">Remove</th>
											</tr>
										</thead>
										<tbody id="tbodyallusers">
											
										</tbody>
									</table>
									<div style="display:none" id="nousers" class="alert alert-info" role="alert">
										<h4>No data to be displayed.</h4>
									</div>
								</div>
							</div>
							<?php } ?>
							<h2 class="resp-accordion" role="tab" aria-controls="tab_item-3">
								<span class="resp-arrow"></span>
							</h2>
							<div class="tab2 resp-tab-content" aria-labelledby="tab_item-3">
								<div class="services-right-agileits img-top">
									<button type="button" class="btn btn-primary" id="addPet" style="float:right"><i class="fa fa-plus"></i> Add Pet</button>
									<button type="button" class="btn btn-primary" style="float:right;margin-right: 10px;" data-toggle="modal" data-target="#posterModal"><i class="fa fa-download"></i> Download Lost Poster</button>
									<button type="button" class="btn btn-primary" style="float:right;margin-right: 10px;" data-toggle="modal" data-target="#foundPosterModal"><i class="fa fa-download"></i> Download Found Poster</button>
									<button type="button" class="btn btn-primary" style="float:right;margin-right: 10px;" data-toggle="modal" data-target="#tagOrderModal"><i class="fa fa-shopping-bag"></i> Order Tag</button>
									
									<h4>My Pets</h4>
									<input type="hidden" id="myRole" value="<?php echo $this->session->userdata('u_role'); ?>">
									<table id="petsTable" class="table table-striped table-bordered" style="width:100%;display:none">
										<thead>
											<tr>
												<th scope="col text-center">Pet Name</th>
												<th scope="col text-center">Specie</th>
												<th scope="col text-center">Gender</th>
												<th scope="col text-center">Fur</th>
												<th scope="col text-center">Fur Color</th>
												<th scope="col text-center">Size</th>
												<th scope="col text-center">Status</th>
												<th scope="col text-center" style="width:25px">Edit</th>
												<th scope="col text-center" style="width:25px">Remove</th>
												<th scope="col text-center" style="width:25px">Share</th>
											</tr>
										</thead>
										<tbody id="tbodyallpets">
											
										</tbody>
									</table>
									<div style="display:none" id="nopets" class="alert alert-info" role="alert">
										<h4>No data to be displayed.</h4>
									</div>
								</div>
							</div>
							<?php if($this->session->userdata('u_role') == 3){ ?>
								<h2 class="resp-accordion" role="tab" aria-controls="tab_item-4">
									<span class="resp-arrow"></span>
								</h2>
								<div class="tab2 resp-tab-content" aria-labelledby="tab_item-4">
									<div class="services-right-agileits img-top">
									<button type="button" class="btn btn-primary" id="addProduct" style="float:right"><i class="fa fa-plus"></i> Add Product</button>
										<h4>Manage Shop</h4>
										<ul class="nav nav-tabs">
											<li class="active">
												<a  href="#1a" data-toggle="tab">Product List</a>
											</li>
											<li>
												<a  href="#2a" data-toggle="tab">For Delivery</a>
											</li>
											<li>
												<a  href="#3a" data-toggle="tab">Shipped</a>
											</li>
											<li>
												<a  href="#4a" data-toggle="tab">Pet Tag Orders</a>
											</li>
											<li>
												<a  href="#5a" data-toggle="tab">ContactID Card Orders</a>
											</li>
										</ul>
										<div class="tab-content clearfix">
											<div class="tab-pane active" id="1a">
												<table id="productTable" class="table table-striped table-bordered" style="width:100%;display:none">
													<thead>
														<tr>
															<th scope="col" style="text-align:left">Product Name</th>
															<th scope="col">Picture 1</th>
															<th scope="col">Picture 2</th>
															<th scope="col">Description</th>
															<th scope="col">Price (USD)</th>
															<th scope="col">Weight (g)</th>
															<th scope="col text-center" style="width:25px">Edit</th>
															<th scope="col text-center" style="width:25px">Remove</th>
														</tr>
													</thead>
													<tbody id="tbodyallproduct">
														
													</tbody>
												</table>
												<div style="display:none" id="noproduct" class="alert alert-info" role="alert">
													<h4>No data to be displayed.</h4>
												</div>
											</div>
											<div class="tab-pane" id="2a">
												<table id="deliveryTable" class="table table-striped table-bordered" style="width:100%;display:none">
													<thead>
														<tr>
															<th scope="col text-center">Order Number</th>
															<th scope="col text-center">Name</th>
															<th scope="col text-center">Home Address</th>
															<th scope="col text-center">Email Address</th>
															<th scope="col text-center">Contact 1</th>
															<th scope="col text-center">Contact 2</th>
															<th scope="col text-center">Date Ordered</th>
															<th scope="col text-center" style="width:25px">Actions</th>
														</tr>
													</thead>
													<tbody id="tbodyalldelivery">
														
													</tbody>
												</table>
												<div style="display:none" id="nodelivery" class="alert alert-info" role="alert">
													<h4>No data to be displayed.</h4>
												</div>
											</div>
											<div class="tab-pane" id="3a">
												<table id="shipTable" class="table table-striped table-bordered" style="width:100%;display:none">
													<thead>
														<tr>
															<th scope="col text-center">Order Number</th>
															<th scope="col text-center">Name</th>
															<th scope="col text-center">Home Address</th>
															<th scope="col text-center">Email Address</th>
															<th scope="col text-center">Contact 1</th>
															<th scope="col text-center">Contact 2</th>
															<th scope="col text-center">Date Shipped</th>
															<th scope="col text-center" style="width:25px">View</th>
														</tr>
													</thead>
													<tbody id="tbodyallship">
														
													</tbody>
												</table>
												<div style="display:none" id="noship" class="alert alert-info" role="alert">
													<h4>No data to be displayed.</h4>
												</div>
											</div>
											<div class="tab-pane" id="4a">
												<table id="tagOrderTable" class="table table-striped table-bordered" style="width:100%;display:none">
													<thead>
														<tr>
															<th scope="col text-center">Order Number</th>
															<th scope="col text-center">Pet Name</th>
															<th scope="col text-center">Home Address</th>
															<th scope="col text-center">Email Address</th>
															<th scope="col text-center">Contact 1</th>
															<th scope="col text-center">Contact 2</th>
															<th scope="col text-center">Status</th>
															<th scope="col text-center">Date Ordered</th>
															<th scope="col text-center" style="width:25px">Actions</th>
														</tr>
													</thead>
													<tbody id="tbodytagOrder">
														
													</tbody>
												</table>
												<div style="display:none" id="notagorder" class="alert alert-info" role="alert">
													<h4>No data to be displayed.</h4>
												</div>
											</div>
											<div class="tab-pane" id="5a">
												<table id="contactIDOrderTable" class="table table-striped table-bordered" style="width:100%;display:none">
													<thead>
														<tr>
															<th scope="col text-center">Order Number</th>
															<th scope="col text-center">Full Name</th>
															<th scope="col text-center">Home Address</th>
															<th scope="col text-center">Email Address</th>
															<th scope="col text-center">Contact 1</th>
															<th scope="col text-center">Contact 2</th>
															<th scope="col text-center">Status</th>
															<th scope="col text-center">Date Ordered</th>
															<th scope="col text-center" style="width:25px">Actions</th>
														</tr>
													</thead>
													<tbody id="tbodyContactIDOrder">
														
													</tbody>
												</table>
												<div style="display:none" id="nocontactidorder" class="alert alert-info" role="alert">
													<h4>No data to be displayed.</h4>
												</div>
											</div>
										</div>
									</div>
								</div>
							<?php } ?>
							<h2 class="resp-accordion" role="tab" aria-controls="tab_item-5">
								<span class="resp-arrow"></span>
							</h2>
							<div class="tab2 resp-tab-content" aria-labelledby="tab_item-5">
								<div class="services-right-agileits img-top">
									<button type="button" class="btn btn-primary" id="addPerson" style="float:right"><i class="fa fa-plus"></i> Add Person</button>
									<button type="button" class="btn btn-primary" style="float:right;margin-right: 10px;" data-toggle="modal" data-target="#personposterModal"><i class="fa fa-download"></i> Download Missing Poster</button>
									<button type="button" class="btn btn-primary" style="float:right;margin-right: 10px;" data-toggle="modal" data-target="#personfoundPosterModal"><i class="fa fa-download"></i> Download Found Poster</button>
									<h4>My People</h4>
									<table id="personTable" class="table table-striped table-bordered" style="width:100%;display:none">
										<thead>
											<tr>
												<th scope="col text-center">Person Name</th>
												<th scope="col text-center">Country, City</th>
												<th scope="col text-center">Gender</th>
												<th scope="col text-center">Contact 1</th>
												<th scope="col text-center">Eye Color</th>
												<th scope="col text-center">Hair Color</th>
												<th scope="col text-center">Status</th>
												<th scope="col text-center" style="width:25px">Edit</th>
												<th scope="col text-center" style="width:25px">Remove</th>
											</tr>
										</thead>
										<tbody id="tbodyallperson">
											
										</tbody>
									</table>
									<div style="display:none" id="noperson" class="alert alert-info" role="alert">
										<h4>No data to be displayed.</h4>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="clearfix"></div>
			</div>
		</div>
	</div>

	<!-- //contact -->
	<!-- w3-agilesale -->
	<div class="w3-agilesale welcome">
		<div class="container">
			<h3 class="agileits-title two">Lost pets. Found pets.<br><span>Pet Shop</span></h3>
			<a href="<?php echo base_url();?>contact" class="button button-isi"><span>Contact Us </span><i class="icon glyphicon glyphicon-arrow-right"></i></a>
		</div>
	</div>
	<!-- //w3-agilesale -->
	<!-- copy rights start here -->
	<div class="copy-w3right">
		<div class="container">
			<div class="top-nav bottom-w3lnav">
				<ul>
					<li><a href="<?php echo base_url();?>home">Home</a></li>
					<li><a href="<?php echo base_url();?>about">About Us</a></li>
					<li><a href="<?php echo base_url();?>lost">Lost</a></li>
					<li><a href="<?php echo base_url();?>found">Found</a></li>
					<li><a href="<?php echo base_url();?>shop">Shop</a></li>
					<li><a href="<?php echo base_url();?>contact">Contact</a></li>
				</ul>
			</div>
			<p>© 2019 Pet Post. All Rights Reserved | Design by W3layouts | Developed by IPHENCreations </p>
		</div>
	</div>
	<div class="modal fade" id="addProductModal" tabindex="-1" role="dialog">
		<div class="modal-dialog modal-lg">
			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>

					<div class="signin-form profile">
						<h3 class="agileinfo_sign">Add Product Data</h3>
						<div class="alert alert-success display-none" id="addProductSuccess">
							<p><strong>Success!</strong> Product is now saved.</p>
						</div>
						<div class="alert alert-danger display-none" id="addProductError">
							<p><strong>Error!</strong> Please try again.</p>
						</div>
						<div class="login-form">
							<form id="addProductForm"  method="post" data-toggle="validator" role="form">
								<div class="row">
									<div class="col-md-6 contact-left">
										<div class="form-group">
											<label for="product_name">Product Name</label>
											<input type="text" name="product_name" id="product_name" required="">
											<div class="help-block with-errors"></div>
										</div>
										<div class="form-group">
											<label for="product_desc">Description</label>
											<textarea name="product_desc" id="product_desc" rows="2" required=""></textarea>
											<div class="help-block with-errors"></div>
										</div>
										<div class="form-group">
											<label for="product_weight">Weight (grams)</label>
											<input type="text" name="product_weight" id="product_weight" required="">
											<div class="help-block with-errors"></div>
										</div>
									</div>
									<div class="col-md-6 contact-right">
										<div class="form-group">
											<label for="product_price">Price (USD)</label>
											<input type="text" name="product_price" id="product_price" required="">
											<div class="help-block with-errors"></div>
										</div>
										<div class="form-group">
											<label for="productPic1" class="control-label">Product Picture 1&ensp;</label>
											<input type="file" name="productPic1" id="productPic1" required="">
											<div class="help-block with-errors"></div>
										</div>
										<div class="form-group">
											<label for="productPic2" class="control-label">Product Picture 2&ensp;</label>
											<input type="file" name="productPic2" id="productPic2" required="">
											<div class="help-block with-errors"></div>
										</div>
									</div>
								</div>
								<div class="tp">
									<input type="submit" value="SAVE NOW" style="width:50%">
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="modal fade" id="updateProductModal" tabindex="-1" role="dialog">
		<div class="modal-dialog modal-lg">
			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>

					<div class="signin-form profile">
						<h3 class="agileinfo_sign">Update Product Data</h3>
						<div class="alert alert-success display-none" id="updateProductSuccess">
							<p><strong>Success!</strong> Product is now updated.</p>
						</div>
						<div class="alert alert-danger display-none" id="updateProductError">
							<p><strong>Error!</strong> Please try again.</p>
						</div>
						<div class="login-form">
							<form id="updateProductForm"  method="post" data-toggle="validator" role="form">
								<div class="row">
									<div>
										<img src="" id="updProductImg" style="max-height: 150px;border: 1px solid #909090;">
										<img src="" id="updProductImg2" style="max-height: 150px;border: 1px solid #909090;">
									</div>
									<div class="col-md-6 contact-left">
										<div class="form-group">
											<label for="upd_product_name">Product Name</label>
											<input type="text" name="upd_product_name" id="upd_product_name" required="">
											<input type="hidden" name="upd_product_id" id="upd_product_id">
											<div class="help-block with-errors"></div>
										</div>
										<div class="form-group">
											<label for="upd_product_desc">Description</label>
											<textarea name="upd_product_desc" id="upd_product_desc" rows="2" required=""></textarea>
											<div class="help-block with-errors"></div>
										</div>
										<div class="form-group">
											<label for="upd_product_weight">Weight (grams)</label>
											<input type="text" name="upd_product_weight" id="upd_product_weight" required="">
											<div class="help-block with-errors"></div>
										</div>
									</div>
									<div class="col-md-6 contact-right">
										<div class="form-group">
											<label for="upd_product_price">Price (USD)</label>
											<input type="text" name="upd_product_price" id="upd_product_price" required="">
											<div class="help-block with-errors"></div>
										</div>
										<div class="form-group">
											<label for="upd_productPic" class="control-label">Product Picture 1&ensp;</label>
											<input type="file" name="upd_productPic" id="upd_productPic">
										</div>
										<div class="form-group">
											<label for="upd_productPic2" class="control-label">Product Picture 2&ensp;</label>
											<input type="file" name="upd_productPic2" id="upd_productPic2">
										</div>
									</div>
								</div>
								<div class="tp">
									<input type="submit" value="SAVE NOW" style="width:50%">
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="modal fade" id="updatePersonModal" tabindex="-1" role="dialog">
		<div class="modal-dialog modal-lg">
			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>

					<div class="signin-form profile">
						<h3 class="agileinfo_sign">Edit Your Person's Info</h3>
						<div class="alert alert-success display-none" id="updPersonSuccess">
							<p><strong>Success!</strong> This person is now updated.</p>
						</div>
						<div class="alert alert-danger display-none" id="updPersonError">
							<p><strong>Error!</strong> Please try again.</p>
						</div>
						<div class="login-form">
							<form id="updatePersonForm"  method="post" data-toggle="validator" role="form">
								<div class="row">
									<div>
										<img src="" id="updPersonImgOutput" style="max-width: 200px;border: 1px solid #909090;">
									</div>
									<div class="col-md-6 contact-left">
										<div class="form-group">
											<label for="upd_person_name">Person Name</label>
											<input type="text" name="upd_person_name" id="upd_person_name" required="">
											<input type="hidden" name="upd_person_id" id="upd_person_id">
											<div class="help-block with-errors"></div>
										</div>
										<div class="form-group">
											<label for="upd_person_country">Country</label>
											<input type="text" name="upd_person_country" id="upd_person_country" required="">
											<div class="help-block with-errors"></div>
										</div>
										<div class="form-group">
											<label for="upd_person_city">City</label>
											<input type="text" name="upd_person_city" id="upd_person_city" required="">
											<div class="help-block with-errors"></div>
										</div>
										
										<div class="form-group">
											<label for="upd_person_gender">Gender</label>
											<select name="upd_person_gender" id="upd_person_gender">
												<option value="1">Female</option>
												<option value="2">Male</option>
											</select>
										</div>
										<div class="form-group">
											<label for="upd_person_eyecolor">Eye Color</label>
											<select name="upd_person_eyecolor" id="upd_person_eyecolor">
												<option value="1">Blue</option>
												<option value="2">Brown</option>
												<option value="3">Green</option>
												<option value="4">Gray</option>
												<option value="5">Mixed</option>
											</select>
										</div>
										<div class="form-group">
											<label for="upd_person_haircolor">Hair Color</label>
											<select name="upd_person_haircolor" id="upd_person_haircolor">
												<option value="1">White</option>
												<option value="2">Gray</option>
												<option value="3">Blond</option>
												<option value="4">Brown</option>
												<option value="5">Red</option>
												<option value="6">Black</option>
												<option value="7">Other</option>
											</select>
										</div>
										<div class="form-group">
											<label for="upd_person_last_seen_date">Last Seen Date</label>
											<input type="date" name="upd_person_last_seen_date" id="upd_person_last_seen_date" required="">
											<div class="help-block with-errors"></div>
										</div>
										<div class="form-group">
											<label for="upd_person_last_seen_place">Last Seen Place</label>
											<input type="text" name="upd_person_last_seen_place" id="upd_person_last_seen_place" required="">
											<div class="help-block with-errors"></div>
										</div>
										<div class="form-group">
											<label for="upd_person_specific_features">Specific Features</label>
											<textarea name="upd_person_specific_features" id="upd_person_specific_features" rows="2" required=""></textarea>
											<div class="help-block with-errors"></div>
										</div>
									</div>
									<div class="col-md-6 contact-right">
										<div class="form-group">
											<label for="upd_person_imgfile" class="control-label">Picture&ensp;</label>
											<input type="file" name="upd_person_imgfile" id="upd_person_imgfile">
										</div>
										<div class="form-group">
											<label for="upd_person_bday">Birth Date</label>
											<input type="date" name="upd_person_bday" id="upd_person_bday" required="">
											<div class="help-block with-errors"></div>
										</div>
										<div class="form-group">
											<label for="upd_person_height">Height (cm)</label>
											<input type="text" name="upd_person_height" id="upd_person_height" required="">
											<div class="help-block with-errors"></div>
										</div>
										<div class="form-group">
											<label for="upd_person_weight">Weight (kg)</label>
											<input type="text" name="upd_person_weight" id="upd_person_weight" required="">
											<div class="help-block with-errors"></div>
										</div>
										<div class="form-group">
											<label for="upd_person_handed">Handed</label>
											<select name="upd_person_handed" id="upd_person_handed">
												<option value="1">Left</option>
												<option value="2">Right</option>
											</select>
										</div>
										<div class="form-group">
											<label for="upd_person_contact1">Contact 1</label>
											<input type="text" name="upd_person_contact1" id="upd_person_contact1" required="">
											<div class="help-block with-errors"></div>
										</div>
										<div class="form-group">
											<label for="upd_person_contact2">Contact 2</label>
											<input type="text" name="upd_person_contact2" id="upd_person_contact2" required="">
											<div class="help-block with-errors"></div>
										</div>
										<div class="form-group">
											<label for="upd_person_contact3">Contact 3</label>
											<input type="text" name="upd_person_contact3" id="upd_person_contact3" required="">
											<div class="help-block with-errors"></div>
										</div>
										<div class="form-group">
											<label for="upd_person_comment">Comment Box</label>
											<textarea name="upd_person_comment" id="upd_person_comment" rows="2" required=""></textarea>
											<div class="help-block with-errors"></div>
										</div>
										<div class="form-group">
											<label for="upd_person_status">Status</label>
											<select name="upd_person_status" id="upd_person_status">
												<option value="1">Missing</option>
												<option value="2">Cancelled</option>
											</select>
										</div>
									</div>
								</div>
								<div class="tp">
									<input type="submit" id="updPersonBtn" value="SAVE NOW" style="width:50%">
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="modal fade" id="addPersonModal" tabindex="-1" role="dialog">
		<div class="modal-dialog modal-lg">
			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>

					<div class="signin-form profile">
						<h3 class="agileinfo_sign">Add Your Person's Info</h3>
						<div class="alert alert-success display-none" id="addPersonSuccess">
							<p><strong>Success!</strong> This person is now saved.</p>
						</div>
						<div class="alert alert-danger display-none" id="addPersonError">
							<p><strong>Error!</strong> Please try again.</p>
						</div>
						<div class="login-form">
							<form id="addPersonForm"  method="post" data-toggle="validator" role="form">
								<div class="row">
									<div class="col-md-6 contact-left">
										<div class="form-group">
											<label for="person_name">Person Name</label>
											<input type="text" name="person_name" id="person_name" required="">
											<div class="help-block with-errors"></div>
										</div>
										<div class="form-group">
											<label for="person_country">Country</label>
											<input type="text" name="person_country" id="person_country" required="">
											<div class="help-block with-errors"></div>
										</div>
										<div class="form-group">
											<label for="person_city">City</label>
											<input type="text" name="person_city" id="person_city" required="">
											<div class="help-block with-errors"></div>
										</div>
										
										<div class="form-group">
											<label for="person_gender">Gender</label>
											<select name="person_gender" id="person_gender">
												<option value="1">Female</option>
												<option value="2">Male</option>
											</select>
										</div>
										<div class="form-group">
											<label for="person_eyecolor">Eye Color</label>
											<select name="person_eyecolor" id="person_eyecolor">
												<option value="1">Blue</option>
												<option value="2">Brown</option>
												<option value="3">Green</option>
												<option value="4">Gray</option>
												<option value="5">Mixed</option>
											</select>
										</div>
										<div class="form-group">
											<label for="person_haircolor">Hair Color</label>
											<select name="person_haircolor" id="person_haircolor">
												<option value="1">White</option>
												<option value="2">Gray</option>
												<option value="3">Blond</option>
												<option value="4">Brown</option>
												<option value="5">Red</option>
												<option value="6">Black</option>
												<option value="7">Other</option>
											</select>
										</div>
										<div class="form-group">
											<label for="person_last_seen_date">Last Seen Date</label>
											<input type="date" name="person_last_seen_date" id="person_last_seen_date" required="">
											<div class="help-block with-errors"></div>
										</div>
										<div class="form-group">
											<label for="person_last_seen_place">Last Seen Place</label>
											<input type="text" name="person_last_seen_place" id="person_last_seen_place" required="">
											<div class="help-block with-errors"></div>
										</div>
										<div class="form-group">
											<label for="person_specific_features">Specific Features</label>
											<textarea name="person_specific_features" id="person_specific_features" rows="2" required=""></textarea>
											<div class="help-block with-errors"></div>
										</div>
									</div>
									<div class="col-md-6 contact-right">
										<div class="form-group">
											<label for="person_imgfile" class="control-label">Picture&ensp;</label>
											<input type="file" name="person_imgfile" id="person_imgfile" required="">
											<div class="help-block with-errors"></div>
										</div>
										<div class="form-group">
											<label for="person_bday">Birth Date</label>
											<input type="date" name="person_bday" id="person_bday" required="">
											<div class="help-block with-errors"></div>
										</div>
										<div class="form-group">
											<label for="person_height">Height (cm)</label>
											<input type="text" name="person_height" id="person_height" required="">
											<div class="help-block with-errors"></div>
										</div>
										<div class="form-group">
											<label for="person_weight">Weight (kg)</label>
											<input type="text" name="person_weight" id="person_weight" required="">
											<div class="help-block with-errors"></div>
										</div>
										<div class="form-group">
											<label for="person_handed">Handed</label>
											<select name="person_handed" id="person_handed">
												<option value="1">Left</option>
												<option value="2">Right</option>
											</select>
										</div>
										<div class="form-group">
											<label for="person_contact1">Contact 1</label>
											<input type="text" name="person_contact1" id="person_contact1" required="">
											<div class="help-block with-errors"></div>
										</div>
										<div class="form-group">
											<label for="person_contact2">Contact 2</label>
											<input type="text" name="person_contact2" id="person_contact2" required="">
											<div class="help-block with-errors"></div>
										</div>
										<div class="form-group">
											<label for="person_contact3">Contact 3</label>
											<input type="text" name="person_contact3" id="person_contact3" required="">
											<div class="help-block with-errors"></div>
										</div>
										<div class="form-group">
											<label for="person_comment">Comment Box</label>
											<textarea name="person_comment" id="person_comment" rows="2" required=""></textarea>
											<div class="help-block with-errors"></div>
										</div>
										<div class="form-group">
											<label for="person_status">Status</label>
											<select name="person_status" id="person_status">
												<option value="1">Missing</option>
												<option value="2">Cancelled</option>
											</select>
										</div>
									</div>
								</div>
								<div class="tp">
									<input type="submit" id="addPersonBtn" value="SAVE NOW" style="width:50%">
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="modal fade" id="addPetModal" tabindex="-1" role="dialog">
		<div class="modal-dialog modal-lg">
			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>

					<div class="signin-form profile">
						<h3 class="agileinfo_sign">Add Your Pet's Info</h3>
						<div class="alert alert-success display-none" id="addPetSuccess">
							<p><strong>Success!</strong> Your pet is now saved.</p>
						</div>
						<div class="alert alert-danger display-none" id="addPetError">
							<p><strong>Error!</strong> Please try again.</p>
						</div>
						<div class="login-form">
							<form id="addPetForm"  method="post" data-toggle="validator" role="form">
								<div class="row">
									<div class="col-md-6 contact-left">
										<div class="form-group">
											<label for="pet_name">Pet Name</label>
											<input type="text" name="pet_name" id="pet_name" required="">
											<div class="help-block with-errors"></div>
										</div>
										<div class="form-group">
											<label for="pet_specie">Specie</label>
											<select name="pet_specie" id="pet_specie">
												<option value="1">Dog</option>
												<option value="2">Cat</option>
												<option value="3">Bird</option>
												<option value="4">Snake</option>
												<option value="5">Horse</option>
												<option value="6">Others</option>
											</select>
										</div>
										<div class="form-group">
											<label for="pet_bday">Birth Date</label>
											<input type="date" name="pet_bday" id="pet_bday" required="">
											<div class="help-block with-errors"></div>
										</div>
										<div class="form-group">
											<label for="pet_gender">Gender</label>
											<select name="pet_gender" id="pet_gender">
												<option value="1">Female</option>
												<option value="2">Male</option>
											</select>
										</div>
										<div class="form-group">
											<label for="pet_neutered">Neutered</label>
											<select name="pet_neutered" id="pet_neutered">
												<option value="1">Yes</option>
												<option value="2">No</option>
											</select>
										</div>
										<div class="form-group">
											<label for="pet_fur">Fur</label>
											<select name="pet_fur" id="pet_fur">
												<option value="1">Longhaired</option>
												<option value="2">Shorthaired</option>
												<option value="3">Hairless</option>
											</select>
										</div>
										<div class="form-group">
											<label for="pet_fur_color">Fur Color</label>
											<select name="pet_fur_color" id="pet_fur_color">
												<option value="1">White</option>
												<option value="2">Beige</option>
												<option value="3">Yellow</option>
												<option value="4">Brown</option>
												<option value="5">Black</option>
												<option value="6">Grey</option>
												<option value="7">Mixed White-Red-Brown</option>
												<option value="8">Mixed Black-White</option>
												<option value="9">Mixed Black-Brown</option>
												<option value="10">Others</option>
											</select>
										</div>
										<div class="form-group">
											<label for="pet_eye_color">Eye Color</label>
											<select name="pet_eye_color" id="pet_eye_color">
												<option value="1">Blue</option>
												<option value="2">Brown</option>
												<option value="3">Black</option>
												<option value="4">Green</option>
												<option value="5">Grey</option>
												<option value="6">Others</option>
											</select>
										</div>
									</div>
									<div class="col-md-6 contact-right">
										<div class="form-group">
											<label for="imgfile" class="control-label">Pet Picture&ensp;</label>
											<input type="file" name="imgfile" id="imgfile" required="">
											<div class="help-block with-errors"></div>
										</div>
										<div class="form-group">
											<label for="pet_size">Size</label>
											<select name="pet_size" id="pet_size">
												<option value="1">Micro</option>
												<option value="2">Mini</option>
												<option value="3">Small</option>
												<option value="4">Medium</option>
												<option value="5">Maxi</option>
												<option value="6">Big</option>
												<option value="7">Giant</option>
											</select>
										</div>
										<div class="form-group">
											<label for="pet_microchip">Microchip</label>
											<input type="text" name="pet_microchip" id="pet_microchip">
										</div>
										<div class="form-group">
											<label for="pet_tattoo">Tatto</label>
											<input type="text" name="pet_tattoo" id="pet_tattoo">
										</div>
										<div class="form-group">
											<label for="pet_status">Pet Status</label>
											<select name="pet_status" id="pet_status">
												<option value="1">Home</option>
												<option value="2">Dead</option>
												<option value="3">Sold</option>
												<option value="4">Lost</option>
												<option value="5">Found</option>
											</select>
										</div>
										<div class="form-group">
											<label for="pet_nfc">NFC</label>
											<select name="pet_nfc" id="pet_nfc">
												<option value="1">Yes</option>
												<option value="2">No</option>
											</select>
										</div>
										<div class="form-group">
											<label for="pet_kennel">Kennel ID</label>
											<input type="text" name="pet_kennel" id="pet_kennel">
										</div>
										<div class="form-group">
											<label for="pet_comment">Comment Box</label>
											<textarea name="pet_comment" id="pet_comment" rows="2" required=""></textarea>
											<div class="help-block with-errors"></div>
										</div>
									</div>
								</div>
								<div class="tp">
									<input type="submit" id="addPetBtn" value="SAVE NOW" style="width:50%">
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="modal fade" id="updatePetModal" tabindex="-1" role="dialog">
		<div class="modal-dialog modal-lg">
			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>

					<div class="signin-form profile">
						<h3 class="agileinfo_sign">Update Your Pet's Info</h3>
						<div class="alert alert-success display-none" id="updatePetSuccess">
							<p><strong>Success!</strong> Your pet's data is now updated.</p>
						</div>
						<div class="alert alert-danger display-none" id="updatePetError">
							<p><strong>Error!</strong> Please try again.</p>
						</div>
						<div class="login-form">
							<form id="updatePetForm"  method="post" data-toggle="validator" role="form">
								<div class="row">
									<div>
										<img src="" id="updImgOutput" style="max-width: 200px;border: 1px solid #909090;">
									</div>
									<div class="col-md-6 contact-left">
										<div class="form-group">
											<label for="upd_pet_name">Pet Name</label>
											<input type="text" name="upd_pet_name" id="upd_pet_name" required="">
											<input type="hidden" name="upd_pet_id" id="upd_pet_id">
											<input type="hidden" id="baseurl" value="<?php echo base_url();?>">
											<input type="hidden" id="usr_id" value="<?php echo $this->session->userdata('u_id');?>">
											<div class="help-block with-errors"></div>
										</div>
										<div class="form-group">
											<label for="upd_pet_specie">Specie</label>
											<select name="upd_pet_specie" id="upd_pet_specie">
												<option value="1">Dog</option>
												<option value="2">Cat</option>
												<option value="3">Bird</option>
												<option value="4">Snake</option>
												<option value="5">Horse</option>
												<option value="6">Others</option>
											</select>
										</div>
										<div class="form-group">
											<label for="upd_pet_bday">Birth Date</label>
											<input type="date" name="upd_pet_bday" id="upd_pet_bday" required="">
											<div class="help-block with-errors"></div>
										</div>
										<div class="form-group">
											<label for="upd_pet_gender">Gender</label>
											<select name="upd_pet_gender" id="upd_pet_gender">
												<option value="1">Female</option>
												<option value="2">Male</option>
											</select>
										</div>
										<div class="form-group">
											<label for="upd_pet_neutered">Neutered</label>
											<select name="upd_pet_neutered" id="upd_pet_neutered">
												<option value="1">Yes</option>
												<option value="2">No</option>
											</select>
										</div>
										<div class="form-group">
											<label for="upd_pet_fur">Fur</label>
											<select name="upd_pet_fur" id="upd_pet_fur">
												<option value="1">Longhaired</option>
												<option value="2">Shorthaired</option>
												<option value="3">Hairless</option>
											</select>
										</div>
										<div class="form-group">
											<label for="upd_pet_fur_color">Fur Color</label>
											<select name="upd_pet_fur_color" id="upd_pet_fur_color">
												<option value="1">White</option>
												<option value="2">Beige</option>
												<option value="3">Yellow</option>
												<option value="4">Brown</option>
												<option value="5">Black</option>
												<option value="6">Grey</option>
												<option value="7">Mixed White-Red-Brown</option>
												<option value="8">Mixed Black-White</option>
												<option value="9">Mixed Black-Brown</option>
												<option value="10">Others</option>
											</select>
										</div>
										<div class="form-group">
											<label for="upd_pet_eye_color">Eye Color</label>
											<select name="upd_pet_eye_color" id="upd_pet_eye_color">
												<option value="1">Blue</option>
												<option value="2">Brown</option>
												<option value="3">Black</option>
												<option value="4">Green</option>
												<option value="5">Grey</option>
												<option value="6">Others</option>
											</select>
										</div>
									</div>
									<div class="col-md-6 contact-right">
										<div class="form-group">
											<label for="upd_imgfile" class="control-label">Pet Picture&ensp;</label>
											<input type="file" name="upd_imgfile" id="upd_imgfile">
										</div>
										<div class="form-group">
											<label for="upd_pet_size">Size</label>
											<select name="upd_pet_size" id="upd_pet_size">
												<option value="1">Micro</option>
												<option value="2">Mini</option>
												<option value="3">Small</option>
												<option value="4">Medium</option>
												<option value="5">Maxi</option>
												<option value="6">Big</option>
												<option value="7">Giant</option>
											</select>
										</div>
										<div class="form-group">
											<label for="upd_pet_microchip">Microchip</label>
											<input type="text" name="upd_pet_microchip" id="upd_pet_microchip">
										</div>
										<div class="form-group">
											<label for="upd_pet_tattoo">Tatto</label>
											<input type="text" name="upd_pet_tattoo" id="upd_pet_tattoo">
										</div>
										<div class="form-group">
											<label for="upd_pet_status">Pet Status</label>
											<select name="upd_pet_status" id="upd_pet_status">
												<option value="1">Home</option>
												<option value="2">Dead</option>
												<option value="3">Sold</option>
												<option value="4">Lost</option>
												<option value="5">Found</option>
											</select>
										</div>
										<div class="form-group">
											<label for="upd_pet_nfc">NFC</label>
											<select name="upd_pet_nfc" id="upd_pet_nfc">
												<option value="1">Yes</option>
												<option value="2">No</option>
											</select>
										</div>
										<div class="form-group">
											<label for="upd_pet_kennel">Kennel ID</label>
											<input type="text" name="upd_pet_kennel" id="upd_pet_kennel">
										</div>
										<div class="form-group">
											<label for="upd_pet_comment">Comment Box</label>
											<textarea name="upd_pet_comment" id="upd_pet_comment" rows="2" required=""></textarea>
											<div class="help-block with-errors"></div>
										</div>
									</div>
								</div>
								<div class="tp">
									<input type="submit" id="updatePetBtn" value="SAVE NOW" style="width:50%">
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	
	<!-- //copy right end here -->
	<!-- password-script -->
	<script type="text/javascript">
		/*window.onload = function () {
			document.getElementById("password1").onchange = validatePassword;
			document.getElementById("password2").onchange = validatePassword;
		}

		function validatePassword() {
			var pass2 = document.getElementById("password2").value;
			var pass1 = document.getElementById("password1").value;
			if (pass1 != pass2)
				document.getElementById("password2").setCustomValidity("Passwords Don't Match");
			else
				document.getElementById("password2").setCustomValidity('');
			//empty string means no validation error
		}*/
	</script>
	<script src="<?php echo base_url();?>js/easy-responsive-tabs.js"></script>
	<script>
		$(document).ready(function () {
			$('#horizontalTab').easyResponsiveTabs({
				type: 'default', //Types: default, vertical, accordion           
				width: 'auto', //auto or any width like 600px
				fit: true, // 100% fit in a container
				closed: 'accordion', // Start closed if in accordion view
				activate: function (event) { // Callback function if tab is switched
					var $tab = $(this);
					var $info = $('#tabInfo');
					var $name = $('span', $info);
					$name.text($tab.text());
					$info.show();
				}
			});
			$('#verticalTab').easyResponsiveTabs({
				type: 'vertical',
				width: 'auto',
				fit: true
			});
		});
	</script>
	<script src="<?php echo base_url();?>js/SmoothScroll.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>js/move-top.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>js/easing.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>js/validator.min.js"></script>
	<script type="text/javascript">
		jQuery(document).ready(function ($) {
			$(".scroll").click(function (event) {
				event.preventDefault();

				$('html,body').animate({
					scrollTop: $(this.hash).offset().top
				}, 1000);
			});
		});
	</script>
	<script type="text/javascript">
		$(document).ready(function () {
			/*
			var defaults = {
				containerID: 'toTop', // fading element id
				containerHoverID: 'toTopHover', // fading element hover id
				scrollSpeed: 1200,
				easingType: 'linear' 
			};
			*/

			$().UItoTop({
				easingType: 'easeOutQuart'
			});

		});
	</script>
	<!-- Placed at the end of the document so the pages load faster -->
	<script src="<?php echo base_url();?>js/jquery.dataTables.min.js"></script>
	<script src="<?php echo base_url();?>js/dataTables.bootstrap4.min.js"></script>
	<script src="<?php echo base_url();?>js/bootstrap.js"></script>
	<script src="<?php echo base_url();?>js/cookie.js"></script>
	<script src="<?php echo base_url();?>js/user.js"></script>
	<script src="<?php echo base_url();?>js/login.js"></script>
	<script src="<?php echo base_url();?>js/settings.js"></script>
	<script src="<?php echo base_url();?>js/shop.js"></script>
	
</body>

</html>