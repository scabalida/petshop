<?php defined('BASEPATH') OR exit('No direct script access allowed');
	
	class missing extends Admin_Controller {
		public function __construct() {
		parent::__construct();
		$this->load->model('m_person');
		$this->load->model('m_user');
    }
		public function index() {
			$this->load->view('missing');
		}
		public function getAllMissing(){
			$query = $this->m_person->getAllMissingPerson();
			
			echo json_encode($query);
		}
		public function profile($id) {
			$data['person'] =  $this->m_person->getThisPersonData($id);
			$data['user'] =  $this->m_user->getUserData($data['person'][0]->user_id);
			$this->load->view('missing_profile',$data);
		}
	}