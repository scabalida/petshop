<?php defined('BASEPATH') OR exit('No direct script access allowed');
	define("ROOT_PATH", $_SERVER['DOCUMENT_ROOT']);
	
	class settings extends Admin_Controller {
		public function __construct() {
		parent::__construct();
		$this->load->model('m_user');
		$this->load->model('m_person');
		$this->load->model('m_pets');
		$this->load->model('m_login');
    }
		public function index() {
			$data['visitors'] = $this->m_login->get_visitors();
			$this->load->view('settings',$data);
		}
		public function test() {
			$this->load->view('test');

		}
		public function downloadFoundPoster() {
			$config['upload_path']  =  "images/poster";
			$config['allowed_types']        = 'gif|jpg|png|jpeg';
			
			$this->load->library('upload', $config);
			$this->load->library('m_pdf');
		
			$mpdf= $this->m_pdf->load();
			$mpdf->allow_charset_conversion=true;
			$mpdf->charset_in='UTF-8';
			
			if ( ! $this->upload->do_upload('addPicFoundPoster'))
            {
				//$error = array('error' => $this->upload->display_errors());
		 
				//echo json_encode($error);
			}else{
				$image = $this->upload->data();
				$data['pic'] = base_url()."images/poster/".$image['file_name'];
				$data['name'] = $this->input->post('addNameFoundPoster');
				$data['zip'] = $this->input->post('addZipFoundPoster');
				$data['street'] = $this->input->post('addStreetFoundPoster');
				$data['contact'] = $this->input->post('addContactFoundPoster');
				$data['breed'] = $this->input->post('addBreedFoundPoster');
				$data['color'] = $this->input->post('addColorFoundPoster');
				$data['gender'] = $this->input->post('addGenderFoundPoster');
				$data['addDet'] = $this->input->post('addAddDetFoundPoster');
				$data['age'] = $this->input->post('addAgeFoundPoster');
				$data['date'] = date("F d, Y", strtotime($this->input->post('addDateFoundPoster')));
				//$this->load->view('found_poster',$data);
				$html = $this->load->view('found_poster',$data,true);
				$mpdf->SetFont('kristenitc');
				$mpdf->WriteHTML($html);
				$mpdf->Output('petpost_found_poster.pdf','D');
				unlink(ROOT_PATH . "/images/poster/".$image['file_name']);
				//print_r($data);
			}
		}
		public function downloadFoundPersonPoster() {
			$config['upload_path']  =  "images/poster";
			$config['allowed_types']        = 'gif|jpg|png|jpeg';
			
			$this->load->library('upload', $config);
			$this->load->library('m_pdf');
		
			$mpdf= $this->m_pdf->load();
			$mpdf->allow_charset_conversion=true;
			$mpdf->charset_in='UTF-8';
			
			if ( ! $this->upload->do_upload('addPicFoundPersonPoster'))
            {
				//$error = array('error' => $this->upload->display_errors());
		 
				//echo json_encode($error);
			}else{
				$image = $this->upload->data();
				$data['pic'] = base_url()."images/poster/".$image['file_name'];
				$data['name'] = $this->input->post('addNameFoundPersonPoster');
				$data['zip'] = $this->input->post('addZipFoundPersonPoster');
				$data['street'] = $this->input->post('addStreetFoundPersonPoster');
				$data['contact'] = $this->input->post('addContactFoundPersonPoster');
				$data['breed'] = $this->input->post('addBreedFoundPersonPoster');
				$data['color'] = $this->input->post('addColorFoundPersonPoster');
				$data['gender'] = $this->input->post('addGenderFoundPersonPoster');
				$data['addDet'] = $this->input->post('addAddDetFoundMissingPoster');
				$data['age'] = $this->input->post('addAgeFoundPersonPoster');
				$data['date'] = date("F d, Y", strtotime($this->input->post('addDateFoundPersonPoster')));
				//$this->load->view('found_poster',$data);
				$html = $this->load->view('found_person_poster',$data,true);
				$mpdf->SetFont('kristenitc');
				$mpdf->WriteHTML($html);
				$mpdf->Output('petpost_found_person_poster.pdf','D');
				unlink(ROOT_PATH . "/images/poster/".$image['file_name']);
				//print_r($data);
			}
		}
		public function downloadPoster() {
			$config['upload_path']  =  "images/poster";
			$config['allowed_types']        = 'gif|jpg|png|jpeg';
			
			$this->load->library('upload', $config);
			$this->load->library('m_pdf');
		
			$mpdf= $this->m_pdf->load();
			$mpdf->allow_charset_conversion=true;
			$mpdf->charset_in='UTF-8';
			
			if ( ! $this->upload->do_upload('addPicLostPoster'))
            {
				//$error = array('error' => $this->upload->display_errors());
		 
				//echo json_encode($error);
			}else{
				$image = $this->upload->data();
				$data['pic'] = base_url()."images/poster/".$image['file_name'];
				$data['name'] = $this->input->post('addNameLostPoster');
				$data['zip'] = $this->input->post('addZipLostPoster');
				$data['street'] = $this->input->post('addStreetLostPoster');
				$data['contact'] = $this->input->post('addContactLostPoster');
				$data['breed'] = $this->input->post('addBreedLostPoster');
				$data['color'] = $this->input->post('addColorLostPoster');
				$data['gender'] = $this->input->post('addGenderLostPoster');
				$data['addDet'] = $this->input->post('addAddDetLostPoster');
				$data['age'] = $this->input->post('addAgeLostPoster');
				$data['date'] = date("F d, Y", strtotime($this->input->post('addDateLostPoster')));
				//$this->load->view('lost_poster',$data);
				$html = $this->load->view('lost_poster',$data,true);
				$mpdf->SetFont('kristenitc');
				$mpdf->WriteHTML($html);
				$mpdf->Output('petpost_lost_poster.pdf','D');
				unlink(ROOT_PATH . "/images/poster/".$image['file_name']);
				//print_r($data);
			}
		}
		public function downloadMissingPoster() {
			$config['upload_path']  =  "images/poster";
			$config['allowed_types']        = 'gif|jpg|png|jpeg';
			
			$this->load->library('upload', $config);
			$this->load->library('m_pdf');
		
			$mpdf= $this->m_pdf->load();
			$mpdf->allow_charset_conversion=true;
			$mpdf->charset_in='UTF-8';
			
			if ( ! $this->upload->do_upload('addPicMissingPoster'))
            {
				//$error = array('error' => $this->upload->display_errors());
		 
				//echo json_encode($error);
			}else{
				$image = $this->upload->data();
				$data['pic'] = base_url()."images/poster/".$image['file_name'];
				$data['name'] = $this->input->post('addNameMissingPoster');
				$data['zip'] = $this->input->post('addZipMissingPoster');
				$data['street'] = $this->input->post('addStreetMissingPoster');
				$data['contact'] = $this->input->post('addContactMissingPoster');
				$data['hair'] = $this->input->post('addHairColorMissingPoster');
				$data['eye'] = $this->input->post('addEyeColorMissingPoster');
				$data['gender'] = $this->input->post('addGenderMissingPoster');
				$data['addDet'] = $this->input->post('addAddDetMissingPoster');
				$data['age'] = $this->input->post('addAgeMissingPoster');
				$data['date'] = date("F d, Y", strtotime($this->input->post('addAddDetMissingPoster')));
				//$this->load->view('lost_poster',$data);
				$html = $this->load->view('missing_poster',$data,true);
				$mpdf->SetFont('kristenitc');
				$mpdf->WriteHTML($html);
				$mpdf->Output('petpost_missing_person_poster.pdf','D');
				unlink(ROOT_PATH . "/images/poster/".$image['file_name']);
				//print_r($data);
			}
		}
		public function getUserData(){
			$id = $this->session->userdata('u_id');
			$query = $this->m_user->getUserData($id);
			
			echo json_encode($query);
			
		}
		public function updateUserData(){
			$id = $this->session->userdata('u_id');
			$data = array(
				'u_fullname' => $this->input->post('upd_name'),
				'u_contact1' => $this->input->post('upd_c_code1')."-".$this->input->post('upd_contact1'),
				'u_contact2' => $this->input->post('upd_c_code2')."-".$this->input->post('upd_contact2'),
				'u_contact3' => $this->input->post('upd_c_code3')."-".$this->input->post('upd_contact3'),
				'u_country'  => $this->input->post('upd_country'),
				'u_city'  	 => $this->input->post('upd_city'),
				'u_zip'  	 => $this->input->post('upd_zip')
			);
			//print_r($data);
			$query = $this->m_user->save($data,$id);
			if($query){
				echo json_encode('true');
			}
			else{
				echo json_encode('false');
			}
		}
		public function chckMD5(){
			echo md5("admin1234")."stekiesca";
		}
		public function chckPassword(){
			$myPassword = $this->m_user->getUserPass();
			
			$password = $this->input->post('upd_password1');
			$newPass = md5($password)."stekiesca";

			if($myPassword[0]->u_password == $newPass){
				echo json_encode('true');
			}
			else{
				echo json_encode('false');
			}
		}
		public function updatePassword(){
			$id = $this->session->userdata('u_id');
			$password = $this->input->post('password');
			$newPass = md5($password)."stekiesca";
			$data = array(
				'u_password' => $newPass
			);

			//print_r($data);
			$query = $this->m_user->save($data,$id);
			if($query){
				echo json_encode('true');
			}
			else{
				echo json_encode('false');
			}
		}
		public function getThisUserData(){
			$id = $this->input->post('id');
			$data = $this->m_user->getUserData($id);
			echo json_encode($data);
		}
		public function getAllUsersExceptAdmin(){
			$data = $this->m_user->getAllUsersExceptAdmin();
			echo json_encode($data);
		}
		public function updateUserDataByAdmin(){
			$id = $this->input->post('ad_u_id');
			$data = array(
				'u_fullname' => $this->input->post('upd_ad_name'),
				'u_contact1' => $this->input->post('upd_ad_c_code1')."-".$this->input->post('upd_ad_contact1'),
				'u_contact2' => $this->input->post('upd_ad_c_code2')."-".$this->input->post('upd_ad_contact2'),
				'u_contact3' => $this->input->post('upd_ad_c_code3')."-".$this->input->post('upd_ad_contact3'),
				'u_country'  => $this->input->post('upd_ad_country'),
				'u_city'  	 => $this->input->post('upd_ad_city'),
				'u_zip'  	 => $this->input->post('upd_ad_zip'),
				'u_role'  	 => $this->input->post('upd_ad_role')
			);
			//print_r($data);
			$query = $this->m_user->save($data,$id);
			if($query){
				echo json_encode('true');
			}
			else{
				echo json_encode('false');
			}
		}
		public function deleteuser(){
			$id = $this->input->post('id');
			
			$query = $this->m_user->delete($id);

			echo json_encode('true');

		}
		
		public function getAllPets(){
			$id = $this->session->userdata('u_id');
			$query = $this->m_pets->getAllPets($id);
			
			echo json_encode($query);
			
		}
		public function addPet(){
			$id = $this->session->userdata('u_id');
			$filePath = "images/uploads/".$id;
			
			if (file_exists($filePath)) {
				$config['upload_path']  =  "images/uploads/".$id;
			} else {
				mkdir("images/uploads/".$id);
				$config['upload_path']  =  "images/uploads/".$id;
			}
			$config['allowed_types']        = 'gif|jpg|png|jpeg';
			$this->load->library('upload', $config);
			
			if ( ! $this->upload->do_upload('imgfile'))
            {
				//$error = array('error' => $this->upload->display_errors());
		 
				//echo json_encode($error);
			}else{
				$image = $this->upload->data();
				if($this->input->post('pet_status') == 4){
					$fromLost  = 1;
				}else{
					$fromLost  = 0;
				}
				
				$data = array(
					'user_id' => $this->session->userdata('u_id'),
					'specie' => $this->input->post('pet_specie'),
					'pet_name' => $this->input->post('pet_name'),
					'birthdate' => $this->input->post('pet_bday'),
					'gender' => $this->input->post('pet_gender'),
					'neutered' => $this->input->post('pet_neutered'),
					'fur'  => $this->input->post('pet_fur'),
					'fur_color'  	 => $this->input->post('pet_fur_color'),
					'eyecolor'  	 => $this->input->post('pet_eye_color'),
					'size'  	 => $this->input->post('pet_size'),
					'microchip' => $this->input->post('pet_microchip'),
					'tatto' => $this->input->post('pet_tattoo'),
					'status' => $this->input->post('pet_status'),
					'from_lost' => $fromLost,
					'nfc_chip' => $this->input->post('pet_nfc'),
					'kennel_id' => $this->input->post('pet_kennel'),
					'comment' => $this->input->post('pet_comment'),
					'picture' => $image['file_name'],
					'date_registered' =>  date("Y-m-d H:i:s")
				);
				//print_r($data);
				$query = $this->m_pets->save($data);
				if($query){
					$data['pet'] = $this->m_pets->getThisPetData($query);
					if($this->input->post('pet_status') == 4){
						$query2 = $this->m_pets->getEmails();
						if(count($query2) > 0){
							foreach($query2 as $u_m){
								$config = array();
									$config['useragent']           = "CodeIgniter";
									$config['mailpath']            = "/usr/sbin/sendmail"; // or "/usr/sbin/sendmail"
									$config['protocol']            = "smtp";
									$config['smtp_host']           = "smtp.unoeuro.com";
									$config['smtp_user']           = "support@petpost.info";
									$config['smtp_pass']           = "jeta59xiware";
									$config['smtp_port']           = "587";
									$config['smtp_crypto']         = "tls";
									$config['mailtype'] = 'html';
									$config['charset']  = 'utf-8';
									$config['newline']  = "\r\n";
									$config['wordwrap'] = TRUE;
									
									$this->load->library('email',$config);
									$this->email->from("support@petpost.info", "HELP! Pet Post");
									$this->email->to($u_m->u_email);
									$message = $this->load->view('pet_lost_email',$data,true);

									$this->email->subject("LOST PET");
									$this->email->message($message);
									$this->email->send();
							}
						}
					}
					echo json_encode(true);
				}
				else{
					echo json_encode(false);
				}
			}
		}
		public function getThisPetData(){
			$id = $this->input->post('id');
			$data = $this->m_pets->getThisPetData($id);
			echo json_encode($data);
		}
		public function updatePet(){
			$id = $this->session->userdata('u_id');
			$pet_id = $this->input->post('pet_id');
			$filePath = "images/uploads/".$id;
			
			if (file_exists($filePath)) {
				$config['upload_path']  =  "images/uploads/".$id;
			} else {
				mkdir("images/uploads/".$id);
				$config['upload_path']  =  "images/uploads/".$id;
			}
			$config['allowed_types']        = 'gif|jpg|png|jpeg';
			$this->load->library('upload', $config);
			
			if($this->input->post('pet_status') == 4){
				$fromLost  = 1;
			}else{
				$fromLost  = 0;
			}
			if ( ! $this->upload->do_upload('upd_imgfile'))
            {
				//$error = array('error' => $this->upload->display_errors());
		 
				//echo json_encode($error);
				$data = array(
					'specie' => $this->input->post('pet_specie'),
					'pet_name' => $this->input->post('pet_name'),
					'birthdate' => $this->input->post('pet_bday'),
					'gender' => $this->input->post('pet_gender'),
					'neutered' => $this->input->post('pet_neutered'),
					'fur'  => $this->input->post('pet_fur'),
					'fur_color'  	 => $this->input->post('pet_fur_color'),
					'eyecolor'  	 => $this->input->post('pet_eye_color'),
					'size'  	 => $this->input->post('pet_size'),
					'microchip' => $this->input->post('pet_microchip'),
					'tatto' => $this->input->post('pet_tattoo'),
					'status' => $this->input->post('pet_status'),
					'from_lost' => $fromLost,
					'nfc_chip' => $this->input->post('pet_nfc'),
					'kennel_id' => $this->input->post('pet_kennel'),
					'comment' => $this->input->post('pet_comment')
				);
			}else{
				$image = $this->upload->data();
				
				$data = array(
					'specie' => $this->input->post('pet_specie'),
					'pet_name' => $this->input->post('pet_name'),
					'birthdate' => $this->input->post('pet_bday'),
					'gender' => $this->input->post('pet_gender'),
					'neutered' => $this->input->post('pet_neutered'),
					'fur'  => $this->input->post('pet_fur'),
					'fur_color'  	 => $this->input->post('pet_fur_color'),
					'eyecolor'  	 => $this->input->post('pet_eye_color'),
					'size'  	 => $this->input->post('pet_size'),
					'microchip' => $this->input->post('pet_microchip'),
					'tatto' => $this->input->post('pet_tattoo'),
					'status' => $this->input->post('pet_status'),
					'from_lost' => $fromLost,
					'nfc_chip' => $this->input->post('pet_nfc'),
					'kennel_id' => $this->input->post('pet_kennel'),
					'comment' => $this->input->post('pet_comment'),
					'picture' => $image['file_name']
				);
			}
			//print_r($data);
				$query = $this->m_pets->updatePet($data,$pet_id);
				if($query){
					$data['pet'] = $this->m_pets->getThisPetData($pet_id);
					if($this->input->post('pet_status') == 4){
						$query2 = $this->m_pets->getEmails();
						if(count($query2) > 0){
							foreach($query2 as $u_m){
								$config = array();
									$config['useragent']           = "CodeIgniter";
									$config['mailpath']            = "/usr/sbin/sendmail"; // or "/usr/sbin/sendmail"
									$config['protocol']            = "smtp";
									$config['smtp_host']           = "smtp.unoeuro.com";
									$config['smtp_user']           = "support@petpost.info";
									$config['smtp_pass']           = "jeta59xiware";
									$config['smtp_port']           = "587";
									$config['smtp_crypto']         = "tls";
									$config['mailtype'] = 'html';
									$config['charset']  = 'utf-8';
									$config['newline']  = "\r\n";
									$config['wordwrap'] = TRUE;
									
									$this->load->library('email',$config);
									$this->email->from("support@petpost.info", "HELP! Pet Post");
									$this->email->to($u_m->u_email);
									$message = $this->load->view('pet_lost_email',$data,true);

									$this->email->subject("LOST PET");
									$this->email->message($message);
									$this->email->send();
							}
						}
					}
					echo json_encode(true);
				}
				else{
					echo json_encode(false);
				}
		}
		public function deletePet(){
			$id = $this->input->post('id');
			
			$query = $this->m_pets->deletePet($id);

			if($query){
					echo json_encode('true');
				}
				else{
					echo json_encode('false');
				}

		}
		public function addPerson(){
			$id = $this->session->userdata('u_id');
			$filePath = "images/persons/".$id;
			
			if (file_exists($filePath)) {
				$config['upload_path']  =  "images/persons/".$id;
			} else {
				mkdir("images/persons/".$id);
				$config['upload_path']  =  "images/persons/".$id;
			}
			$config['allowed_types']        = 'gif|jpg|png|jpeg';
			$this->load->library('upload', $config);
			
			if ( ! $this->upload->do_upload('person_imgfile'))
            {
				//$error = array('error' => $this->upload->display_errors());
		 
				//echo json_encode($error);
			}else{
				$image = $this->upload->data();
				
				$data = array(
					'user_id' => $this->session->userdata('u_id'),
					'person_name' => $this->input->post('person_name'),
					'country' => $this->input->post('person_country'),
					'city' => $this->input->post('person_city'),
					'bday' => $this->input->post('person_bday'),
					'gender' => $this->input->post('person_gender'),
					'eyecolor' => $this->input->post('person_eyecolor'),
					'haircolor' => $this->input->post('person_haircolor'),
					'height' => $this->input->post('person_height'),
					'weight' => $this->input->post('person_weight'),
					'handed' => $this->input->post('person_handed'),
					'specific_features' => $this->input->post('person_specific_features'),
					'contact1' => $this->input->post('person_contact1'),
					'contact2' => $this->input->post('person_contact2'),
					'contact3' => $this->input->post('person_contact3'),
					'comment' => $this->input->post('person_comment'),
					'status' => $this->input->post('person_status'),
					'status' => $this->input->post('person_status'),
					'last_seen_place' => $this->input->post('person_last_seen_place'),
					'last_seen_date' => $this->input->post('person_last_seen_date'),
					'pic' => $image['file_name'],
					'date_registered' =>  date("Y-m-d H:i:s")
				);
				//print_r($data);
				$query = $this->m_person->save($data);
				if($query){
					echo json_encode(true);
				}
				else{
					echo json_encode(false);
				}
			}
		}
		public function getAllPerson(){
			$id = $this->session->userdata('u_id');
			$query = $this->m_person->getAllPerson($id);
			
			echo json_encode($query);
			
		}
		public function deletePerson(){
			$id = $this->input->post('id');
			
			$query = $this->m_person->deletePerson($id);

			if($query){
					echo json_encode('true');
				}
				else{
					echo json_encode('false');
				}

		}
		public function getThisPersonData(){
			$id = $this->input->post('id');
			$data = $this->m_person->getThisPersonData($id);
			echo json_encode($data);
		}
		public function updatePerson(){
			$id = $this->session->userdata('u_id');
			$person_id = $this->input->post('person_id');
			$filePath = "images/persons/".$id;
			
			if (file_exists($filePath)) {
				$config['upload_path']  =  "images/persons/".$id;
			} else {
				mkdir("images/persons/".$id);
				$config['upload_path']  =  "images/persons/".$id;
			}
			$config['allowed_types']        = 'gif|jpg|png|jpeg';
			$this->load->library('upload', $config);

			if ( ! $this->upload->do_upload('upd_person_imgfile'))
            {
				//$error = array('error' => $this->upload->display_errors());
		 
				//echo json_encode($error);
				$data = array(
					'user_id' => $this->session->userdata('u_id'),
					'person_name' => $this->input->post('person_name'),
					'country' => $this->input->post('person_country'),
					'city' => $this->input->post('person_city'),
					'bday' => $this->input->post('person_bday'),
					'gender' => $this->input->post('person_gender'),
					'eyecolor' => $this->input->post('person_eyecolor'),
					'haircolor' => $this->input->post('person_haircolor'),
					'height' => $this->input->post('person_height'),
					'weight' => $this->input->post('person_weight'),
					'handed' => $this->input->post('person_handed'),
					'specific_features' => $this->input->post('person_specific_features'),
					'contact1' => $this->input->post('person_contact1'),
					'contact2' => $this->input->post('person_contact2'),
					'contact3' => $this->input->post('person_contact3'),
					'comment' => $this->input->post('person_comment'),
					'status' => $this->input->post('person_status'),
					'status' => $this->input->post('person_status'),
					'last_seen_place' => $this->input->post('person_last_seen_place'),
					'last_seen_date' => $this->input->post('person_last_seen_date'),
				);
			}else{
				$image = $this->upload->data();
				
				$data = array(
					'user_id' => $this->session->userdata('u_id'),
					'person_name' => $this->input->post('person_name'),
					'country' => $this->input->post('person_country'),
					'city' => $this->input->post('person_city'),
					'bday' => $this->input->post('person_bday'),
					'gender' => $this->input->post('person_gender'),
					'eyecolor' => $this->input->post('person_eyecolor'),
					'haircolor' => $this->input->post('person_haircolor'),
					'height' => $this->input->post('person_height'),
					'weight' => $this->input->post('person_weight'),
					'handed' => $this->input->post('person_handed'),
					'specific_features' => $this->input->post('person_specific_features'),
					'contact1' => $this->input->post('person_contact1'),
					'contact2' => $this->input->post('person_contact2'),
					'contact3' => $this->input->post('person_contact3'),
					'comment' => $this->input->post('person_comment'),
					'status' => $this->input->post('person_status'),
					'status' => $this->input->post('person_status'),
					'last_seen_place' => $this->input->post('person_last_seen_place'),
					'last_seen_date' => $this->input->post('person_last_seen_date'),
					'pic' => $image['file_name'],
				);
			}
			//print_r($data);
				$query = $this->m_person->updatePerson($data,$person_id);
				if($query){
					echo json_encode(true);
				}
				else{
					echo json_encode(false);
				}
		}
	}