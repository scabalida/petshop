<?php defined('BASEPATH') OR exit('No direct script access allowed');
	
	class user extends Admin_Controller {
		public function __construct() {
			parent::__construct();
			$this->load->model('m_user');
			$this->load->model('m_contact_id_order');
		}
		public function test(){

		}
		public function orderContactID(){
			$id = $this->session->userdata('u_id');
			$filePath = "images/contactid/".$id;
			
			if (file_exists($filePath)) {
				$config['upload_path']  =  "images/contactid/".$id;
			} else {
				mkdir("images/contactid/".$id);
				$config['upload_path']  =  "images/contactid/".$id;
			}
			$config['allowed_types']        = 'gif|jpg|png|jpeg';
			$this->load->library('upload', $config);
			
			if ( ! $this->upload->do_upload('c_tag_picture'))
            {
				//$error = array('error' => $this->upload->display_errors());
		 
				//echo json_encode($error);
			}else{
				$image = $this->upload->data();
				
				$data = array(
					'owner_id' => $this->session->userdata('u_id'),
					'owner_email' => $this->session->userdata('u_email'),
					'owner_fullname' => $this->session->userdata('u_fullname'),
					'owner_image' => $image['file_name'],
					'country' => $this->input->post('c_tag_country'),
					'street' => $this->input->post('c_tag_street'),
					'city' => $this->input->post('c_tag_city'),
					'zip' => $this->input->post('c_tag_zip'),
					'contact1' => $this->input->post('c_tag_contact'),
					'contact2'  => $this->input->post('c_tag_contact2'),
					'order_status'  => 0,
					'date_purchased' =>  date("Y-m-d H:i:s")
				);
				//print_r($data);
				$query = $this->m_contact_id_order->save($data);
				if($query){
					echo json_encode($query);
				}
				else{
					echo json_encode(0);
				}
			}
		}
		public function getAllContactIDOrders(){
			
			$query = $this->m_contact_id_order->getAllOrders();
			
			echo json_encode($query);
			
		}
		public function payContactIDNow($id){
				
				$usr_pay = 14.99;
				$shippingFee = 5;
				
				//$usr_pay = 0.1;
				//$shippingFee = 0.1;
				$config['business'] 			= 'petpost.info@gmail.com';
				//$config['business'] 			= 'cabalida.stephen@gmail.com';
				$config['cpp_header_image'] 	= ''; //Image header url [750 pixels wide by 90 pixels high]
				$config['return'] 				= base_url(). "settings";
				$config['cancel_return'] 		= base_url(). "settings";
				$config['notify_url'] 			= $notifyURL = base_url().'shop/saveDB'; //IPN Post
				$config['production'] 			= TRUE; //Its false by default and will use sandbox
				$this->load->library('paypal',$config);
				
				$this->paypal->add("CONTACT ID CARD",$usr_pay,1,1);
				$this->paypal->add("SHIPPING FEE",$shippingFee,1,$id);
				$this->paypal->add("-----------------",0,1,5);

				$this->paypal->pay();
		}
		public function updateToContactIDOrderShipped(){
			$data = array(
					'order_status' => 2,
					'date_shipped' => date("Y-m-d H:i:s")
				);
			$id = $this->input->post('id');
			$query = $this->m_contact_id_order->updateToShipped($data,$id);
			
			if($query){
				echo json_encode(true);
			}else{
				echo json_encode(false);
			}
			
		}
		public function getContactIDOrder(){
			$id =  $this->input->post('id');
			$query = $this->m_contact_id_order->getTagOrder($id);
			
			echo json_encode($query);
			
		}
		public function chckEmail(){
			$email = $this->input->post('em');
			$query = $this->m_user->chckEmail($email);
			if($query){
				echo json_encode('true');
			}else{
				echo json_encode('false');
			}
		}
		
		public function addUser(){
			$password = $this->input->post('pass');
			$newPass = md5($password)."stekiesca";
			$data = array(
				'u_fullname' => $this->input->post('name'),
				'u_contact1' => $this->input->post('c_code1')."-".$this->input->post('contact1'),
				'u_contact2' => $this->input->post('c_code2')."-".$this->input->post('contact2'),
				'u_contact3' => $this->input->post('c_code3')."-".$this->input->post('contact3'),
				'u_country'  => $this->input->post('country'),
				'u_city'  	 => $this->input->post('city'),
				'u_zip'  	 => $this->input->post('zip'),
				'u_email'  	 => $this->input->post('email'),
				'u_password' => $newPass,
				'date_registered' =>  date("Y-m-d"),
				'date_expired' =>  "0000-00-00",
				'membership' =>  0,
				'u_role' => $this->input->post('role'),
				'u_rights' => 0
			);
			if($this->input->post('role') == 1){
				$usr_pay = 20;
			}else{
				$usr_pay = 40;
			}
			//print_r($data);
			$query = $this->m_user->save($data);
			if($query){
				
				$id = md5($query);
				$hash = "7d7edf4122b9d9be2d4af422a1817af3";
				$email = $this->input->post('email') ;
				$link = base_url()."login/updaterights/".$hash.$query;
				$config = array();
				$config['useragent']           = "CodeIgniter";
				$config['mailpath']            = "/usr/sbin/sendmail"; // or "/usr/sbin/sendmail"
				$config['protocol']            = "smtp";
				$config['smtp_host']           = "smtp.unoeuro.com";
				$config['smtp_user']           = "support@petpost.info";
				$config['smtp_pass']           = "jeta59xiware";
				$config['smtp_port']           = "587";
				$config['smtp_crypto']         = "tls";
				$config['mailtype'] = 'html';
				$config['charset']  = 'utf-8';
				$config['newline']  = "\r\n";
				$config['wordwrap'] = TRUE;
				$this->load->library('email',$config);
				$this->email->from("support@petpost.info", "Pet Post");
				$this->email->to($email);
				$message = "<html><body><h3>Hi,<br><br>Click this link to verify your email.<br>".$link."<br><br>Thanks,<br>Pet Post</h3></body></html>";
				//$message = "Verification Link:".$link;
				$this->email->subject("Email Verification");
				$this->email->message($message);
				$this->email->send();
				
				
				echo json_encode(array("id"=>$query,"usr_pay"=>$usr_pay,"status"=>'true'));
			}
			else{
				echo json_encode(array("status"=>'false'));
			}
			
		}
		public function paynow($id,$usr_pay){
				$usr_pay = 40;
				$config['business'] 			= 'petpost.info@gmail.com';
				//$config['business'] 			= 'cabalida.stephen@gmail.com';
				$config['cpp_header_image'] 	= ''; //Image header url [750 pixels wide by 90 pixels high]
				$config['return'] 				= base_url();
				$config['cancel_return'] 		= base_url();
				$config['notify_url'] 			= $notifyURL = base_url().'shop/saveDB'; //IPN Post
				$config['production'] 			= TRUE; //Its false by default and will use sandbox
				$this->load->library('paypal',$config);
				
				$this->paypal->add("Sign Up Payment",$usr_pay,1,$id);
				$this->paypal->add("-----------------",0,1,1);
				//$this->paypal->add("SHIPPING FEE",$shippingFee,1,$inv_id);

				$this->paypal->pay();
			
		}
		public function editUserData(){
			$id = $this->input->post('id');
			
			$data = array(
				'u_fullname' => $this->input->post('fname'),
				'u_email' =>  $this->input->post('email'),
				'u_bday' =>  $this->input->post('bday'),
				'u_role' =>  $this->input->post('role')
			);
			//print_r($data);
			$query = $this->m_user->save($data,$id);
			if($query){
				echo json_encode('true');
			}
			else{
				echo json_encode('false');
			}
			
		}
		public function deleteuser(){
			$id = $this->input->post('id');
			
			$query = $this->m_user->delete($id);

			echo json_encode('true');

		}

		public function getAllUsers(){
			$id = $this->session->userdata('u_id');
			//echo $id;
			$data= $this->m_user->getAllUsers($id);
			echo json_encode($data);
		}

		public function updateUnameAndPass(){
			$id = $this->session->userdata('u_id');
			$data = array(
				'u_password' => md5($this->input->post('newpassword')),
				'u_username' => $this->input->post('newusername')
			);
			
			$query = $this->m_user->save($data,$id);
			if($query){
				$setdata = array(
					'u_username' => $this->input->post('newusername')
				);
				$this->session->set_userdata($setdata);
				echo json_encode('true');
			}else{
				echo json_encode('false');
			}
			
		}


	}