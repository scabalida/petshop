<?php defined('BASEPATH') OR exit('No direct script access allowed');
	
	class contact extends Admin_Controller {
		public function __construct() {
		parent::__construct();

    }
		public function index() {
			$this->load->view('contact');
		}
		public function test(){

				$config = array();
				$config['useragent']           = "CodeIgniter";
				$config['mailpath']            = "/usr/sbin/sendmail"; // or "/usr/sbin/sendmail"
				$config['protocol']            = "sendmail";
				$config['mailtype'] = 'html';
				$config['charset']  = 'utf-8';
				$config['newline']  = "\r\n";
				$config['wordwrap'] = TRUE;
				$this->load->library('email',$config);
				$this->email->from("test@gmail.com", "Test Mail");
				$this->email->to("support@petpost.info");
				$this->email->subject("Pet Post Contact Form");
				$this->email->message("Hello World.");

				if($this->email->send()){
					echo json_encode('true');
				}else{
					echo json_encode('false');
				}
		}
		public function sendEmail(){
				$name = $this->input->post('e_name');
				$email = $this->input->post('e_email');
				$contact = $this->input->post('e_contact');
				$message = $this->input->post('e_message');
				$config = array();
				$config['useragent']           = "CodeIgniter";
				$config['mailpath']            = "/usr/sbin/sendmail"; // or "/usr/sbin/sendmail"
				$config['protocol']            = "smtp";
				$config['smtp_host']           = "smtp.unoeuro.com";
				$config['smtp_user']           = "support@petpost.info";
				$config['smtp_pass']           = "jeta59xiware";
				$config['smtp_port']           = "587";
				$config['smtp_crypto']         = "tls";
				$config['mailtype'] = 'html';
				$config['charset']  = 'utf-8';
				$config['newline']  = "\r\n";
				$config['wordwrap'] = TRUE;
				$this->load->library('email',$config);
				$this->email->from($email, ucwords($name));
				$this->email->to("support@petpost.info");
				$this->email->subject("Pet Post Contact Form - ".ucwords($name)." ".$contact);
				$this->email->message($message);

				if($this->email->send()){
					echo json_encode('true');
				}else{
					echo json_encode('false');
				}
		}
	}