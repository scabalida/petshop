<?php defined('BASEPATH') OR exit('No direct script access allowed');
	
	class shop extends Admin_Controller {
		public function __construct() {
		parent::__construct();
		header('Access-Control-Allow-Origin: *');
		header('Access-Control-Allow-Methods: GET, POST, PATCH, PUT, DELETE, OPTIONS');
		header('Access-Control-Allow-Headers: Origin, Content-Type, X-Auth-Token');
		$this->load->model('m_user');
		$this->load->model('m_pets');
		$this->load->model('m_shop');
		$this->load->model('m_tag_order');
		$this->load->model('m_contact_id_order');
		$this->load->library('paypal_lib');
    }
		public function index() {
			$this->load->view('shop');
		}
		public function sendTestMail(){
			$id = 17;
				$query =  $this->m_shop->getInvoice($id);
				$data['invoice_number'] =  str_pad($id, 10, '0', STR_PAD_LEFT);
				$data['email'] = $query[0]->email;
				$data['country'] = $query[0]->country;
				$data['address'] = $query[0]->home_address;
				$data['contact1'] = $query[0]->contact1;
				$data['contact2'] = $query[0]->contact2;
				$data['total_payment'] = $query[0]->total_payment;
				$data['fullname'] = ucwords($query[0]->fullname);
				$data['order_date'] = date("l, d F, Y h:i:s", strtotime($query[0]->datetime));
				$data['order_data'] = array();
				$prod = json_decode($query[0]->products);
				for($x = 0; $x<count($prod); $x++){
					$thisData = [$prod[$x][0],$prod[$x][1],1];
					array_push($data['order_data'],$thisData);
				}	
				
				$config = array();
				$config['useragent']           = "CodeIgniter";
				$config['mailpath']            = "/usr/sbin/sendmail"; // or "/usr/sbin/sendmail"
				$config['protocol']            = "smtp";
				$config['smtp_host']           = "smtp.unoeuro.com";
				$config['smtp_user']           = "support@petpost.info";
				$config['smtp_pass']           = "jeta59xiware";
				$config['smtp_port']           = "587";
				$config['smtp_crypto']         = "tls";
				$config['mailtype'] = 'html';
				$config['charset']  = 'utf-8';
				$config['newline']  = "\r\n";
				$config['wordwrap'] = TRUE;
				$this->load->library('email',$config);
				$this->email->from("support@petpost.info", "Pet Post Shop");
				$this->email->to($query[0]->email);
				$message = $this->load->view('order_email',$data,true);
				//$message = "Verification Link:".$link;
				$invID = str_pad($id, 10, '0', STR_PAD_LEFT);
				$this->email->subject("Order Being Processed #".$invID);
				$this->email->message($message);

				$this->email->send();
		}
		public function test() {
			$id = 4;
			$query =  $this->m_shop->getInvoice($id);
			$data['invoice_number'] =  str_pad($id, 10, '0', STR_PAD_LEFT);
			$data['email'] = $query[0]->email;
			$data['country'] = $query[0]->country;
			$data['address'] = $query[0]->home_address;
			$data['contact1'] = $query[0]->contact1;
			$data['contact2'] = $query[0]->contact2;
			$data['total_payment'] = $query[0]->total_payment;
			$data['fullname'] = ucwords($query[0]->fullname);
			$data['order_date'] = date("l, d F, Y h:i:s", strtotime($query[0]->datetime));
			$data['order_data'] = array();
			$prod = json_decode($query[0]->products);
			for($x = 0; $x<count($prod); $x++){
				$thisData = [$prod[$x][0],$prod[$x][1],1];
				array_push($data['order_data'],$thisData);
			}
			//print_r($data['order_data']);
			$this->load->view('order_email',$data);
		}
		public function addProduct(){
			$id = $this->session->userdata('u_id');
			$filePath = "images/products/".$id;
			$img1 = "";
			$img2 = "";
			if (file_exists($filePath)) {
				$config['upload_path']  =  "images/products/".$id;
			} else {
				mkdir("images/products/".$id);
				$config['upload_path']  =  "images/products/".$id;
			}
			$config['allowed_types']        = 'gif|jpg|png|jpeg';
			$this->load->library('upload', $config);
			
			if ( ! $this->upload->do_upload('productPic1') )
            {
				//$error = array('error' => $this->upload->display_errors());
		 
				//echo json_encode($error);
			}else{
				$image = $this->upload->data();
				$img1 = $image['file_name'];

			}
			if ( ! $this->upload->do_upload('productPic2') )
            {
				//$error = array('error' => $this->upload->display_errors());
		 
				//echo json_encode($error);
			}else{
				$image = $this->upload->data();
				$img2 = $image['file_name'];

			}
			$data = array(
				'user_id' => $this->session->userdata('u_id'),
				'product_name' => $this->input->post('product_name'),
				'product_desc' => $this->input->post('product_desc'),
				'product_price' => $this->input->post('product_price'),
				'product_weight' => $this->input->post('product_weight'),
				'product_pic' => $img1,
				'product_pic2' => $img2,
				'product_status' => 1,
				'date_added' =>  date("Y-m-d H:i:s")
			);
			//print_r($data);
			$query = $this->m_shop->save($data);
			if($query){
				echo json_encode(true);
			}
			else{
				echo json_encode(false);
			}
		}
		
		public function getAllProducts(){
			$query = $this->m_shop->getAllProducts();
			
			echo json_encode($query);
			
		}
		
		public function getThisProductData(){
			$id = $this->input->post('id');
			$data = $this->m_shop->getThisProductData($id);
			echo json_encode($data);
		}
		public function updateProduct(){
			$product_id = $this->input->post('product_id');
			$id = $this->session->userdata('u_id');
			$filePath = "images/products/".$id;
			
			if (file_exists($filePath)) {
				$config['upload_path']  =  "images/products/".$id;
			} else {
				mkdir("images/products/".$id);
				$config['upload_path']  =  "images/products/".$id;
			}
			$config['allowed_types']        = 'gif|jpg|png|jpeg';
			$this->load->library('upload', $config);
			if($this->input->post('pictureone') == "" && $this->input->post('picturetwo') == ""){
				$data = array(
					'product_name' => $this->input->post('product_name'),
					'product_desc' => $this->input->post('product_desc'),
					'product_price' => $this->input->post('product_price'),
					'product_weight' => $this->input->post('product_weight')
				);
			}else if($this->input->post('pictureone') != "" && $this->input->post('picturetwo') == ""){
				if ( ! $this->upload->do_upload('upd_productPic'))
				{
					//$error = array('error' => $this->upload->display_errors());
			 
					//echo json_encode($error);
					
				}else{
					$image = $this->upload->data();
					
					$data = array(
						'product_name' => $this->input->post('product_name'),
						'product_desc' => $this->input->post('product_desc'),
						'product_price' => $this->input->post('product_price'),
						'product_weight' => $this->input->post('product_weight'),
						'product_pic' => $image['file_name']
					);
				}
			}else if($this->input->post('pictureone') == "" && $this->input->post('picturetwo') != ""){
				if ( ! $this->upload->do_upload('upd_productPic2'))
				{
					//$error = array('error' => $this->upload->display_errors());
			 
					//echo json_encode($error);
					
				}else{
					$image = $this->upload->data();
					
					$data = array(
						'product_name' => $this->input->post('product_name'),
						'product_desc' => $this->input->post('product_desc'),
						'product_price' => $this->input->post('product_price'),
						'product_weight' => $this->input->post('product_weight'),
						'product_pic2' => $image['file_name']
					);
				}
			}else if($this->input->post('pictureone') != "" && $this->input->post('picturetwo') != ""){
				if ( ! $this->upload->do_upload('upd_productPic'))
				{
					//$error = array('error' => $this->upload->display_errors());
			 
					//echo json_encode($error);
				}else{
					$image = $this->upload->data();
					$img1 = $image['file_name'];

				}
				if ( ! $this->upload->do_upload('upd_productPic2'))
				{
					//$error = array('error' => $this->upload->display_errors());
			 
					//echo json_encode($error);
				}else{
					$image = $this->upload->data();
					$img2 = $image['file_name'];

				}
				$data = array(
					'product_name' => $this->input->post('product_name'),
					'product_desc' => $this->input->post('product_desc'),
					'product_price' => $this->input->post('product_price'),
					'product_weight' => $this->input->post('product_weight'),
					'product_pic' => $img1,
					'product_pic2' => $img2
				);
			}

			//print_r($data);
			$query = $this->m_shop->updateProduct($data,$product_id);
			if($query){
				echo json_encode(true);
			}
			else{
				echo json_encode(false);
			}
		}
		public function deleteProduct(){
			$id = $this->input->post('id');
			
			$query = $this->m_shop->deleteProduct($id);

			if($query){
					echo json_encode('true');
				}
				else{
					echo json_encode('false');
				}

		}
		public function checkout() {
			$products = $this->input->post('arrData');
			$shippingFee = $this->input->post('shippingFee');
			$newArr = json_decode(json_encode($products),true);
			$allProd = $newArr[0];
			$cart = json_decode($allProd);
			
			$prodData  = array();
			foreach ($cart as $paypal_items) {
				$thisData = [$paypal_items->product_id,$paypal_items->name,$paypal_items->pic,$paypal_items->user_id];
				array_push($prodData,$thisData);
			}
			
				$data = array(
					'fullname' => $this->input->post('inv_name'),
					'country' => $this->input->post('inv_country'),
					'home_address' => $this->input->post('inv_address'),
					'email' => $this->input->post('inv_email'),
					'contact1' => $this->input->post('inv_contact'),
					'contact2' => $this->input->post('inv_contact2'),
					'products' => json_encode($prodData),
					'total_payment' => $this->input->post('TotalFee'),
					'datetime' => date("Y-m-d H:i:s"),
					'order_status' => 0,
				);
				
			$query = $this->m_shop->saveInvoice($data);
			$inv_id = $query;
			
			if (isset($_COOKIE['allProd'])) {
				unset($_COOKIE['allProd']);
				setcookie('allProd', '', time() - 3600, '/'); // empty value and old timestamp
			}
			$config['business'] 			= 'petpost.info@gmail.com';
			//$config['business'] 			= 'cabalida.stephen@gmail.com';
			$config['cpp_header_image'] 	= ''; //Image header url [750 pixels wide by 90 pixels high]
			$config['return'] 				= base_url().'shop/success';
			$config['cancel_return'] 		= base_url().'shop/cancel';
			$config['notify_url'] 			= $notifyURL = base_url().'shop/saveDB'; //IPN Post
			$config['production'] 			= TRUE; //Its false by default and will use sandbox
			$this->load->library('paypal',$config);

			foreach ($cart as $paypal_items) {
				$this->paypal->add($paypal_items->name,$paypal_items->price,1,$paypal_items->product_id);
			}
			$this->paypal->add("SHIPPING FEE",$shippingFee,1,$inv_id);
			$this->paypal->add("-----------------",0,1,2);

			$this->paypal->pay();
		//	print_r($newArr);
		}
		public function success(){
			header('Location: '.base_url().'shop');
		}
		 
		public function cancel(){
			header('Location: '.base_url().'shop');
		}
		public function saveDB(){
			//paypal return transaction details array
			$paypalInfo	= $this->input->post();
			$checkStat = $paypalInfo["item_number".$paypalInfo['num_cart_items']];
			if($checkStat == 1){
				$numb = "item_number".($paypalInfo['num_cart_items'] - 1);
				$u_id = $paypalInfo[$numb];
				$data = array(
					'date_expired' =>  date('Y-m-d', strtotime('+1 year', strtotime(date("Y-m-d")))),
					'membership' => 1
				);
				
				$this->m_user->save($data,$u_id);
			}else if($checkStat == 2){
				$numb = "item_number".($paypalInfo['num_cart_items'] - 1);
				$id = $paypalInfo[$numb];
				$data = array(
					'order_status' => 1
				);
				
				$this->m_shop->updateInvoice($data,$id);
			 
				$query =  $this->m_shop->getInvoice($id);
				$data['invoice_number'] =  str_pad($id, 10, '0', STR_PAD_LEFT);
				$data['email'] = $query[0]->email;
				$data['country'] = $query[0]->country;
				$data['address'] = $query[0]->home_address;
				$data['contact1'] = $query[0]->contact1;
				$data['contact2'] = $query[0]->contact2;
				$data['total_payment'] = $query[0]->total_payment;
				$data['fullname'] = ucwords($query[0]->fullname);
				$data['order_date'] = date("l, d F, Y h:i:s", strtotime($query[0]->datetime));
				$data['order_data'] = array();
				$prod = json_decode($query[0]->products);
				for($x = 0; $x<count($prod); $x++){
					$thisData = [$prod[$x][0],$prod[$x][1],1];
					array_push($data['order_data'],$thisData);
				}	
				
				$config = array();
				$config['useragent']           = "CodeIgniter";
				$config['mailpath']            = "/usr/sbin/sendmail"; // or "/usr/sbin/sendmail"
				$config['protocol']            = "smtp";
				$config['smtp_host']           = "smtp.unoeuro.com";
				$config['smtp_user']           = "support@petpost.info";
				$config['smtp_pass']           = "jeta59xiware";
				$config['smtp_port']           = "587";
				$config['smtp_crypto']         = "tls";
				$config['mailtype'] = 'html';
				$config['charset']  = 'utf-8';
				$config['newline']  = "\r\n";
				$config['wordwrap'] = TRUE;
				$this->load->library('email',$config);
				$this->email->from("support@petpost.info", "Pet Post Shop");
				$this->email->to($query[0]->email);
				$message = $this->load->view('order_email',$data,true);
				$invID = str_pad($id, 10, '0', STR_PAD_LEFT);
				$this->email->subject("Order Being Processed #".$invID);
				$this->email->message($message);

				$this->email->send();
			}else if($checkStat == 3){
				$numb = "item_number".($paypalInfo['num_cart_items'] - 1);
				$id = $paypalInfo[$numb];
				$getExp = $this->m_user->getDateExpiration($id);
				$oldExpDate = strtotime($getExp[0]->date_expired);
				$toDate = strtotime(date("Y-m-d"));
				$newExpDate = "";
				
				if($oldExpDate > $toDate){
					$newExpDate = date('Y-m-d', strtotime('+1 year', $oldExpDate));
				}else{
					$newExpDate = date('Y-m-d', strtotime('+1 year', $toDate));
				}
				
				$data = array(
					'date_expired' => $newExpDate,
					'membership' => 1
				);
				//print_r($data);
				$this->m_user->save($data,$id);
			}else if($checkStat == 4){
				$numb = "item_number".($paypalInfo['num_cart_items'] - 1);
				$id = $paypalInfo[$numb];
				
				$data = array(
					'order_status' => 1
				);
				//print_r($data);
				$this->m_tag_order->updateStatus($data,$id);
				
				$query =  $this->m_tag_order->getTagInvoice($id);
				$data['invoice_number'] =  str_pad($id, 10, '0', STR_PAD_LEFT);
				$data['email'] = $query[0]->owner_email;
				$data['country'] = $query[0]->country;
				$data['address'] = $query[0]->street . "," . $query[0]->city . "," . $query[0]->zip;
				$data['contact1'] = $query[0]->contact1;
				$data['contact2'] = $query[0]->contact2;
				$data['fullname'] = ucwords($query[0]->owner_fullname);
				$data['order_date'] = date("l, d F, Y h:i:s", strtotime($query[0]->date_purchased));
				
				$config = array();
				$config['useragent']           = "CodeIgniter";
				$config['mailpath']            = "/usr/sbin/sendmail"; // or "/usr/sbin/sendmail"
				$config['protocol']            = "smtp";
				$config['smtp_host']           = "smtp.unoeuro.com";
				$config['smtp_user']           = "support@petpost.info";
				$config['smtp_pass']           = "jeta59xiware";
				$config['smtp_port']           = "587";
				$config['smtp_crypto']         = "tls";
				$config['mailtype'] = 'html';
				$config['charset']  = 'utf-8';
				$config['newline']  = "\r\n";
				$config['wordwrap'] = TRUE;
				$this->load->library('email',$config);
				$this->email->from("support@petpost.info", "Pet Post Shop");
				$this->email->to($query[0]->owner_email);
				$message = $this->load->view('tag_order_email',$data,true);
				$invID = str_pad($id, 10, '0', STR_PAD_LEFT);
				$this->email->subject("Order Being Processed #".$invID);
				$this->email->message($message);

				$this->email->send();
			}else if($checkStat == 5){
				$numb = "item_number".($paypalInfo['num_cart_items'] - 1);
				$id = $paypalInfo[$numb];
				
				$data = array(
					'order_status' => 1
				);
				//print_r($data);
				$this->m_contact_id_order->updateStatus($data,$id);
				
				$query =  $this->m_contact_id_order->getTagInvoice($id);
				$data['invoice_number'] =  str_pad($id, 10, '0', STR_PAD_LEFT);
				$data['email'] = $query[0]->owner_email;
				$data['country'] = $query[0]->country;
				$data['address'] = $query[0]->street . "," . $query[0]->city . "," . $query[0]->zip;
				$data['contact1'] = $query[0]->contact1;
				$data['contact2'] = $query[0]->contact2;
				$data['fullname'] = ucwords($query[0]->owner_fullname);
				$data['order_date'] = date("l, d F, Y h:i:s", strtotime($query[0]->date_purchased));
				
				$config = array();
				$config['useragent']           = "CodeIgniter";
				$config['mailpath']            = "/usr/sbin/sendmail"; // or "/usr/sbin/sendmail"
				$config['protocol']            = "smtp";
				$config['smtp_host']           = "smtp.unoeuro.com";
				$config['smtp_user']           = "support@petpost.info";
				$config['smtp_pass']           = "jeta59xiware";
				$config['smtp_port']           = "587";
				$config['smtp_crypto']         = "tls";
				$config['mailtype'] = 'html';
				$config['charset']  = 'utf-8';
				$config['newline']  = "\r\n";
				$config['wordwrap'] = TRUE;
				$this->load->library('email',$config);
				$this->email->from("support@petpost.info", "Pet Post Shop");
				$this->email->to($query[0]->owner_email);
				$message = $this->load->view('contactid_order_email',$data,true);
				$invID = str_pad($id, 10, '0', STR_PAD_LEFT);
				$this->email->subject("Order Being Processed #".$invID);
				$this->email->message($message);

				$this->email->send();
			}
					
		}
		
		public function getForDelivery(){
			$query = $this->m_shop->getForDelivery();
			
			echo json_encode($query);
			
		}
		public function updateToShipped(){
			$data = array(
					'order_status' => 2,
					'date_shipped' => date("Y-m-d H:i:s")
				);
			$id = $this->input->post('id');
			$query = $this->m_shop->updateToShipped($data,$id);
			
			if($query){
				echo json_encode(true);
			}else{
				echo json_encode(false);
			}
			
		}
		public function getShipped(){
			$query = $this->m_shop->getShipped();
			
			echo json_encode($query);
			
		}
		public function showProducts(){
			$id = $this->input->post('id');
			$query = $this->m_shop->showProducts($id);
			
			echo json_encode($query);
			
		}
		
	}