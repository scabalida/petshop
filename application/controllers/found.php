<?php defined('BASEPATH') OR exit('No direct script access allowed');
	
	class found extends Admin_Controller {
		public function __construct() {
		parent::__construct();
		$this->load->model('m_pets');
    }
		public function index() {
			$this->load->view('found');
		}
		public function mylist(){
			$this->load->view('found_list');
		}
		public function addToList(){
			$query = $this->m_pets->addPetToList($this->input->post('id'));
			
			if($query == 1){
				echo json_encode('1');
			}
			else if($query == 2){
				echo json_encode('2');
			}
			else if($query == 3){
				echo json_encode('3');
			}
		}
		public function getMyFoundList(){
			$query = $this->m_pets->getMyPetFoundList();
			
			echo json_encode($query);
		}
		public function getAllFoundPet(){
			$query = $this->m_pets->getAllFoundPet();
			
			echo json_encode($query);
		}
		public function searchFoundPet(){
			$data = array();
			if($this->input->post('petID') != "" && $this->input->post('petID') != null){
				$pushArray = array(
					"row_name" => "pet_id",
					"sql" => 0,
					"row_value" => $this->input->post('petID')
				);
				array_push($data,$pushArray);
			}
			if($this->input->post('memberID') != "" && $this->input->post('memberID') != null){
				$pushArray = array(
					"row_name" => "user_id",
					"sql" => 0,
					"row_value" => $this->input->post('memberID')
				);
				array_push($data,$pushArray);
			}
			if($this->input->post('petName') != "" && $this->input->post('petName') != null){
				$pushArray = array(
					"row_name" => "pet_name",
					"sql" => 1,
					"row_value" => $this->input->post('petName')
				);
				array_push($data,$pushArray);
			}
			if($this->input->post('petSpecie') != "" && $this->input->post('petSpecie') != null){
				$pushArray = array(
					"row_name" => "specie",
					"sql" => 0,
					"row_value" => $this->input->post('petSpecie')
				);
				array_push($data,$pushArray);
			}
			if($this->input->post('petFurColor') != "" && $this->input->post('petFurColor') != null){
				$pushArray = array(
					"row_name" => "fur_color",
					"sql" => 0,
					"row_value" => $this->input->post('petFurColor')
				);
				array_push($data,$pushArray);
			}
			if($this->input->post('petFur') != "" && $this->input->post('petFur') != null){
				$pushArray = array(
					"row_name" => "fur",
					"sql" => 0,
					"row_value" => $this->input->post('petFur')
				);
				array_push($data,$pushArray);
			}
			if($this->input->post('petEyeColor') != "" && $this->input->post('petEyeColor') != null){
				$pushArray = array(
					"row_name" => "eyecolor",
					"sql" => 0,
					"row_value" => $this->input->post('petEyeColor')
				);
				array_push($data,$pushArray);
			}
			if($this->input->post('petSize') != "" && $this->input->post('petSize') != null){
				$pushArray = array(
					"row_name" => "size",
					"sql" => 0,
					"row_value" => $this->input->post('petSize')
				);
				array_push($data,$pushArray);
			}
			if($this->input->post('petGender') != "" && $this->input->post('petGender') != null){
				$pushArray = array(
					"row_name" => "gender",
					"sql" => 0,
					"row_value" => $this->input->post('petGender')
				);
				array_push($data,$pushArray);
			}
			if($this->input->post('petMicrochip') != "" && $this->input->post('petMicrochip') != null){
				$pushArray = array(
					"row_name" => "microchip",
					"sql" => 1,
					"row_value" => $this->input->post('petMicrochip')
				);
				array_push($data,$pushArray);
			}
			if($this->input->post('petTattoo') != "" && $this->input->post('petTattoo') != null){
				$pushArray = array(
					"row_name" => "tatto",
					"sql" => 1,
					"row_value" => $this->input->post('petTattoo')
				);
				array_push($data,$pushArray);
			}
			if($this->input->post('petNfc') != "" && $this->input->post('petNfc') != null){
				$pushArray = array(
					"row_name" => "nfc_chip",
					"sql" => 1,
					"row_value" => $this->input->post('petNfc')
				);
				array_push($data,$pushArray);
			}
			if($this->input->post('petKennel') != "" && $this->input->post('petKennel') != null){
				$pushArray = array(
					"row_name" => "kennel_id",
					"sql" => 1,
					"row_value" => $this->input->post('petKennel')
				);
				array_push($data,$pushArray);
			}
			$query = $this->m_pets->searchFoundPet($data);
			echo json_encode($query);
		}

	}