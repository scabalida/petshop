<?php defined('BASEPATH') OR exit('No direct script access allowed');
	
	class renew extends Admin_Controller {
		public function __construct() {
		parent::__construct();
		$this->load->model('m_user');
    }
    
		public function index(){
			$this->load->view('renew');
		}
		public function test(){
			$id = 1;
			$query = $this->m_user->getDateExpiration($id);
			$oldExpDate = strtotime($query[0]->date_expired);
			$toDate = strtotime(date("Y-m-d"));
			$newExpDate = "";
			
			if($oldExpDate > $toDate){
				$newExpDate = date('Y-m-d', strtotime('+1 year', $oldExpDate));
			}else{
				$newExpDate = date('Y-m-d', strtotime('+1 year', $toDate));
			}
			
			$data = array(
				'date_expired' => $newExpDate,
				'membership' => 1
			);
			//print_r($data);
			$this->m_user->renewNow($data,$id);
		}
		public function getID(){
			$email = $this->input->post('r_email');
			$query = $this->m_user->getID($email);
			if(count($query) > 0){
				echo json_encode(array("status"=>'true',"id"=>$query[0]->u_id));
			}else{
				echo json_encode(array("status"=>'true'));
			}
		}
		public function pay($id,$role){
				if($role == 1){
					$usr_pay = 20;
				}else{
					$usr_pay = 40;
				}
				$data = array(
					'u_role' => $role
				);
				//print_r($data);
				$this->m_user->save($data,$id);
				$config['business'] 			= 'petpost.info@gmail.com';
				//$config['business'] 			= 'cabalida.stephen@gmail.com';
				$config['cpp_header_image'] 	= ''; //Image header url [750 pixels wide by 90 pixels high]
				$config['return'] 				= base_url();
				$config['cancel_return'] 		= base_url();
				$config['notify_url'] 			= $notifyURL = base_url().'shop/saveDB'; //IPN Post
				$config['production'] 			= TRUE; //Its false by default and will use sandbox
				$this->load->library('paypal',$config);
				//$usr_pay = 0.1;
				$this->paypal->add("Renew Membership Payment",$usr_pay,1,$id);
				$this->paypal->add("-----------------",0,1,3);
				//$this->paypal->add("SHIPPING FEE",$shippingFee,1,$inv_id);

				$this->paypal->pay();
			
		}

	}