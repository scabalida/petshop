<?php defined('BASEPATH') OR exit('No direct script access allowed');
	
	class pet extends Admin_Controller {
		public function __construct() {
		parent::__construct();
		$this->load->model('m_user');
		$this->load->model('m_pets');
		$this->load->model('m_comments');
		$this->load->model('m_tag_order');

    }
		public function sendTestMail(){
				$id = 1;
				$query =  $this->m_tag_order->getTagInvoice($id);
				$data['invoice_number'] =  str_pad($id, 10, '0', STR_PAD_LEFT);
				$data['email'] = $query[0]->owner_email;
				$data['country'] = $query[0]->country;
				$data['address'] = $query[0]->street . "," . $query[0]->city . "," . $query[0]->zip;
				$data['contact1'] = $query[0]->contact1;
				$data['contact2'] = $query[0]->contact2;
				$data['fullname'] = ucwords($query[0]->owner_fullname);
				$data['order_date'] = date("l, d F, Y h:i:s", strtotime($query[0]->date_purchased));
				
				$config = array();
				$config['useragent']           = "CodeIgniter";
				$config['mailpath']            = "/usr/sbin/sendmail"; // or "/usr/sbin/sendmail"
				$config['protocol']            = "smtp";
				$config['smtp_host']           = "smtp.unoeuro.com";
				$config['smtp_user']           = "support@petpost.info";
				$config['smtp_pass']           = "jeta59xiware";
				$config['smtp_port']           = "587";
				$config['smtp_crypto']         = "tls";
				$config['mailtype'] = 'html';
				$config['charset']  = 'utf-8';
				$config['newline']  = "\r\n";
				$config['wordwrap'] = TRUE;
				$this->load->library('email',$config);
				$this->email->from("support@petpost.info", "Pet Post Shop");
				$this->email->to($query[0]->owner_email);
				$message = $this->load->view('tag_order_email',$data,true);
				//$message = "Verification Link:".$link;
				$invID = str_pad($id, 10, '0', STR_PAD_LEFT);
				$this->email->subject("Order Being Processed #".$invID);
				$this->email->message($message);

				$this->email->send();
		}
		public function profile($id) {
			$data['pet'] =  $this->m_pets->getThisPetData($id);
			$data['user'] =  $this->m_user->getUserData($data['pet'][0]->user_id);
			$this->load->view('petprofile',$data);
		}
		
		public function addComment(){

			$config['upload_path']  =  "images/comments";

			$config['allowed_types']        = 'gif|jpg|png|jpeg';
			$this->load->library('upload', $config);
			
			if ( ! $this->upload->do_upload('c_imgfile'))
            {
				//$error = array('error' => $this->upload->display_errors());
		 
				//echo json_encode($error);
			}else{
				$image = $this->upload->data();
				
				$data = array(
					'pet_id' => $this->input->post('pet_id'),
					'pet_owner_id' => $this->input->post('pet_owner_id'),
					'fullname' => $this->input->post('c_fname'),
					'address' => $this->input->post('c_address'),
					'email' => $this->input->post('c_email'),
					'contact' => $this->input->post('c_contact'),
					'other_info' => $this->input->post('other_info'),
					'date_found' => $this->input->post('date_found'),
					'time_found' => $this->input->post('time_found'),
					'date_commented'  => date("Y-m-d H:i:s"),
					'picture'  	 =>  $image['file_name']
				);
				//print_r($data);
				$query = $this->m_comments->save($data);
				if($query){
					
					echo json_encode(true);
				}
				else{
					echo json_encode(false);
				}
			}
		}
		public function orderTag(){
			$id = $this->session->userdata('u_id');
			$filePath = "images/tags/".$id;
			
			if (file_exists($filePath)) {
				$config['upload_path']  =  "images/tags/".$id;
			} else {
				mkdir("images/tags/".$id);
				$config['upload_path']  =  "images/tags/".$id;
			}
			$config['allowed_types']        = 'gif|jpg|png|jpeg';
			$this->load->library('upload', $config);
			
			if ( ! $this->upload->do_upload('tag_picture'))
            {
				//$error = array('error' => $this->upload->display_errors());
		 
				//echo json_encode($error);
			}else{
				$image = $this->upload->data();
				
				$data = array(
					'owner_id' => $this->session->userdata('u_id'),
					'owner_email' => $this->session->userdata('u_email'),
					'owner_fullname' => $this->session->userdata('u_fullname'),
					'pet_name' => $this->input->post('tag_petname'),
					'country' => $this->input->post('tag_country'),
					'street' => $this->input->post('tag_street'),
					'city' => $this->input->post('tag_city'),
					'zip' => $this->input->post('tag_zip'),
					'contact1' => $this->input->post('tag_contact'),
					'contact2'  => $this->input->post('tag_contact2'),
					'order_status'  => 0,
					'pet_image' => $image['file_name'],
					'date_purchased' =>  date("Y-m-d H:i:s")
				);
				//print_r($data);
				$query = $this->m_tag_order->save($data);
				if($query){
					echo json_encode($query);
				}
				else{
					echo json_encode(0);
				}
			}
		}
		
		public function payTagNow($id){
				
				$usr_pay = 19.99;
				$shippingFee = 5;
				
				//$usr_pay = 0.5;
				//$shippingFee = 0.5;
				$config['business'] 			= 'petpost.info@gmail.com';
				//$config['business'] 			= 'cabalida.stephen@gmail.com';
				$config['cpp_header_image'] 	= ''; //Image header url [750 pixels wide by 90 pixels high]
				$config['return'] 				= base_url(). "settings";
				$config['cancel_return'] 		= base_url(). "settings";
				$config['notify_url'] 			= $notifyURL = base_url().'shop/saveDB'; //IPN Post
				$config['production'] 			= TRUE; //Its false by default and will use sandbox
				$this->load->library('paypal',$config);
				
				$this->paypal->add("PET TAG",$usr_pay,1,1);
				$this->paypal->add("SHIPPING FEE",$shippingFee,1,$id);
				$this->paypal->add("-----------------",0,1,4);

				$this->paypal->pay();
		}
		public function getPetComments(){
			$id =  $this->input->post('id');
			$query = $this->m_comments->getPetComments($id);
			
			echo json_encode($query);
			
		}
		public function getAllOrders(){
			
			$query = $this->m_tag_order->getAllOrders();
			
			echo json_encode($query);
			
		}
		public function getTagOrder(){
			$id =  $this->input->post('id');
			$query = $this->m_tag_order->getTagOrder($id);
			
			echo json_encode($query);
			
		}
		public function updateToShipped(){
			$data = array(
					'order_status' => 2,
					'date_shipped' => date("Y-m-d H:i:s")
				);
			$id = $this->input->post('id');
			$query = $this->m_tag_order->updateToShipped($data,$id);
			
			if($query){
				echo json_encode(true);
			}else{
				echo json_encode(false);
			}
			
		}
		

	}