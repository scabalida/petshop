<?php

	class m_tag_order extends MY_Model
	{
		protected $_table_name = 'tag_orders';
		protected $_order_by = 'date_purchased';
		
		public function __construct()
		{
			parent::__construct();
		}
		public function getTagInvoice($id)
		{
			$this->db->select('*');
			$this->db->where('id', $id);
			$this->db->from('tag_orders');
			
			$query = $this->db->get();

			return $query->result();
		
		}
		public function updateStatus($data,$id)
		{
			$query = $this->db->update('tag_orders', $data, array('id' => $id));

			if($query){
				return true;
			}else{
				return false;
			}
		
		}
		public function getAllOrders()
		{
			$this->db->select('*');
			$this->db->where('order_status', 1);
			$this->db->or_where('order_status', 2);
			$this->db->from('tag_orders');
			
			$query = $this->db->get();
			$newArray = array();
			foreach($query->result() as $row){
				$data = array(
					"id" => $row->id,
					"prod_id" => str_pad($row->id, 10, '0', STR_PAD_LEFT),
					"fullname" => ucwords($row->owner_fullname),
					"pet_name" => $row->pet_name,
					"address" => $row->street . "," . $row->city . "," . $row->zip . " " . $row->country,
					"email" => $row->owner_email,
					"contact1" => $row->contact1,
					"contact2" => $row->contact2,
					"pet_image" => base_url() . "images/tags/". $row->owner_id . "/" .$row->pet_image,
					"date_purchased" => date("F d, Y h:i:s", strtotime($row->date_purchased)),
					"order_status" => $row->order_status,
				);
				array_push($newArray,$data);
			}
			
			return $newArray;
		
		}
		public function getTagOrder($id)
		{
			$this->db->select('*');
			$this->db->where('id', $id);
			$this->db->from('tag_orders');
			
			$query = $this->db->get();
			$newArray = array();
			foreach($query->result() as $row){
				$data = array(
					"id" => $row->id,
					"prod_id" => str_pad($row->id, 10, '0', STR_PAD_LEFT),
					"fullname" => ucwords($row->owner_fullname),
					"pet_name" => $row->pet_name,
					"street" => $row->street,
					"city" => $row->city,
					"zip" => $row->zip,
					"country" => $row->country,
					"email" => $row->owner_email,
					"contact1" => $row->contact1,
					"contact2" => $row->contact2,
					"pet_image" => base_url() . "images/tags/". $row->owner_id . "/" .$row->pet_image,
					"date_purchased" => date("F d, Y h:i:s", strtotime($row->date_purchased)),
					"order_status" => $row->order_status
				);
				array_push($newArray,$data);
			}
			
			return $newArray;
		
		}
		public function updateToShipped($data,$id)
		{
			$query = $this->db->update('tag_orders', $data, array('id' => $id));

			if($query){
				return true;
			}else{
				return false;
			}
		
		}
	}