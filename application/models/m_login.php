<?php

	class m_login extends MY_Model
	{
		protected $_table_name = 'user_info';
		protected $_order_by = 'date_registered';
		
		public function __construct()
		{
			parent::__construct();
		}
		
		public function login($email, $pass)
		{
			$user = $this->get_by( 

				array (
					'u_email' => $email,
					'u_password' => $pass,
				));
			

			if($user)
			{
				//if($user[0]->u_rights == 1){
					if($user[0]->date_expired != "0000-00-00"){
						if(strtotime($user[0]->date_expired) >= strtotime(date("Y-m-d"))){
							foreach ($user as $row)
							{
								$data = array (
									'u_id'   			=> $row->u_id,
									'u_fullname' 		=> $row->u_fullname,
									'u_contact1' 		=> $row->u_contact1,
									'u_contact2' 		=> $row->u_contact2,
									'u_contact3' 		=> $row->u_contact3,
									'u_country' 		=> $row->u_country,
									'u_city' 			=> $row->u_city,
									'u_zip' 			=> $row->u_zip,
									'u_email' 			=> $row->u_email,
									'date_registered' 	=> $row->date_registered,
									'u_role' 			=> $row->u_role,
									'loggedin' 			=> TRUE
								);
								$this->session->set_userdata($data);
							}
							return 0;
						}else{
							return 2;
						}
					}else{
						return 1;
					}
				/*}else{
					return 1;
				}*/
			}
			else{
				return 1;
				
			}
		}
		public function logout()
		{
			$this->session->set_userdata(array());
			$this->session->sess_destroy();
			//redirect('login');
			redirect(base_url(),'refresh');
		}
		public function get_visitors()
		{
			$query = $this->db->get('visitor_counter');
			return count($query->result());
		}
		public function save_visitor($ip)
		{
			$query = $this->db->get_where('visitor_counter', array('ip_address' => $ip));
			if(count($query->result()) == 0){
				$this->db->insert('visitor_counter', array('ip_address' => $ip));
			}
		}
	}