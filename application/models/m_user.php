<?php

	class m_user extends MY_Model
	{
		protected $_table_name = 'user_info';
		protected $_order_by = 'date_registered';
		
		public function __construct()
		{
			parent::__construct();
		}
		public function insertTest()
		{
			$data = array(
				"name" => "Johnny"
			);
			$this->db->insert('test', $data);
		
		}
		public function renewNow($data,$id)
		{
			$query = $this->db->update('user_info', $data, array('u_id' => $id));

			if($query){
				return true;
			}else{
				return false;
			}
		
		}
		
		public function getAllDateExpiration()
		{
			$this->db->select('date_expired, u_email');
			$this->db->from('user_info');
			
			$query = $this->db->get();
			$newArray = array();
			foreach($query->result() as $row){
				$userYear =  date('Y', strtotime($row->date_expired));
				$userMonth =  date('m', strtotime($row->date_expired));
				$userDay =  date('d', strtotime($row->date_expired));
				$todYear = date("Y");
				$todMonth = date("m");
				$todDay= date("d");
				if(strtotime($row->date_expired) >= strtotime(date("Y-m-d"))){
					if($userYear == $todYear && $userMonth == $todMonth){
						array_push($newArray, $row->u_email);
					}
				}
			}
			return $newArray;
		
		}
		public function getDateExpiration($id)
		{
			$this->db->select('date_expired');
			$this->db->where('u_id', $id);
			$this->db->from('user_info');
			
			$query = $this->db->get();

			return $query->result();
		
		}
		public function getID($email)
		{
			$this->db->select('u_id');
			$this->db->where('u_email', $email);
			$this->db->from('user_info');
			
			$query = $this->db->get();

			return $query->result();
		
		}
		public function chckEmail($email)
		{
			$this->db->select('u_email');
			$this->db->where('u_email', $email);
			$this->db->from('user_info');
			
			$query = $this->db->get();

			if(count($query->result()) > 0){
				return true;
			}else{
				return false;
			}
		
		}
		public function getAllUsersExceptAdmin()
		{
			$this->db->select('u_id, u_fullname, u_contact1, u_contact2, u_contact3, u_email, u_country, u_city, u_zip, date_registered, u_role, u_rights');
			$this->db->where('u_id !=', $this->session->userdata('u_id'));
			$this->db->where('u_role !=', "3");
			$this->db->from('user_info');
			
			$query = $this->db->get();

			return $query->result();
		
		}
		public function getUserData($id)
		{
			$this->db->select('u_id, u_fullname, u_contact1, u_contact2, u_contact3, u_email, u_country, u_city, u_zip, date_registered, u_role, u_rights, date_expired');
			$this->db->where('u_id', $id);
			$this->db->from('user_info');
			
			$query = $this->db->get();

			return $query->result();
		
		}
		public function getUserPass()
		{	
			$id = $this->session->userdata('u_id');
			$this->db->select('u_password');
			$this->db->where('u_id', $id);
			$this->db->from('user_info');
			
			$query = $this->db->get();

			return $query->result();
		
		}

	}