<?php

	class m_pets extends MY_Model
	{
		protected $_table_name = 'pet_info';
		protected $_order_by = 'date_registered';
		
		public function __construct()
		{
			parent::__construct();
		}
		public function getMyPetFoundList()
		{
			$this->db->select('*');
			$this->db->where('user_id', $this->session->userdata('u_id'));
			$this->db->from('found_pet_list');
			
			$query = $this->db->get();
			$newArray = array();
			foreach($query->result() as $row){
				$query1 = $this->db->get_where('pet_info', array('pet_id'=>$row->pet_id));
				array_push($newArray, $query1->result());
			}
			return $newArray;
		
		}
		public function addPetToList($id)
		{
			$query1 = $this->db->get_where('found_pet_list', array('pet_id' => $id,'user_id'=>$this->session->userdata('u_id')));
			
			if(count($query1->result()) <= 0){
				$query2 = $this->db->insert('found_pet_list', array('pet_id' => $id,'user_id'=>$this->session->userdata('u_id')));
				if($query2){
					return 1;
				}else{
					return 2;
				}
			}else{
				return 3;
			}
		
		}
		public function getAllPets($id)
		{
			$this->db->select('*');
			$this->db->where('user_id', $id);
			$this->db->from('pet_info');
			$this->db->order_by("date_registered", "desc");
			
			$query = $this->db->get();

			return $query->result();
		
		}
		public function getThisPetData($id)
		{
			$this->db->select('*');
			$this->db->where('pet_id', $id);
			$this->db->from('pet_info');
			
			$query = $this->db->get();

			return $query->result();
		
		}
		public function updatePet($data,$id)
		{
			$query = $this->db->update('pet_info', $data, array('pet_id' => $id));

			if($query){
				return true;
			}else{
				return false;
			}
		
		}
		public function deletePet($id)
		{
			$query = $this->db->delete('pet_info', array('pet_id' => $id));
			
			if($query){
				return true;
			}else{
				return false;
			}
		}
		public function getEmails()
		{
			$city = $this->session->userdata("u_city");
			$id = $this->session->userdata("u_id");

			$this->db->select('u_email');
			$this->db->where('u_id !=', $id);
			$this->db->like('u_city', $city);
			$this->db->from('user_info');
			
			$query = $this->db->get();

			return $query->result();
		
		}
		public function getAllLostPet()
		{
			$this->db->select('*');
			$this->db->where('status', 4);
			$this->db->from('pet_info');
			$this->db->order_by("date_registered", "desc");
			
			$query = $this->db->get();

			return $query->result();
		
		}
		public function getAllFoundPet()
		{
			$this->db->select('*');
			$this->db->where('from_lost', 1);
			$this->db->where('status', 5);
			$this->db->from('pet_info');
			$this->db->order_by("date_registered", "desc");
			
			$query = $this->db->get();

			return $query->result();
		
		}
		
		public function searchPet($data)
		{
			$this->db->select('*');
			$this->db->where('status', 4);
			for($x = 0; $x < count($data); $x++)
			{
				if($data[$x]['sql'] == 0){	
					$this->db->where($data[$x]['row_name'], $data[$x]['row_value']);
				}else{
					$this->db->like($data[$x]['row_name'], $data[$x]['row_value'], 'after');
				}
			}
			$this->db->from('pet_info');
			$this->db->order_by("date_registered", "desc");
			
			$query = $this->db->get();

			return $query->result();
		
		}
		public function searchFoundPet($data)
		{
			$this->db->select('*');
			$this->db->where('from_lost', 1);
			$this->db->where('status', 5);
			for($x = 0; $x < count($data); $x++)
			{
				if($data[$x]['sql'] == 0){	
					$this->db->where($data[$x]['row_name'], $data[$x]['row_value']);
				}else{
					$this->db->like($data[$x]['row_name'], $data[$x]['row_value'], 'after');
				}
			}
			$this->db->from('pet_info');
			$this->db->order_by("date_registered", "desc");
			
			$query = $this->db->get();

			return $query->result();
		
		}
	}