<?php

	class m_person extends MY_Model
	{
		protected $_table_name = 'missing_person';
		protected $_order_by = 'date_registered';
		
		public function __construct()
		{
			parent::__construct();
		}
		public function getAllPerson($id)
		{
			$this->db->select('*');
			$this->db->where('user_id', $id);
			$this->db->from('missing_person');
			$this->db->order_by("date_registered", "desc");
			
			$query = $this->db->get();

			return $query->result();
		
		}
		public function deletePerson($id)
		{
			$query = $this->db->delete('missing_person', array('person_id' => $id));
			
			if($query){
				return true;
			}else{
				return false;
			}
		}
		public function getThisPersonData($id)
		{
			$this->db->select('*');
			$this->db->where('person_id', $id);
			$this->db->from('missing_person');
			
			$query = $this->db->get();

			return $query->result();
		
		}
		public function updatePerson($data,$id)
		{
			$query = $this->db->update('missing_person', $data, array('person_id' => $id));

			if($query){
				return true;
			}else{
				return false;
			}
		
		}
		public function getAllMissingPerson()
		{
			$this->db->select('*');
			$this->db->where('status', 1);
			$this->db->from('missing_person');
			$this->db->order_by("date_registered", "desc");
			
			$query = $this->db->get();

			return $query->result();
		
		}

	}