<?php

	class m_shop extends MY_Model
	{
		protected $_table_name = 'pet_shop';
		protected $_order_by = 'date_added';
		
		public function __construct()
		{
			parent::__construct();
		}
		public function getAllProducts()
		{
			$this->db->select('*');
			$this->db->where('product_status', "1");
			$this->db->from('pet_shop');
			$this->db->order_by("date_added", "desc");
			
			$query = $this->db->get();

			return $query->result();
		
		}
		public function getThisProductData($id)
		{
			$this->db->select('*');
			$this->db->where('product_id', $id);
			$this->db->from('pet_shop');
			
			$query = $this->db->get();

			return $query->result();
		
		}
		public function saveInvoice($data)
		{
			$query = $this->db->insert('shop_invoice', $data);
			if($query){
				return $this->db->insert_id();
			}else{
				return false;
			}
		
		}
		public function updateProduct($data,$id)
		{
			$query = $this->db->update('pet_shop', $data, array('product_id' => $id));

			if($query){
				return true;
			}else{
				return false;
			}
		
		}
		public function updateInvoice($data,$id)
		{
			$query = $this->db->update('shop_invoice', $data, array('id' => $id));

			if($query){
				return true;
			}else{
				return false;
			}
		
		}
		public function deleteProduct($id)
		{
			$query = $this->db->delete('pet_shop', array('product_id' => $id));
			
			if($query){
				return true;
			}else{
				return false;
			}
		}
		public function getInvoice($id)
		{
			$this->db->select('*');
			$this->db->where('id', $id);
			$this->db->from('shop_invoice');
			
			$query = $this->db->get();

			return $query->result();
		
		}
		public function getForDelivery()
		{
			$this->db->select('*');
			$this->db->where('order_status', 1);
			$this->db->from('shop_invoice');
			
			$query = $this->db->get();
			$newArray = array();
			foreach($query->result() as $row){
				$data = array(
					"id" => str_pad($row->id, 10, '0', STR_PAD_LEFT),
					"prod_id" => $row->id,
					"fullname" => ucwords($row->fullname),
					"country" => $row->country,
					"home_address" => $row->home_address,
					"email" => $row->email,
					"contact1" => $row->contact1,
					"contact2" => $row->contact2,
					"products" => $row->products,
					"total_payment" => $row->total_payment,
					"datetime" => date("F d, Y h:i:s", strtotime($row->datetime)),
					"order_status" => $row->order_status,
				);
				array_push($newArray,$data);
			}
			
			return $newArray;
		
		}
		public function updateToShipped($data,$id)
		{
			$query = $this->db->update('shop_invoice', $data, array('id' => $id));

			if($query){
				return true;
			}else{
				return false;
			}
		
		}
		public function getShipped()
		{
			$this->db->select('*');
			$this->db->where('order_status', 2);
			$this->db->from('shop_invoice');
			
			$query = $this->db->get();
			$newArray = array();
			foreach($query->result() as $row){
				$data = array(
					"id" => str_pad($row->id, 10, '0', STR_PAD_LEFT),
					"prod_id" => $row->id,
					"fullname" => ucwords($row->fullname),
					"country" => $row->country,
					"home_address" => $row->home_address,
					"email" => $row->email,
					"contact1" => $row->contact1,
					"contact2" => $row->contact2,
					"products" => $row->products,
					"total_payment" => $row->total_payment,
					"date_shipped" => date("F d, Y h:i:s", strtotime($row->date_shipped)),
					"order_status" => $row->order_status,
				);
				array_push($newArray,$data);
			}
			
			return $newArray;
		}
		public function showProducts($id)
		{
			$this->db->select('*');
			$this->db->where('id', $id);
			$this->db->from('shop_invoice');
			
			$query = $this->db->get();

			$newArray = array();
			foreach($query->result() as $row){
				$data = array(
					"id" => $row->id,
					"fullname" => ucwords($row->fullname),
					"country" => $row->country,
					"home_address" => $row->home_address,
					"email" => $row->email,
					"contact1" => $row->contact1,
					"contact2" => $row->contact2,
					"products" => json_decode($row->products),
					"total_payment" => $row->total_payment,
					"datetime" => date("F d, Y h:i:s", strtotime($row->datetime)),
					"order_status" => $row->order_status,
				);
				array_push($newArray,$data);
			}
			
			return $newArray;
		
		}
		
		public function getThreeProducts()
		{
			$this->db->select('*');
			$this->db->where('product_status', "1");
			$this->db->from('pet_shop');
			$this->db->order_by("date_added", "desc");
			$this->db->limit(3);
			
			$query = $this->db->get();

			return $query->result();
		
		}

	}