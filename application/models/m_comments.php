<?php

	class m_comments extends MY_Model
	{
		protected $_table_name = 'pet_comments';
		protected $_order_by = 'date_commented';
		
		public function __construct()
		{
			parent::__construct();
		}
		public function getPetComments($id)
		{
			$this->db->select('*');
			$this->db->where('pet_id', $id);
			$this->db->from('pet_comments');
			$this->db->order_by("date_commented", "desc");
			
			$query = $this->db->get();

			return $query->result();
		
		}
	
	}